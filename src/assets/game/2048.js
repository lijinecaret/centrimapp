// document.addEventListener("DOMContentLoaded", function () {
//     // Wait till the browser is ready to render the game (avoids glitches)
//     window.requestAnimationFrame(function () {
//       var manager = new GameManager(4, KeyboardInputManager, HTMLActuator);
//     });
//   });
  
  
//   function GameManager(size, InputManager, Actuator) {
//     this.size         = size; // Size of the grid
//     this.inputManager = new InputManager;
//     this.actuator     = new Actuator;
  
//     this.startTiles   = 2;
  
//     this.inputManager.on("move", this.move.bind(this));
//     this.inputManager.on("restart", this.restart.bind(this));
  
//     this.setup();
//   }
  
//   // Restart the game
//   GameManager.prototype.restart = function () {
//     this.actuator.restart();
//     this.setup();
//   };
  
//   // Set up the game
//   GameManager.prototype.setup = function () {
//     this.grid         = new Grid(this.size);
  
//     this.score        = 0;
//     this.over         = false;
//     this.won          = false;
  
//     // Add the initial tiles
//     this.addStartTiles();
  
//     // Update the actuator
//     this.actuate();
//   };
  
//   // Set up the initial tiles to start the game with
//   GameManager.prototype.addStartTiles = function () {
//     for (var i = 0; i < this.startTiles; i++) {
//       this.addRandomTile();
//     }
//   };
  
//   // Adds a tile in a random position
//   GameManager.prototype.addRandomTile = function () {
//     if (this.grid.cellsAvailable()) {
//       var value = Math.random() < 0.9 ? 2 : 4;
//       var tile = new Tile(this.grid.randomAvailableCell(), value);
  
//       this.grid.insertTile(tile);
//     }
//   };
  
//   // Sends the updated grid to the actuator
//   GameManager.prototype.actuate = function () {
//     this.actuator.actuate(this.grid, {
//       score: this.score,
//       over:  this.over,
//       won:   this.won
//     });
//   };
  
//   // Save all tile positions and remove merger info
//   GameManager.prototype.prepareTiles = function () {
//     this.grid.eachCell(function (x, y, tile) {
//       if (tile) {
//         tile.mergedFrom = null;
//         tile.savePosition();
//       }
//     });
//   };
  
//   // Move a tile and its representation
//   GameManager.prototype.moveTile = function (tile, cell) {
//     this.grid.cells[tile.x][tile.y] = null;
//     this.grid.cells[cell.x][cell.y] = tile;
//     tile.updatePosition(cell);
//   };
  
//   // Move tiles on the grid in the specified direction
//   GameManager.prototype.move = function (direction) {
//     // 0: up, 1: right, 2:down, 3: left
//     var self = this;
  
//     if (this.over || this.won) return; // Don't do anything if the game's over
  
//     var cell, tile;
  
//     var vector     = this.getVector(direction);
//     var traversals = this.buildTraversals(vector);
//     var moved      = false;
  
//     // Save the current tile positions and remove merger information
//     this.prepareTiles();
  
//     // Traverse the grid in the right direction and move tiles
//     traversals.x.forEach(function (x) {
//       traversals.y.forEach(function (y) {
//         cell = { x: x, y: y };
//         tile = self.grid.cellContent(cell);
  
//         if (tile) {
//           var positions = self.findFarthestPosition(cell, vector);
//           var next      = self.grid.cellContent(positions.next);
  
//           // Only one merger per row traversal?
//           if (next && next.value === tile.value && !next.mergedFrom) {
//             var merged = new Tile(positions.next, tile.value * 2);
//             merged.mergedFrom = [tile, next];
  
//             self.grid.insertTile(merged);
//             self.grid.removeTile(tile);
  
//             // Converge the two tiles' positions
//             tile.updatePosition(positions.next);
  
//             // Update the score
//             self.score += merged.value;
  
//             // The mighty 2048 tile
//             if (merged.value === 2048) self.won = true;
//           } else {
//             self.moveTile(tile, positions.farthest);
//           }
  
//           if (!self.positionsEqual(cell, tile)) {
//             moved = true; // The tile moved from its original cell!
//           }
//         }
//       });
//     });
  
//     if (moved) {
//       this.addRandomTile();
  
//       if (!this.movesAvailable()) {
//         this.over = true; // Game over!
//       }
  
//       this.actuate();
//     }
//   };
  
//   // Get the vector representing the chosen direction
//   GameManager.prototype.getVector = function (direction) {
//     // Vectors representing tile movement
//     var map = {
//       0: { x: 0,  y: -1 }, // up
//       1: { x: 1,  y: 0 },  // right
//       2: { x: 0,  y: 1 },  // down
//       3: { x: -1, y: 0 }   // left
//     };
  
//     return map[direction];
//   };
  
//   // Build a list of positions to traverse in the right order
//   GameManager.prototype.buildTraversals = function (vector) {
//     var traversals = { x: [], y: [] };
  
//     for (var pos = 0; pos < this.size; pos++) {
//       traversals.x.push(pos);
//       traversals.y.push(pos);
//     }
  
//     // Always traverse from the farthest cell in the chosen direction
//     if (vector.x === 1) traversals.x = traversals.x.reverse();
//     if (vector.y === 1) traversals.y = traversals.y.reverse();
  
//     return traversals;
//   };
  
//   GameManager.prototype.findFarthestPosition = function (cell, vector) {
//     var previous;
  
//     // Progress towards the vector direction until an obstacle is found
//     do {
//       previous = cell;
//       cell     = { x: previous.x + vector.x, y: previous.y + vector.y };
//     } while (this.grid.withinBounds(cell) &&
//              this.grid.cellAvailable(cell));
  
//     return {
//       farthest: previous,
//       next: cell // Used to check if a merge is required
//     };
//   };
  
//   GameManager.prototype.movesAvailable = function () {
//     return this.grid.cellsAvailable() || this.tileMatchesAvailable();
//   };
  
//   // Check for available matches between tiles (more expensive check)
//   GameManager.prototype.tileMatchesAvailable = function () {
//     var self = this;
  
//     var tile;
  
//     for (var x = 0; x < this.size; x++) {
//       for (var y = 0; y < this.size; y++) {
//         tile = this.grid.cellContent({ x: x, y: y });
  
//         if (tile) {
//           for (var direction = 0; direction < 4; direction++) {
//             var vector = self.getVector(direction);
//             var cell   = { x: x + vector.x, y: y + vector.y };
  
//             var other  = self.grid.cellContent(cell);
//             if (other) {
//             }
  
//             if (other && other.value === tile.value) {
//               return true; // These two tiles can be merged
//             }
//           }
//         }
//       }
//     }
  
//     return false;
//   };
  
//   GameManager.prototype.positionsEqual = function (first, second) {
//     return first.x === second.x && first.y === second.y;
//   };
  
  
  
//   function Grid(size) {
//     this.size = size;
  
//     this.cells = [];
  
//     this.build();
//   }
  
//   // Build a grid of the specified size
//   Grid.prototype.build = function () {
//     for (var x = 0; x < this.size; x++) {
//       var row = this.cells[x] = [];
  
//       for (var y = 0; y < this.size; y++) {
//         row.push(null);
//       }
//     }
//   };
  
//   // Find the first available random position
//   Grid.prototype.randomAvailableCell = function () {
//     var cells = this.availableCells();
  
//     if (cells.length) {
//       return cells[Math.floor(Math.random() * cells.length)];
//     }
//   };
  
//   Grid.prototype.availableCells = function () {
//     var cells = [];
  
//     this.eachCell(function (x, y, tile) {
//       if (!tile) {
//         cells.push({ x: x, y: y });
//       }
//     });
  
//     return cells;
//   };
  
//   // Call callback for every cell
//   Grid.prototype.eachCell = function (callback) {
//     for (var x = 0; x < this.size; x++) {
//       for (var y = 0; y < this.size; y++) {
//         callback(x, y, this.cells[x][y]);
//       }
//     }
//   };
  
//   // Check if there are any cells available
//   Grid.prototype.cellsAvailable = function () {
//     return !!this.availableCells().length;
//   };
  
//   // Check if the specified cell is taken
//   Grid.prototype.cellAvailable = function (cell) {
//     return !this.cellOccupied(cell);
//   };
  
//   Grid.prototype.cellOccupied = function (cell) {
//     return !!this.cellContent(cell);
//   };
  
//   Grid.prototype.cellContent = function (cell) {
//     if (this.withinBounds(cell)) {
//       return this.cells[cell.x][cell.y];
//     } else {
//       return null;
//     }
//   };
  
//   // Inserts a tile at its position
//   Grid.prototype.insertTile = function (tile) {
//     this.cells[tile.x][tile.y] = tile;
//   };
  
//   Grid.prototype.removeTile = function (tile) {
//     this.cells[tile.x][tile.y] = null;
//   };
  
//   Grid.prototype.withinBounds = function (position) {
//     return position.x >= 0 && position.x < this.size &&
//            position.y >= 0 && position.y < this.size;
//   };
  
  
//   function HTMLActuator() {
//     this.tileContainer    = document.getElementsByClassName("tile-container")[0];
//     this.scoreContainer   = document.getElementsByClassName("score-container")[0];
//     this.messageContainer = document.getElementsByClassName("game-message")[0];
  
//     this.score = 0;
//   }
  
//   HTMLActuator.prototype.actuate = function (grid, metadata) {
//     var self = this;
  
//     window.requestAnimationFrame(function () {
//       self.clearContainer(self.tileContainer);
  
//       grid.cells.forEach(function (column) {
//         column.forEach(function (cell) {
//           if (cell) {
//             self.addTile(cell);
//           }
//         });
//       });
  
//       self.updateScore(metadata.score);
  
//       if (metadata.over) self.message(false); // You lose
//       if (metadata.won) self.message(true); // You win!
//     });
//   };
  
//   HTMLActuator.prototype.restart = function () {
//     this.clearMessage();
//   };
  
//   HTMLActuator.prototype.clearContainer = function (container) {
//     while (container.firstChild) {
//       container.removeChild(container.firstChild);
//     }
//   };
  
//   HTMLActuator.prototype.addTile = function (tile) {
//     var self = this;
  
//     var element   = document.createElement("div");
//     var position  = tile.previousPosition || { x: tile.x, y: tile.y };
//     positionClass = this.positionClass(position);
  
//     // We can't use classlist because it somehow glitches when replacing classes
//     var classes = ["tile", "tile-" + tile.value, positionClass];
//     this.applyClasses(element, classes);
  
//     element.textContent = tile.value;
  
//     if (tile.previousPosition) {
//       // Make sure that the tile gets rendered in the previous position first
//       window.requestAnimationFrame(function () {
//         classes[2] = self.positionClass({ x: tile.x, y: tile.y });
//         self.applyClasses(element, classes); // Update the position
//       });
//     } else if (tile.mergedFrom) {
//       classes.push("tile-merged");
//       this.applyClasses(element, classes);
  
//       // Render the tiles that merged
//       tile.mergedFrom.forEach(function (merged) {
//         self.addTile(merged);
//       });
//     } else {
//       classes.push("tile-new");
//       this.applyClasses(element, classes);
//     }
  
//     // Put the tile on the board
//     this.tileContainer.appendChild(element);
//   };
  
//   HTMLActuator.prototype.applyClasses = function (element, classes) {
//     element.setAttribute("class", classes.join(" "));
//   };
  
//   HTMLActuator.prototype.normalizePosition = function (position) {
//     return { x: position.x + 1, y: position.y + 1 };
//   };
  
//   HTMLActuator.prototype.positionClass = function (position) {
//     position = this.normalizePosition(position);
//     return "tile-position-" + position.x + "-" + position.y;
//   };
  
//   HTMLActuator.prototype.updateScore = function (score) {
//     this.clearContainer(this.scoreContainer);
  
//     var difference = score - this.score;
//     this.score = score;
  
//     this.scoreContainer.textContent = this.score;
  
//     if (difference > 0) {
//       var addition = document.createElement("div");
//       addition.classList.add("score-addition");
//       addition.textContent = "+" + difference;
  
//       this.scoreContainer.appendChild(addition);
//     }
//   };
  
//   HTMLActuator.prototype.message = function (won) {
//     var type    = won ? "game-won" : "game-over";
//     var message = won ? "You win!" : "Game over!"
  
//     // if (ga) ga("send", "event", "game", "end", type, this.score);
  
//     this.messageContainer.classList.add(type);
//     this.messageContainer.getElementsByTagName("p")[0].textContent = message;
//   };
  
//   HTMLActuator.prototype.clearMessage = function () {
//     this.messageContainer.classList.remove("game-won", "game-over");
//   };
  
  
  
//   function KeyboardInputManager() {
//     this.events = {};
  
//     this.listen();
//   }
  
//   KeyboardInputManager.prototype.on = function (event, callback) {
//     if (!this.events[event]) {
//       this.events[event] = [];
//     }
//     this.events[event].push(callback);
//   };
  
//   KeyboardInputManager.prototype.emit = function (event, data) {
//     var callbacks = this.events[event];
//     if (callbacks) {
//       callbacks.forEach(function (callback) {
//         callback(data);
//       });
//     }
//   };
  
//   KeyboardInputManager.prototype.listen = function () {
//     var self = this;
  
//     var map = {
//       38: 0, // Up
//       39: 1, // Right
//       40: 2, // Down
//       37: 3, // Left
//       75: 0, // vim keybindings
//       76: 1,
//       74: 2,
//       72: 3
//     };
  
//     document.addEventListener("keydown", function (event) {
//       var modifiers = event.altKey || event.ctrlKey || event.metaKey ||
//                       event.shiftKey;
//       var mapped    = map[event.which];
  
//       if (!modifiers) {
//         if (mapped !== undefined) {
//           event.preventDefault();
//           self.emit("move", mapped);
//         }
  
//         if (event.which === 32) self.restart.bind(self)(event);
//       }
//     });
  
//     var retry = document.getElementsByClassName("retry-button")[0];
//     retry.addEventListener("click", this.restart.bind(this));
  
//     // Listen to swipe events
//     var gestures = [Hammer.DIRECTION_UP, Hammer.DIRECTION_RIGHT,
//                     Hammer.DIRECTION_DOWN, Hammer.DIRECTION_LEFT];
  
//     var gameContainer = document.getElementsByClassName("game-container")[0];
//     var handler       = Hammer(gameContainer, {
//       drag_block_horizontal: true,
//       drag_block_vertical: true
//     });
    
//     handler.on("swipe", function (event) {
//       event.gesture.preventDefault();
//       mapped = gestures.indexOf(event.gesture.direction);
  
//       if (mapped !== -1) self.emit("move", mapped);
//     });
//   };
  
//   KeyboardInputManager.prototype.restart = function (event) {
//     event.preventDefault();
//     this.emit("restart");
//   };
  
  
  
  
  
//   function Tile(position, value) {
//     this.x                = position.x;
//     this.y                = position.y;
//     this.value            = value || 2;
  
//     this.previousPosition = null;
//     this.mergedFrom       = null; // Tracks tiles that merged together
//   }
  
//   Tile.prototype.savePosition = function () {
//     this.previousPosition = { x: this.x, y: this.y };
//   };
  
//   Tile.prototype.updatePosition = function (position) {
//     this.x = position.x;
//     this.y = position.y;
//   };
  






!function(t){
  var e={};
  function i(n){
    if(e[n])
    return e[n].exports;
    var r=e[n]={
      i:n,l:!1,exports:{}
    };
    return t[n].call(r.exports,r,r.exports,i),r.l=!0,r.exports
  }
  i.m=t,i.c=e,
  i.d=function(t,e,n){
    i.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:n})},
    i.r=function(t){
      "undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),
      Object.defineProperty(t,"__esModule",{value:!0})},
      i.t=function(t,e){
        if(1&e&&(t=i(t)),8&e)
        return t;
        if(4&e&&"object"==typeof t&&t&&t.__esModule)
        return t;
        var n=Object.create(null);
        if(i.r(n),
        Object.defineProperty(n,"default",{enumerable:!0,value:t}),
        2&e&&"string"!=typeof t)
        for(var r in t)i.d(n,r,function(e){
          return t[e]}.bind(null,r));
          return n
        },i.n=function(t){
          var e=t&&t.__esModule?function(){
            return t.default
          }:function(){
            return t
          };
          return i.d(e,"a",e),e},
          i.o=function(t,e){
            return Object.prototype.hasOwnProperty.call(t,e)},
            i.p="",i(i.s=0)}([function(t,e,i){i(1),t.exports=i(2)},function(t,e){},
              function(t,e,i){
                "use strict";
                Object.defineProperty(e,"__esModule",{value:!0});
                var n=i(3);
                window.requestAnimationFrame((function(){return new n.default(4)})),
                "serviceWorker"in navigator&&window.addEventListener("load",(function(){
                  navigator.serviceWorker.register("./service-worker.js")}))},
                  function(t,e,i){
                    "use strict";Object.defineProperty(e,"__esModule",{value:!0});var n=i(4),r=i(5),s=i(6),o=i(7),
                    a=function(t,e){
                      var i=this;
                      void 0===e&&(e=2),
                      this.init=function(){
                        i.grid=new n.default(i.size),
                        i.score=0,i.isOver=!1,i.isWin=!1,i.renderStartTiles(),i.update()},
                        this.restart=function(){
                          i.domUpdater.restart(),
                          i.init()},
                          this.renderStartTiles=function(){
                            for(var t=0;t<i.startTiles;t++)i.renderTile()},
                            this.renderTile=function(){
                              if(i.grid.hasCellsAvailable()){
                                var t=Math.random()<.9?2:4,e=new r.default(i.grid.getAvailableCell(),t);
                                i.grid.insertTile(e)}},
                                this.update=function(){
                                  i.domUpdater.update(i.grid,{score:i.score,isOver:i.isOver,isWin:i.isWin})},
                                  this.updateTiles=function(){
                                    i.grid.eachCell((function(t,e,i){i&&(i.mergedFrom=null,i.save())}))},
                                    this.moveTile=function(t,e){i.grid.cells[t.x][t.y]=null,i.grid.cells[e.x][e.y]=t,t.update(e)},
                                    this.move=function(t){if(!i.isOver&&!i.isWin){
                                      var e,n,s=!1,
                                      o=i.getCoordinates(t),
                                      a=i.getCellsInDirection(o);
                                      i.updateTiles(),a.x.forEach((function(t){
                                        a.y.forEach((function(a){
                                          if(e={x:t,y:a},n=i.grid.getCellContent(e)){
                                            var u=i.getLastPosition(e,o),c=i.grid.getCellContent(u.next);
                                            if(c&&c.value===n.value&&!c.mergedFrom){
                                              var l=new r.default(u.next,2*n.value);
                                              l.mergedFrom=[n,c],i.grid.insertTile(l),i.grid.removeTile(n),
                                              n.update(u.next),i.score+=l.value,2048===l.value&&(i.isWin=!0)}
                                              else i.moveTile(n,u.last);i.arePositionsEqual(e,n)||(s=!0)}}))})),
                                              s&&(i.renderTile(),i.hasMovesLeft()||(i.isOver=!0),i.update())}},
                                              this.getCoordinates=function(t){return{0:{x:0,y:-1},1:{x:1,y:0},2:{x:0,y:1},3:{x:-1,y:0}}[t]},
                                              this.getCellsInDirection=function(t){
                                                for(var e={x:[],y:[]},n=0;n<i.size;n++)e.x.push(n),e.y.push(n);
                                                return 1===t.x&&(e.x=e.x.reverse()),1===t.y&&(e.y=e.y.reverse()),e},
                                                this.getLastPosition=function(t,e){var n;do{t={x:(n=t).x+e.x,y:n.y+e.y}}
                                                while(i.grid.isInBounds(t)&&i.grid.isCellAvailable(t));
                                                return{last:n,next:t}},
                                                this.hasMovesLeft=function(){
                                                  return i.grid.hasCellsAvailable()||i.tileMatchesAvailable()},
                                                  this.tileMatchesAvailable=function(){
                                                    for(var t,e=0;e<i.size;e++)
                                                    for(var n=0;n<i.size;n++)
                                                    if(t=i.grid.getCellContent({x:e,y:n}))
                                                    for(var r=0;r<i.size;r++){
                                                    var s=i.getCoordinates(r),
                                                      o={x:e+s.x,y:n+s.y},a=i.grid.getCellContent(o);
                                                      if(a&&a.value===t.value)
                                                      return!0}
                                                      return!1},
                                                      this.arePositionsEqual=function(t,e){
                                                        return t.x===e.x&&t.y===e.y},
                                                        this.size=t,this.startTiles=e,
                                                        this.gestureManager=new o.default({gameContainer:document.querySelector(".game"),
                                                        retryButton:document.querySelector(".retry")}),
                                                        this.domUpdater=new s.default,
                                                        this.gestureManager.on("move",this.move.bind(this)),
                                                        this.gestureManager.on("restart",this.restart.bind(this)),this.init()};e.default=a},
                                                        function(t,e,i){"use strict";
                                                        Object.defineProperty(e,"__esModule",{value:!0});
                                                        var n=function(t){var e=this;
                                                          this.getAvailableCell=function(){var t=e.availableCells();
                                                            if(t.length)return t[Math.floor(Math.random()*t.length)]},
                                                            this.eachCell=function(t){for(var i=0;i<e.size;i++)for(var n=0;n<e.size;n++)t(i,n,e.cells[i][n])},this.hasCellsAvailable=function(){return!!e.availableCells().length},this.isCellAvailable=function(t){return!e.isCellOccupied(t)},this.getCellContent=function(t){return e.isInBounds(t)?e.cells[t.x][t.y]:null},this.insertTile=function(t){e.cells[t.x][t.y]=t},
                                                            this.removeTile=function(t){e.cells[t.x][t.y]=null},
                                                            this.isInBounds=function(t){return t.x>=0&&t.x<e.size&&t.y>=0&&t.y<e.size},
                                                            this.build=function(){for(var t=0;t<e.size;t++)for(var i=e.cells[t]=[],n=0;n<e.size;n++)i.push(null)},
                                                            this.availableCells=function(){var t=[];return e.eachCell((function(e,i,n){n||t.push({x:e,y:i})})),t},
                                                            this.isCellOccupied=function(t){return!!e.getCellContent(t)},this.size=t,this.cells=[],this.build()};e.default=n},
                                                            function(t,e,i){"use strict";Object.defineProperty(e,"__esModule",{value:!0});var n=function(t,e){
                                                              var i=this;this.save=function(){i.prevPosition={x:i.x,y:i.y}},this.update=function(t){i.x=t.x,i.y=t.y},
                                                              this.x=t.x,this.y=t.y,this.value=e||2,this.mergedFrom=null,this.prevPosition=null};e.default=n},function(t,e,i){
                                                                "use strict";Object.defineProperty(e,"__esModule",{value:!0});var n=function(){var t=this;
                                                                  this.update=function(e,i){window.requestAnimationFrame((function(){t.clearContainer(t.tileContainer),e.cells.forEach((function(e){e.forEach((function(e){e&&t.addTile(e)}))})),t.updateScore(i.score),i.isOver&&t.getMessage(!1),i.isWin&&t.getMessage(!0)}))},this.restart=function(){t.clearMessage()},
                                                                  this.clearContainer=function(t){for(;t.firstChild;)t.removeChild(t.firstChild)},
                                                                  this.addTile=function(e){
                                                                    var i=document.createElement("div"),
                                                                    
                                                                    n=e.prevPosition||{x:e.x,y:e.y},r=t.getPositionClass(n),
                                                                    s=["tile","tile-"+e.value,r];
                                                                    t.setClass(i,s),
                                                                   
                                                                    i.textContent=e.value.toString(),
                                                                    e.prevPosition?window.requestAnimationFrame((function(){
                                                                      s[2]=t.getPositionClass({x:e.x,y:e.y}),
                                                                      t.setClass(i,s)})):e.mergedFrom?(s.push("tile-merged"),
                                                                      t.setClass(i,s),e.mergedFrom.forEach((function(e){
                                                                        t.addTile(e)}))):(s.push("tile-new"),
                                                                        t.setClass(i,s)),
                                                                        t.tileContainer.appendChild(i)},
                                                                        this.setClass=function(t,e){
                                                                          t.setAttribute("class",e.join(" "))},
                                                                          this.normalizePosition=function(t){
                                                                            return{x:t.x+1,y:t.y+1}},
                                                                            this.getPositionClass=function(e){
                                                                              var i=t.normalizePosition(e);
                                                                              return"tile-position-"+i.x+"-"+i.y},
                                                                              
                                                                              this.updateScore=function(e){
                                                                                var i=e-t.score;if(t.score=e,t.clearContainer(t.scoreContainer),t.scoreContainer.textContent=t.score.toString(),i>0){
                                                                                  var n=document.createElement("div");
                                                                                  n.classList.add("score-addition"),
                                                                                  n.textContent="+"+i,t.scoreContainer.appendChild(n),t.updateBestScore()}},
                                                                                  this.updateBestScore=function(){
                                                                                    var e=localStorage.getItem(t.storageKey),i=t.score;e||localStorage.setItem(t.storageKey,i.toString()),
                                                                                    e&&i>parseInt(e,10)&&localStorage.setItem(t.storageKey,i.toString()),t.bestScoreContainer.textContent=localStorage.getItem(t.storageKey)},
                                                                                    this.getMessage=function(e){var i=e?"game-won":"game-over",n=e?"You win!":"Game over!";
                                                                                    t.messageContainer.classList.add(i),t.messageContainer.querySelector("p").textContent=n},
                                                                                    this.clearMessage=function(){t.messageContainer.classList.remove("game-won","game-over")},
                                                                                    this.tileContainer=document.querySelector(".tiles"),
                                                                                    this.scoreContainer=document.querySelector(".current-score"),
                                                                                    this.messageContainer=document.querySelector(".message"),
                                                                                    this.bestScoreContainer=document.querySelector(".best-score"),
                                                                                    this.score=0,this.storageKey="2048-best-score",this.updateBestScore()};e.default=n},
                                                                                    function(t,e,i){"use strict";Object.defineProperty(e,"__esModule",{value:!0});
                                                                                    var n=i(8);e.eventsMap={38:0,39:1,40:2,37:3};
                                                                                    var r=function(t){var i=this;this.events={},
                                                                                    this.on=function(t,e){i.events[t]||(i.events[t]=[]),i.events[t].push(e)},
                                                                                    this.bindKeyboardEvents=function(){document.addEventListener("keydown",(function(t){var n=t.altKey||t.ctrlKey||t.metaKey||t.shiftKey,r=e.eventsMap[t.which];n||(void 0!==r&&(t.preventDefault(),i.emit("move",r)),32===t.which&&i.restart())}))},this.bindTouchEvents=function(){var t=new n(i.gameContainer,{recognizers:[[n.Swipe,{direction:n.DIRECTION_ALL}]]}),e=[n.DIRECTION_UP,n.DIRECTION_RIGHT,n.DIRECTION_DOWN,n.DIRECTION_LEFT];t.on("swipe",(function(t){t.preventDefault();var n=e.indexOf(t.direction);-1!==n&&i.emit("move",n)}))},this.bindRestart=function(){i.retryButton.addEventListener("click",i.restart.bind(i))},this.listen=function(){i.bindKeyboardEvents(),i.bindTouchEvents(),i.bindRestart()},this.emit=function(t,e){var n=i.events[t];n&&n.forEach((function(t){return t(e)}))},this.restart=function(){i.emit("restart")},this.gameContainer=t.gameContainer,this.retryButton=t.retryButton,this.events={},this.listen()};e.default=r},function(t,e,i){var n;




                                                                                      /*! Hammer.JS - v2.0.7 - 2016-04-22
 * http://hammerjs.github.io/
 *
 * Copyright (c) 2016 Jorik Tangelder;
 * Licensed under the MIT license */
!function(r,s,o,a){
  "use strict";
  var u,c=["","webkit","Moz","MS","ms","o"],l=s.createElement("div"),
  h=Math.round,p=Math.abs,f=Date.now;
  function d(t,e,i){
    return setTimeout(E(t,i),e)
  }
  function v(t,e,i){
    return!!Array.isArray(t)&&(m(t,i[e],i),!0)
  }
  function m(t,e,i){
    var n;
    if(t)if(t.forEach)t.forEach(e,i);
    else if(void 0!==t.length)for(n=0;n<t.length;)e.call(i,t[n],n,t),n++;
    else for(n in t)t.hasOwnProperty(n)&&e.call(i,t[n],n,t)
  }
  function g(t,e,i){
    var n="DEPRECATED METHOD: "+e+"\n"+i+" AT \n";
    return function(){
      var e=new Error("get-stack-trace"),i=e&&e.stack?e.stack.replace(/^[^\(]+?[\n$]/gm,"").replace(/^\s+at\s+/gm,"").replace(/^Object.<anonymous>\s*\(/gm,"{anonymous}()@"):"Unknown Stack Trace",s=r.console&&(r.console.warn||r.console.log);
      return s&&s.call(r.console,n,i),t.apply(this,arguments)}}u="function"!=typeof Object.assign?function(t){if(null==t)throw new TypeError("Cannot convert undefined or null to object");
      for(var e=Object(t),i=1;i<arguments.length;i++){
        var n=arguments[i];if(null!=n)for(var r in n)n.hasOwnProperty(r)&&(e[r]=n[r])
      }
      return e
    }:Object.assign;var y=g((function(t,e,i){
      for(var n=Object.keys(e),r=0;r<n.length;)(!i||i&&void 0===t[n[r]])&&(t[n[r]]=e[n[r]]),r++;return t}),"extend","Use `assign`."),T=g((function(t,e){return y(t,e,!0)}),"merge","Use `assign`.");
      function C(t,e,i){
        var n,r=e.prototype;(n=t.prototype=Object.create(r)).constructor=t,n._super=r,i&&u(n,i)
      }
      function E(t,e){
        return function(){
          return t.apply(e,arguments)}}function x(t,e){
            return"function"==typeof t?t.apply(e&&e[0]||void 0,e):t
          }
          function b(t,e){
            return void 0===t?e:t
          }
          function I(t,e,i){m(w(e),(function(e){t.addEventListener(e,i,!1)}))}function S(t,e,i){m(w(e),(function(e){t.removeEventListener(e,i,!1)}))}function P(t,e){for(;t;){if(t==e)return!0;t=t.parentNode}return!1}function _(t,e){return t.indexOf(e)>-1}function w(t){return t.trim().split(/\s+/g)}function A(t,e,i){if(t.indexOf&&!i)return t.indexOf(e);
            for(var n=0;n<t.length;){if(i&&t[n][i]==e||!i&&t[n]===e)return n;n++}
            return-1}function O(t){return Array.prototype.slice.call(t,0)}
            function M(t,e,i){for(var n=[],r=[],s=0;s<t.length;){
              var o=e?t[s][e]:t[s];A(r,o)<0&&n.push(t[s]),r[s]=o,s++}return i&&(n=e?n.sort((function(t,i){
                return t[e]>i[e]})):n.sort()),n}function D(t,e){for(var i,n,r=e[0].toUpperCase()+e.slice(1),s=0;s<c.length;){
                  if((n=(i=c[s])?i+r:e)in t)return n;s++}}var z=1;function R(t){var e=t.ownerDocument||t;return e.defaultView||e.parentWindow||r}var N="ontouchstart"in r,F=void 0!==D(r,"PointerEvent"),q=N&&/mobile|tablet|ip(ad|hone|od)|android/i.test(navigator.userAgent),L=["x","y"],W=["clientX","clientY"];
                  function Y(t,e){var i=this;this.manager=t,this.callback=e,this.element=t.element,this.target=t.options.inputTarget,this.domHandler=function(e){x(t.options.enable,[t])&&i.handler(e)},this.init()}function X(t,e,i){var n=i.pointers.length,r=i.changedPointers.length,s=1&e&&n-r==0,o=12&e&&n-r==0;i.isFirst=!!s,i.isFinal=!!o,s&&(t.session={}),i.eventType=e,
                  function(t,e){var i=t.session,n=e.pointers,r=n.length;i.firstInput||(i.firstInput=j(e));r>1&&!i.firstMultiple?i.firstMultiple=j(e):1===r&&(i.firstMultiple=!1);var s=i.firstInput,o=i.firstMultiple,a=o?o.center:s.center,u=e.center=k(n);e.timeStamp=f(),e.deltaTime=e.timeStamp-s.timeStamp,e.angle=K(a,u),e.distance=B(a,u),function(t,e){var i=e.center,n=t.offsetDelta||{},r=t.prevDelta||{},s=t.prevInput||{};1!==e.eventType&&4!==s.eventType||(r=t.prevDelta={x:s.deltaX||0,y:s.deltaY||0},n=t.offsetDelta={x:i.x,y:i.y});e.deltaX=r.x+(i.x-n.x),e.deltaY=r.y+(i.y-n.y)}(i,e),e.offsetDirection=U(e.deltaX,e.deltaY);
                  var c=H(e.deltaTime,e.deltaX,e.deltaY);e.overallVelocityX=c.x,e.overallVelocityY=c.y,e.overallVelocity=p(c.x)>p(c.y)?c.x:c.y,e.scale=o?(l=o.pointers,h=n,B(h[0],h[1],W)/B(l[0],l[1],W)):1,e.rotation=o?function(t,e){return K(e[1],e[0],W)+K(t[1],t[0],W)}(o.pointers,n):0,e.maxPointers=i.prevInput?e.pointers.length>i.prevInput.maxPointers?e.pointers.length:i.prevInput.maxPointers:e.pointers.length,function(t,e){var i,n,r,s,o=t.lastInterval||e,a=e.timeStamp-o.timeStamp;if(8!=e.eventType&&(a>25||void 0===o.velocity)){
                    var u=e.deltaX-o.deltaX,c=e.deltaY-o.deltaY,l=H(a,u,c);n=l.x,r=l.y,i=p(l.x)>p(l.y)?l.x:l.y,s=U(u,c),t.lastInterval=e}else i=o.velocity,n=o.velocityX,r=o.velocityY,s=o.direction;e.velocity=i,e.velocityX=n,e.velocityY=r,e.direction=s}(i,e);var l,h;var d=t.element;P(e.srcEvent.target,d)&&(d=e.srcEvent.target);e.target=d}(t,i),t.emit("hammer.input",i),t.recognize(i),t.session.prevInput=i}
                  function j(t){for(var e=[],i=0;i<t.pointers.length;)e[i]={clientX:h(t.pointers[i].clientX),clientY:h(t.pointers[i].clientY)},i++;return{timeStamp:f(),pointers:e,center:k(e),deltaX:t.deltaX,deltaY:t.deltaY}}function k(t){var e=t.length;if(1===e)return{x:h(t[0].clientX),y:h(t[0].clientY)};for(var i=0,n=0,r=0;r<e;)i+=t[r].clientX,n+=t[r].clientY,r++;return{x:h(i/e),y:h(n/e)}}function H(t,e,i){return{x:e/t||0,y:i/t||0}}function U(t,e){return t===e?1:p(t)>=p(e)?t<0?2:4:e<0?8:16}function B(t,e,i){i||(i=L);var n=e[i[0]]-t[i[0]],r=e[i[1]]-t[i[1]];return Math.sqrt(n*n+r*r)}function K(t,e,i){i||(i=L);var n=e[i[0]]-t[i[0]],r=e[i[1]]-t[i[1]];return 180*Math.atan2(r,n)/Math.PI}Y.prototype={handler:function(){},init:function(){this.evEl&&I(this.element,this.evEl,this.domHandler),this.evTarget&&I(this.target,this.evTarget,this.domHandler),this.evWin&&I(R(this.element),this.evWin,this.domHandler)},destroy:function(){this.evEl&&S(this.element,this.evEl,this.domHandler),this.evTarget&&S(this.target,this.evTarget,this.domHandler),this.evWin&&S(R(this.element),this.evWin,this.domHandler)}};var V={mousedown:1,mousemove:2,mouseup:4};function G(){this.evEl="mousedown",this.evWin="mousemove mouseup",this.pressed=!1,Y.apply(this,arguments)}C(G,Y,{handler:function(t){var e=V[t.type];1&e&&0===t.button&&(this.pressed=!0),2&e&&1!==t.which&&(e=4),this.pressed&&(4&e&&(this.pressed=!1),this.callback(this.manager,e,{pointers:[t],changedPointers:[t],pointerType:"mouse",srcEvent:t}))}});
                  var Z={pointerdown:1,pointermove:2,pointerup:4,pointercancel:8,pointerout:8},$={2:"touch",3:"pen",4:"mouse",5:"kinect"},J="pointerdown",Q="pointermove pointerup pointercancel";function tt(){this.evEl=J,this.evWin=Q,Y.apply(this,arguments),this.store=this.manager.session.pointerEvents=[]}r.MSPointerEvent&&!r.PointerEvent&&(J="MSPointerDown",Q="MSPointerMove MSPointerUp MSPointerCancel"),C(tt,Y,{handler:function(t){var e=this.store,i=!1,n=t.type.toLowerCase().replace("ms",""),r=Z[n],s=$[t.pointerType]||t.pointerType,o="touch"==s,a=A(e,t.pointerId,"pointerId");1&r&&(0===t.button||o)?a<0&&(e.push(t),a=e.length-1):12&r&&(i=!0),a<0||(e[a]=t,this.callback(this.manager,r,{pointers:e,changedPointers:[t],pointerType:s,srcEvent:t}),i&&e.splice(a,1))}});var et={touchstart:1,touchmove:2,touchend:4,touchcancel:8};function it(){this.evTarget="touchstart",this.evWin="touchstart touchmove touchend touchcancel",this.started=!1,Y.apply(this,arguments)}function nt(t,e){var i=O(t.touches),n=O(t.changedTouches);return 12&e&&(i=M(i.concat(n),"identifier",!0)),[i,n]}C(it,Y,{handler:function(t){var e=et[t.type];if(1===e&&(this.started=!0),this.started){var i=nt.call(this,t,e);12&e&&i[0].length-i[1].length==0&&(this.started=!1),this.callback(this.manager,e,{pointers:i[0],changedPointers:i[1],pointerType:"touch",srcEvent:t})}}});var rt={touchstart:1,touchmove:2,touchend:4,touchcancel:8};function st(){this.evTarget="touchstart touchmove touchend touchcancel",this.targetIds={},Y.apply(this,arguments)}function ot(t,e){var i=O(t.touches),n=this.targetIds;if(3&e&&1===i.length)return n[i[0].identifier]=!0,[i,i];var r,s,o=O(t.changedTouches),a=[],u=this.target;if(s=i.filter((function(t){return P(t.target,u)})),1===e)for(r=0;r<s.length;)n[s[r].identifier]=!0,r++;for(r=0;r<o.length;)n[o[r].identifier]&&a.push(o[r]),12&e&&delete n[o[r].identifier],r++;return a.length?[M(s.concat(a),"identifier",!0),a]:void 0}C(st,Y,{handler:function(t){var e=rt[t.type],i=ot.call(this,t,e);i&&this.callback(this.manager,e,{pointers:i[0],changedPointers:i[1],pointerType:"touch",srcEvent:t})}});function at(){Y.apply(this,arguments);var t=E(this.handler,this);this.touch=new st(this.manager,t),this.mouse=new G(this.manager,t),this.primaryTouch=null,this.lastTouches=[]}function ut(t,e){1&t?(this.primaryTouch=e.changedPointers[0].identifier,ct.call(this,e)):12&t&&ct.call(this,e)}function ct(t){var e=t.changedPointers[0];if(e.identifier===this.primaryTouch){var i={x:e.clientX,y:e.clientY};this.lastTouches.push(i);var n=this.lastTouches;setTimeout((function(){var t=n.indexOf(i);t>-1&&n.splice(t,1)}),2500)}}
                  function lt(t){for(var e=t.srcEvent.clientX,i=t.srcEvent.clientY,n=0;n<this.lastTouches.length;n++){var r=this.lastTouches[n],s=Math.abs(e-r.x),o=Math.abs(i-r.y);if(s<=25&&o<=25)return!0}return!1}C(at,Y,{handler:function(t,e,i){var n="touch"==i.pointerType,r="mouse"==i.pointerType;if(!(r&&i.sourceCapabilities&&i.sourceCapabilities.firesTouchEvents)){if(n)ut.call(this,e,i);else if(r&&lt.call(this,i))return;this.callback(t,e,i)}},destroy:function(){this.touch.destroy(),this.mouse.destroy()}});var ht=D(l.style,"touchAction"),pt=void 0!==ht,ft=function(){if(!pt)return!1;var t={},e=r.CSS&&r.CSS.supports;return["auto","manipulation","pan-y","pan-x","pan-x pan-y","none"].forEach((function(i){t[i]=!e||r.CSS.supports("touch-action",i)})),t}();function dt(t,e){this.manager=t,this.set(e)}dt.prototype={set:function(t){"compute"==t&&(t=this.compute()),pt&&this.manager.element.style&&ft[t]&&(this.manager.element.style[ht]=t),this.actions=t.toLowerCase().trim()},update:function(){this.set(this.manager.options.touchAction)},compute:function(){var t=[];return m(this.manager.recognizers,(function(e){x(e.options.enable,[e])&&(t=t.concat(e.getTouchAction()))})),function(t){if(_(t,"none"))return"none";var e=_(t,"pan-x"),i=_(t,"pan-y");if(e&&i)return"none";if(e||i)return e?"pan-x":"pan-y";if(_(t,"manipulation"))return"manipulation";return"auto"}(t.join(" "))},preventDefaults:function(t){var e=t.srcEvent,i=t.offsetDirection;
                 if(this.manager.session.prevented)e.preventDefault();
                 else{var n=this.actions,r=_(n,"none")&&!ft.none,s=_(n,"pan-y")&&!ft["pan-y"],o=_(n,"pan-x")&&!ft["pan-x"];
                 if(r){var a=1===t.pointers.length,u=t.distance<2,c=t.deltaTime<250;if(a&&u&&c)return}if(!o||!s)return r||s&&6&i||o&&24&i?this.preventSrc(e):void 0}},preventSrc:function(t){
                   this.manager.session.prevented=!0,t.preventDefault()}};
                   function vt(t){this.options=u({},this.defaults,t||{}),this.id=z++,this.manager=null,this.options.enable=b(this.options.enable,!0),
                   this.state=1,this.simultaneous={},this.requireFail=[]}function mt(t){return 16&t?"cancel":8&t?"end":4&t?"move":2&t?"start":""}
                   function gt(t){return 16==t?"down":8==t?"up":2==t?"left":4==t?"right":""}
                   function yt(t,e){var i=e.manager;return i?i.get(t):t}
                   function Tt(){vt.apply(this,arguments)}function Ct(){Tt.apply(this,arguments),this.pX=null,this.pY=null}
                   function Et(){Tt.apply(this,arguments)}function xt(){vt.apply(this,arguments),this._timer=null,this._input=null}
                   function bt(){Tt.apply(this,arguments)}function It(){Tt.apply(this,arguments)}function St(){vt.apply(this,arguments),this.pTime=!1,this.pCenter=!1,this._timer=null,this._input=null,this.count=0}
                   function Pt(t,e){return(e=e||{}).recognizers=b(e.recognizers,Pt.defaults.preset),new _t(t,e)}vt.prototype={defaults:{},set:function(t){return u(this.options,t),this.manager&&this.manager.touchAction.update(),this},recognizeWith:function(t){if(v(t,"recognizeWith",this))return this;
                   var e=this.simultaneous;return e[(t=yt(t,this)).id]||(e[t.id]=t,t.recognizeWith(this)),this},dropRecognizeWith:function(t){return v(t,"dropRecognizeWith",this)||(t=yt(t,this),delete this.simultaneous[t.id]),this},requireFailure:function(t){if(v(t,"requireFailure",this))return this;
                   var e=this.requireFail;return-1===A(e,t=yt(t,this))&&(e.push(t),t.requireFailure(this)),this},dropRequireFailure:function(t){if(v(t,"dropRequireFailure",this))return this;t=yt(t,this);
                   var e=A(this.requireFail,t);return e>-1&&this.requireFail.splice(e,1),this},hasRequireFailures:function(){return this.requireFail.length>0},canRecognizeWith:function(t){return!!this.simultaneous[t.id]},emit:function(t){var e=this,i=this.state;function n(i){e.manager.emit(i,t)}i<8&&n(e.options.event+mt(i)),n(e.options.event),t.additionalEvent&&n(t.additionalEvent),i>=8&&n(e.options.event+mt(i))},tryEmit:function(t){
                     if(this.canEmit())return this.emit(t);this.state=32},canEmit:function(){for(var t=0;t<this.requireFail.length;){if(!(33&this.requireFail[t].state))return!1;t++}return!0},recognize:function(t){var e=u({},t);
                     if(!x(this.options.enable,[this,e]))return this.reset(),void(this.state=32);56&this.state&&(this.state=1),this.state=this.process(e),30&this.state&&this.tryEmit(e)},process:function(t){},getTouchAction:function(){},reset:function(){}},C(Tt,vt,{defaults:{pointers:1},attrTest:function(t){
                       var e=this.options.pointers;return 0===e||t.pointers.length===e},process:function(t){var e=this.state,i=t.eventType,n=6&e,r=this.attrTest(t);return n&&(8&i||!r)?16|e:n||r?4&i?8|e:2&e?4|e:2:32}}),C(Ct,Tt,{defaults:{event:"pan",threshold:10,pointers:1,direction:30},getTouchAction:function(){
                         var t=this.options.direction,e=[];return 6&t&&e.push("pan-y"),24&t&&e.push("pan-x"),e},directionTest:function(t){var e=this.options,i=!0,n=t.distance,r=t.direction,s=t.deltaX,o=t.deltaY;
                          return r&e.direction||(6&e.direction?(r=0===s?1:s<0?2:4,i=s!=this.pX,n=Math.abs(t.deltaX)):(r=0===o?1:o<0?8:16,i=o!=this.pY,n=Math.abs(t.deltaY))),t.direction=r,i&&n>e.threshold&&r&e.direction},attrTest:function(t){
                            return Tt.prototype.attrTest.call(this,t)&&(2&this.state||!(2&this.state)&&this.directionTest(t))},emit:function(t){this.pX=t.deltaX,this.pY=t.deltaY;var e=gt(t.direction);e&&(t.additionalEvent=this.options.event+e),this._super.emit.call(this,t)}}),C(Et,Tt,{defaults:{event:"pinch",threshold:0,pointers:2},getTouchAction:function(){
                              return["none"]},attrTest:function(t){return this._super.attrTest.call(this,t)&&(Math.abs(t.scale-1)>this.options.threshold||2&this.state)},emit:function(t){
                                if(1!==t.scale){var e=t.scale<1?"in":"out";t.additionalEvent=this.options.event+e}this._super.emit.call(this,t)}}),C(xt,vt,{defaults:{event:"press",pointers:1,time:251,threshold:9},getTouchAction:function(){
                                  return["auto"]},process:function(t){var e=this.options,i=t.pointers.length===e.pointers,n=t.distance<e.threshold,r=t.deltaTime>e.time;
                                    if(this._input=t,!n||!i||12&t.eventType&&!r)this.reset();
                                    else if(1&t.eventType)this.reset(),this._timer=d((function(){this.state=8,this.tryEmit()}),e.time,this);
                                    else if(4&t.eventType)return 8;
                                    return 32},reset:function(){clearTimeout(this._timer)},emit:function(t){8===this.state&&(t&&4&t.eventType?this.manager.emit(this.options.event+"up",t):(this._input.timeStamp=f(),this.manager.emit(this.options.event,this._input)))}}),C(bt,Tt,{defaults:{event:"rotate",threshold:0,pointers:2},getTouchAction:function(){
                                      return["none"]},attrTest:function(t){return this._super.attrTest.call(this,t)&&(Math.abs(t.rotation)>this.options.threshold||2&this.state)}}),C(It,Tt,{defaults:{event:"swipe",threshold:10,velocity:.3,direction:30,pointers:1},getTouchAction:function(){
                                        return Ct.prototype.getTouchAction.call(this)},attrTest:function(t){var e,i=this.options.direction;
                                          return 30&i?e=t.overallVelocity:6&i?e=t.overallVelocityX:24&i&&(e=t.overallVelocityY),this._super.attrTest.call(this,t)&&i&t.offsetDirection&&t.distance>this.options.threshold&&t.maxPointers==this.options.pointers&&p(e)>this.options.velocity&&4&t.eventType},emit:function(t){var e=gt(t.offsetDirection);e&&this.manager.emit(this.options.event+e,t),this.manager.emit(this.options.event,t)}}),C(St,vt,{defaults:{event:"tap",pointers:1,taps:1,interval:300,time:250,threshold:9,posThreshold:10},getTouchAction:function(){
                                            return["manipulation"]},process:function(t){var e=this.options,i=t.pointers.length===e.pointers,n=t.distance<e.threshold,r=t.deltaTime<e.time;if(this.reset(),1&t.eventType&&0===this.count)return this.failTimeout();if(n&&r&&i){if(4!=t.eventType)return this.failTimeout();var s=!this.pTime||t.timeStamp-this.pTime<e.interval,o=!this.pCenter||B(this.pCenter,t.center)<e.posThreshold;if(this.pTime=t.timeStamp,this.pCenter=t.center,o&&s?this.count+=1:this.count=1,this._input=t,0===this.count%e.taps)return this.hasRequireFailures()?(this._timer=d((function(){this.state=8,this.tryEmit()}),e.interval,this),2):8}return 32},failTimeout:function(){return this._timer=d((function(){this.state=32}),this.options.interval,this),32},reset:function(){clearTimeout(this._timer)},emit:function(){8==this.state&&(this._input.tapCount=this.count,this.manager.emit(this.options.event,this._input))}}),Pt.VERSION="2.0.7",Pt.defaults={domEvents:!1,touchAction:"compute",enable:!0,inputTarget:null,inputClass:null,preset:[[bt,{enable:!1}],[Et,{enable:!1},["rotate"]],[It,{direction:6}],[Ct,{direction:6},["swipe"]],[St],[St,{event:"doubletap",taps:2},["tap"]],[xt]],cssProps:{userSelect:"none",touchSelect:"none",touchCallout:"none",contentZooming:"none",userDrag:"none",tapHighlightColor:"rgba(0,0,0,0)"}};function _t(t,e){var i;this.options=u({},Pt.defaults,e||{}),this.options.inputTarget=this.options.inputTarget||t,this.handlers={},this.session={},this.recognizers=[],this.oldCssProps={},this.element=t,this.input=new((i=this).options.inputClass||(F?tt:q?st:N?at:G))(i,X),this.touchAction=new dt(this,this.options.touchAction),wt(this,!0),m(this.options.recognizers,(function(t){var e=this.add(new t[0](t[1]));t[2]&&e.recognizeWith(t[2]),t[3]&&e.requireFailure(t[3])}),this)}function wt(t,e){var i,n=t.element;n.style&&(m(t.options.cssProps,(function(r,s){i=D(n.style,s),e?(t.oldCssProps[i]=n.style[i],n.style[i]=r):n.style[i]=t.oldCssProps[i]||""})),e||(t.oldCssProps={}))}_t.prototype={set:function(t){return u(this.options,t),t.touchAction&&this.touchAction.update(),t.inputTarget&&(this.input.destroy(),this.input.target=t.inputTarget,this.input.init()),this},stop:function(t){this.session.stopped=t?2:1},recognize:function(t){var e=this.session;if(!e.stopped){var i;this.touchAction.preventDefaults(t);var n=this.recognizers,r=e.curRecognizer;(!r||r&&8&r.state)&&(r=e.curRecognizer=null);for(var s=0;s<n.length;)i=n[s],2===e.stopped||r&&i!=r&&!i.canRecognizeWith(r)?i.reset():i.recognize(t),!r&&14&i.state&&(r=e.curRecognizer=i),s++}},get:function(t){if(t instanceof vt)return t;for(var e=this.recognizers,i=0;i<e.length;i++)if(e[i].options.event==t)return e[i];return null},add:function(t){if(v(t,"add",this))return this;var e=this.get(t.options.event);return e&&this.remove(e),this.recognizers.push(t),t.manager=this,this.touchAction.update(),t},remove:function(t){if(v(t,"remove",this))return this;if(t=this.get(t)){var e=this.recognizers,i=A(e,t);-1!==i&&(e.splice(i,1),this.touchAction.update())}return this},on:function(t,e){if(void 0!==t&&void 0!==e){var i=this.handlers;return m(w(t),(function(t){i[t]=i[t]||[],i[t].push(e)})),this}},off:function(t,e){if(void 0!==t){var i=this.handlers;return m(w(t),(function(t){e?i[t]&&i[t].splice(A(i[t],e),1):delete i[t]})),this}},emit:function(t,e){this.options.domEvents&&function(t,e){var i=s.createEvent("Event");i.initEvent(t,!0,!0),i.gesture=e,e.target.dispatchEvent(i)}(t,e);var i=this.handlers[t]&&this.handlers[t].slice();if(i&&i.length){e.type=t,e.preventDefault=function(){e.srcEvent.preventDefault()};for(var n=0;n<i.length;)i[n](e),n++}},destroy:function(){this.element&&wt(this,!1),this.handlers={},this.session={},this.input.destroy(),this.element=null}},u(Pt,{INPUT_START:1,INPUT_MOVE:2,INPUT_END:4,INPUT_CANCEL:8,STATE_POSSIBLE:1,STATE_BEGAN:2,STATE_CHANGED:4,STATE_ENDED:8,STATE_RECOGNIZED:8,STATE_CANCELLED:16,STATE_FAILED:32,DIRECTION_NONE:1,DIRECTION_LEFT:2,DIRECTION_RIGHT:4,DIRECTION_UP:8,DIRECTION_DOWN:16,DIRECTION_HORIZONTAL:6,DIRECTION_VERTICAL:24,DIRECTION_ALL:30,Manager:_t,Input:Y,TouchAction:dt,TouchInput:st,MouseInput:G,PointerEventInput:tt,TouchMouseInput:at,SingleTouchInput:it,Recognizer:vt,AttrRecognizer:Tt,Tap:St,Pan:Ct,Swipe:It,Pinch:Et,Rotate:bt,Press:xt,on:I,off:S,each:m,merge:T,extend:y,assign:u,inherit:C,bindFn:E,prefixed:D}),(void 0!==r?r:"undefined"!=typeof self?self:{}).Hammer=Pt,void 0===(n=function(){return Pt}.call(e,i,e,t))||(t.exports=n)}(window,document)}]);








                                            
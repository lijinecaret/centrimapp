import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateMaintenanceRequestPageRoutingModule } from './create-maintenance-request-routing.module';

import { CreateMaintenanceRequestPage } from './create-maintenance-request.page';
// import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // ApplicationPipesModule,
    CreateMaintenanceRequestPageRoutingModule
  ],
  declarations: [CreateMaintenanceRequestPage]
})
export class CreateMaintenanceRequestPageModule {}

import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment-timezone';
import { Platform, LoadingController, ToastController, ActionSheetController, ModalController, PopoverController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { LocationsComponent } from '../components/locations/locations.component';
import { ActivityImageComponent } from '../components/activity-image/activity-image.component';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
@Component({
  selector: 'app-create-maintenance-request',
  templateUrl: './create-maintenance-request.page.html',
  styleUrls: ['./create-maintenance-request.page.scss'],
})
export class CreateMaintenanceRequestPage implements OnInit {
  // @ViewChild(MultiFileUploadComponent,{static:false}) fileField: MultiFileUploadComponent;
title:any;
location:any;
loc:any;
parent:any;
priority:any="1";
type:any;
description:any;
currentDate:Date;
due_date:Date;
image:any=[];
subscription:Subscription;
other_type:any;
img:any=[];
imageResponse:any=[];
keyboardStyle = { width: '100%', height: '0px' };
typeArray:any=[];
isLoading:boolean=false;

preferred_status:boolean=false;
entry_permission:boolean=false;

priority_status:boolean=true;
photo_status:boolean=true;

preferred_date:any;

tz:any;
rv:any;
permission:any='0';
  constructor(private http:HttpClient,private storage:Storage,private config:HttpConfigService,private router:Router,
    private platform:Platform,private loadingCtrl:LoadingController,private toast:ToastController,
    private keyboard:Keyboard,private actionsheetCntlr:ActionSheetController,
    private route:ActivatedRoute,private modalCntrl:ModalController,
    private popCntrl:PopoverController) { }

  ngOnInit() {
    // this.imagePicker.requestReadPermission();
    this.keyboard.onKeyboardWillShow().subscribe( {
    
      next: x => {
        this.keyboardStyle.height = x.keyboardHeight + 'px';
        
       
      },
      error: e => {
        console.log(e);
      }
      
    });
    this.keyboard.onKeyboardWillHide().subscribe( {
      next: x => {
        this.keyboardStyle.height = '0px';
        // document.getElementById('des').scrollIntoView(false);
      },
      error: e => {
        console.log(e);
      }
    });
    

  }
  pushDesc(){
    console.log("push desc");
    
    this.keyboard.onKeyboardDidShow().subscribe(()=>{
      document.getElementById('des').scrollIntoView(true)
    })
  }
  pushTitle(){
    console.log("push desc");
    
    this.keyboard.onKeyboardDidShow().subscribe(()=>{
      document.getElementById('req').scrollIntoView(true)
    })
  }
  pushLoc(){
    console.log("push desc");
    
    this.keyboard.onKeyboardDidShow().subscribe(()=>{
      document.getElementById('loc').scrollIntoView(true)
    })
  }
  async ionViewWillEnter(){
    this.img=[];
    this.imageResponse=[];
    this.typeArray=JSON.parse(this.route.snapshot.paramMap.get('type'));
    this.rv=await this.storage.get('RES-RV-SETTING');
    this.tz=await this.storage.get('TIMEZONE');
    this.preferred_date=moment.tz(this.tz).format();
    // this.getPublicSettings();
    if(this.rv==1){
    this.getResidentPropertyLocation();
    // }else{
    //   this.getResidentRoom();
    }
   
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      
      this.back();
    });
  }
  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      mode:'md',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage();
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
    
  }
  
  
  async pickImage(){
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):
    //   let base64Image = 'data:image/jpeg;base64,' + imageData;
      
    //   this.imageResponse.push(base64Image);
    //   // this.img.push(imageData)
    //   this.uploadImage(base64Image);
       
    // }, (err) => {
    //   // Handle error
    // });


  
      const image = await Camera.getPhoto({
        quality: 90,
        allowEditing: false,
        resultType: CameraResultType.Base64,
        correctOrientation:true,
        source:CameraSource.Camera
      });
    
      // image.webPath will contain a path that can be set as an image src.
      // You can access the original file using image.path, which can be
      // passed to the Filesystem API to read the raw data of the image,
      // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
      var imageUrl = image.base64String;
    
      // Can be set to the src of an image now
      let base64Image = 'data:image/jpeg;base64,' + imageUrl;
      console.log('image:',imageUrl);
      this.imageResponse.push(base64Image);
      this.uploadImage(base64Image);
    
  }

  async addImage(){
    console.log("imagR:",this.imageResponse);
    // let options;
    // options={
    //   maximumImagesCount: 5,
    //   outputType: 1
    // }
    // if(this.platform.is('ios')){
    //   options.disable_popover=true
    // }
      // this.imagePicker.getPictures(options).then((results) => {
      //   for (var i = 0; i < results.length; i++) {
      //       console.log('Image URI: ' + results[i]);
            
            
            
      //        if((results[i]==='O')||results[i]==='K'){
      //         console.log("no img");
              
      //         }else{
      //           // this.img.push(results[i]);
      //         this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
      //         this.uploadImage('data:image/jpeg;base64,' + results[i])
      //         }
              
         
             
      //   }
      // }, (err) => { });

      const image = await Camera.pickImages({
        quality: 90,
        correctOrientation:true,
        limit:5
        
      });
    
    
      for (var i = 0; i < image.photos.length; i++) {
              console.log('Image URI: ' + image.photos[i]);
              
              
              const contents = await Filesystem.readFile({
                path: image.photos[i].path
              });
              
                this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
                this.uploadImage('data:image/jpeg;base64,' + contents.data)
              
                
           
               
          }
    
  
    
    console.log("imagg:",image.photos,image.photos[0].path);
    
    }
    removeImg(i){
      this.imageResponse.splice(i,1);
      this.img.splice(i,1);
    }
async submit(){
  
  // this.storage.ready().then(()=>{
    const uid=await this.storage.get('USER_ID');
      const bid=await this.storage.get('BRANCH');
        this.currentDate=new Date();
        if(this.priority==2){
          this.due_date=this.currentDate
        }else if(this.priority==1){
          this.due_date=new Date(Date.now()+2*24*60*60*1000);
        }else if(this.priority==0){
          this.due_date=new Date(Date.now()+3*24*60*60*1000);
        }
        if(this.description==undefined||this.description==''){
          this.description=null
        }
        if(this.loc==undefined){
          this.loc=null
        }
        // let files = this.fileField.getFiles();
        // console.log(files);
        let current=this.currentDate.getFullYear()+'-'+this.fixDigit((this.currentDate.getMonth()+1))+'-'+this.fixDigit(this.currentDate.getDate());
        let due=this.due_date.getFullYear()+'-'+this.fixDigit((this.due_date.getMonth()+1))+'-'+this.fixDigit(this.due_date.getDate());
        
        // let formData = new FormData();
        // formData.append('request_title', this.title); // Add any other data you want to send
        // formData.append('location', this.location);
        // formData.append('risk_rating', '1');
        // formData.append('hazard', '1'); // Add any other data you want to send
        // formData.append('priority', this.priority);
        // formData.append('out_of_order', '1');
        // formData.append('status', '0'); // Add any other data you want to send
        // formData.append('due_date', due);
        // formData.append('request_date', current);
        // formData.append('type', this.type); // Add any other data you want to send
        // formData.append('priority', this.priority);
        // formData.append('other_type', null);
        // formData.append('description', this.description);
        // formData.append('created_by', uid); // Add any other data you want to send
        // formData.append('user_id', uid);
        // formData.append('branch_id', bid);
        // files.forEach((file) => {
        //   formData.append('images[]', file.rawFile);
        // });
        
        if(this.type!=0){
          this.other_type=null
        }
        
        if(/^\d+$/.test(this.title)|| this.title==undefined || this.title==''){
          this.presentAlert('Please enter a proper request title.');
        // }else if(/^\d+$/.test(this.location)|| this.location==undefined || this.location==''){
        //   this.presentAlert('Please enter proper location.');
        }else if(this.type==undefined){
          this.presentAlert('Please select the type.');
        }else{
          this.showLoading();

        // let body={
        //   request_title:this.title,
        //   location:this.location,
        //   risk_rating:1,
        //   hazard:1,
        //   priority:this.priority,
        //   out_of_order:1,
        //   status:0,
        //   due_date:due,
        //   request_date:current,
        //   type:this.type,
        //   other_type:this.other_type,
        //   description:this.description,
        //   created_by:uid,
        //   user_id:uid,
        //   branch_id:bid,
        //   images:this.img
        // }
        let body;
        body={
          request_title:this.title,
          location:this.loc,
          location_id:null,
          risk_rating:3,
          hazard:1,
          priority:this.priority,
          out_of_order:1,
          status:0,
          due_date:due,
          request_date:current,
          type:this.type,
          other_type:null,
          description:this.description,
          created_by:uid,
          user_id:uid,
          branch_id:bid,
          images:this.img,
          category:0,
          assets:null,
          recurring:0,
          repeat:null,
          endon:null,
          enddate:null,
          endafteroccu:null,
          daily:null,
          weekly:null,
          weekcount:null,
          dayof:null,
          monthly:null,
          monthofyear:null,
          technician:null,
          show_duedate:0
        }
        
          if(this.preferred_status){
            body.preferred_datetime=moment(this.preferred_date).format('YYYY-MM-DD HH:mm:ss')
          }
          if(this.entry_permission){
            body.permission_to_enter=this.permission
          }

          if(this.rv==1){
            const type=await this.storage.get('USER_TYPE');
            let resident;
            const uid=await this.storage.get('USER_ID');
            if(type==5){
              resident=uid
            }else{
              resident= await this.storage.get('RESIDENT_ID');
            }
            body.created_for=resident
          }
        
        console.log("body:",body);
        let headers=await this.config.getHeader();
        let url=this.config.domain_url+'store_maintenance_request';
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          this.presentAlert('Your maintenance request has been placed.');
          this.dismissLoader();
          this.back();
          
        },error=>{
          this.dismissLoader();
          this.presentAlert('Something went wrong.Please try again.');
          console.log(error);
          
        })
      }
  //     })
  //   })
  // })
   
}

fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
}
async presentAlert(message) {
  const alert = await this.toast.create({
    message: message,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}
async showLoading() {
  this.isLoading=true;
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}
async dismissLoader() {
  this.isLoading=false;
  return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}


back(){
  this.title='';
  this.location='';
  this.priority="1";
  this.parent='';
  this.description='';

  this.image=[];

  this.other_type='';
  this.img=[];
  this.imageResponse=[];
  this.router.navigate(['/maintenance'],{replaceUrl:true})
}

async uploadImage(img){
  let headers=await this.config.getHeader();
  if(this.isLoading==false){
    this.showLoading();
    }
  let url=this.config.domain_url+'upload_file';
  let body={file:img}
console.log("body:",img);

  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    
    // this.img.push(res.data);
    this.img.push(res.data);
    this.dismissLoader();
    // console.log("uploaded:",this.img);
  },error=>{
    console.log(error);
    this.dismissLoader();
  })
}

async openLocation() {

  const popover = await this.modalCntrl.create({
    component: LocationsComponent,
   
    componentProps:{
      data:this.loc,
      // branch:this.branch
    },
    cssClass:'locations-modal'
  });

  popover.onDidDismiss().then((dataReturned) => {
    if(dataReturned.data!=undefined){
      this.loc=dataReturned.data.id;
      this.location=dataReturned.data.loc_name;
      this.parent=dataReturned.data.parent
    }
    console.log("data:",dataReturned);
   
  });
  return await popover.present();
}

removeLoc(){
     
  this.loc=undefined;
  this.location=undefined;
  this.parent='';

}

async showImage(){
  const popover = await this.popCntrl.create({
    component: ActivityImageComponent,
    // event: ev,
    componentProps:{
      data:this.img,
      flag:1
    },
    cssClass:'image_pop'
  });
  return await popover.present();
}

async getPublicSettings(){
 
  const bid=await this.storage.get('BRANCH');
    const type=await this.storage.get('USER_TYPE');
      
        this.tz=await this.storage.get('TIMEZONE');
          let headers=await this.config.getHeader();
          // let branch;
          // if (type == 1 || type == 4 || type == 9) {
          //   branch = this.branch
          // } else {
          //   branch = bid
          // }
        
        let url=this.config.domain_url+'maintenance_public_form_settings/'+bid
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log('pub:',res,url,this.rv);
          if(res.data&&res.data.date_time==1){
            this.preferred_status=true;
            this.preferred_date=moment.tz(this.tz).format();
          }else{
            this.preferred_status=false
          }
          if(res.data&&res.data.entry_permission==1){
            this.entry_permission=true
          }else{
            this.entry_permission=false
          }
          
          if(res.data&&res.data.priority==1){
            this.priority_status=true
          }else{
            this.priority_status=false
          }
          if(res.data&&res.data.photos==1){
            this.photo_status=true
          }else{
            this.photo_status=false
          }
        })
        
    
}
async getResidentRoom(){
  const data=await this.storage.get('USER_TYPE');
  const cid=await this.storage.get('COMPANY_ID');
  const bid=await this.storage.get('BRANCH');
  const uid=await this.storage.get('USER_ID');
  let resident;
  if(data==5){
    resident=uid
  }else{
    resident= await this.storage.get('RESIDENT_ID');
  }

    let headers=await this.config.getHeader();
   
    const id=await this.storage.get('RESIDENT_ID');
    let url=this.config.domain_url+'consumer/'+id;
                
    this.http.get(url,{headers}).subscribe((data:any)=>{

      console.log('res:',data);
      
       this.location=data.data.room;
    })
  
}

async getResidentPropertyLocation(){
  const type=await this.storage.get('USER_TYPE');
  let resident;
  const uid=await this.storage.get('USER_ID');
  if(type==5){
    resident=uid
  }else{
    resident= await this.storage.get('RESIDENT_ID');
  }
  let headers=await this.config.getHeader();
    let url = this.config.domain_url + 'get_resident_location/'+resident;
    
    
    this.http.get(url,{headers}).subscribe((res: any) => {
      console.log('resloc:',res);
      if(res.data){
      this.location=res.data.name;
      this.loc=res.data.id
      }
    }, error => {
      console.log(error);

    })

}

}

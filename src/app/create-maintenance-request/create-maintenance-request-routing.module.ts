import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateMaintenanceRequestPage } from './create-maintenance-request.page';

const routes: Routes = [
  {
    path: '',
    component: CreateMaintenanceRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateMaintenanceRequestPageRoutingModule {}

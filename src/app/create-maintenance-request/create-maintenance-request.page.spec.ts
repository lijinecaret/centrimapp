import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateMaintenanceRequestPage } from './create-maintenance-request.page';

describe('CreateMaintenanceRequestPage', () => {
  let component: CreateMaintenanceRequestPage;
  let fixture: ComponentFixture<CreateMaintenanceRequestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMaintenanceRequestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateMaintenanceRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { IonRouterOutlet, Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';




@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  keyboardStyle = { width: '100%', height: '0px' };
  email:any;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  constructor(private router: Router,private http:HttpClient,private config:HttpConfigService,private keyboard:Keyboard,
    private toastCntlr:ToastController) { 

      this.backButtonEvent();
        
       


  }

  ngOnInit() {
    this.keyboard.onKeyboardWillShow().subscribe( {
      next: x => {
        this.keyboardStyle.height = x.keyboardHeight + 'px';
      },
      error: e => {
        console.log(e);
      }
    });
    this.keyboard.onKeyboardWillHide().subscribe( {
      next: x => {
        this.keyboardStyle.height = '0px';
      },
      error: e => {
        console.log(e);
      }
    });
  }


  backButtonEvent() {
    document.addEventListener("backbutton", () => {
    this.routerOutlets.forEach((outlet: IonRouterOutlet) => {

      this.router.navigateByUrl('login');
    // if (outlet && outlet.canGoBack()) {
    // outlet.pop();
    // }else if(this.router.url === "stories"){
    // navigator['app'].exitApp(); // work for ionic 4
    // } else if (!outlet.canGoBack()) {
    // navigator['app'].exitApp(); // work for ionic 4
    // }
    });
    });
    }

    reset(){
      if(this.email){
        var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      
        if(!pattern.test(this.email)){
          this.presentAlert('Please enter a valid email Id.');
        }else{
      let url=this.config.domain_url+'forgetPassword';
      
      let body={
        email:this.email
      }
      
        this.http.post(url,body).subscribe((res:any)=>{
          console.log(res);
          if(res.status=='error'){
            this.presentAlert(res.message)
          }else{
          this.presentAlert('A mail has been send to your registered email id.');
          this.router.navigateByUrl('/login');
          }
          
        },error=>{
          console.log(error);
          this.presentAlert('Something went wrong. Please verify your email id. ');
          
        })
      }
    }else{
        this.presentAlert('Please enter an email id.')
      }
    
    }
    async presentAlert(mes) {
      const alert = await this.toastCntlr.create({
        message: mes,
        duration: 3000,
        position:'top'       
      });
      alert.present(); //update
    }
}

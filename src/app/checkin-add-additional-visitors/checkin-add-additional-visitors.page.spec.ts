import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckinAddAdditionalVisitorsPage } from './checkin-add-additional-visitors.page';

describe('CheckinAddAdditionalVisitorsPage', () => {
  let component: CheckinAddAdditionalVisitorsPage;
  let fixture: ComponentFixture<CheckinAddAdditionalVisitorsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinAddAdditionalVisitorsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckinAddAdditionalVisitorsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

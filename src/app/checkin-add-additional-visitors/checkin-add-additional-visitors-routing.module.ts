import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckinAddAdditionalVisitorsPage } from './checkin-add-additional-visitors.page';

const routes: Routes = [
  {
    path: '',
    component: CheckinAddAdditionalVisitorsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckinAddAdditionalVisitorsPageRoutingModule {}

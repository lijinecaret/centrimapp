import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-checkin-add-additional-visitors',
  templateUrl: './checkin-add-additional-visitors.page.html',
  styleUrls: ['./checkin-add-additional-visitors.page.scss'],
})
export class CheckinAddAdditionalVisitorsPage implements OnInit {
  res_id: any;
  purpose: any;
  anArray: any = [];
  count: any = 1;
  info: any;
  additional: any[] = [];
  onPremises: any[] = [];
  more: any;
  user: any;
  subscription: Subscription;
  from:any;
  disable:boolean=false;
  constructor(private route: ActivatedRoute, private router: Router, private storage: Storage,
    private toastCntlr: ToastController, private config: HttpConfigService, private http: HttpClient,
    private platform: Platform) { }

  ngOnInit() {
  }
  async ionViewWillEnter() {
    this.disable=false;
    // this.storage.ready().then(() => {
      const data=await this.storage.get('VIS-ADDITIONAL-VISITOR-INFO');
        this.info = data;
    //   })
    // })
    this.res_id = this.route.snapshot.paramMap.get('res_id');
    this.purpose = this.route.snapshot.paramMap.get('purpose');
    this.onPremises = JSON.parse(this.route.snapshot.paramMap.get('onPremises'));
    this.user = JSON.parse(this.route.snapshot.paramMap.get('user_info'));
    this.from=this.route.snapshot.paramMap.get('from');
    let count=this.route.snapshot.paramMap.get('count');
    if(count){
      this.count=count
    }
    // console.log("more:",this.more);
    this.user.additional_info=[];
    // this.anArray=[];
    // if(this.more==undefined||this.more=='undefined'||this.more==null){
    //   this.anArray.push({'value':''});
    // }else{
    //   this.more.forEach(element => {
    //     this.anArray.push({'value':element.value})
    //   });

    // }
    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.router.navigate(['/checkin-additional-visitors', { res_id: this.res_id, purpose: this.purpose, user_info: JSON.stringify(this.user),from:this.from }]);
      // this.back() ;

    });
  }
  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  // Add(){
  //   // this.anArray.forEach(element => {
  //     // if(element.value==''){
  //     //   this.presentToast('Please fill the field.');
  //     // }else{
  //       this.anArray.push({'value':''});
  //     // }

  //   // });

  //   }
  increment() {
    if(this.count<5){
    this.count++;
    }else{
      this.presentAlert('You can not add more than 5 visitors')
    }
  }

  decrement() {
    if(this.count>1){
    this.count--;
    }
  }

  next() {
    if(this.count>5){
      this.presentAlert('You can not add more than 5 visitors')
    }else{
    this.disable=true;
    this.additional = [];
    console.log("person:", this.anArray);
    if (this.info == 0) {
      if (this.anArray[0].value == '') {
        this.presentAlert('Please fill the field.');
        this.disable=false;
      } else {
        this.anArray.forEach(element => {
          if (element.value != '') {
            this.additional.push({ 'value': element.value });
          }
        });
        this.count = this.anArray.length;
        this.confirm();
      }

    } else {
      this.additional = null;
      this.confirm();
    }

  }
  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top' 
    });
    alert.present(); //update
  }

  confirm() {
   
    this.disable=false;
    this.router.navigate(['/checkin-additional-info', {  res_id: this.res_id, purpose: this.purpose, count: this.count, onPremises: JSON.stringify(this.onPremises), user_info: JSON.stringify(this.user),from:this.from }])
  }

  back() {
    this.router.navigate(['/checkin-additional-visitors', { res_id: this.res_id, purpose: this.purpose, onPremises: JSON.stringify(this.onPremises), user_info: JSON.stringify(this.user),from:this.from }])
  }
}

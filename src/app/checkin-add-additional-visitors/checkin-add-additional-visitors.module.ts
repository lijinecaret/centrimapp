import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckinAddAdditionalVisitorsPageRoutingModule } from './checkin-add-additional-visitors-routing.module';

import { CheckinAddAdditionalVisitorsPage } from './checkin-add-additional-visitors.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckinAddAdditionalVisitorsPageRoutingModule
  ],
  declarations: [CheckinAddAdditionalVisitorsPage]
})
export class CheckinAddAdditionalVisitorsPageModule {}

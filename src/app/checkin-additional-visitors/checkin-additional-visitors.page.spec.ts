import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckinAdditionalVisitorsPage } from './checkin-additional-visitors.page';

describe('CheckinAdditionalVisitorsPage', () => {
  let component: CheckinAdditionalVisitorsPage;
  let fixture: ComponentFixture<CheckinAdditionalVisitorsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinAdditionalVisitorsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckinAdditionalVisitorsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

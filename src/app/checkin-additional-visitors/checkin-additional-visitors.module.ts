import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckinAdditionalVisitorsPageRoutingModule } from './checkin-additional-visitors-routing.module';

import { CheckinAdditionalVisitorsPage } from './checkin-additional-visitors.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckinAdditionalVisitorsPageRoutingModule
  ],
  declarations: [CheckinAdditionalVisitorsPage]
})
export class CheckinAdditionalVisitorsPageModule {}

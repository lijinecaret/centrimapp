import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import momenttz from 'moment-timezone';
import { Subscription } from 'rxjs';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-checkin-additional-visitors',
  templateUrl: './checkin-additional-visitors.page.html',
  styleUrls: ['./checkin-additional-visitors.page.scss'],
})
export class CheckinAdditionalVisitorsPage implements OnInit {
res_id:any;
purpose:any;
onPremises:any[]=[];
user:any;
subscription:Subscription;
from:any;
disable:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private storage:Storage,
    private http:HttpClient,private config:HttpConfigService,private platform:Platform) { }

  ngOnInit() {
  }


  ionViewWillEnter(){
    this.disable=false;
    this.res_id=this.route.snapshot.paramMap.get('res_id');
    this.purpose=this.route.snapshot.paramMap.get('purpose');
    this.onPremises=JSON.parse(this.route.snapshot.paramMap.get('onPremises'));
    this.user=JSON.parse(this.route.snapshot.paramMap.get('user_info'));
    this.from=this.route.snapshot.paramMap.get('from');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      // this.router.navigate(['/checkin-additional-visitors',{res_id:this.res_id,purpose:this.purpose}]) ;
      this.back() ;
   
    }); 
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
  async select(i){
    this.disable=true;
    // this.storage.ready().then(()=>{
      const decl=await this.storage.get('VIS-HEALTH-DECLARATION');
        const donot=await this.storage.get('VIS-DONOTHAVE');
        const terms=await this.storage.get('VIS-TERMS');
    if(i==1){
      this.disable=false;
      this.router.navigate(['/checkin-add-additional-visitors',{res_id:this.res_id,purpose:this.purpose,onPremises:JSON.stringify(this.onPremises), user_info: JSON.stringify(this.user),from:this.from}])
    }else{
      if(decl==1||donot==1){
        this.disable=false;
      this.router.navigate(['/checkin-health-declaration',{res_id:this.res_id,purpose:this.purpose,count:0,onPremises:JSON.stringify(this.onPremises), user_info: JSON.stringify(this.user),from:this.from}])
      }else if(terms==1){
        this.disable=false;
        this.router.navigate(['/checkin-vis-guidelines',{res_id:this.res_id,purpose:this.purpose,count:0,flag:8,onPremises:JSON.stringify(this.onPremises), user_info: JSON.stringify(this.user),from:this.from}])
      }else{
        this.confirm();
      }
    }
//   })
// })
// })
//     })
  }
  async back(){
    // this.storage.ready().then(() => {
        
      const temp=await this.storage.get('VIS-TEMPARATURE');
       
    
    const vac=await this.storage.get('VIS-VACCINATION');
      if(temp==1||vac==1){
      this.router.navigate(['/checkin-vaccine-info',{res_id:this.res_id,purpose:this.purpose,onPremises:JSON.stringify(this.onPremises),from:this.from,flag:1,user_info: JSON.stringify(this.user)}])
      }else{
        if(this.from==1){
          this.router.navigate(['/checkin-scan-qr']);
          }else if(this.from==3){
            this.router.navigate(['/checkin-phone-num',{res_id:this.res_id,onPremises:JSON.stringify(this.onPremises),from:this.from}])
          }else{
            this.router.navigate(['/checkin-choose-consumer',{onPremises:JSON.stringify(this.onPremises),res_id:this.res_id}]);
          }
        // this.router.navigate(['/checkin-visit-purpose',{res_id:this.res_id,onPremises:JSON.stringify(this.onPremises),purpose:this.purpose}])
      }
//     })
//   })
// })
  }

  async confirm(){
    // this.storage.ready().then(()=>{
      const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
          const tz=await this.storage.get('TIMEZONE');
            const name=await this.storage.get('NAME');
              const ph=await this.storage.get('PHONE');
              const img=await this.storage.get('PRO_IMG');
          let additional=[];
          // let count;
          
            
            // count=0;
          
          
          
          // if(this.img=='undefined'){
          //   this.img=null
          // }
          // if(this.address=='null' || this.address=='undefined'){
          //   this.address=null;
          // }
         
         
          
          
        let phone=null;
        if(ph){
        let phn=ph.toString();
        if(phn.charAt( 0 )==='+'){
         phone = phn.substring(1);
        }else{
          phone=phn
        }
      }
          let vac_status,vac_date,vac,vac_proof,inf_vaccine_status,temp,
          inf_vaccine_date,inf_vaccine_proof,firstDose,secondDose,vaccinated,covidVaccinated,uploaded_date=null;
  if(this.user){
    temp=this.user.temp
  
          if(this.user.vaccine){
            vac_status=this.user.vaccine.vac_status;
            vac_date=this.user.vaccine.vac_date;
            vac_proof=this.user.vaccine.proof;
            vac=this.user.vaccine.id;
            firstDose=this.user.vaccine.firstDose;
            secondDose=this.user.vaccine.secondDose;
            inf_vaccine_status=this.user.vaccine.inf_vaccine_status
            inf_vaccine_date=this.user.vaccine.inf_vaccine_date;
            inf_vaccine_proof=this.user.vaccine.inf_vaccine_proof;
            vaccinated=this.user.vaccine.vaccinated;
            covidVaccinated=this.user.vaccine.covidVaccinated;
            uploaded_date=this.user.vaccine.uploaded_date;
          }
  
        }
          // let res_name=item.user.name;
          let url=this.config.domain_url+'create_visitor_list';
          let headers=await this.config.getHeader();
          let body={
            visitor_type:'0',
            checkin_checkout:'0',
            name:name,
            phone:'+'+phone,
            address:null,
            additional_visitor_count:null,
            additional_visitor:null,
            checkin_time:momenttz().tz(tz).format('yyyy-MM-DD HH:mm:ss'),
            checkout_time:null,
            i_donot_have:null,
            whom_to_visit:'0',
            resident_user_id:this.res_id,
            companian:null,
            purpose:null,
           
            body_temparature_array:null,
            
            photo:img,
            signature:null,
            qna:null,
            recent_test:null,
            test_date:null,
            test_result:null,
            via_app:1,
            visitor_id:null,
            
            body_temparature:temp,
            
            temp_measure:0,
            
              vaccinated:vaccinated,
              covid_vaccinated:covidVaccinated,
              device_id:3,
              
              
              vaccine_status:vac_status,
              vaccine_date:vac_date,
              vaccine :vac,
              expiry_date:null,
              proof:vac_proof,
              uploaded_by:null,
              uploaded_date:uploaded_date,
              // vaccine_status_array:vac_status_array,
              vaccine_array:null,
              vaccine_date_array:null,
              // vaccine_dose_array:vac_dose_array,
              proof_array:null,
              vaccine_phone_array:null,
              firstdose_date_array:null,
              seconddose_date_array:null,
              vaccine_already_have:null,
              firstdose_date:firstDose,
              seconddose_date:secondDose,
              inf_vaccine_status:inf_vaccine_status,
              inf_vaccine_date:inf_vaccine_date,
              inf_vaccine_proof:inf_vaccine_proof,
              inf_vaccine_status_array:null,
              inf_vaccine_date_array:null,
              inf_vaccine_proof_array:null
  
            
          }
          console.log("body:",body);
          
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
             console.log(res);
             this.router.navigate(['/confirm-checkin',{ref_id:res.data.reference_id,additional:null}])
             this.disable=false;
          })
          // })
  //       })
  //     })
  //   })
  //     })
  
  // })
  }
}

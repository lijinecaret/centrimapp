import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckinAdditionalVisitorsPage } from './checkin-additional-visitors.page';

const routes: Routes = [
  {
    path: '',
    component: CheckinAdditionalVisitorsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckinAdditionalVisitorsPageRoutingModule {}

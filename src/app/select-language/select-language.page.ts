import { Component, OnInit, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Subscription } from 'rxjs';
import { Platform, IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-select-language',
  templateUrl: './select-language.page.html',
  styleUrls: ['./select-language.page.scss'],
})
export class SelectLanguagePage implements OnInit {
  @ViewChildren('slideWithNav') slideWithNav: IonSlides;
  sliderOne:any;
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 3,
    
  };
  currentDate:any;
  sid:any;
  cid:any;
  languages=[];
  subscription:Subscription;
  screen:any;
  flag:any;
  constructor(private route:ActivatedRoute,private storage:Storage,private http:HttpClient,private config:HttpConfigService,
    private router:Router,private orient:ScreenOrientation,private platform:Platform) { }

  ngOnInit() {
    if(window.innerWidth>=700){
      this.screen=1;
      }else{
          this.screen=2;
      }
    

  }
async ionViewWillEnter(){
  this.orient.lock(this.orient.ORIENTATIONS.LANDSCAPE);
   





      this.sid=this.route.snapshot.paramMap.get("sid");
      this.flag=this.route.snapshot.paramMap.get("flag");
      if(this.flag==1){
      // this.storage.ready().then(()=>{
        
          const data=await this.storage.get('SURVEY_CID');
            this.cid=data;
            console.log("company");
            
            let url=this.config.feedback_url+'get_survey_language';
            // let body={
            //             company_id:"14",
            //             survey_bid:"6"
            //         }
  
            let body = `company_id=${this.cid}&survey_bid=${this.sid}`;
           
            console.log("body:",body);
            
            let headers=new HttpHeaders({
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            })
  
  
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              this.languages=res.display_all_survey_languages;
              console.log(res);
              console.log("lan:",this.languages);
              this.sliderOne =
                  {
                    isBeginningSlide: true,
                    isEndSlide: false,
                    isActive:false,
                    lang:[]=this.languages
                  };
              console.log("slider:",this.sliderOne.lang);
            },error=>{
              console.log(error);
              
            })      
              
      //     })
      // })
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/survey']) ;
      }); 
    }
    else{
      // this.storage.ready().then(()=>{
        
        const data=await this.storage.get('SURVEY_CID');
          this.cid=data;
          console.log("company");
          
          let url=this.config.feedback_url+'get_review_language';
          // let body={
          //             company_id:"14",
          //             survey_bid:"6"
          //         }
  
          let body = `company_id=${this.cid}&review_bid=${this.sid}`;
         
          console.log("body:",body);
          
          let headers=new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          })
  
  
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            this.languages=res.display_all_review_languages;
            console.log(res);
            console.log("lan:",this.languages);
            this.sliderOne =
                {
                  isBeginningSlide: true,
                  isEndSlide: false,
                  isActive:false,
                  lang:[]=this.languages
                };
            console.log("slider:",this.sliderOne.lang);
          },error=>{
            console.log(error);
            
          })      
            
    //     })
    // })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/feedback']) ;
    }); 




    


    }




    
    

 
   

}
 //Move to Next slide
 slideNext(object, slideView) {
  slideView.slideNext(500).then(() => {
    this.checkIfNavDisabled(object, slideView);
  });
}

//Move to previous slide
slidePrev(object, slideView) {
  slideView.slidePrev(500).then(() => {
    this.checkIfNavDisabled(object, slideView);
  });;
}
SlideDidChange(object, slideView) {
  this.checkIfNavDisabled(object, slideView);
  object.isActive= true;
}
checkIfNavDisabled(object, slideView) {
  this.checkisBeginning(object, slideView);
  this.checkisEnd(object, slideView);
}

checkisBeginning(object, slideView) {
  slideView.isBeginning().then((istrue) => {
    object.isBeginningSlide = istrue;
  });
}
checkisEnd(object, slideView) {
  slideView.isEnd().then((istrue) => {
    object.isEndSlide = istrue;
  });
}
setlang(item){
 
   
    // this.currentDate=index;
    if(this.flag==2){
    this.router.navigate(['/feedback-fivesmiley',{sid:this.sid,lid:item.language_id}]);
    }else{
      this.router.navigate(['/survey-fivesmiley',{sid:this.sid,lid:item.language_id}]);
    }
 }
 ionViewDidLeave(){
  

    this.subscription.unsubscribe();
}

back(){
  if(this.flag==1){
  this.router.navigate(['/survey'])
  }else{
    this.router.navigate(['/feedback']) ;
  }
}

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.page.html',
  styleUrls: ['./survey.page.scss'],
})
export class SurveyPage implements OnInit {
subscription:Subscription;
survey:any=[];
id:any;
cid:any;
constructor(private router:Router,private storage:Storage,private config:HttpConfigService,private http:HttpClient,
  private platform:Platform,private orient:ScreenOrientation) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){

    this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
    // this.storage.ready().then(()=>{
      const dat=await this.storage.get('USER_ID');
        let id=dat;
        this.id=id.toString();
        const res=await this.storage.get('SURVEY_CID');
          let cid=res;
          this.cid=cid.toString();
          let url=this.config.feedback_url+'get_all_surveys';
          let body = `company_id=${this.cid}&user_id=${this.id}`;
          //  body=({
          //   company_id:"14",
          //   user_id:"3"
          // })
          // body.set('company_id','14');
          // body.set('user_id','3');
          console.log("body:",body);
          
          let headers=new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          })
          this.http.post(url,body,{headers}).subscribe((data:any)=>{
            this.survey=data.data;
            console.log(data);
            
          })

    //     })
    //   })
    // })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu']) ;
    }); 

  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }
 
  next(item){
    // this.router.navigate(['/feedback-fivesmiley']);
    this.router.navigate(['/select-language',{sid:item.id,flag:1}]);
  }
}

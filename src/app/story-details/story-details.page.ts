import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { PopoverController, Platform, AlertController, IonContent, ModalController, ToastController } from '@ionic/angular';
import { ViewImageComponent } from '../components/ImageViewer/view-image/view-image.component';
import { File } from '@ionic-native/file/ngx';
import { Subscription } from 'rxjs';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { TaggedConsumerInfoComponent } from '../components/tagged-consumer-info/tagged-consumer-info.component';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';
import { ReportAbuseComponent } from '../components/report-abuse/report-abuse.component';
import { ReportReasonComponent } from '../components/report-reason/report-reason.component';

@Component({
  selector: 'app-story-details',
  templateUrl: './story-details.page.html',
  styleUrls: ['./story-details.page.scss'],
})
export class StoryDetailsPage implements OnInit {
  @ViewChild('content', { static: false }) content: IonContent;
 
  @ViewChild('comment' , { static: false }) comment: any;
keyboardStyle = { width: '100%', height: '0px' };
id:any;
token:string;
user_id:any;
user_img:string;
story:any={};
creator:any={};
story_time:any;
commentText:string;
comments:any=[];
images:any=[];
videos:any=[];
category:any=[];
tagged:any=[];
date:number;
current:number;
com_time:number;
time:string;
name:string;
p_name:string;
doc:any=[];
status:any;
subscription:Subscription;
contentStyle ={top:'75px'};
key:boolean;
media:any=[];
start:any=0;
end:any=5;
flag:any;
act_img:any[]=[];
  constructor(private keyboard:Keyboard,private route:ActivatedRoute,public http:HttpClient,private storage:Storage,
    private config:HttpConfigService,public popoverController:PopoverController,private platform:Platform,
    private file:File,private router:Router,private toastCntlr:ToastController,
    public element:ElementRef,private alertController:AlertController,private iab:InAppBrowser,
    private dialog:SpinnerDialog,private modalCntrl:ModalController) {


    
   }

  ngOnInit() {

    if(this.platform.is('android')){
      this.contentStyle.top='75px'
      this.key=true;
    }else if(this.platform.is('ios')){
      this.contentStyle.top='95px'
      this.key=false;
    }

    console.log("con:",this.contentStyle);
    

  // work around for keyboard hiding input field
  

    this.keyboard.onKeyboardWillShow().subscribe( {
      next: x => {
        this.keyboardStyle.height = x.keyboardHeight + 'px';
        
        console.log("key:",this.keyboardStyle);
        
      },
      error: e => {
        console.log(e);
      }
    });
    this.keyboard.onKeyboardWillHide().subscribe( {
      next: x => {
        this.keyboardStyle.height = '0px';
        console.log("key:",this.keyboardStyle);
      },
      error: e => {
        console.log(e);
      }
    });

    // let textArea = this.element.nativeElement.getElementsByTagName('textarea')[0];
    // textArea.style.height = 'auto';
    // if (textArea.scrollHeight < 100) {
    // textArea.style.height = textArea.scrollHeight + "px";
    // textArea.style.overflowY = 'hidden';
    // } else {
    // textArea.style.height = "100px";
    // textArea.style.overflowY = 'auto';
    // }
    
  }
  async ionViewWillEnter(){
    this.current=(new Date()).getTime();
    this.comments=[];
    this.media=[];
    this.doc=[];

    // fetch post id through navigation
    this.id = this.route.snapshot.paramMap.get('post_id');
    this.flag= this.route.snapshot.paramMap.get('flag');
   console.log("date=",this.current);

    // this.storage.ready().then(()=>{
      
      const da=await this.storage.get('TOKEN');
        this.token=da;
      // });
      const dat=await this.storage.get('USER_ID');
        this.user_id=dat;
      // });
      const img=await this.storage.get('PRO_IMG');
        this.user_img=img;
      // });
      const data=await this.storage.get('NAME');
        this.name=data;
        this.p_name=this.name.substring(0,1);
    //   });
    // })
    this.getContent();
    this.loadComments();
    // this.updateView();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/stories']) ;
    }); 

  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }
// like the post
  
    async like(id,liked){
      console.log("like invoked");
      
      if(liked==0){
        this.status=1;
      }else{
        this.status=0;
      }
      console.log("likesta:",this.status);
      
      // this.storage.ready().then(()=>{
      
        
        
        
          const data=await this.storage.get('USER_ID');
            
            this.user_id=data;
            
       
      let story_id=id;
      let body={
        post_id: story_id,
        user_id: this.user_id,
        action_type: '1',
        status:this.status
      };
      console.log("body:",body);
      let url=this.config.domain_url+'update_post_action';
      let headers= await this.config.getHeader();
      this.http.post(url,body,{headers}).subscribe(data=>{
        console.log(data);
      if(liked==0){
        this.story.liked=1;
      }else{
        this.story.liked=0;
      }
        
      this.story.liked==1 ? this.story.like_count++ : this.story.like_count--;
      },error=>{
        console.log(error);
        
      })
  //   });
  // });
      
  }


// fetch story details
async getContent(){
  this.doc=[];
  // this.storage.ready().then(()=>{
      
    
    
    
    const data=await this.storage.get('USER_ID');
    
      this.user_id=data;
  let url=this.config.domain_url+'storydetails';
  let body={
    post_id:this.id,
    user_id:this.user_id

  }
  let headers= await this.config.getHeader();
  console.log(url,body);
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      this.story=res.story;
      this.creator=res.story.creator;
      this.images=res.story.images;
      this.videos=res.story.videos;
      this.category=res.story.category;
      this.status=res.story.liked;
      this.tagged=res.story.tagged_users;
      

      if(this.story.post_type==6){
        if(res.relatedActivityImage&&res.relatedActivityImage.length){
          this.act_img=res.relatedActivityImage;
          res.relatedActivityImage.forEach(element => {
            let item={img:element.activity_image,vid:0};
          this.media.push(item);
          });
        }


      }else{
      if(this.images.length==0){
        this.videos.forEach(element => {
          
          let item={img:element.thumbnail,vid:1};
          this.media.push(item);
        });
      }else if(this.videos.length==0){
        this.images.forEach(element => {
          let item={img:element.post_attachment,vid:0};
          this.media.push(item);
        });
      }else{
        this.images.forEach(element => {
          let item={img:element.post_attachment,vid:0};
          this.media.push(item);
        });
        this.videos.forEach(element => {
          
          let item={img:element.thumbnail,vid:1};
          this.media.push(item);
        });
      }

    }




      var mon = new Array();
      mon[0] = "Jan";
      mon[1] = "Feb";
      mon[2] = "Mar";
      mon[3] = "Apr";
      mon[4] = "May";
      mon[5] = "Jun";
      mon[6] = "Jul";
      mon[7] = "Aug";
      mon[8] = "Sep";
      mon[9] = "Oct";
      mon[10] = "Nov";
      mon[11] = "Dec";

      let t=new Date(res.story.activate_date.replace(' ','T'));
      let d=mon[t.getMonth()]+' '+t.getDate()+' '+t.getFullYear();

      var hours = t.getHours();
      let minutes = t.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
     var hour= hours < 10 ? '0'+hours:hours
      var minute = minutes < 10 ? '0'+minutes : minutes;
      this.story_time= d+', '+hour + ':' + minute + ' ' + ampm;
      console.log(this.story_time);
      
      res.story.attachments.forEach(el=>{
        let attch_name=el.post_attachment.substr(el.post_attachment.lastIndexOf('_') + 1);
        let ext=el.post_attachment.substr(el.post_attachment.lastIndexOf('.') + 1);
        this.doc.push({'title':attch_name,'post_attachment':el.post_attachment,'ext':ext});
        console.log("ext:",ext)
      })
      console.log("date_1:",this.doc);
      
      
    },error=>{
      console.log(error);
      
    })
//   })
// })
}

// fetch comments

async loadComments(){
  this.comments=[];
  let body={
    post_id: this.id
  }
  let url=this.config.domain_url+'comments';
  let headers= await this.config.getHeader();
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      
      // this.comments=res.comments;
      res.comments.forEach(ele=>{
        // let x=this.current.getTime();
        this.date=(new Date(ele.updated_at)).getTime();
        let t=this.current-this.date;
        let min=Math.floor(t/ (60*1000));
        let hr = Math.floor(t/ 3600 / 1000); // round to nearest second
        let days= Math.floor(t/ (60*60*24*1000));
        let month=Math.floor(t/ (60*60*24*1000*31));
        let yr=Math.floor(t/ (60*60*24*1000*31*12))
        // console.log("time:",this.com_time);
        if(month>12){
          this.time=yr+ ' yrs';
        }
        else if(days>31){
          this.time=month+ ' m';
        }
        else if(hr>24){
          
          this.time=days+ ' d';
        }else if(min>60){
          this.time=hr+' hrs ';
        }
        else if(min<=60 && min>1){
          this.time=min+' min';
        }
        else{
          this.time="Now";
        }
        console.log("t:",this.time);
        
        this.comments.push({'content':ele.content,'user':ele.user,'updated_at':ele.updated_at,'time':this.time,'id':ele.id});
        
      })
      
      console.log("res:",this.comments);
    },error=>{
      console.log(error);
      
    })
}

// post a comment

async postComment(id){
  console.log("com:",this.commentText)
  
  // this.storage.ready().then(()=>{
      
    
    
    
      const data=await this.storage.get('USER_ID');
      
        this.user_id=data;
        if(this.commentText==undefined|| this.commentText==""){
          console.log("cccc:",this.commentText);
          
        }else{
    
  let story_id=id;
    let body={
      post_id: story_id,
      user_id: this.user_id,
      action_type: '2',
      comment: this.commentText
    };
    console.log("body:",body);
    
    let url=this.config.domain_url+'create_post_action';
    let headers= await this.config.getHeader();
    this.http.post(url,body,{headers}).subscribe(data=>{
      console.log(data);
      this.story.comment_count++;
      this.loadComments();
      this.content.scrollToPoint(0, this.comment.nativeElement.offsetTop, 500);
      // document.getElementById('comment').scrollIntoView();
      // this.ionViewWillEnter();
    },error=>{
      console.log(error);
      
    });
    this.commentText="";
    
  }
//   });
// });

}


async showImages(ev: any) {

  const popover = await this.popoverController.create({
    component: ViewImageComponent,
    event: ev,
    componentProps:{
      data:this.images,
      video:this.videos,
      act_img:this.act_img
    },
    cssClass:'image_pop'
  });
  return await popover.present();
}


// open document
openDoc(item){


  // window.open(encodeURI(item.post_attachment),"_system","location=yes");


  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
  hideurlbar:'yes',
  zoom:'yes',
  beforeload:'yes',
      clearcache:'yes'
  }
  this.platform.ready().then(() => {
    if(item.post_attachment.includes('.pdf')){
      this.showpdf(item.post_attachment)
  }else{
  const browser = this.iab.create('https://docs.google.com/viewer?url='+item.post_attachment+'&embedded=true','_blank',options);
  this.dialog.show();
  browser.on('loadstart').subscribe(() => {
    console.log('start');
    this.dialog.hide();   
   
  }, err => {
    console.log(err);
    
    this.dialog.hide();
  })

  browser.on('loadstop').subscribe(()=>{
    console.log('stop');
    
    this.dialog.hide();;
  }, err =>{
    this.dialog.hide();
  })

  browser.on('loaderror').subscribe(()=>{
    this.dialog.hide();
  }, err =>{
    this.dialog.hide();
  })
  
  browser.on('exit').subscribe(()=>{
    this.dialog.hide();
  }, err =>{
    this.dialog.hide();
  })
}
  })

  // let path = null;
  //     if(this.platform.is('ios')){
  //       path=this.file.documentsDirectory;
  //     }
  //     else if(this.platform.is('android')){
  //       path=this.file.externalDataDirectory;
  //     }
  //       const fileTransfer: FileTransferObject=this.transfer.create();
  //       fileTransfer.download(item.post_attachment,path+'Attachment').then(entry=>{
  //         console.log("download");
          
  //         let url=entry.toURL();
  //         console.log("url",url);
          
  //         var fileExtension = item.post_attachment.substr(item.post_attachment.lastIndexOf('.') + 1);
  //         let fileMIMEType=this.getMIMEtype(fileExtension);
  //       // if(fileExtension=='pdf'){
  //       this.fileOpener.open(url,fileMIMEType);
  //       // }
  //       // else if(fileExtension=='doc'){
  //       //   this.fileOpener.open(url,'application/msword');
  //       // }
  //       });
}
// refresh the page
doRefresh(event) {
  this.ionViewWillEnter();

  setTimeout(() => {
    
    event.target.complete();
  }, 2000);
}




getMIMEtype(extn){
  let ext=extn.toLowerCase();
  let MIMETypes={
    'txt' :'text/plain',
    'docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'doc' : 'application/msword',
    'pdf' : 'application/pdf',
    'jpg' : 'image/jpeg',
    'bmp' : 'image/bmp',
    'png' : 'image/png',
    'xls' : 'application/vnd.ms-excel',
    'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'rtf' : 'application/rtf',
    'ppt' : 'application/vnd.ms-powerpoint',
    'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
  }
  return MIMETypes[ext];
}



async deletePop(id,idx){
  const alert = await this.alertController.create({
    // header: 'Log out',
    message: 'Do you want to delete this comment?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Yes',
        handler: () => {
          this.deleteComment(id,idx);
        }
      }
    ]
  });

  await alert.present();
}
async deleteComment(id,idx){

let url=this.config.domain_url+'comment/'+id;
    let headers= await this.config.getHeader();
    this.http.delete(url,{headers}).subscribe(data=>{
      console.log(data);
      this.presentAlert('Deleted successfully')
      this.comments.splice(idx,1);
      // this.loadComments();
      this.story.comment_count--;
    },error=>{
      console.log(error);
    });
   
}


viewMore(){
  this.end +=5;
}

async taggedCon() {
  const modal = await this.modalCntrl.create({
    component: TaggedConsumerInfoComponent,
    componentProps: {
      data:this.tagged
    }
  });
  
  return await modal.present();
}
async showpdf(file){
  const modal = await this.modalCntrl.create({
    component: ViewpdfComponent,
    cssClass:'fullWidthModal',
    componentProps: {
      
      data:file,
      
       
    },
    
    
  });
  return await modal.present();
}
async updateView(){
  const data=await this.storage.get('USER_ID');
            
  this.user_id=data;
  


let body={
post_id: this.id,
user_id: this.user_id,
action_type: '3',

};
console.log("body:",body);
let url=this.config.domain_url+'post_user_action';
let headers= await this.config.getHeader();
this.http.post(url,body,{headers}).subscribe(data=>{
console.log(data);



},error=>{
console.log(error);

})
}


async report(item,ev){
  const popover = await this.popoverController.create({
    component: ReportAbuseComponent,
    event:ev,
    componentProps:{
      type:2
    },
    backdropDismiss:true,
    cssClass:'message-options'
    
    
  });
  popover.onDidDismiss().then((data)=>{
   if(data.data){
    this.reason(item,data.data);
   }
  })
  return await popover.present();
}


async reason(item,type){
  const modal = await this.modalCntrl.create({
    component: ReportReasonComponent,
    cssClass:'report-reason-modal',
    backdropDismiss:true,
    
    
  });
  modal.onDidDismiss().then((data)=>{
    let reason;
    if(data.role=='1'){
      if(data.data){
     reason=data.data
    }else{
      reason='';
    }
      this.reportPost(item,reason,type);
  }
  })
  
  return await modal.present();
}
async reportPost(item,reason,type){

const uid=await this.storage.get('USER_ID');
const bid=await this.storage.get('BRANCH');
 const cid=await this.storage.get('COMPANY_ID');
      let url=this.config.domain_url+'report-issue';
      let headers= await this.config.getHeader()
      let body;
      body={
        user_id:uid,
        reason:reason
      }
      if(type==2){
        body.content_type='STORYCOMMENT';
        body.request_id=item.id
      }else{
        body.content_type='USER';
        body.request_id=item.user.user_id
      }
      console.log('report:',body)
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.loadComments();
        if(type==2){
          
        this.presentAlert('This comment has been reported. We shall verify and take appropriate action');
        }else{
          this.presentAlert('Request to block user has been submitted. We shall verify and take appropriate action');
        }
      },error=>{
        this.presentAlert('Something went wrong.Please try again later');
      })
}

async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}
goToActivity(){
  this.router.navigate(['/activity-details',{act_id:this.story.activity_id}]);
}
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StoryDetailsPage } from './story-details.page';

const routes: Routes = [
  {
    path: '',
    component: StoryDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StoryDetailsPageRoutingModule {}

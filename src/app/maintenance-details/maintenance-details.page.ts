import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import { Platform, PopoverController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { MultiFileUploadComponent } from '../components/multi-file-upload/multi-file-upload.component';

@Component({
  selector: 'app-maintenance-details',
  templateUrl: './maintenance-details.page.html',
  styleUrls: ['./maintenance-details.page.scss'],
})
export class MaintenanceDetailsPage implements OnInit {
details:any=[];
comment:any=[];
images:any=[];
technician:any=[];
type:any=[];
selected_type:any;
selected_tec:any;
risk:any;
hazard:any;
out_of_order:any;
priority:any;
request_id:any;
filter:any;
subscription:Subscription;
pick:boolean=false;
status:any;
start:any=0;
end:any=5;
// user_id:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private route:ActivatedRoute,
    private storage:Storage,private router:Router,private platform:Platform,private popoverController:PopoverController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.details=[];
    this.comment=[];
    this.images=[];
    this.technician=[];
    this.type=[];
    this.request_id=this.route.snapshot.paramMap.get('req_id');
    this.filter=this.route.snapshot.paramMap.get('filter');
    let url=this.config.domain_url+'get_request_details/'+this.request_id;
    let headers= await this.config.getHeader()
    
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log(res);
      this.details=res.data;
      this.images=res.images;
      this.comment=res.comment;
      this.technician=res.technician;
      this.type=res.type;
      this.details.forEach(element => {
        this.priority=element.priority.toString();
        this.risk=element.risk_rating.toString();
        this.hazard=element.hazard.toString();
        this.out_of_order=element.out_of_order.toString();
        if(element.type!=null){
        this.selected_type=element.type.id.toString();
        }
        if(element.technician!=null){
          this.selected_tec=element.technician.user_id.toString();
        }
        if(element.status==0 && element.technician==null){
          this.pick=true;
        }
        console.log("p:",this.priority)
      });
    },error=>{
      console.log(error);
      
    })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      if(this.filter==0){
        this.router.navigate(['/menu']);
      }   else{
      this.router.navigate(['/maintenance-list',{filter:this.filter}],{replaceUrl:true}) ;
      }
    }); 
  }
back(){
  if(this.filter==0){
    this.router.navigate(['/menu']);
  }   else{
  this.router.navigate(['/maintenance-list',{filter:this.filter}]) ;
  }
}

async changeType(typeId){
  console.log("type:",typeId);
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');
    let headers= await this.config.getHeader()
      let url=this.config.domain_url+'update_request_status';
      let body={
          id:this.request_id,
          from_id:data,
          status:status
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
  //   })
  // })
}

async changeStatus(status){
  console.log("status:",status);
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'update_request_status';
      let body={
          id:this.request_id,
          from_id:data,
          status:status
      }
      console.log("body:",body)
      this.http.post(url,body).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
  //   })
  // })
  
  
  
  
}
async changeTechnician(tech){
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');
  let url=this.config.domain_url+'assign_technician';
      let body={
          id:this.request_id,
          technician:tech,
          from_id:data
      }
      this.http.post(url,body).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
  //   })

  // })
}
changePriority(priority){
  console.log("priority:",priority);
  let url=this.config.domain_url+'update_request_priority';
      let body={
          id:this.request_id,
          priority:priority
      }
      this.http.post(url,body).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
  
}
ChangeRisk(risk){
  let url=this.config.domain_url+'update_risk_rating';
      let body={
          id:this.request_id,
          risk_rating:risk
      }
      this.http.post(url,body).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
}
changeHazard(hazard){
  let url=this.config.domain_url+'update_hazard';
      let body={
          id:this.request_id,
          hazard:hazard
      }
      this.http.post(url,body).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
}
ChangeOutofOrder(out_of_order){
  let url=this.config.domain_url+'update_out_of_order';
      let body={
          id:this.request_id,
          out_of_order:out_of_order
      }
      this.http.post(url,body).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
}


addComment(){
  this.router.navigate(['/maintenance-comment',{flag:1,req_id:this.request_id}])
}

async pickJob(){
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');

      let url=this.config.domain_url+'assign_technician';
      let body={
          id:this.request_id,
          technician:data.toString(),
          from_id:data
      }
      this.http.post(url,body).subscribe((res:any)=>{
        console.log(res);
        this.pick=false;
      },error=>{
        console.log(error);
        
      })

  //   })
  // })
}

async showImages(ev: any) {

  const popover = await this.popoverController.create({
    component: MultiFileUploadComponent,
    event: ev,
    componentProps:{
      data:this.images
    },
    cssClass:'image_pop'
  });
  return await popover.present();
}
viewMore(){
  this.end +=5;
}
}

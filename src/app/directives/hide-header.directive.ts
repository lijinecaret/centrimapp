import { Directive, OnInit, Input, Renderer2, HostListener, SimpleChanges, ElementRef } from '@angular/core';
import { DomController, IonContent } from '@ionic/angular';

@Directive({
  selector: '[appHideHeader]',
  host: {
    '(ionScroll)': 'onContentScroll($event)'
  }
})
export class HideHeaderDirective{
 
  @Input("header") header: HTMLElement;
  headerHeight;
  scrollContent;

  contentHeight: number;
  scrollHeight: number;
  lastScrollPosition: number;
  lastValue: number = 0;

  constructor(public element: ElementRef) {
  }
 

  ngOnInit(){
    this.headerHeight = this.header.clientHeight;
    // this.renderer.setElementStyle(this.header, 'webkitTransition', 'top 700ms');
    this.scrollContent = this.element.nativeElement.getElementsByClassName("scroll-content")[0];
    // this.renderer.setElementStyle(this.scrollContent, 'webkitTransition', 'margin-top 700ms');
  }

  onContentScroll(event){
    if(event.scrollTop > 56){
    //   this.renderer.setElementStyle(this.header, "top", "-56px")
    //   this.renderer.setElementStyle(this.scrollContent, "margin-top", "0px")
    // } else {
    //   this.renderer.setElementStyle(this.header, "top", "0px");
    //   this.renderer.setElementStyle(this.scrollContent, "margin-top", "56px")
    }
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
// import { GetObjectService } from '../services/get-object.service';
import { GetfeedobjectService } from '../services/getfeedobject.service';
@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {
  subscription:Subscription;
  survey:any=[];
  id:any;
  cid:any;
  flag:any;
  @Input() data;
  constructor(private router:Router,private storage:Storage,private config:HttpConfigService,private http:HttpClient,
    private platform:Platform,private orient:ScreenOrientation,private route:ActivatedRoute,private objService:GetfeedobjectService) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    // this.survey=this.objService.getExtras();
    this.flag=this.route.snapshot.paramMap.get("flag");
    console.log("survey:",this.survey);
   
    this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
    // if(this.flag==2){
      this.getContent();
  //  }else{
  //   this.survey=this.objService.getExtras();
  //  }
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu']) ;
    }); 

  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }
 
  next(item){
    // this.router.navigate(['/select-category',{sid:item.review_branch_id,flag:2}]);
    // this.router.navigate(['/select-language',{sid:item.review_branch_id,flag:2}]);
    this.router.navigate(['/feedback-type',{sid:item.review_branch_id,flag:2}])
    
  }



  async getContent(){
    // this.storage.ready().then(()=>{
      const dat=await this.storage.get('BRANCH');
        let id=dat.toString();
        // this.id=id.toString()
        const x=await this.storage.get('SURVEY_CID');
          let cid=x.toString();
          // this.cid=cid.toString()
          let url=this.config.feedback_url+'get_all_reviews';
          let body = `company_id=${cid}&branch_id=${id}`;
          //  body=({
          //   company_id:"14",
          //   user_id:"3"
          // })
          // body.set('company_id','14');
          // body.set('user_id','3');
          console.log("body:",body);
          
          let headers=new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          })
          this.http.post(url,body,{headers}).subscribe((data:any)=>{
            
            this.survey=data.data
            console.log(data);
            
          })
  
    //     })
    //   })
    // })
  }

}

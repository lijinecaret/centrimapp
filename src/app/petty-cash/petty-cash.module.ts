import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PettyCashPageRoutingModule } from './petty-cash-routing.module';

import { PettyCashPage } from './petty-cash.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PettyCashPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [PettyCashPage]
})
export class PettyCashPageModule {}

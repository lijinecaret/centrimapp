import { Component, OnInit } from '@angular/core';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { ModalController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { PettycashConsumersComponent } from '../components/pettycash-consumers/pettycash-consumers.component';

@Component({
  selector: 'app-petty-cash',
  templateUrl: './petty-cash.page.html',
  styleUrls: ['./petty-cash.page.scss'],
})
export class PettyCashPage implements OnInit {
details:any=[];
bal:any=[];
balance:number;
subscription:Subscription;
profile_view:boolean;
members:any=[];
petty:any={};
membername:any;
memberpic:any;
contentStyle={position: 'relative',top: '0px'};
res_branch:any;
flag:any;
cid:any;
  constructor(private config:HttpConfigService,private http:HttpClient,private storage:Storage,private router:Router,
    private platform:Platform,private modalCntrl:ModalController,private route:ActivatedRoute) { }

  ngOnInit() {
    if(this.platform.is('ios')){
      // this.contentStyle.position='relative';
      this.contentStyle.top='60px';
    }
    else if(this.platform.is('android')){
      this.contentStyle.top='0px';
    }
  }
  ionViewWillEnter(){
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.flag=this.route.snapshot.paramMap.get('flag');
    // this.getContent();
    this.getDetails();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/resident-overview',{flag:this.flag}]) ;
  });  
  
  }
  
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }
  async getContent(){
    this.details=[];
    this.bal=[];
    // this.storage.ready().then(()=>{
      const type=await this.storage.get('USER_TYPE');
        if(type==5){
         
          this.profile_view=false;
              const data=await this.storage.get('RESIDENT_ID');
                
                    
                let url=this.config.domain_url+'pettycash/'+data;
                let headers= await this.config.getHeader();
                  this.http.get(url,{headers}).subscribe((res:any)=>{
                    console.log(res);
                    // this.details=res.petty_cash;
                    
                    this.balance=0;
                    res.data.reverse().forEach(ele=>{
                      this.balance=this.balance+ele.amount;
                      this.petty={'petty_cash':ele,'bal':this.balance};
                      this.bal.push(this.petty)
                    })
                    console.log("bal:",this.bal);
                    this.details=this.bal.reverse();
                   
                    
                  console.log("res:",this.details,"bal:",this.bal);
                  
                    
                  },error=>{
                    console.log(error);
                    
                  })
            
              // })
            }else if(type==6){
        this.members=[];
              // this.getMembers();
      
              this.profile_view=true;
              // this.storage.ready().then(()=>{
              const id=await this.storage.get('USER_ID');
                const cid=await this.storage.get('COMPANY_ID');
                  const bid=await this.storage.get('BRANCH');
              let url=this.config.domain_url+'myfamily/'+id;
              let headers= await this.config.getHeader();
              
              this.http.get(url,{headers}).subscribe((res:any)=>{
                console.log("fam:",res)
                // res.data.forEach(el => {
                  res.data[0].family.forEach(ele => {
                    this.members.push({parent:ele.parent_details[0],share:ele.share_details});
                    

                    console.log("firstmem:",this.members)
                    let uid=res.data[0].family[0].parent_details[0].id;
                    this.res_branch=res.data[0].family[0].parent_details[0].wing.branch_id;
                    if(res.data[0].family[0].share_details==0){

                      console.log("no access");
                    }else{
                    let url=this.config.domain_url+'pettycash/'+uid;
                    
                    this.http.get(url,{headers}).subscribe((res:any)=>{
                    console.log(res,url);
                    // this.details=res.petty_cash;
                    this.balance=0;
                    res.data.reverse().forEach(ele=>{
                      this.balance=this.balance+ele.amount;
                      this.petty={'petty_cash':ele,'bal':this.balance};
                      this.bal.push(this.petty)
                    })
                    console.log("bal:",this.bal);
                    this.details=this.bal.reverse();
                   
                    
                  console.log("res:",this.details,"bal:",this.bal);
                    
                    },error=>{
                        console.log(error);
                        
                    })

                  }
                  })
                
                  this.memberpic=this.members[0].parent.user.profile_pic;
                    this.membername=this.members[0].parent.user.name;
                })
          //     })
          //   })
          //   })
          // })
        }
    //   })


    // })
    
  }



  async switchProfile(consumerId){
      this.details=[];
      this.bal=[];
    let url=this.config.domain_url+'pettycash/'+consumerId;
    let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log(res);
        // this.details=res.petty_cash;
        this.balance=0;
        res.data.reverse().forEach(ele=>{
          this.balance=this.balance+ele.amount;
          this.petty={'petty_cash':ele,'bal':this.balance};
          this.bal.push(this.petty)
        })
       console.log("bal:",this.bal);
       this.details=this.bal.reverse();
      console.log("res:",this.details);
      
        
      },error=>{
        console.log(error);
        
      })

  }


async showConsumer(){
  const modal = await this.modalCntrl.create({
    component: PettycashConsumersComponent,
   
    componentProps: {
      data:this.members
    }
    
  });
  modal.onDidDismiss().then((dataReturned) => {
    if(dataReturned.data!=undefined||dataReturned.data!=null){
      this.switchProfile(dataReturned.data.parent.id);
      this.membername=dataReturned.data.parent.user.name;
      this.memberpic=dataReturned.data.parent.user.profile_pic
    }
  })
  return await modal.present();
}

async getDetails(){
  let url=this.config.domain_url+'pettycash/'+this.cid;
                    let headers= await this.config.getHeader();
                    this.http.get(url,{headers}).subscribe((res:any)=>{
                    console.log(res,url);
                    // this.details=res.petty_cash;
                    this.balance=0;
                    res.data.reverse().forEach(ele=>{
                      this.balance=this.balance+ele.amount;
                      this.petty={'petty_cash':ele,'bal':this.balance};
                      this.bal.push(this.petty)
                    })
                    console.log("bal:",this.bal);
                    this.details=this.bal.reverse();
                   
                    
                  console.log("res:",this.details,"bal:",this.bal);
                    
                    },error=>{
                        console.log(error);
                        
                    })

}
  
}

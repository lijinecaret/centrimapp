import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckinHealthDeclarationPage } from './checkin-health-declaration.page';

const routes: Routes = [
  {
    path: '',
    component: CheckinHealthDeclarationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckinHealthDeclarationPageRoutingModule {}

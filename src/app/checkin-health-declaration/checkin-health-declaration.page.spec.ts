import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckinHealthDeclarationPage } from './checkin-health-declaration.page';

describe('CheckinHealthDeclarationPage', () => {
  let component: CheckinHealthDeclarationPage;
  let fixture: ComponentFixture<CheckinHealthDeclarationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinHealthDeclarationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckinHealthDeclarationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

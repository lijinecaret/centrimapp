import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckinHealthDeclarationPageRoutingModule } from './checkin-health-declaration-routing.module';

import { CheckinHealthDeclarationPage } from './checkin-health-declaration.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckinHealthDeclarationPageRoutingModule
  ],
  declarations: [CheckinHealthDeclarationPage]
})
export class CheckinHealthDeclarationPageModule {}

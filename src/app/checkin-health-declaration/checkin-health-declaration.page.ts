import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import momenttz from 'moment-timezone';
import moment from 'moment';
import { ModalController, Platform, ToastController } from '@ionic/angular';
import { TemperatureWarningComponent } from '../components/temperature-warning/temperature-warning.component';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-checkin-health-declaration',
  templateUrl: './checkin-health-declaration.page.html',
  styleUrls: ['./checkin-health-declaration.page.scss'],
})
export class CheckinHealthDeclarationPage implements OnInit {
  custom_fields:any[]=[];
  i_donot_have:any[]=[];
  doNotHaveStatus:any;
  notHaveArray:any[]=[];
  donotHave:any[]=[];
  more:any;
  temp:any;
  scale:any='0';
  purpose:any;
  res_id:any;
  decl:any[]=[];
  count:any;
  test:any;
  testStatus:any;
  testorNot:any;
  tempStatus:any;
  minDate=new Date().toISOString();
  testDate:any;
  testResult:any;
  qna:any[]=[];
  postvOrNot:any;
  backfrom:any;
  onPremises:any[]=[];
  anArray:any=[];
temp_array:any=[];
temp_copy_array={};
vacOrNot:any;
vacStatus:any;
vaccination:any;
vaccine:any;
vacArray:any[]=[];
vacDate:any;
expDate:any;

subscription:Subscription;
user:any;
from:any;
disable:boolean=false;
  public CollectedData: {QuestionId : number , OptionId : number,not_prefered:number}[];

  constructor(private router:Router,private route:ActivatedRoute,private http:HttpClient,
    private config:HttpConfigService,private storage:Storage,private toast:ToastController,
    private modalCntl:ModalController,private platform:Platform) { }

  ngOnInit() {
  }


  async ionViewWillEnter(){
    this.disable=false;
    this.i_donot_have=[];
    this.custom_fields=[];
    this.decl=[];
    this.anArray=[];
    this.CollectedData=[];
    this.temp_array=[];
    this.temp_copy_array={};
    this.temp='';
    this.testorNot='';
    this.more=JSON.parse(this.route.snapshot.paramMap.get('more'));
    this.res_id=this.route.snapshot.paramMap.get('res_id');
    this.purpose=this.route.snapshot.paramMap.get('purpose');
    this.count=this.route.snapshot.paramMap.get('count');
    this.backfrom=this.route.snapshot.paramMap.get('back');
    this.from=this.route.snapshot.paramMap.get('from');
    this.onPremises=JSON.parse(this.route.snapshot.paramMap.get('onPremises'));
    this.user=JSON.parse(this.route.snapshot.paramMap.get('user_info'));
    this.qna=[];
    if(this.backfrom==1){
    this.temp=this.route.snapshot.paramMap.get('temp');
    this.scale=this.route.snapshot.paramMap.get('scale');
    this.donotHave=this.route.snapshot.paramMap.get('notHave').split(/\, +/);
    this.notHaveArray=this.donotHave;
    this.CollectedData=JSON.parse(this.route.snapshot.paramMap.get('qna'));
    this.temp_array=JSON.parse(this.route.snapshot.paramMap.get('multi_temp'));
    this.testStatus=this.route.snapshot.paramMap.get('recent_test');
    this.testorNot=parseInt(this.route.snapshot.paramMap.get('recent_test'));
    this.testDate=this.route.snapshot.paramMap.get('testDate');
    this.testResult=this.route.snapshot.paramMap.get('result');
    this.postvOrNot=parseInt(this.route.snapshot.paramMap.get('result'));
    this.vacStatus=this.route.snapshot.paramMap.get('vacStatus');
    this.vacDate=this.route.snapshot.paramMap.get('vacDate');
    this.expDate=this.route.snapshot.paramMap.get('expDate');
    this.vaccine=this.route.snapshot.paramMap.get('vaccine').toString();
    this.vacOrNot=parseInt(this.route.snapshot.paramMap.get('vacStatus'));
    console.log("no:",this.CollectedData,this.donotHave,this.notHaveArray,this.postvOrNot);
    if(this.CollectedData.length>0){
      this.CollectedData.forEach(element => {
        this.qna.push(element.QuestionId,element.OptionId);
      });
    }
    

    let arr=this.temp_array.slice(1);
    arr.forEach((element,index) => {
      
        let value=Object.values(element);
        for(let val of value){
      this.anArray.push({'value':val});
        }
      console.log("val:",value);
      
    });
  }
    if(this.backfrom==1){

    }else{
    for(let i=0;i<this.count;i++){
      this.anArray.push({'value':''});
    }
  }
    // this.storage.ready().then(()=>{
      const test=await this.storage.get('VIS-RECENT-TEST-DETAILS');
        this.test=test;
      // })
      
      const temp=await this.storage.get('VIS-TEMPARATURE');
        this.tempStatus=temp;
      // })
      const vac=await this.storage.get('VIS-VACCINATION');
        this.vaccination=vac;
        if(this.vaccination==1){
          this.getVaccineList();
        }
      // })
    
     
          let url=this.config.domain_url+'visitor_settings';
          let headers=await this.config.getHeader();
          this.http.get(url,{headers}).subscribe((res:any)=>{
            console.log(res);
            this.doNotHaveStatus=res.data.i_donot_have;
            if(this.doNotHaveStatus==1){
             this.i_donot_have= res.data.i_donot_have_items.split(/\, +/);
            }
              this.custom_fields=res.data.custom_field;
              console.log("i_donot:",this.qna,this.qna[this.qna.indexOf(48)+1]);
              
          })
        // })
    //   })
    // })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      // this.router.navigate(['/checkin-scan-qr']) ;
      this.back() ;
   
    }); 
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }


  notHave(ev,item){
    console.log("selectNotHave:",this.notHaveArray,item)
    if(ev.currentTarget.checked==true){
      if(this.notHaveArray.includes(item)){
      }else{
        this.notHaveArray.push(item);
      }
    }else{
      if(this.notHaveArray.includes(item)){
        this.notHaveArray.splice(this.notHaveArray.indexOf(item));
      }
    }
    console.log("selected:",this.notHaveArray)
  }



  


  async next(){
    
    console.log("test:",this.notHaveArray,this.i_donot_have,this.testResult);
    
    
    // this.storage.ready().then(()=>{
      // const temp=await this.storage.get('VIS-TEMPARATURE');
      //   this.tempStatus=temp;
      //   this.storage.get('VIS-VACCINATION').then(vac=>{
          const test=await this.storage.get('VIS-RECENT-TEST-DETAILS');
   if(this.CollectedData.length<this.custom_fields.length){
      this.presentToast('Please answer all questions.');
    }else if(this.doNotHaveStatus==1&&(this.notHaveArray.length!=this.i_donot_have.length)){
      this.presentToast('Please check all the boxes to ensure you do not have any symptoms.')
    }else if(this.CollectedData.some(x=>x.not_prefered==1)){
      this.warning();
      
      // this.router.navigate(['/health-warning',{name:this.name,phone:this.phone,address:this.address,img:this.img,more:JSON.stringify(this.more),res:this.res,res_name:this.res_name,vpType:this.vpType,tovisit:this.tovisit,temp:this.temp,scale:this.scale,notHave:nothave,qna:JSON.stringify(this.CollectedData),purpose:this.purpose,flag:this.flag,from:this.from,todec:this.todec,count:this.count}]);
    }else if(test==1&&(this.testStatus==undefined)){
      console.log("testornot:",this.testorNot);
      
      this.presentToast('Please provide your COVID-19 test details.');
    }else if(this.testStatus==1&&(this.testDate==undefined||this.testResult==undefined)){
      console.log("terminated 1");
      
      this.presentToast('Please provide your COVID-19 test details.');
    // }else if(vac==1&&this.vacStatus==undefined){
    //   console.log("terminated 2");
    //   this.presentToast('Please provide your vaccination details.');
    // }else if(vac==1&&this.vacStatus==1&&(this.vaccine==undefined||this.vacDate==undefined)){
    //   console.log("terminated 3");
    //   this.presentToast('Please provide your vaccination details.');
    // }else if(vac==1&&this.vacStatus==1&&this.vaccine==1&&this.expDate==undefined){
    //   this.presentToast('Please provide your vaccination details.');
    // }else if(this.tempStatus==1&&(this.temp==undefined || this.temp=='')){
    //   console.log("terminated 4");
    //         this.presentToast('Please enter your body temperature.');
            
    // }else if(this.tempStatus==1&&parseInt(this.temp)>38){
    //   console.log("higher temp");
      
    //         this.warning();
           
            
    }else if(this.testStatus==1&&this.testResult==1){
      console.log("covid +ve");
      
              this.warning();
        
    // }
        

    // else if(this.tempStatus==1&&parseInt(this.count)>0){
    //   console.log("i am here");
      
    //       if(this.anArray.some(el=>parseInt(el.value)>38)){
    //         console.log("visit temp high");
            
    //         this.warning();
          
    //       }
    //       else if(this.anArray.some(el=>(el.value)=='')){
    //         console.log("emptyArray");
            
    //         this.presentToast('Please enter body temperature of all the visitors.');
          
    //     }else if(this.anArray.some(el=>(el.value)==null)){
    //       console.log("emptyArray");
          
    //       this.presentToast('Please enter body temperature of all the visitors.');
        
    //   }else{
    //     console.log("next1");
    //       this.gotoNext();
    //     }

    //     if(parseInt(this.count)>0){
    //       console.log("count>0");
          
    //       if(this.anArray.some(el=>(el.value)=='')){
    //         console.log("emptyArray");
            
    //         this.presentToast('Please enter body temperature of all the visitors.');
    //       }
    //     // }else{
    //     //   this.gotoNext();
    //   }else{
    
    //     console.log('gotonext2')
    //     this.gotoNext();
    //   }
    
    }else{
      console.log('gotonext3')
      this.disable=true;
      this.gotoNext();
    }
  
// })
//       })
// })
// })
  }


  mark(event,id,prefered){
    console.log("ev:",event);
    let not_prefered;
    if((prefered==1&&event.detail.value==1)||(prefered==0&&event.detail.value==0)||prefered==2){
      not_prefered=0
    }else{
      not_prefered=1
    }
    if(this.CollectedData.length==0){
     
    this.CollectedData.push({QuestionId:id,OptionId:event.detail.value,not_prefered:not_prefered});
    
  }else{
    const targetIdx = this.CollectedData.map(item => item.QuestionId).indexOf(id);
    
    
    
    if(targetIdx>=0)
      {
        this.CollectedData[targetIdx].OptionId =event.detail.value;
        this.CollectedData[targetIdx].not_prefered=not_prefered;
        
      }
    else{
      this.CollectedData.push({QuestionId:id,OptionId:event.detail.value,not_prefered:not_prefered});
      
    }
    console.log("marked:",this.CollectedData);
    
  }
  }


  covidTest(ev){
    console.log("change:",ev);
    // console.log("click:",ev);
    
    
      if(ev.detail.value=='1'){
        this.testStatus=1
      }else{
        this.testStatus=0;
        this.testResult=0;
      }
      console.log("status:",this.testStatus)
  }

  covidTestResult(ev){
    if(ev.detail.value=='1'){
      this.testResult=1
    }else{
      this.testResult=0;
    }
}
// async warning(){
//   const modal = await this.modalCntl.create({
//     component: WarningComponent,
//     cssClass:'warningModal',
//     componentProps: {
//      name:this.name,
    
      
//     },
//     id:'warning'
//   });
 
//   return await modal.present();
// }


getItem(id){
  const i=this.CollectedData.map(i=>i.QuestionId).indexOf(id);
  return this.qna[i].OptionId;
}


async confirm(){


  // this.storage.ready().then(()=>{
    const cid=await this.storage.get('COMPANY_ID');
      const bid=await this.storage.get('BRANCH');
        const tz=await this.storage.get('TIMEZONE');
          const name=await this.storage.get('NAME');
            const ph=await this.storage.get('PHONE');
            const img=await this.storage.get('PRO_IMG');
        let additional=[];
        let phone_array=[];
        let temp_array=[];
        // let count;
        if(this.user){
        if(!this.user.additional_info){
          additional=null;
          phone_array=null;
          temp_array=null;
        }else{
          temp_array=this.user.temp_array;
          this.user.additional_info.forEach(element => {
            additional.push(element.name);
            let phone = element.phone.substring(1);
          
            phone_array.push('+61'+phone)
          
          });
        }
      }
          // count=0;
        
        if(this.count=='null'||this.count=='undefined'){
          this.count=null
        }
        let qna=[];
        if(this.CollectedData.length>0){
          this.CollectedData.forEach(element => {
            qna.push(element.QuestionId.toString(),element.OptionId.toString());
          });
        }
        // if(this.img=='undefined'){
        //   this.img=null
        // }
        // if(this.res=='null'){
        //   this.res=null
        // }if(this.address=='null'){
        //   this.address=null;
        // }
        // if(this.purpose=='null'){
        //   this.purpose=null;
        // }
        let test_date;
        if(this.testDate==undefined){
         test_date=null
        }else{
          test_date=moment(this.testDate).format('yyyy-MM-DD');
        }
        let vacDate;
        if(this.vacDate==undefined){
          vacDate=moment(this.vacDate).format('yyyy-MM-DD');

        }else{
          vacDate=null
        }
        // let expDate;
        // if(this.expDate==undefined){
        //   expDate=moment(this.expDate).format('yyyy-MM-DD')
        // }else{
        //   expDate=null
        // }
        // if(this.vaccine==undefined){
        //   this.vaccine=null
        // }
        // let vaccinated=0;
        // let covid_vaccinated=0;
        // if(this.vacStatus==0){
        //   // expDate=null;
        //   vacDate=null;
        //   this.vaccine=null;
        //   vaccinated=null;
        //   covid_vaccinated=null
        // }
        let phone=null;
        if(ph){
        let phn=ph.toString();
        if(phn.charAt( 0 )==='+'){
         phone = phn.substring(1);
        }else{
          phone=phn
        }
      }

        let nothave;
   
        if(this.notHaveArray.length>0){
          nothave=this.notHaveArray[0];
          this.notHaveArray.forEach(ele=>{
            if(nothave==ele){
    
            }else{
              nothave=nothave+', '+ele
            }
          })
        }

        // let temp;
        // let tempArray={};
        // if(parseInt(this.count)>0){
        //   temp=null;
        //   tempArray=this.temp_copy_array;
        // }else{
        //   temp=this.temp;
        //   tempArray=null;
        // }

        let vac_status,vac_date,vac,vac_proof,inf_vaccine_status,temp,
          inf_vaccine_date,inf_vaccine_proof,firstDose,secondDose,vaccinated,covidVaccinated,uploaded_date=null;
        let vac_status_array=[];
        let vac_date_array=[];
        let vac_array=[];
        let proof_array=[]
        let vac_dose_array=[];
        let first_array=[];
        let second_array=[];
        
        let already_have=[];
        
        let inf_vaccine_status_array=[];
        let inf_vaccine_date_array=[];
        let inf_vaccine_proof_array=[];
        
    
    
  if(this.user){
        if(this.user.vaccine){
          vac_status=this.user.vaccine.vac_status;
          vac_date=this.user.vaccine.vac_date;
          vac_proof=this.user.vaccine.proof;
          vac=this.user.vaccine.id;
          firstDose=this.user.vaccine.firstDose;
          secondDose=this.user.vaccine.secondDose;
          inf_vaccine_status=this.user.vaccine.inf_vaccine_status
          inf_vaccine_date=this.user.vaccine.inf_vaccine_date;
          inf_vaccine_proof=this.user.vaccine.inf_vaccine_proof;
          vaccinated=this.user.vaccine.vaccinated;
          covidVaccinated=this.user.vaccine.covidVaccinated;
          uploaded_date=this.user.vaccine.uploaded_date;
        }
        if(this.user.additional&&this.user.additional.length){
          this.user.additional.forEach(element => {
            vac_array.push(element.id);
            // vac_date_array.push(element.vac_date);
            vac_dose_array.push(element.dose);
            proof_array.push(element.proof);
            vac_status_array.push(element.vac_status)
          });
        }else{
          vac_status_array=null;
          // vac_date_array=null;
          vac_array=null;
          proof_array=null
          vac_dose_array=null;
        }
        
        if(this.user.firstDoseArray){      
          if(this.user.firstDoseArray.length>0){
            first_array=this.user.firstDoseArray
          }else{
            first_array=null
          }
        }else{
          first_array=null
        }
          if(this.user.secondDoseArray){
          if(this.user.secondDoseArray.length>0){
            second_array=this.user.secondDoseArray
          }else{
            second_array=null
          }
        }else{
          second_array=null
        }
         
        if(this.user.vaccine_date_array){
          if(this.user.vaccine_date_array.length>0){
            vac_date_array=this.user.vaccine_date_array
          }else{
            vac_date_array=null
          }
        }else{
          vac_date_array=null
        }
        //   if(this.user.vaccine_phone){
        //   if(this.user.vaccine_phone.length>0){
        //     phone_array=this.user.vaccine_phone
        //   }else{
        //     phone_array=null
        //   }
        // }else{
        //   phone_array=null
        // }
        if(this.user.inf_vaccine_status_array){
          if(this.user.inf_vaccine_status_array.length>0){
            inf_vaccine_status_array=this.user.inf_vaccine_status_array
          }else{
            inf_vaccine_status_array=null
          }
        }else{
          inf_vaccine_status_array=null
        }
        if(this.user.inf_vaccine_date_array){
          if(this.user.inf_vaccine_date_array.length>0){
            inf_vaccine_date_array=this.user.inf_vaccine_date_array
          }else{
            inf_vaccine_date_array=null
          }
        }else{
          inf_vaccine_date_array=null
        }
        
        if(this.user.inf_vaccine_proof_array){
          if(this.user.inf_vaccine_proof_array.length>0){
            inf_vaccine_proof_array=this.user.inf_vaccine_proof_array
          }else{
            inf_vaccine_proof_array=null
          }
        }else{
          inf_vaccine_proof_array=null
        }
        
        if(this.user.already_have){
          if(this.user.already_have.length>0){
            already_have=this.user.already_have
          }else{
            already_have=null
          }
        }else{
          already_have=null
        }
      }else{
        vac_status_array=null
        vac_date_array=null
         vac_array=null
         proof_array=null
         vac_dose_array=null
         first_array=null
         second_array=null
        
         already_have=null
        
         inf_vaccine_status_array=null
         inf_vaccine_date_array=null
         inf_vaccine_proof_array=null
      }
        
        let url=this.config.domain_url+'create_visitor_list';
        let headers=await this.config.getHeader();
        let body={
          visitor_type:'0',
          checkin_checkout:'0',
          name:name,
          phone:'+'+phone,
          address:null,
          additional_visitor_count:this.count,
          additional_visitor:additional,
          checkin_time:momenttz().tz(tz).format('yyyy-MM-DD HH:mm:ss'),
          checkout_time:null,
          i_donot_have:nothave,
          whom_to_visit:'0',
          resident_user_id:this.res_id,
          companian:null,
          purpose:null,
          body_temparature:temp,
          
          temp_measure:this.scale,
          photo:img,
          signature:null,
          qna:qna,
          recent_test:this.testStatus,
          test_date:test_date,
          test_result:this.testResult,
          via_app:1,
            visitor_id:null,
            vaccinated:vaccinated,
            covid_vaccinated:covidVaccinated,
            device_id:3,
            body_temparature_array:temp_array,
            
            vaccine_status:vac_status,
            vaccine_date:vac_date,
            vaccine :vac,
            expiry_date:null,
            proof:vac_proof,
            uploaded_by:null,
            uploaded_date:uploaded_date,
            // vaccine_status_array:vac_status_array,
            vaccine_array:vac_array,
            vaccine_date_array:vac_date_array,
            // vaccine_dose_array:vac_dose_array,
            proof_array:proof_array,
            vaccine_phone_array:phone_array,
            firstdose_date_array:first_array,
            seconddose_date_array:second_array,
            vaccine_already_have:already_have,
            firstdose_date:firstDose,
            seconddose_date:secondDose,
            inf_vaccine_status:inf_vaccine_status,
            inf_vaccine_date:inf_vaccine_date,
            inf_vaccine_proof:inf_vaccine_proof,
            inf_vaccine_status_array:inf_vaccine_status_array,
            inf_vaccine_date_array:inf_vaccine_date_array,
            inf_vaccine_proof_array:inf_vaccine_proof_array

          
        }
        console.log("body:",body);
        
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
           console.log(res);
           this.disable=false;
           this.router.navigate(['/confirm-checkin',{ref_id:res.data.reference_id,additional:this.count}])
        },error=>{
          this.disable=false;
        })
  //       })
  //     })
  //   })
  // })
  //   })
  // })


  
  // this.router.navigate(['/digital-badge',{name:this.name,ref_id:this.ref_id,phone:this.phone,img:this.img,res:this.res,checkin:this.checkin,vpType:this.vpType}])
}

async gotoNext(){
  console.log("next function");
  // this.storage.ready().then(()=>{
    const terms=await this.storage.get('VIS-TERMS');
      const name=await this.storage.get('NAME');
  if(this.backfrom==1){
    this.temp_copy_array={};
    this.temp_array.forEach((element,index )=> {
      Object.entries(element).map(([key,value])=>{
        
        this.temp_copy_array[key]=value;
      })
    })
  }else{
    if(parseInt(this.count)>0&&this.more==null){
      
      console.log("more:",this.more);
      
      this.temp_array.push({'person1':this.temp});
      this.temp_copy_array['person1']=this.temp;
      this.anArray.forEach((element,index) => {
        let key='person'+(index+2);
        this.temp_array.push({[key]:element.value});
        this.temp_copy_array[key]=element.value;
      });
      console.log("tempArray:",this.temp_copy_array);
      
    }else if(this.more!==null){
      this.temp_array.push({[name]:this.temp});
      this.temp_copy_array[name]=this.temp;
      this.more.forEach((element,index) => {
        this.temp_array.push({[element.value]:this.anArray[index].value});
        this.temp_copy_array[element.value]=this.anArray[index].value;
      });
      console.log("tempArray:",this.temp_copy_array);
    }
  }
  

      if(terms==0){
        console.log("confirm");
        this.confirm();
      }else{
        let nothave;
   
    if(this.notHaveArray.length>0){
      nothave=this.notHaveArray[0];
      this.notHaveArray.forEach(ele=>{
        if(nothave==ele){

        }else{
          nothave=nothave+', '+ele
        }
      })
    }   
    this.disable=false;
    this.router.navigate(['/checkin-vis-guidelines',{more:JSON.stringify(this.more),res_id:this.res_id,purpose:this.purpose,count:this.count,temp:this.temp,scale:this.scale,notHave:nothave,qna:JSON.stringify(this.CollectedData),testDate:this.testDate,recent_test:this.testStatus,result:this.testResult,flag:2,onPremises:JSON.stringify(this.onPremises),multi_temp:JSON.stringify(this.temp_array),vacStatus:this.vacStatus,vacDate:this.vacDate,expDate:this.expDate,vaccine:this.vaccine,user_info:JSON.stringify(this.user),from:this.from}])
      }
      
//     })
//   })
// })
}

async presentToast(mes){
  const alert = await this.toast.create({
    message: mes,
    duration: 3000,
    cssClass:'toast-mess',
    position:'top'      
  });
  alert.present();
}
async warning(){
  const modal = await this.modalCntl.create({
    component: TemperatureWarningComponent,
    cssClass:'warningModal',
    
    id:'warning'
  });
 
  return await modal.present();
}
back(){
  if(this.count==0){
    this.temp='';
  this.testorNot='';
    this.router.navigate(['/checkin-additional-visitors',{res_id:this.res_id,purpose:this.purpose,onPremises:JSON.stringify(this.onPremises),from:this.from,user_info: JSON.stringify(this.user)}])
  }else{
    this.router.navigate(['/checkin-add-additional-visitors',{res_id:this.res_id,purpose:this.purpose,onPremises:JSON.stringify(this.onPremises),more:JSON.stringify(this.more),from:this.from,user_info: JSON.stringify(this.user)}]);
  }
  
}
vaccinated(ev){
  console.log("change:",ev);
  // console.log("click:",ev);
  
  
    if(ev.detail.value=='1'){
      this.vacStatus=1
    }else{
      this.vacStatus=0;
      // this.testResult=0;
    }
    console.log("status:",this.testStatus)
}
selectVaccine(vac){
    console.log("selectedVac:",this.vaccine);
    // this.vaccine=vac
    
}
async getVaccineList(){
  let headers=await this.config.getHeader();
  let url=this.config.domain_url+'get_vaccine';
  this.http.get(url,{headers}).subscribe((res:any)=>{
    console.log('vac:',res);
    this.vacArray=res.data;
  })
}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckinAdditionalInfoPageRoutingModule } from './checkin-additional-info-routing.module';

import { CheckinAdditionalInfoPage } from './checkin-additional-info.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckinAdditionalInfoPageRoutingModule
  ],
  declarations: [CheckinAdditionalInfoPage]
})
export class CheckinAdditionalInfoPageModule {}

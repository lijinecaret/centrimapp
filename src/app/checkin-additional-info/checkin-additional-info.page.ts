import { HttpClient ,HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import momenttz from 'moment-timezone';
@Component({
  selector: 'app-checkin-additional-info',
  templateUrl: './checkin-additional-info.page.html',
  styleUrls: ['./checkin-additional-info.page.scss'],
})
export class CheckinAdditionalInfoPage implements OnInit {
  anArray:any=[];
  subscription:Subscription;
  user:any;
  phone:any;
  name:any;
  vaccine:any;
  count_num:any=1;
  address:any;
  branch:any;
  addressStatus:any;
  res_id: any;
  purpose: any;
  onPremises: any[] = [];
  count:any;
  from:any;
  disable:boolean=false;
  constructor(private router:Router,private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private platform:Platform,private route:ActivatedRoute,private toast:ToastController) { }

  ngOnInit() {
  }

ionViewWillEnter(){
  this.disable=false;
  this.res_id = this.route.snapshot.paramMap.get('res_id');
  this.purpose = this.route.snapshot.paramMap.get('purpose');
  this.onPremises = JSON.parse(this.route.snapshot.paramMap.get('onPremises'));
  this.user = JSON.parse(this.route.snapshot.paramMap.get('user_info'));
  this.count=this.route.snapshot.paramMap.get('count');
  this.from=this.route.snapshot.paramMap.get('from');
  // this.anArray=[];
  //   this.anArray.push({'value':''});
    this.user=JSON.parse(this.route.snapshot.paramMap.get('user_info'));
    if(!this.user){
      this.user={}
    }
    if(!this.user.additional_info){
    this.user.additional_info=[];
    }
    if(this.user.additional_info.length){
    this.count_num=this.user.additional_info.length+1;
    }
    console.log('add:',this.user.additional_info)
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        
      this.back();
    
      }); 
      
  }
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }
  async getDetails(){
    this.disable=true;
    let phone;
  if(this.name==undefined || this.name==''){
    this.presentToast('Please enter your name');
    this.disable=false;
  }else if(this.phone==undefined || this.phone==''){
    this.presentToast('Please enter your phone number');
    this.disable=false;
  }else if(this.phone.toString().length!=10){
    this.presentToast('Please enter a 10 digit phone number');
    this.disable=false;
  }else if(this.phone.charAt( 0 )!=='0'){
    this.presentToast('Phone number should starts with 0');
    this.disable=false;
  }else{
   
      phone = this.phone.substring(1);
   
    // this.storage.ready().then(()=>{
    
        const bid=await this.storage.get('BRANCH');
          // const bid = await this.storage.get('BRANCH');
          let url=this.config.domain_url+'validate_checkin_details';
          
          // if(this.phone.charAt( 0 )==='0'){
          //   phone = this.phone.substring(1);
          // }else{
          //   this.presentToast('Phone number should starts with 0');
          //   // phone=this.phone
          // }
          let headers=await this.config.getHeader();
          let body={
            mobile:'+61'+phone,
            branch_id:bid,
            name:this.name
          }
          // this.storage.ready().then(()=>{
          const data=await this.storage.get('VIS-ADDRESS-REQUIRED');
         this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log("repvis:",res);
            if(res.status=='success'){
              // this.storage.get("VIS-ADDRESS-REQUIRED").then(data=>{
                
                if(data==1){
                  if(this.address==undefined||this.address==''){
                    this.presentToast('Please enter address');
                    this.disable=false;
                  }else{
                    
                    this.gotoNext();
                   
                  }
                }else{
                  
                  this.gotoNext();
                  
                }
              // })
            }else{
              this.presentToast('Someone already checked in using this phone number.');
              this.disable=false;
            }
          })
        // })
          // })
        // })
   
    // })
    // this.router.navigate(['/vis-address',{name:this.name,phone:this.phone}]);
  }
 
  }

  async presentToast(mes){
    const alert = await this.toast.create({
      message: mes,
      duration: 3000,
      cssClass:'toast-mess',
      position:'top'       
    });
    alert.present();
  }
  async gotoNext(){
console.log('user:',this.user,'\ncount:',this.count);

    // let phone = this.phone.substring(1);
    let address;
        if(this.address==undefined||this.address==''){
          address=null
        }else{
          address=this.address
        }
    this.user.additional_info.push({name:this.name,phone:this.phone,address:address});
    // const temp = await this.storage.get('VIS-TEMPARATURE');
       
    // const vac = await this.storage.get('VIS-VACCINATION');this.storage.ready().then(() => {
    const temp=await this.storage.get('VIS-TEMPARATURE');
      
      const vac=await this.storage.get('VIS-VACCINATION');
    if(temp==1||vac==1){
      this.disable=false;
    this.router.navigate(['/checkin-additional-vaccine-info',{ res_id: this.res_id, purpose: this.purpose, count: this.count, onPremises: JSON.stringify(this.onPremises),user_info:JSON.stringify(this.user),from:this.from}])
    }else{
      if(this.user.additional_info.length!==parseInt(this.count)){
        this.disable=false;
        this.router.navigate(['/checkin-additional-info',{ res_id: this.res_id, purpose: this.purpose, count: this.count, onPremises: JSON.stringify(this.onPremises),user_info: JSON.stringify(this.user),from:this.from}],{replaceUrl:true})
      }else{
        // this.storage.ready().then(()=>{
            const decl=await this.storage.get('VIS-HEALTH-DECLARATION');
              const donot=await this.storage.get('VIS-DONOTHAVE');
              console.log("decl:",decl);
      
              const terms=await this.storage.get('VIS-TERMS');
              if(decl==1||donot==1){
                this.disable=false;
                this.router.navigate(['/checkin-health-declaration',{res_id:this.res_id,purpose:this.purpose,count:this.count,back:0,onPremises:JSON.stringify(this.onPremises),user_info: JSON.stringify(this.user),from:this.from}]);
              }else if(terms==1){
                this.disable=false;
                  this.router.navigate(['/checkin-vis-guidelines',{res_id:this.res_id,purpose:this.purpose,count:this.count,flag:1,onPremises:JSON.stringify(this.onPremises),user_info: JSON.stringify(this.user),from:this.from}])
      
              }else{

                // this.storage.ready().then(()=>{
                  const cid=await this.storage.get('COMPANY_ID');
                    const bid=await this.storage.get('BRANCH');
                      const tz=await this.storage.get('TIMEZONE');
                        const name=await this.storage.get('NAME');
                          const ph=await this.storage.get('PHONE');
                          const img=await this.storage.get('PRO_IMG');
                      let additional=[];
                      let phone_array=[];
                      let temp_array=[];
                      // let count;
                      if(this.user){
                      if(!this.user.additional_info){
                        additional=null;
                        phone_array=null;
                        temp_array=null;
                      }else{
                        temp_array=this.user.temp_array;
                        this.user.additional_info.forEach(element => {
                          additional.push(element.name);
                          let phone = element.phone.substring(1);
                        
                          phone_array.push('+61'+phone)
                        
                        });
                      }
                    }
                        // count=0;
                      
                      if(this.count=='null'||this.count=='undefined'){
                        this.count=null
                      }
                      
                      // if(this.img=='undefined'){
                      //   this.img=null
                      // }
                      // if(this.res=='null'){
                      //   this.res=null
                      // }if(this.address=='null'){
                      //   this.address=null;
                      // }
                      // if(this.purpose=='null'){
                      //   this.purpose=null;
                      // }
                      
                      // let expDate;
                      // if(this.expDate==undefined){
                      //   expDate=moment(this.expDate).format('yyyy-MM-DD')
                      // }else{
                      //   expDate=null
                      // }
                      // if(this.vaccine==undefined){
                      //   this.vaccine=null
                      // }
                      // let vaccinated=0;
                      // let covid_vaccinated=0;
                      // if(this.vacStatus==0){
                      //   // expDate=null;
                      //   vacDate=null;
                      //   this.vaccine=null;
                      //   vaccinated=null;
                      //   covid_vaccinated=null
                      // }
                      let phone=null;
                      if(ph){
                      let phn=ph.toString();
                      if(phn.charAt( 0 )==='+'){
                       phone = phn.substring(1);
                      }else{
                        phone=phn
                      }
                    }
              
                      
              
                      // let temp;
                      // let tempArray={};
                      // if(parseInt(this.count)>0){
                      //   temp=null;
                      //   tempArray=this.temp_copy_array;
                      // }else{
                      //   temp=this.temp;
                      //   tempArray=null;
                      // }
              
                      let vac_status,vac_date,vac,vac_proof,inf_vaccine_status,temp,
                        inf_vaccine_date,inf_vaccine_proof,firstDose,secondDose,vaccinated,covidVaccinated,uploaded_date=null;
                      let vac_status_array=[];
                      let vac_date_array=[];
                      let vac_array=[];
                      let proof_array=[]
                      let vac_dose_array=[];
                      let first_array=[];
                      let second_array=[];
                      
                      let already_have=[];
                      
                      let inf_vaccine_status_array=[];
                      let inf_vaccine_date_array=[];
                      let inf_vaccine_proof_array=[];
                      
                  
                  
                if(this.user){
                  temp=this.user.temp
                      if(this.user.vaccine){
                        vac_status=this.user.vaccine.vac_status;
                        vac_date=this.user.vaccine.vac_date;
                        vac_proof=this.user.vaccine.proof;
                        vac=this.user.vaccine.id;
                        firstDose=this.user.vaccine.firstDose;
                        secondDose=this.user.vaccine.secondDose;
                        inf_vaccine_status=this.user.vaccine.inf_vaccine_status
                        inf_vaccine_date=this.user.vaccine.inf_vaccine_date;
                        inf_vaccine_proof=this.user.vaccine.inf_vaccine_proof;
                        vaccinated=this.user.vaccine.vaccinated;
                        covidVaccinated=this.user.vaccine.covidVaccinated;
                        uploaded_date=this.user.vaccine.uploaded_date;
                      }
                      if(this.user.additional&&this.user.additional.length){
                        this.user.additional.forEach(element => {
                          vac_array.push(element.id);
                          // vac_date_array.push(element.vac_date);
                          vac_dose_array.push(element.dose);
                          proof_array.push(element.proof);
                          vac_status_array.push(element.vac_status)
                        });
                      }else{
                        vac_status_array=null;
                        // vac_date_array=null;
                        vac_array=null;
                        proof_array=null
                        vac_dose_array=null;
                      }
                      
                      if(this.user.firstDoseArray){      
                        if(this.user.firstDoseArray.length>0){
                          first_array=this.user.firstDoseArray
                        }else{
                          first_array=null
                        }
                      }else{
                        first_array=null
                      }
                        if(this.user.secondDoseArray){
                        if(this.user.secondDoseArray.length>0){
                          second_array=this.user.secondDoseArray
                        }else{
                          second_array=null
                        }
                      }else{
                        second_array=null
                      }
                       
                      if(this.user.vaccine_date_array){
                        if(this.user.vaccine_date_array.length>0){
                          vac_date_array=this.user.vaccine_date_array
                        }else{
                          vac_date_array=null
                        }
                      }else{
                        vac_date_array=null
                      }
                      //   if(this.user.vaccine_phone){
                      //   if(this.user.vaccine_phone.length>0){
                      //     phone_array=this.user.vaccine_phone
                      //   }else{
                      //     phone_array=null
                      //   }
                      // }else{
                      //   phone_array=null
                      // }
                      if(this.user.inf_vaccine_status_array){
                        if(this.user.inf_vaccine_status_array.length>0){
                          inf_vaccine_status_array=this.user.inf_vaccine_status_array
                        }else{
                          inf_vaccine_status_array=null
                        }
                      }else{
                        inf_vaccine_status_array=null
                      }
                      if(this.user.inf_vaccine_date_array){
                        if(this.user.inf_vaccine_date_array.length>0){
                          inf_vaccine_date_array=this.user.inf_vaccine_date_array
                        }else{
                          inf_vaccine_date_array=null
                        }
                      }else{
                        inf_vaccine_date_array=null
                      }
                      
                      if(this.user.inf_vaccine_proof_array){
                        if(this.user.inf_vaccine_proof_array.length>0){
                          inf_vaccine_proof_array=this.user.inf_vaccine_proof_array
                        }else{
                          inf_vaccine_proof_array=null
                        }
                      }else{
                        inf_vaccine_proof_array=null
                      }
                      
                      if(this.user.already_have){
                        if(this.user.already_have.length>0){
                          already_have=this.user.already_have
                        }else{
                          already_have=null
                        }
                      }else{
                        already_have=null
                      }
                    }else{
                      vac_status_array=null
                      vac_date_array=null
                       vac_array=null
                       proof_array=null
                       vac_dose_array=null
                       first_array=null
                       second_array=null
                      
                       already_have=null
                      
                       inf_vaccine_status_array=null
                       inf_vaccine_date_array=null
                       inf_vaccine_proof_array=null
                    }
                      
                      let url=this.config.domain_url+'create_visitor_list';
                      let headers=await this.config.getHeader();
                      let body={
                        visitor_type:'0',
                        checkin_checkout:'0',
                        name:name,
                        phone:'+'+phone,
                        address:null,
                        additional_visitor_count:this.count,
                        additional_visitor:additional,
                        checkin_time:momenttz().tz(tz).format('yyyy-MM-DD HH:mm:ss'),
                        checkout_time:null,
                        i_donot_have:null,
                        whom_to_visit:'0',
                        resident_user_id:this.res_id,
                        companian:null,
                        purpose:null,
                        body_temparature:temp,
                        
                        temp_measure:0,
                        photo:img,
                        signature:null,
                        qna:null,
                        recent_test:null,
                        test_date:null,
                        test_result:null,
                        via_app:1,
                          visitor_id:null,
                          vaccinated:vaccinated,
                          covid_vaccinated:covidVaccinated,
                          device_id:3,
                          body_temparature_array:temp_array,
                          
                          vaccine_status:vac_status,
                          vaccine_date:vac_date,
                          vaccine :vac,
                          expiry_date:null,
                          proof:vac_proof,
                          uploaded_by:null,
                          uploaded_date:uploaded_date,
                          // vaccine_status_array:vac_status_array,
                          vaccine_array:vac_array,
                          vaccine_date_array:vac_date_array,
                          // vaccine_dose_array:vac_dose_array,
                          proof_array:proof_array,
                          vaccine_phone_array:phone_array,
                          firstdose_date_array:first_array,
                          seconddose_date_array:second_array,
                          vaccine_already_have:already_have,
                          firstdose_date:firstDose,
                          seconddose_date:secondDose,
                          inf_vaccine_status:inf_vaccine_status,
                          inf_vaccine_date:inf_vaccine_date,
                          inf_vaccine_proof:inf_vaccine_proof,
                          inf_vaccine_status_array:inf_vaccine_status_array,
                          inf_vaccine_date_array:inf_vaccine_date_array,
                          inf_vaccine_proof_array:inf_vaccine_proof_array
              
                        
                      }
                      console.log("body:",body);
                      
                      this.http.post(url,body,{headers}).subscribe((res:any)=>{
                         console.log(res);
                         this.disable=false;
                         this.router.navigate(['/confirm-checkin',{ref_id:res.data.reference_id,additional:this.count}])
                      },error=>{
                        this.disable=false;
                      })
                //       })
                //     })
                //   })
                // })
                //   })
                // })
              }
        //     })
        //   })
        // })
        // })
      }
    }
  // })
// })
  }

  back(){
    this.router.navigate(['/checkin-add-additional-visitors', {res_id: this.res_id, purpose: this.purpose, onPremises: JSON.stringify(this.onPremises), user_info: JSON.stringify(this.user),from:this.from,count:this.count }])
  }


}

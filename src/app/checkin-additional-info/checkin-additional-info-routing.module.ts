import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckinAdditionalInfoPage } from './checkin-additional-info.page';

const routes: Routes = [
  {
    path: '',
    component: CheckinAdditionalInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckinAdditionalInfoPageRoutingModule {}

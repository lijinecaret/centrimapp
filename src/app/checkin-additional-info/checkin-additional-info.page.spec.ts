import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckinAdditionalInfoPage } from './checkin-additional-info.page';

describe('CheckinAdditionalInfoPage', () => {
  let component: CheckinAdditionalInfoPage;
  let fixture: ComponentFixture<CheckinAdditionalInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinAdditionalInfoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckinAdditionalInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

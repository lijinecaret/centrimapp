import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OnboardingP1Page } from './onboarding-p1.page';

describe('OnboardingP1Page', () => {
  let component: OnboardingP1Page;
  let fixture: ComponentFixture<OnboardingP1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnboardingP1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OnboardingP1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

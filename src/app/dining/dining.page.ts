import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { ModalController, Platform } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Storage } from '@ionic/storage-angular';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';
@Component({
  selector: 'app-dining',
  templateUrl: './dining.page.html',
  styleUrls: ['./dining.page.scss'],
})
export class DiningPage implements OnInit {
menu:any=[];
current:any=[];
subscription:Subscription;
trustedVideoUrl: SafeResourceUrl;
  constructor(private domSanitizer: DomSanitizer,private http:HttpClient,private config:HttpConfigService,private platform:Platform,
    private file:File,private router:Router,private modalCntrl:ModalController,
    private iab:InAppBrowser,private storage:Storage,private dialog:SpinnerDialog) { }

  ngOnInit() {
    
  }

ionViewWillEnter(){
  this.getContent();
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/menu']) ;
  }); 

}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}
async getContent(){
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('TOKEN');
      let token=data;
      const bid=await this.storage.get('BRANCH');
        let branch=bid.toString();
  
  let url=this.config.domain_url+'diningmenu';
  let headers= await this.config.getHeader();
  console.log("head:",this.config.head);
  this.http.get(url,{headers}).subscribe((data:any)=>{
    this.menu=data.allmenus;
    this.current=data.current_menu;
    console.log("menu:",data);
    
  })
// })
//     })
//   })
}
openDoc(item){
//  window.open(encodeURI(item.file),"_system","location=yes");
  // let path = null;
  //   if(this.platform.is('ios')){
  //     path=this.file.documentsDirectory;
  //   }
  //   else if(this.platform.is('android')){
  //     path=this.file.externalDataDirectory;
  //   }
  //   const options: DocumentViewerOptions = {
  //     title: item.id
  //   }
  //   // this.viewer.viewDocument(item.file, 'application/pdf', options)
   
  //     const fileTransfer: FileTransferObject=this.transfer.create();
  //     fileTransfer.download(item.file,path+item.menu).then(entry=>{
  //       console.log("download");
        
  //       let url=entry.toURL();
  //       this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(item.file);
  //       console.log("url",url);
  //       var fileExtension = item.file.substr(item.file.lastIndexOf('.') + 1);
  //       if(fileExtension=='pdf'){
  //       this.fileOpener.open(this.trustedVideoUrl,'application/pdf');
  //       }
  //       else if(fileExtension=='doc'){
  //         this.fileOpener.open(item.file,'application/msword');
  //       }else if(fileExtension=='jpg' || fileExtension=="jpeg"){
  //         this.fileOpener.open(url,'image/jpeg');
  //       }
        
  //       console.log("ext:",fileExtension);
        
  //     },error=>{
  //       console.log(error);
        
  //     });
      // this.router.navigate(['/feedback-yesorno',{url:item.file}]);

      let options:InAppBrowserOptions ={
        location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'yes',
      beforeload:'yes',
      clearcache:'yes'
      }
      this.platform.ready().then(() => {
        if(item.file.includes('.pdf')){
          this.showpdf(item.file)
      }else{
      const browser = this.iab.create('https://docs.google.com/gview?embedded=true&url='+item.file,'_blank',options);
      this.dialog.show();
      browser.on('loadstart').subscribe(() => {
        console.log('start');
        this.dialog.hide();   
       
      }, err => {
        console.log(err);
        
        this.dialog.hide();
      })
    
      browser.on('loadstop').subscribe(()=>{
        console.log('stop');
        
        this.dialog.hide();;
      }, err =>{
        this.dialog.hide();
      })
    
      browser.on('loaderror').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
      
      browser.on('exit').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
    }
    })
    }
    async showpdf(file){
      const modal = await this.modalCntrl.create({
        component: ViewpdfComponent,
        cssClass:'fullWidthModal',
        componentProps: {
          
          data:file,
          
           
        },
        
        
      });
      return await modal.present();
    }
  }
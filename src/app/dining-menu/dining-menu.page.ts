import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-dining-menu',
  templateUrl: './dining-menu.page.html',
  styleUrls: ['./dining-menu.page.scss'],
})
export class DiningMenuPage implements OnInit {
  subscription:Subscription;
  currentMenu:any[]=[];
  upcoming:any[]=[];
  access:any=1;
    constructor(private router:Router,private platform:Platform,private http:HttpClient,private storage:Storage,
      private config:HttpConfigService,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.currentMenu=[];
    this.upcoming=[];
    this.showLoading();
   
    const tz=await this.storage.get('TIMEZONE');
    this.getUserPermission();
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'get_menu_list';
    let body={
      date:moment().tz(tz).format('YYYY-MM-DD')
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      
      let cm=res.data.current_menu;
      if(cm&&cm.length){
        cm.forEach(ele=>{
          if(ele.status==2){
            this.currentMenu.push(ele)
          }
        })
      }
      
      let um=res.data.other_menu;
      if(um&&um.length){
        um.forEach(ele=>{
          if(ele.status==2){
            this.upcoming.push(ele)
          }
        })
      }
      this.dismissLoader();
    },error=>{
      this.dismissLoader();
    });
    
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
   this.back();
   }); 
   
   }
   ionViewWillLeave() { 
   this.subscription.unsubscribe();
   }
   back(){
    this.router.navigate(['/menu']);
   }
   placeOrder(item){
    // this.router.navigate(['/dining-take-order',{menu:JSON.stringify(item)}],{replaceUrl:true})
    this.router.navigate(['din-choose-season-date',{menu:JSON.stringify(item),flag:1}],{replaceUrl:true})
  }
  viewMenu(item){
      // this.router.navigate(['/dining-view-menu',{menu:JSON.stringify(item)}],{replaceUrl:true})
    this.router.navigate(['din-choose-season-date',{menu:JSON.stringify(item),flag:2}],{replaceUrl:true})

  }
   async showLoading() {
      
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      
      // message: 'Please wait...',
      spinner: null,
      // duration: 3000,
    });
    return await loading.present();
  }
  
  async dismissLoader() {
    
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  async getUserPermission(){
   
    const data= await this.storage.get("USER_ID");
     
      const type=await this.storage.get('USER_TYPE');
      const consumer= await this.storage.get('RESIDENT_ID');
      let url=this.config.domain_url+'profile/'+data;
          
            
            
            console.log(url);
            let headers=await this.config.getHeader();
             this.http.get(url,{headers}).subscribe((res:any)=>{
              console.log('per:',res);
              if(type==5){
                if(res.data.dining_access==0){
                  this.access=0
                }else{
                  this.access=1
                }
              }else{
                if(res.data.family&&res.data.family.length){
                  const idx=res.data.family.map(x=>x.resident_user_id).indexOf(consumer);
                  if(idx>=0){
                    this.access=res.data.family[idx].dining_access
                  }
                  console.log('selectedRes:',consumer,'\nindex:',idx,'\narrayitem:',res.data.family[idx].resident_user_id,'\naccess:',this.access);
                  
                }
              }
             })
              
         
          
  }
}

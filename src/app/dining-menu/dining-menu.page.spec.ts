import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningMenuPage } from './dining-menu.page';

describe('DiningMenuPage', () => {
  let component: DiningMenuPage;
  let fixture: ComponentFixture<DiningMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningMenuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

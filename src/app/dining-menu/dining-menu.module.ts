import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningMenuPageRoutingModule } from './dining-menu-routing.module';

import { DiningMenuPage } from './dining-menu.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningMenuPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DiningMenuPage]
})
export class DiningMenuPageModule {}

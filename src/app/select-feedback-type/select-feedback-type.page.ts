import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { ActionSheetController, IonContent, LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { ThankyouComponent } from '../components/thankyou/thankyou.component';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment-timezone';
// import { ImagePicker } from '@ionic-native/image-picker/ngx';
@Component({
  selector: 'app-select-feedback-type',
  templateUrl: './select-feedback-type.page.html',
  styleUrls: ['./select-feedback-type.page.scss'],
})
export class SelectFeedbackTypePage implements OnInit {
  subscription:Subscription;
  category:any=[];
  type:any;
  imageResponse:any=[];
img:any[]=[];
feedback:any;
cat_id:any;
cat:HTMLElement;
  feed:HTMLElement;
  show=4;
  contacts:any[]=[];
  con_id:any;
  utype:any;
  @ViewChild(IonContent, { static: false }) content: IonContent;
  constructor(private router:Router,private orient:ScreenOrientation,private platform:Platform,
    private config:HttpConfigService,private http:HttpClient,private loadingCtrl:LoadingController,
    private modalCntlr:ModalController,private actionsheetCntlr:ActionSheetController,
    private camera:Camera,private storage:Storage,private toast:ToastController,public elRef: ElementRef) { }

  ngOnInit() {
    // this.imagePicker.requestReadPermission();
  }
async ionViewWillEnter(){
  this.contacts=[];
  // this.storage.ready().then(()=>{
    const utype=await this.storage.get('USER_TYPE');
      this.utype=utype;
  //   })
  // })
  this.getFamily();
  // this.orient.lock(this.orient.ORIENTATIONS.LANDSCAPE);

      this.cat=this.elRef.nativeElement.querySelector('#cat');
      this.feed=this.elRef.nativeElement.querySelector('#feed');
      this.imageResponse=[];
      this.img=[];
      this.type;
      this.cat_id;
      this.feedback;
    
      console.log(this.type,this.cat_id,this.feedback);
      
      let url=this.config.domain_url+'feedback_categories'
    // let url='http://52.65.155.193/centrim_api/api/feedback_categories';
    this.http.get(url).subscribe((res:any)=>{
      this.category=res.data
    })
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
        
          this.router.navigate(['/menu']) ;
       
      });       
}
next(id){
  this.type=id;
  this.show=1;
  // this.content.scrollToPoint(0, this.cat.offsetTop, 1000);
  // this.router.navigate(['/select-category',{type:id}])
}
async openModal() {
  const modal = await this.modalCntlr.create({
    component: ThankyouComponent,
    componentProps: {
      "flag":2,
      data:this.type
    }
  });
  return await modal.present();
}

gotoFeedback(cat){
  this.cat_id=cat.id;
  this.show=2
  // this.content.scrollToPoint(0, this.feed.offsetTop, 1000);
}

addImage(){
  let options;
    options={
      maximumImagesCount: 5,
      outputType: 1
    }
    if(this.platform.is('ios')){
      options.disable_popover=true
    }
    // this.imagePicker.getPictures(options).then((results) => {
    //   for (var i = 0; i < results.length; i++) {
    //       // console.log('Image URI: ' + results[i]);
    //       if((results[i]==='O')||results[i]==='K'){
    //         console.log("no img");
            
    //         }else{
    //           // this.img.push(results[i]);
    //        let im='data:image/jpeg;base64,'+results[i]
    //        this.imageResponse.push(im);
    //        this.img.push(results[i]);
    //         }
          
           
    //   }
    // }, (err) => { });
  
  console.log("imagg:",this.imageResponse);
}


async selectImage() {
  const actionSheet = await this.actionsheetCntlr.create({
    header: "Select Image source",
    buttons: [{
      text: 'Load from Library',
      handler: () => {
        this.addImage();
      }
    },
    {
      text: 'Use Camera',
      handler: () => {
        this.pickImage();
      }
    },
    {
      text: 'Cancel',
      role: 'cancel'
    }
    ]
  });
  await actionSheet.present();
}


pickImage(){
  const options: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.CAMERA,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }
  this.camera.getPicture(options).then((imageData) => {
    // imageData is either a base64 encoded string or a file URI
    // If it's base64 (DATA_URL):
    let base64Image = 'data:image/jpeg;base64,' + imageData;
    
    this.imageResponse.push(base64Image);
    this.img.push(imageData);
     
  }, (err) => {
    // Handle error
  });
}

ionViewDidLeave(){
  this.type='';
  this.cat_id='';
  this.feedback='';
  this.show=4;
}

removeImg(i){
  this.imageResponse.splice(i,1);
  this.img.splice(i,1);
}


async submit(){
  if(this.feedback==undefined || this.feedback==''){
    
    this.presentAlert('Please enter your feedback');
  }else if(this.type==undefined || this.type==''){
    
    this.presentAlert('Please select a feedback type');
  }else if(this.cat_id==undefined || this.cat_id==''){
    
    this.presentAlert('Please select a category');
  }else{
  // this.storage.ready().then(()=>{
         let res_id;
         if(this.con_id==undefined||this.con_id==''){
           res_id=null
         }else{
           res_id=this.con_id
         }
   const uid=await this.storage.get('USER_ID');
     const cid=await this.storage.get('COMPANY_ID');
       const bid=await this.storage.get('BRANCH');
         const tz=await this.storage.get('TIMEZONE');
      let url=this.config.domain_url+'SaveFeedbackResponse';
      // let url='http://52.65.155.193/centrim_api/api/SaveFeedbackResponse';
      let headers=new HttpHeaders({'branch_id':bid.toString()})
      let body;
      let date=new Date();
      if(this.img.length==0){
        body={
          company_id:cid,
          user_id:uid,
          type:this.type,
          category_id:this.cat_id,
          feedback:this.feedback,
          via_app:1,
          images:null,
          created_time:moment().tz(tz).format('YYYY-MM-DD HH:mm:ss'),
          resident_id:res_id
        }
      }else{
        body={
          company_id:cid,
          user_id:uid,
          type:this.type,
          category_id:this.cat_id,
          feedback:this.feedback,
          via_app:1,
          images:this.img,
          created_time:moment().tz(tz).format('YYYY-MM-DD HH:mm:ss'),
          resident_id:res_id
        }
      }
      // var s="";
      // for (var i=0;i< this.imageResponse.length;i++)
      // {
      //   s+=`&images[]=${this.imageResponse[i]}`;
      // }
      // let body = `company_id=${cid}&user_id=${uid}&type=${this.type}&category_id=${this.cat_id}&feedback=${this.feedback}&via_app=1`+s;
   
      console.log("body:",body,tz);
      this.showLoading()
    
      // let headers=new HttpHeaders({
      //   'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      // })

      this.http.post(url,body,{headers}).subscribe((data:any)=>{
       
        console.log(data);

        if(data.status=="success"){
          this.dismissLoader();
        this.openModal();
        }else{
          this.dismissLoader();
          this.presentAlert('Something went wrong.Please try again.');
        }

      },error=>{
        console.log(error);
        
      })
//     })
//   })
//   })
// })
// })
}
}
async presentAlert(message) {
  const alert = await this.toast.create({
    message: message,
    cssClass:'toastStyle',
    duration: 3000      
  });
  alert.present(); //update
}



async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}
async dismissLoader() {
  return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}
previous(){
  if(this.show==1){
    this.show=0
  }else if(this.show==2){
    this.show=1
  }else if(this.show==0){
    this.show=4
  }
}


async getFamily(){
  
  // this.storage.ready().then(()=>{
    const uid=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'showfamily/'+uid;
      let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log("parent:",res);
        
        res.data[0].family.forEach(element => {
            this.contacts.push(element.parent_details[0])
          });
       
          console.log("arr:",this.contacts);
          if(this.contacts.length<=1){
            this.show=0;
          }
      })

      
  //   })
  // })
  
  
}
selectConsumer(id,item){
  // if(item.share_details==0){
  //   this.presentAlert("you don't access to this profile right now.")
  // }else{
  this.con_id=id;
  this.show=0;
  // }
}
}

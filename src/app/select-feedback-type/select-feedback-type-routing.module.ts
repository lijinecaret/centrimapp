import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectFeedbackTypePage } from './select-feedback-type.page';

const routes: Routes = [
  {
    path: '',
    component: SelectFeedbackTypePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectFeedbackTypePageRoutingModule {}

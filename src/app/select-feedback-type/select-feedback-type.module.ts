import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectFeedbackTypePageRoutingModule } from './select-feedback-type-routing.module';

import { SelectFeedbackTypePage } from './select-feedback-type.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectFeedbackTypePageRoutingModule
  ],
  declarations: [SelectFeedbackTypePage]
})
export class SelectFeedbackTypePageModule {}

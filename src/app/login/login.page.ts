import { Component, HostListener, OnInit } from '@angular/core';
import { ToastController, Platform, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

import { Storage } from '@ionic/storage-angular';
import { HttpClient } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Subscription } from 'rxjs';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  keyboardStyle = { width: '100%', height: '0px' };
  device:any;
  email:string;
  password:string;
  subscription:Subscription;
  agree:any=1;
  isLoading:boolean=true;
  hide_pswd:boolean=true;
  passwordType:any='password';
  constructor(private router:Router,private auth: AuthService,private config:HttpConfigService,
    private toastCntlr:ToastController,private storage:Storage,private http:HttpClient,
    private platform:Platform,private keyboard:Keyboard,private loadingCtrl:LoadingController,
    private iab:InAppBrowser) { 
    // hide tab bar
   

  }
  ionViewWillEnter(){
    
   
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      
       
      navigator['app'].exitApp(); 
    
      
    }); 
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
  ngOnInit() {
    this.keyboard.onKeyboardWillShow().subscribe( {
      next: x => {
        this.keyboardStyle.height = x.keyboardHeight + 'px';
      },
      error: e => {
        console.log(e);
      }
    });
    this.keyboard.onKeyboardWillHide().subscribe( {
      next: x => {
        this.keyboardStyle.height = '0px';
      },
      error: e => {
        console.log(e);
      }
    });
   
  }
 

  

  



    // login function

    // login() {
    // // jwt authentication using auth service
    //   this.auth.login(this.email,this.password).subscribe(async res => {
    //     // if (res) {
    //       console.log("res:",res.data.user_id);
    //       this.storage.ready().then(()=>{
    //         this.storage.get("uuid").then(data=>{
    //           console.log("uuid",data);
    //           let url=this.config.domain_url+'devicetoken';
    //           if(this.platform.is('ios')){
    //             this.device=2
    //           }
    //           else if(this.platform.is('android')){
    //             this.device=1
    //           }
              
    //           let body={
    //             user_id:res.data.user_id,
    //             device_type:this.device,
    //             device_token:data,
    //             app_version:1
    //           }
    //           let headers= await this.config.getHeader();
    //           console.log("body:",body);
    //           this.http.post(url,body,{headers}).subscribe((res:any)=>{
    //             console.log("device:",res);
                
    //           },error=>{
    //             console.log(error);
                
    //           })
              
    //         })
    //       })
    //       // this.showLoading();
    //       this.router.navigate(['/stories',{user_id:res.data.user_id,branch_id:res.data.user_details.branch_id,flag:1}]);
    //       // this.dismissLoader();
    //     },error=> {
    //       this.presentAlert();
    //       console.log("err",error);
          
    //   });

    // //   console.log("body:",body);
    // }
    async presentAlert(mes) {
      const alert = await this.toastCntlr.create({
        message: mes,
        cssClass:'toastStyle',
        duration: 3000,
        position:'top'       
      });
      alert.present(); //update
    }
    async showLoading() {
      const loading = await this.loadingCtrl.create({
       
        message: 'Please wait...',
        spinner: 'circles',
        duration: 3000 
      });
      return await loading.present();
  }
  
    async dismissLoader() {
        return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
    }

    login(){
      if(this.agree==0){
        this.presentAlert('Please agree to terms & conditions.')
      }else{
      let body={
        email:this.email,
        password:this.password,
        nw_id:0
      }
      this.keyboard.hide();
      this.http.post(this.config.login_path,body).subscribe((res:any)=>{
        
             if((res.data.user_type_id==5) ||(res.data.user_type_id==6)){
              
              

              this.storage.set("BRANCH",res.data.user_details.branch_id);
              this.storage.set("PCFBRANCH",res.data.user_details.user_branch.pcs_branch_id);
              this.storage.set('RVSETTINGS',res.data.user_details.user_branch.retirement_living);
              this.storage.set("BRANCH_COPY",res.data.user_details.branch_id);
        this.storage.set("TOKEN", res.token);
       
             this.storage.set("EXPIRES_IN", res.expires);
             this.storage.set("SURVEY_CID", res.company_id);
             this.storage.set("USER_ID", res.data.user_id);
             this.storage.set("USER_TYPE",res.data.user_type_id);
             this.storage.set("NAME", res.data.name);
             this.storage.set("USER_NAME", res.data.user_name);
             this.storage.set("EMAIL", res.data.email);
             this.storage.set("PRO_IMG",res.data.profile_pic);
             this.storage.set("PHONE",res.data.phone);
             this.storage.set("COMPANY_ID",res.data.nw_id);
             this.storage.set("COMPANY_NAME",res.data.company.c_name);
             this.storage.set("BRANCH_NAME", res.data.branchname);
             if(res.data.user_type_id!==6){
             this.storage.set("RESIDENT_ID", res.data.resident_id);
             this.storage.set("RESIDENT_NAME",res.data.name)
             this.storage.set("RESIDENT_IMG",res.data.profile_pic);
             this.storage.set('RES-RV-SETTING',res.data.user_details.user_branch.retirement_living);
             }
             this.storage.set("TIMEZONE", res.data.user_details.user_branch.timezone);
           
             this.storage.set('firstLogin',res.data.first_login);
             this.storage.set('SUBDOMAIN',res.data.company.subdomain)
            
                  if(res.data.first_login==0&&res.data.has_dp==0){
                      let img;
                      if(res.data.profile_pic){
                        img=res.data.profile_pic
                      }else{
                        img='https://app.centrim.life/assets/img/user/user.png'
                      }
                      let email;
                      if(res.data.no_email==1){
                        email=null
                      }else{
                        email=res.data.email
                      }
                      // let phone;
                      // if(res.data.mobile){
                      //   phone=res.data.mobile
                      // }else{
                      //   phone=res.data.phone
                      // }
                 
                      let info={email:email,name:res.data.name,uname:res.data.user_name,img:img,phone:res.data.mobile,mobile:res.data.phone}
                      this.router.navigate(['/onboarding-welcome',{info:JSON.stringify(info)}],{replaceUrl:true})
                      if(res.data.user_type_id==6)
                        this.storage.set('CHECKIN',0);
                     
                      
                  }
                  else{
                    this.storage.set('USERTOKEN',res.user_token);
                    this.storage.set("loggedIn",1);
                    this.router.navigate(['/menu'],{replaceUrl:true});
                      let update_url=this.config.domain_url+'update_login_info';
      let update_body={
        user_id:res.data.user_id,
        branch_id:res.data.user_details.branch_id,
        
      }
      this.http.post(update_url,update_body).subscribe((res:any)=>{
       
        
      })
                  }
             
            }
            // this.showLoading();
            else{
              console.log("else worked");
              
              this.presentAlert('Please enter the valid credentials.');
            
            }
           
           },error=> {
            
             this.presentAlert('Please enter the valid credentials.');
            
             
         });
        }
    }
    terms(i){
      // if(status.currentTarget.checked==true)
      if(this.isLoading==false){
      if(i.currentTarget.checked==true){
        this.agree=1;
      }else{
        this.agree=0;
      }
    }
  }
  @HostListener('touchstart')
onTouchStart() {
  console.log('pageload:',this.isLoading)
  this.isLoading=false;
}
openTerms(){
  
    let options:InAppBrowserOptions ={
      location:'yes',
    hideurlbar:'yes',
    zoom:'no',
    hidenavigationbuttons:'yes'
    }
    // const browser = this.iab.create('https://app.centrim.life/terms-and-conditions','_blank',options);
    const browser=this.iab.create('https://app.centrim.life/legal','_blank',options)
  
}
showPassword(i){
  // if(this.password){
  this.hide_pswd=!this.hide_pswd;
  if(i==2){
    this.passwordType='text';
  }else{
    this.passwordType='password'
  }
// }
}
}

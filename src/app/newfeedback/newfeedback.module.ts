import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewfeedbackPageRoutingModule } from './newfeedback-routing.module';

import { NewfeedbackPage } from './newfeedback.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewfeedbackPageRoutingModule
  ],
  declarations: [NewfeedbackPage]
})
export class NewfeedbackPageModule {}

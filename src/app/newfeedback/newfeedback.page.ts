import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera, CameraResultType } from '@capacitor/camera';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { ActionSheetController, LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { ThankyouComponent } from '../components/thankyou/thankyou.component';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment-timezone';
// import { ImagePicker } from '@ionic-native/image-picker/ngx';
// import moment from 'moment';
@Component({
  selector: 'app-newfeedback',
  templateUrl: './newfeedback.page.html',
  styleUrls: ['./newfeedback.page.scss'],
})
export class NewfeedbackPage implements OnInit {
  imageResponse:any=[];
  type:any;
  cat_id:any;
  feedback:any;
  subscription:Subscription;
  img:any[]=[];
  hideHeader:boolean=false;
  constructor(private route:ActivatedRoute,private storage:Storage,private http:HttpClient,private toast:ToastController,
    private platform:Platform,private modalCntlr:ModalController,private keyboard:Keyboard,private router:Router,
    private actionsheetCntlr:ActionSheetController,
    private config:HttpConfigService,private loadingCtrl:LoadingController) { }

  ngOnInit() {
    // this.imagePicker.requestReadPermission();
    this.keyboard.onKeyboardWillShow().subscribe( {
      next: x => {
        this.hideHeader=true
      },
      error: e => {
        console.log(e);
      }
    });
    this.keyboard.onKeyboardWillHide().subscribe( {
      next: x => {
        this.hideHeader=false;
      },
      error: e => {
        console.log(e);
      }
    });


  }


ionViewWillEnter(){
  this.type=this.route.snapshot.paramMap.get('type');
  this.cat_id=this.route.snapshot.paramMap.get('cat_id');
       this.imageResponse=[];
       this.img=[];

       
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    
    this.router.navigate(['/select-category',{type:this.type}]);
  });     
}
ionViewDidLeave(){
     this.dismissLoader();
    this.subscription.unsubscribe();
}

  async openModal() {
    const modal = await this.modalCntlr.create({
      component: ThankyouComponent,
      componentProps: {
        "flag":2,
        data:this.type
      }
    });
    return await modal.present();
  }
  
  
  
  addImage(){
    let options;
    options={
      maximumImagesCount: 5,
      outputType: 1
    }
    if(this.platform.is('ios')){
      options.disable_popover=true
    }
      // this.imagePicker.getPictures(options).then((results) => {
      //   for (var i = 0; i < results.length; i++) {
      //       // console.log('Image URI: ' + results[i]);
      //       if((results[i]==='O')||results[i]==='K'){
      //         console.log("no img");
              
      //         }else{
      //           // this.img.push(results[i]);
      //        let im='data:image/jpeg;base64,'+results[i]
      //        this.imageResponse.push(im);
      //        this.img.push(results[i]);
      //         }
            
             
      //   }
      // }, (err) => { });
    
    console.log("imagg:",this.imageResponse);
  }
  
  
  async selectImage() {
    // const actionSheet = await this.actionsheetCntlr.create({
    //   header: "Select Image source",
    //   buttons: [{
    //     text: 'Load from Library',
    //     handler: () => {
    //       this.addImage();
    //     }
    //   },
    //   {
    //     text: 'Use Camera',
    //     handler: () => {
    //       this.pickImage();
    //     }
    //   },
    //   {
    //     text: 'Cancel',
    //     role: 'cancel'
    //   }
    //   ]
    // });
    // await actionSheet.present();
    this.pickImage();
  }
  
  
  async pickImage(){
    // const options: CameraOptions = {
    //   quality: 100,
    //   sourceType: this.camera.PictureSourceType.CAMERA,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   // imageData is either a base64 encoded string or a file URI
    //   // If it's base64 (DATA_URL):
    //   let base64Image = 'data:image/jpeg;base64,' + imageData;
      
    //   this.imageResponse.push(base64Image);
    //   this.img.push(imageData);
       
    // }, (err) => {
    //   // Handle error
    // });
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true
    });
  
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
    var imageUrl = image.base64String;
  
    // Can be set to the src of an image now
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    this.imageResponse.push(base64Image);
    this.img.push(base64Image);
  }
  
  
  removeImg(i){
    this.imageResponse.splice(i,1);
    this.img.splice(i,1);
  }


 async submit(){
    if(this.feedback==undefined || this.feedback==''){
      
      this.presentAlert('Please enter your feedback');
    }else{
    // this.storage.ready().then(()=>{
           
     const uid=await this.storage.get('USER_ID');
       const cid=await this.storage.get('COMPANY_ID');
         const bid=await this.storage.get('BRANCH');
           const tz=await this.storage.get('TIMEZONE');
        let url=this.config.domain_url+'SaveFeedbackResponse';
        // let url='http://52.65.155.193/centrim_api/api/SaveFeedbackResponse';
        let headers=new HttpHeaders({'branch_id':bid.toString()})
        let body;
        let date=new Date();
        if(this.img.length==0){
          body={
            company_id:cid,
            user_id:uid,
            type:this.type,
            category_id:this.cat_id,
            feedback:this.feedback,
            via_app:1,
            images:null,
            created_time:moment().tz(tz).format('YYYY-MM-DD HH:mm:ss')
          }
        }else{
          body={
            company_id:cid,
            user_id:uid,
            type:this.type,
            category_id:this.cat_id,
            feedback:this.feedback,
            via_app:1,
            images:this.img,
            created_time:moment().tz(tz).format('YYYY-MM-DD HH:mm:ss')
          }
        }
        // var s="";
        // for (var i=0;i< this.imageResponse.length;i++)
        // {
        //   s+=`&images[]=${this.imageResponse[i]}`;
        // }
        // let body = `company_id=${cid}&user_id=${uid}&type=${this.type}&category_id=${this.cat_id}&feedback=${this.feedback}&via_app=1`+s;
     
        console.log("body:",body,tz);
        this.showLoading()
      
        // let headers=new HttpHeaders({
        //   'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        // })
  
        this.http.post(url,body,{headers}).subscribe((data:any)=>{
         
          console.log(data);

          if(data.status=="success"){
            this.dismissLoader();
          this.openModal();
          }else{
            this.dismissLoader();
            this.presentAlert('Something went wrong.Please try again.');
          }
  
        },error=>{
          console.log(error);
          
        })
  //     })
  //   })
  //   })
  // })
  // })
}
  }
  async presentAlert(message) {
    const alert = await this.toast.create({
      message: message,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'       
    });
    alert.present(); //update
  }



  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }
}

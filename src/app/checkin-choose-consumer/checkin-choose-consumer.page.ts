import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-checkin-choose-consumer',
  templateUrl: './checkin-choose-consumer.page.html',
  styleUrls: ['./checkin-choose-consumer.page.scss'],
})
export class CheckinChooseConsumerPage implements OnInit {
members:any[]=[];
onPremises:any[]=[];
res_id:any;
subscription:Subscription;
rv:any;
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,
    private router:Router,private route:ActivatedRoute,private toastCntlr:ToastController,
    private platform:Platform) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.onPremises=JSON.parse(this.route.snapshot.paramMap.get('onPremises'));
    this.res_id=this.route.snapshot.paramMap.get('res_id');
    console.log(this.onPremises);
    this.rv=await this.storage.get('RVSETTINGS');
    this.getFamily();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/checkin-scan-qr']) ;
      // this.back() ;
   
    }); 
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
  async getFamily(){
    this.members=[];
    // this.storage.ready().then(()=>{
      const id=await this.storage.get('USER_ID');
      
          
        
      
      let url=this.config.domain_url+'myfamily/'+id;
      let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log("fam:",res)
         
     
      
          res.data[0].family.forEach(ele => {
            this.members.push(ele.parent_details[0]);
          })
        })
//       })
//     })
//     })
//   })
// })
  }

  async next(item){
    if(this.onPremises.includes(item.user_id)){
    // this.router.navigate(['/checkin-visit-purpose',{res_id:item.user_id,flag:2,onPremises:JSON.stringify(this.onPremises),}])
    this.res_id=item.user_id
    // this.storage.ready().then(() => {
      const ph=await this.storage.get('PHONE');
        if(ph==null||ph=='+61'||ph.toString().length<10){
          this.router.navigate(['/checkin-phone-num',{res_id:this.res_id,onPremises:JSON.stringify(this.onPremises),from:2}])
        }else{
      const temp=await this.storage.get('VIS-TEMPARATURE');
       
    
    const vac=await this.storage.get('VIS-VACCINATION');
      if(temp==1||vac==1){
      this.router.navigate(['/checkin-vaccine-info',{res_id:this.res_id,onPremises:JSON.stringify(this.onPremises),from:2}])
      }else{
        this.router.navigate(['/checkin-additional-visitors', {res_id:this.res_id,onPremises:JSON.stringify(this.onPremises),from:2}])
      }
  //   })
  // })
}
//   })
// })



    }else{
        this.presentAlert('This resident is not at premises right now.');
    }
  }
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'       
    });
    alert.present(); //update
  }
}

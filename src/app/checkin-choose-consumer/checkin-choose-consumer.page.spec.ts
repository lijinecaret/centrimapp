import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckinChooseConsumerPage } from './checkin-choose-consumer.page';

describe('CheckinChooseConsumerPage', () => {
  let component: CheckinChooseConsumerPage;
  let fixture: ComponentFixture<CheckinChooseConsumerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinChooseConsumerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckinChooseConsumerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

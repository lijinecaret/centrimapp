import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckinChooseConsumerPageRoutingModule } from './checkin-choose-consumer-routing.module';

import { CheckinChooseConsumerPage } from './checkin-choose-consumer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckinChooseConsumerPageRoutingModule
  ],
  declarations: [CheckinChooseConsumerPage]
})
export class CheckinChooseConsumerPageModule {}

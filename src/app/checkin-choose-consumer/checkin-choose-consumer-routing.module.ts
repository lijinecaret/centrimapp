import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckinChooseConsumerPage } from './checkin-choose-consumer.page';

const routes: Routes = [
  {
    path: '',
    component: CheckinChooseConsumerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckinChooseConsumerPageRoutingModule {}

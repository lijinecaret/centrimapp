import { Component, OnInit } from '@angular/core';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpEvent, HttpEventType, HttpHeaders, HttpRequest } from '@angular/common/http';
import { ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';
import { DownloadComponent } from '../components/download/download.component';

// declare var $;
@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.page.html',
  styleUrls: ['./newsletter.page.scss'],
})

export class NewsletterPage implements OnInit {
news:any=[];
subscription:Subscription;
progress:any;
isDownloading=false;
hide: boolean = false;
terms: any;
  constructor(private config:HttpConfigService,private http:HttpClient,private platform:Platform,private file:File,
   private storage:Storage,private router:Router,private modalCntlr:ModalController,private popCntl:PopoverController,
    private iab:InAppBrowser,private dialog:SpinnerDialog,private httpD:HTTP,private toastCntlr:ToastController) { }

  ngOnInit() {
    
  }
  ionViewWillEnter(){
    this.getContent();
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/menu']) ;
});  

}

ionViewWillLeave() { 
this.subscription.unsubscribe();
this.hide = false;
}
async getContent(){
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('COMPANY_ID');
      
        const tok=await this.storage.get('TOKEN');
          let token=tok;
          const bid=await this.storage.get('BRANCH');
            let branch=bid.toString();
            let headers= await this.config.getHeader();
      let url=this.config.domain_url+'newsletters';
      // let headers= await this.config.getHeader();
      let body={
        company_id:data,
        type:2
      }
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          this.news=res.data;
         
          
        },error=>{
          console.log(error);
          
        })
//     })
//   })
// })
//     })
  
}
openDoc(item){
  // window.open(encodeURI(item.attachment),"_system","location=yes");


  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
  hideurlbar:'yes',
  hidespinner:'yes',
  beforeload:'yes',
  clearcache:'yes'
  // zoom:'yes',
  // EnableViewPortScale:'yes'
  }
  this.platform.ready().then(() => {
    if(item.attachment.includes('.pdf')){
      this.showpdf(item.attachment)
  }else{
  const browser = this.iab.create(encodeURI('https://docs.google.com/gview?embedded=true&url='+item.attachment),'_blank',options);

 
  this.dialog.show();

  // browser.on('beforeload').subscribe((event)=>{
  //   console.log('beforeload:',event.url.match(".pdf"))
  //   if(event.url.match(".pdf")){
     
  //     // Open PDFs in system browser (instead of InAppBrowser)
  //     this.iab.create(event.url, "_system");
  // }
  // })
 
  browser.on('loadstart').subscribe((event) => {
    console.log('start:',event);
        
       
    this.dialog.hide();  
      
   
  }, err => {
    console.log('starterr:',err);
    
    this.dialog.hide();
  })

setTimeout(()=>{
  browser.on('loadstop').subscribe((event)=>{
    console.log('stop:',event);
    
    this.dialog.hide();;
  }, err =>{
    console.log('stoperr:',err);
    
    this.dialog.hide();
  })
},5000)

  browser.on('loaderror').subscribe((event)=>{
    console.log('err:',event);
    
    this.dialog.hide();
  }, err =>{
    console.log('err:',err);
    
    this.dialog.hide();
  })
  
  browser.on('exit').subscribe((event)=>{
    this.dialog.hide();
  }, err =>{
    this.dialog.hide();
  })

}
})
    }



    download1(item){
      const req = new HttpRequest('GET', item.attachment, {
        reportProgress: true,
        responseType: "blob"//blob type pls
    });

    //all possible events
    this.http.request(req).subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
            case HttpEventType.Sent:
                console.log('Request sent!');
                break;
            case HttpEventType.ResponseHeader:
                console.log('Response header received!');
                break;
            case HttpEventType.DownloadProgress:
              this.isDownloading=true;
                //i use a gloal progress variable to save progress in percent
                this.progress = Math.trunc(event.loaded / event.total * 100);
                break;
            case HttpEventType.Response:
                //do whatever you have to do with the file using event.body
                console.log('😺 Done!', event.body);
                //i'm gonna write the file in my case
                this.file.writeFile(this.file.dataDirectory, item.name, event.body, {replace: true}).then( (file) =>{
                
                         //using a cordova plugin to move my file from a tmp folder to the user image folder
                        //https://github.com/quiply/SaveImage
        //  (<any>window).cordova.plugins.imagesaver.saveImageToGallery(this.file.dataDirectory+'tmp.jpg', onSaveImageSuccess, onSaveImageError);

        //                 function onSaveImageSuccess(){
        //                 //in case of imagesave success
        //                 }
        //                 function onSaveImageError(error) {
        //                 //in case of imagesaver fail
        //                 }
                 }).catch(
                 (err) => {
                   this.progress=0;
                   this.isDownloading=false;
                     //in case of file.writefile fail
                 });
                 this.progress=0;
                 this.isDownloading=false;
        }
    });
    }


    download(item){
      if(this.platform.is('android')){
      this.file.checkDir(this.file.externalRootDirectory, 'Download').then(_ => console.log('Directory exists')).catch(err =>{
        console.log("no directory:",this.file.externalRootDirectory);
        this.file.createDir(this.file.externalRootDirectory,'Download',true).then(()=>{
          console.log("directory created");
          
        })
      })
       
      this.httpD.downloadFile(
        item.attachment,
        '',
        '',
        this.file.externalRootDirectory+'/Download/'+item.name.replace(/\s+/g, "_")
      )
        .then(response => {
          // prints 200
          console.log("res:",response);
          
          console.log(response.status);
          this.presentAlert('Downloaded.')
        })
        .catch(response => {
          // prints 403
          console.log(response);
          
          console.log(response.status);
          // prints Permission denied
          console.log(response.error);
        });
      }else{
        this.file.checkDir(this.file.tempDirectory, 'Centrimlife').then(_ => console.log('Directory exists')).catch(err =>{
          console.log("no directory");
          this.file.createDir(this.file.tempDirectory,'Centrimlife',true).then(()=>{
            console.log("directory created");
            
          })
        })
         
        this.httpD.downloadFile(
          item.attachment,
          '',
          '',
          this.file.tempDirectory+'/Centrimlife/'+item.name.replace(/\s+/g, "_")
        )
          .then(response => {
            // prints 200
            console.log("res:",response);
            
            console.log(response.status);
            this.presentAlert('Downloaded')
          })
          .catch(response => {
            // prints 403
            console.log(response);
            
            console.log(response.status);
            // prints Permission denied
            console.log(response.error);
          });
        
      }
    }

    async presentAlert(mes) {
      const alert = await this.toastCntlr.create({
        message: mes,
        duration: 3000,
        position:'top'       
      });
      alert.present(); //update
    }
    async showpdf(file){
      const modal = await this.modalCntlr.create({
        component: ViewpdfComponent,
        cssClass:'fullWidthModal',
        componentProps: {
          
          data:file,
          
           
        },
        
        
      });
      return await modal.present();
    }

    async downloadLetter(item,ev){
      const popover = await this.popCntl.create({
        component: DownloadComponent,
        event:ev,
        
        cssClass:'cancelpop',
        
      });

      popover.onDidDismiss().then((data)=>{
        //  this.ionViewWillEnter();
        if(data.data!=undefined){
         this.download(item)
        }
        })
      return await popover.present();
    }
    cancel() {
      this.hide = false;
      this.terms = '';
    }
    search() {
      this.hide = true;
    }
}

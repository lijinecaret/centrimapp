import { Component, OnInit, ViewChild } from '@angular/core';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, Platform, ToastController, LoadingController, IonContent, PopoverController, ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { ReportAbuseComponent } from '../components/report-abuse/report-abuse.component';
import { ReportReasonComponent } from '../components/report-reason/report-reason.component';
// import { Content } from '@angular/compiler/src/render3/r3_ast';




@Component({
  selector: 'app-stories',
  templateUrl: './stories.page.html',
  styleUrls: ['./stories.page.scss'],
})
export class StoriesPage implements OnInit {
  tabStatus:boolean=false;
  status:any;
  st:any;
  banner:boolean=true;
  name:string;
  p_name:string;
  token:string;
  user_id:any;
  branch:any;
  keyboardStyle = { width: '100%', height: '0px' };
  pinned:any=null;
  posts:any[]=[];
  announcements:any[]=[];
  
  user_post:any[]=[];
  user_img:string;
  url: string;
  commentText:string;
  page_number = 1;
  no_post:boolean;
  has_more_items: boolean = true;
  subscription:Subscription;
  welcome:string;
  pin_creator:any;
  lid:any;
  // counter=0;
  index:any;
  notify:any;
  device:any;
  // applaunched:boolean;


  poll:any={};
noOfItem:number=1;
// noOfPollQue:number=1;

nextQue:boolean=true;
preQue:boolean=false;
submitStatus=false;
que_id:any;
opt_id:any;
// selected:any;
// opt:any;
thank:boolean=false;
selectedIndex: number =0;
scrollPosition = 0;
skip:any=0;
@ViewChild('content' , { static: false }) content: IonContent;
public Data : {pollId:number , QuestionId : number , OptionId : number}[];
// public Data: [];
public CollectedData: {pollId:number ,QuestionId : number , OptionId : number}[];
modules: any[] = [];
msgcount:any=0;
  constructor(private keyboard:Keyboard,public http:HttpClient,private storage:Storage,private httpConfigService: HttpConfigService,
    private router:Router,private nav:NavController,private platform:Platform,private toastCntlr:ToastController,
    private loadingCtrl:LoadingController,private route:ActivatedRoute,private popoverCntrl:PopoverController,
    private modalCntl:ModalController) { 

  }

  async ionViewWillEnter(){
   
    this.page_number=1;
    this.pinned=null;
    this.posts=[];
    this.announcements=[];
    this.user_post=[];
      this.tabStatus=false;
      this.Data=[];
      
      this.poll=null;
      this.CollectedData=[];
      this.noOfItem=1;
      this.selectedIndex =0;
      this.showLoading();
    
      console.log("willEnter invoked");
      // get data from storage
    // this.storage.ready().then(()=>{
      const name=await this.storage.get('NAME');
        this.name=name;
        this.p_name=this.name.substr(0,1);
      // });
      
      
      const da=await this.storage.get('PRO_IMG');
        this.user_img=da;
      // })
      let headers= await this.httpConfigService.getHeader();
      const cid=await this.storage.get('COMPANY_ID');
      let murl = this.httpConfigService.domain_url + 'get_modules_family/' + cid;
      this.http.get(murl,{headers}).subscribe((mod: any) => {
        console.log("modules:", mod);
        // this.show = true;
        this.modules = mod
        

      })

      
    if(this.tabStatus==false){
      this.getPosts(false, "");
      }else{
        this.getMyPosts(false,'');
      }

      const data=await this.storage.get('USER_ID');
        let url=this.httpConfigService.domain_url+'notification/'+data;
        let body={
          id:data,
          offset:0,
          limit:20
        }
        this.http.get(url,{headers}).subscribe((res:any)=>{
          
          this.notify=res.data.length;
          console.log("len:", this.notify);
        
        })
        let m_url=this.httpConfigService.domain_url+'msgcount';
      
      let body1={
        user_one:data
      }
      this.http.post(m_url,body1,{headers}).subscribe((mes:any)=>{
        
        this.msgcount=mes.data.msgcount;
      

      })
      this.storage.get("BRANCH").then(bid=>{
        let url1=this.httpConfigService.domain_url+'publicPolls';
       
        // let url1='http://52.65.155.193/centrim_api/api/publicPolls';
         let body={user_id:data,branch_id:bid}
         this.http.post(url1,body,{headers}).subscribe((res:any)=>{
           console.log('poll:',res);
           this.poll=res.data[0];
           
         })
     
        })
    
    let date=new Date();
    let hr=date.getHours();
    console.log("hr:",hr)
    if(hr<=6){
      this.welcome='Welcome'
    }
    else if(hr<12){
      this.welcome='Good morning'
    }
    else if(hr<17){
      this.welcome='Good afternoon'
    }
    else if(hr>=17){
      this.welcome='Good evening'
    }
   
    
      
  
   
     
    
       
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      this.router.navigate(['/menu']) ; 
  
    }); 

  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
     // Save scroll position
     this.content.getScrollElement().then(data => {
      console.log(data.scrollTop);
      this.scrollPosition = data.scrollTop;
    });
 }
 notification(){
   this.router.navigate(['/notifications',{flag:1}]);
 }
  ngOnInit() {

  // work around method for keyboard covering the input field
  this.keyboard.onKeyboardWillShow().subscribe( {
    next: x => {
      this.keyboardStyle.height = x.keyboardHeight + 'px';
    },
    error: e => {
      console.log(e);
    }
  });
  this.keyboard.onKeyboardWillHide().subscribe( {
    next: x => {
      this.keyboardStyle.height = '0px';
    },
    error: e => {
      console.log(e);
    }
  });
    

  }
  // select newsfeed tab all 
  getContent(){
    this.tabStatus=false;
    this.ionViewWillEnter();
    
  }

  // select newsfeed tab related to me
  setContent(){
    this.showLoading();
    console.log("user:",this.user_id,this.token);
    this.tabStatus=true;
    this.skip=0;
    this.getMyPosts(false,'');
  }

// like a post

  async like(item){
    // this.status=liked;
    // this.storage.ready().then(()=>{
      
      const data=await this.storage.get('USER_ID');
        
        this.user_id=data;
        
        // this.status=id;
        // this.index=idx;
        if(item.liked==true  ){
           this.status=0
          //  this.lid=id
        }else{
          this.status=1
        }
    let story_id=item.id;
    let body={
      post_id: story_id,
      user_id: this.user_id,
      action_type: '1',
      status:this.status
    };
    console.log("body:",body);
    let url=this.httpConfigService.domain_url+'update_post_action';
    let headers= await this.httpConfigService.getHeader();
    this.http.post(url,body,{headers}).subscribe((data:any)=>{
      console.log(data);
      console.log("like:",data);
      item.liked=!item.liked
      item.liked ? item.likes_count++ : item.likes_count--;
      // feed.IsLike ? feed.LikesCount++ : feed.LikesCount--;
     
    },error=>{
      console.log(error);
      
    });
//   })
// })
  
  
  }

  // close welcome banner
  hideBanner(){
    this.banner=false;
  }

// get newsfeed posts

async getPosts(isFirstLoad, event){

    // if(this.flag==1){
  //   this.user_id=this.route.snapshot.paramMap.get("user_id");
  //   this.branch=this.route.snapshot.paramMap.get("branch_id");
  // }else{
    this.getAnnouncement();
  // this.storage.ready().then(()=>{
        
    
      const data=await this.storage.get('USER_ID');
        
        this.user_id=data;
       
        
            const res=await this.storage.get("BRANCH");
              this.branch=res;
    this.url = '?page=' + this.page_number;
 console.log("uer",this.user_id,"brn",this.branch);
  (await this.httpConfigService.getListItems(this.url, this.user_id, this.branch)).subscribe((data:any)=>{
      console.log("res:",data);
      if(data.pinned==null){
        this.pinned=null
      
      }else{
        this.pinned=data.pinned;
        this.pin_creator=this.pinned.creator.name;
        console.log("pin:",this.pinned.creator.name)
      }
      
      // this.posts=data.story.data;
      for (let i = 0; i < data.story.data.length; i++) {
        this.posts.push(data.story.data[i]);
      }
      this.dismissLoader();
      console.log("post:",this.scrollPosition);
      // this.content.scrollToPoint(0, this.scrollPosition);
      if (isFirstLoad)
      event.target.complete();

    this.page_number++;
    }, error => {
      console.log(error);
      this.dismissLoader();
    })
  // })
// })
//   })
// }

}

async getAnnouncement(){
  let headers= await this.httpConfigService.getHeader();
  const data=await this.storage.get('USER_ID');
  let url=this.httpConfigService.domain_url+'announcement_list/'+data;
        
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log(res);
this.announcements=res.tag_announcements;
        })
    //   })
    // })
}

// infinite scrolling

doInfinite(event) {
  this.getPosts(true, event)
}
doInfiniteMyPost(event){
  this.getMyPosts(true,event)
}

// post a comment

async comment(id){
  // this.storage.ready().then(()=>{
    
    
    
      const data=await this.storage.get('USER_ID');
        
        this.user_id=data;
       
   
  let story_id=id;
    let body={
      post_id: story_id,
      user_id: this.user_id,
      action_type: 2,
      comment: this.commentText
    };
    let url=this.httpConfigService.domain_url+'create_post_action';
    let headers=await this.httpConfigService.getHeader();
    console.log("head:",headers);
    
    this.http.post(url,body,{headers}).subscribe(data=>{
      console.log(data);
      this.ionViewWillEnter();
    },error=>{
      console.log(error);
      
    });
    this.commentText="";
  // });
  // });

}

// view story details
gotoDetails(id){
    // this.router.navigate(['/story-details',{post_id:id}]);
    this.nav.navigateRoot(['/story-details',{post_id:id}])

}
// get user related posts
async getMyPosts(isFirstLoad,event){
  // this.storage.ready().then(()=>{
    
    
    
      const data=await this.storage.get('USER_ID');
        
        this.user_id=data;
        
    
  let body={
    user_id : this.user_id,
    skip:this.skip,
    take:20
  }
  let url=this.httpConfigService.domain_url+'filter_user_post';
  let headers=await this.httpConfigService.getHeader();
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
        
        // this.user_post=data.data.tag_post;
        let idx=this.user_post.length;
        for (let i = idx; i < data.data.length; i++) {
          this.user_post.push(data.data[i]);
        }
        console.log("len:",data);
        if (isFirstLoad)
      event.target.complete();
        
        
        if(this.user_post.length==0){
          this.no_post=true;
        }else{
          this.no_post=false;
        }
        this.skip=this.skip+20;
        this.dismissLoader();
      },error=>{
        console.log(error);
        this.dismissLoader();
      })
    // });
    // });
  
}

doRefresh(event) {
  this.page_number = 1;
  this.posts=[];
  this.ionViewWillEnter();

  setTimeout(() => {
    
    event.target.complete();
  }, 2000);
}

  async exitAlert() {
    const alert = await this.toastCntlr.create({
      message: 'Press again to exit.',
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'       
    });
    alert.present(); //update
  }
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
}

  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }




  next(id,que,q){
    if(this.Data.length==0){
      const targetIdx = this.CollectedData.map(item => item.QuestionId).indexOf(q.id);
      console.log("already present:",targetIdx,que.id);
      
    if(targetIdx>=0)
      {
        this.nextqus(que);
      }else{
      console.log("select one option");
      
      this.presentAlert("Please select an option")
      }
    }else{
      if(this.CollectedData.length==0){
        this.CollectedData.push({pollId:id ,QuestionId:this.Data[0].QuestionId,OptionId:this.Data[0].OptionId});
        this.nextqus(que);
        this.Data=[];
      }else{
        console.log("second entry");
        
      // console.log("pollId:",this.CollectedData.some(item=>item.pollId))
      // const found = this.CollectedData.some(item=>item.pollId==id)
      // if(!found){
      if(this.CollectedData[0].pollId==id){
        console.log("first ele:",this.CollectedData[0].pollId,id,que);
        
        const targetIdx = this.CollectedData.map(item => item.QuestionId).indexOf(this.Data[0].QuestionId);
        console.log("already present:",targetIdx);
        
      if(targetIdx>=0)
        {
          console.log("change option");
          
  
          this.CollectedData[targetIdx].OptionId =this.Data[0].OptionId;
          
        }else{
          console.log("new entry");
          
          this.CollectedData.push({pollId:id ,QuestionId:this.Data[0].QuestionId,OptionId:this.Data[0].OptionId})
      
        
        }
        this.nextqus(que);
        this.Data=[];
      }
   else{
  
      console.log("submit first poll");
      this.presentAlert("Please Submit your answer before attempting another poll.")
      
    }
  }
  }
  }
   nextqus(que){
    
    if(this.noOfItem <=que.length){ 
      // this.nextQue=false;
      this.noOfItem += 1; 
      this.preQue=true;
      this.selectedIndex =   this.selectedIndex + 1;
      if(this.noOfItem>que.length){
        this.noOfItem -=1;
        this.nextQue=false
        this.submitStatus=true;
        this.selectedIndex =   this.selectedIndex - 1;
      }
    } else { 
      console.log('END OF DAY 1') 
    }
  }
  previous(que){
    if(this.noOfItem <= que.length){
      this.selectedIndex =   this.selectedIndex - 1;
      this.nextQue=true;
   
    if(this.noOfItem!=1){
    this.noOfItem += -1; 
    }else{
      this.noOfItem=1;
    }
    if(this.noOfItem==1){
      this.preQue=false;
    }else{
      this.preQue=true;
    }
  } else { 
    console.log('END OF DAY 1') 
  }
  console.log("no:",this.noOfItem,"len:",que.length)
  }
  
   markPoll(id,que,ans,qArray){
    this.Data=[{pollId:id.id,QuestionId:que.id,OptionId:ans.id}]
    console.log("mark:",this.Data);
    
   }
  
  
  
   submit(item,idx){
    console.log('submit clicked');
    
     if(item.questions.length==1){
      if(this.Data.length==0){
        console.log("select one option");
        
        this.presentAlert("Please select an option")
        
      }else{
        this.CollectedData.push({pollId:item.id ,QuestionId:this.Data[0].QuestionId,OptionId:this.Data[0].OptionId});
        this.submitPoll(idx);
      }
     }else{
       this.submitPoll(idx);
     }
    
     
  
   }
  
   async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      duration: 3000,
      position:'top'       
    });
    alert.present(); //update
  }
  
  
  async submitPoll(idx){
    console.log("data:",this.CollectedData);
  
    for(let data of this.CollectedData){
      //  
      if(this.que_id==''||this.que_id==undefined)
      this.que_id = data.QuestionId;
      if(this.que_id==data.QuestionId){
          console.log("firstQue");
          
      }else{
       this.que_id += "*" + data.QuestionId;
      }
  
      if(this.opt_id==''||this.opt_id==undefined)
      this.opt_id = data.OptionId;
      // if(this.opt==''||this.opt==undefined)
      // this.opt = data.OptionId;
      if(this.opt_id==data.OptionId){
        console.log("firstOpt");
        
    }else{
      //  this.opt_id += "*_*" + data.OptionId;
       this.opt_id +="*"+data.OptionId;
    }
     }
     console.log("Que:",this.que_id,"Opt:",this.opt_id);
     let headers= await this.httpConfigService.getHeader();
    //  this.storage.ready().then(()=>{
       const uid=await this.storage.get('USER_ID');
        let url=this.httpConfigService.domain_url+'markPoll';
        // let url='http://52.65.155.193/centrim_api/api/markPoll';
        let body={
          user_id:uid,
          questions:this.que_id,
          answers:this.opt_id
        }
  
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          if(res.status=='success'){
            this.poll=null;
            this.thank=true;
            this.submitStatus=false;
            this.nextQue=true;
            this.Data=[];
            this.CollectedData=[];
            this.noOfItem=1;
            this.selectedIndex =0;
            this.que_id='';
            this.opt_id='';
            setTimeout(() => { 
             this.thank=false;
           }, 3000)
          }else{
            this.presentAlert('Something went wrong.Please try again.')
          }
          
        })
  
  
     
  
    //    })
    //  })
  }

  async report(item,ev){
    const popover = await this.popoverCntrl.create({
      component: ReportAbuseComponent,
      event:ev,
      componentProps:{
        type:1
      },
      backdropDismiss:true,
      cssClass:'message-options'
      
      
    });
    popover.onDidDismiss().then((data)=>{
     if(data.data==1){
      this.reason(item);
     }
    })
    return await popover.present();
  }


  async reason(item){
    const modal = await this.modalCntl.create({
      component: ReportReasonComponent,
      cssClass:'report-reason-modal',

      backdropDismiss:true,
      
      
    });
    modal.onDidDismiss().then((data)=>{
      console.log('dataa:',data)
      let reason;
        if(data.role=='1'){
          if(data.data){
         reason=data.data
        }else{
          reason='';
        }
        this.reportPost(item,reason);
      }
    })
    
    return await modal.present();
  }
  async reportPost(item,reason){

 const uid=await this.storage.get('USER_ID');
 const bid=await this.storage.get('BRANCH');
 const cid=await this.storage.get('COMPANY_ID');
        let url=this.httpConfigService.domain_url+'report-issue';
       let headers= await this.httpConfigService.getHeader()
        let body={
          user_id:uid,
          content_type:'STORY',
          request_id:item.id,
          reason:reason
        }
        console.log('report:',body)
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          this.presentAlert('This post has been reported. We shall verify and take appropriate action');
        },error=>{
          this.presentAlert('Something went wrong.Please try again later');
        })
  }


  gotoActivity(){
    if(this.modules.includes('Activity')){
      this.router.navigate(['/activities'])
    }else{
      this.presentAlert('Access denied');
    }
  }
  gotoMessage(){
    if(this.modules.includes('Messages')){
      this.router.navigate(['/message'])
    }else{
      this.presentAlert('Access denied');
    }
  }
}
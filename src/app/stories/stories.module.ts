import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StoriesPageRoutingModule } from './stories-routing.module';

import { StoriesPage } from './stories.page';
import { DateformatPipe } from '../pipes/dateformat.pipe';
import { ApplicationPipesModule } from '../application-pipes.module';

// import { HideHeaderDirective, ScrollHideConfig } from '../directives/hide-header.directive';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StoriesPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [StoriesPage],
  
})
export class StoriesPageModule {

}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides, LoadingController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-poll',
  templateUrl: './poll.page.html',
  styleUrls: ['./poll.page.scss'],
})
export class PollPage implements OnInit {
poll:any[]=[];
noOfItem:number=1;
// noOfPollQue:number=1;
subscription:Subscription;
nextQue:boolean=true;
preQue:boolean=false;
submitStatus=false;
que_id:any;
opt_id:any;
selected:any;
// opt:any;
thank:boolean=false;
selectedIndex: number =0;
  // maxNumberOfTabs : number = 0;
public Data : {pollId:number , QuestionId : number , OptionId : number}[];
// public Data: [];
public CollectedData: {pollId:number ,QuestionId : number , OptionId : number}[];
@ViewChildren(IonSlides) slides: QueryList<IonSlides>;
slide:any;
viewEntered = false;
  constructor(private http:HttpClient,private storage:Storage,private router:Router,private platform:Platform,
    private toastCntlr:ToastController,private config:HttpConfigService,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.viewEntered = true;
  }
 async ionViewWillEnter(){
  // this.Data = [{pollId:0 , QuestionId: 0 ,OptionId : 0  }];
  this.showLoading();
  this.Data=[];
  this.CollectedData=[];
  this.noOfItem=1;
  this.selectedIndex =0;
  //  this.storage.ready().then(()=>{
     const uid=await this.storage.get('USER_ID');
     const bid=await this.storage.get('BRANCH');
      let url=this.config.domain_url+'publicPolls';
      let headers= await this.config.getHeader()
      //  let url='http://52.65.155.193/centrim_api/api/publicPolls';
       let body={user_id:uid,branch_id:bid}
       this.http.post(url,body,{headers}).subscribe((res:any)=>{
         
         this.poll=res.data;
        //  for(let i=0;i<=this.poll.length;i++){
        //    for(let j=0;j<=this.poll[i].questions.length;j++){
        //     for(let c=0;c<=this.poll[i].questions[j].answers.length;c++){
        //       console.log(this.poll[i].questions[j].answers[c]);
              
        //       this.poll[i].questions[j].answers[c].selected=false;
        //     }
        //    }
        //  }
        this.dismissLoader();
         console.log(this.poll);
        //  this.maxNumberOfTabs = this.wizardData.length > 0 ? this.wizardData.length : 0;
       },err=>{
        this.dismissLoader();
       })
  //    })
  //  })
     
   this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
   
    this.router.navigate(['/menu']);
  }); 
 }
 ionViewDidLeave(){
    this.subscription.unsubscribe();
}
next(id,que,q,j){
  if(this.Data.length==0){
    const targetIdx = this.CollectedData.map(item => item.QuestionId).indexOf(q.id);
      console.log("already present:",targetIdx,que.id);
    
    if(targetIdx>=0)
      {
        this.nextqus(que,j);
      }else{
    console.log("select one option");
    
    this.presentAlert("Please select an option")
      }
    
  }else{
    if(this.CollectedData.length==0){
      this.CollectedData.push({pollId:id ,QuestionId:this.Data[0].QuestionId,OptionId:this.Data[0].OptionId});
      this.nextqus(que,j);
      this.Data=[];
      
    }else{
      console.log("second entry");
      
    // console.log("pollId:",this.CollectedData.some(item=>item.pollId))
    // const found = this.CollectedData.some(item=>item.pollId==id)
    // if(!found){
    if(this.CollectedData[0].pollId==id &&this.Data[0].pollId==id){
      console.log("first ele:",this.CollectedData[0].pollId,id,que);
      
      const targetIdx = this.CollectedData.map(item => item.QuestionId).indexOf(this.Data[0].QuestionId);
      console.log("already present:",targetIdx);
      
    if(targetIdx>=0)
      {
        console.log("change option");
        

        this.CollectedData[targetIdx].OptionId =this.Data[0].OptionId;
        // this.selectedIndex =   this.selectedIndex + 1;
        
      }else{
        console.log("new entry");
      
        this.CollectedData.push({pollId:id ,QuestionId:this.Data[0].QuestionId,OptionId:this.Data[0].OptionId})
        
        
      }
      this.nextqus(que,j);
      // this.selectedIndex =   this.selectedIndex + 1;
      this.Data=[];
    }
 else{
    if(this.Data[0].pollId!=id){
      this.presentAlert("Please choose an option.")
    }else{
    console.log("submit first poll");
    this.presentAlert("Please Submit your answer before attempting another poll.")
    }
  }
}
}
}
 nextqus(que,j){
  
  console.log("next:",this.selectedIndex);
  if(this.noOfItem <=que.length){ 
    // this.nextQue=false;
    this.noOfItem += 1; 
    this.preQue=true;
    this.selectedIndex =   this.selectedIndex + 1;
    if(this.noOfItem>que.length){
      this.noOfItem -=1;
      this.nextQue=false
      this.submitStatus=true;
      this.selectedIndex =   this.selectedIndex - 1;
    }
  } else { 
    console.log('END OF DAY 1') 
  }
  this.slide=j;
  this.slides.toArray().forEach(async (slider, index) => {
    console.log("slide:",j,"slider:",index);
  
    // console.log(`Slider ${index} selected index: ${await slider.getActiveIndex()}`);
    if(index===j){
    slider.slideNext(500).then(() => {
    
    });
  }
  })
}
previous(que,j){
  // this.submitStatus=false;
  if(this.noOfItem <= que.length){
    this.selectedIndex = this.selectedIndex - 1;
    this.nextQue=true;
    this.submitStatus=false;
  if(this.noOfItem!=1){
  this.noOfItem += -1; 
  }else{
    this.noOfItem=1;
  }
  if(this.noOfItem==1){
    this.preQue=false;
  }else{
    this.preQue=true;
  }
  console.log("selected:",this.selectedIndex,this.CollectedData[this.selectedIndex],this.Data);
  
} else { 
  console.log('END OF DAY 1') 
}
console.log("no:",this.noOfItem,"len:",que.length)
this.slides.toArray().forEach(async (slider, index) => {
  console.log("slide:",j,"slider:",index);
  
  // console.log(`Slider ${index} selected index: ${await slider.getActiveIndex()}`);
  if(index==j){
  slider.slidePrev(500).then(() => {
  
  });
}
})

}

 markPoll(id,que,ans,qArray,i){
  this.Data=[{pollId:id.id,QuestionId:que.id,OptionId:ans.id}]
  console.log("mark:",this.Data,this.CollectedData);
  this.selected=i;
 }



 submit(item,idx){
   if(item.questions.length==1){
    if(this.Data.length==0){
      console.log("select one option");
      
      this.presentAlert("Please select an option")
      
    }else{
      this.CollectedData.push({pollId:item.id ,QuestionId:this.Data[0].QuestionId,OptionId:this.Data[0].OptionId});
      this.submitPoll(idx);
    }
   }else{
     this.submitPoll(idx);
   }
  
   

 }

 async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}


async submitPoll(idx){
  console.log("data:",this.CollectedData);

  for(let data of this.CollectedData){
    //  
    if(this.que_id==''||this.que_id==undefined)
    this.que_id = data.QuestionId;
    if(this.que_id==data.QuestionId){
        console.log("firstQue");
        
    }else{
     this.que_id += "*" + data.QuestionId;
    }

    if(this.opt_id==''||this.opt_id==undefined)
    this.opt_id = data.OptionId;
    // if(this.opt==''||this.opt==undefined)
    // this.opt = data.OptionId;
    if(this.opt_id==data.OptionId){
      console.log("firstOpt");
      
  }else{
    //  this.opt_id += "*_*" + data.OptionId;
     this.opt_id +="*"+data.OptionId;
  }
   }
   console.log("Que:",this.que_id,"Opt:",this.opt_id);

  //  this.storage.ready().then(()=>{
     const uid=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'markPoll';
      // let url='http://52.65.155.193/centrim_api/api/markPoll';
      let body={
        user_id:uid,
        questions:this.que_id,
        answers:this.opt_id
      }
      let headers= await this.config.getHeader();
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        if(res.status=='success'){
          this.poll.splice(idx,1);
          this.Data=[];
            this.CollectedData=[];
            this.noOfItem=1;
            this.selectedIndex =0;
            this.que_id='';
            this.opt_id='';
          this.thank=true;
          this.submitStatus=false;
          this.nextQue=true;
          this.preQue=false;
          setTimeout(() => { 
           this.thank=false;
         }, 3000)
        }else{
          this.presentAlert('Something went wrong.Please try again.')
        }
        
      })


   

  //    })
  //  })
}


selectChange(event){
console.log("step:",event);

}

async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookedcallListPage } from './bookedcall-list.page';

describe('BookedcallListPage', () => {
  let component: BookedcallListPage;
  let fixture: ComponentFixture<BookedcallListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookedcallListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookedcallListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

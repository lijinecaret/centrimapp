import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookedcallListPage } from './bookedcall-list.page';

const routes: Routes = [
  {
    path: '',
    component: BookedcallListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookedcallListPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { HttpClient } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { ModalController, Platform, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MoreCallbookComponent } from '../components/more-callbook/more-callbook.component';
import { CancelComponent } from '../components/cancel/cancel.component';
import { CommentComponent } from '../components/comment/comment.component';
// import { Jitsi } from 'capacitor-jitsi-meet';

// declare var jitsiplugin;

@Component({
  selector: 'app-bookedcall-list',
  templateUrl: './bookedcall-list.page.html',
  styleUrls: ['./bookedcall-list.page.scss'],
})
export class BookedcallListPage implements OnInit {
today:any=[];
upcoming:any=[];
subscription:Subscription;
user_type:any;
rv:any;
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,private iab:InAppBrowser,
    private platform:Platform,private router:Router,private popCntlr:PopoverController,private modalCntl:ModalController) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.today=[];
    this.upcoming=[];
    // this.storage.ready().then(()=>{
      const type=await this.storage.get('USER_TYPE');
        this.user_type=type;
      // });
  this.rv=await this.storage.get('RVSETTINGS');

      const data=await this.storage.get('USER_ID');
        let url=this.config.domain_url+'consumer_viabookings';
        let headers= await this.config.getHeader();
        let body={
          user_id:data
        }

        console.log('head:',{headers})
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          // this.today=res.data.today;
          // this.upcoming=res.data.upcoming;
          res.data.today.forEach(element => {
            if(element.status==1){
              this.today.push(element);
            }
          });
          res.data.upcoming.forEach(element => {
            if(element.status==1){
              this.upcoming.push(element);
            }
          });
          console.log("today:",this.today);
          console.log("upcoming",this.upcoming)
          
        })
    //   })
    // })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu']) ;
    }); 
  
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }


  async start(item){

    // const roomId =item.link.substr(item.link.lastIndexOf('/') + 1);

    // const result = await Jitsi.joinConference({
    //   // required parameters
      
    //   roomName: roomId, // room identifier for the conference
    //   url: 'https://meet.jit.si', // endpoint of the Jitsi Meet video bridge
  
    //   // recommended settings for production build. see full list of featureFlags in the official Jitsi Meet SDK documentation
    //   featureFlags: {
    //       'prejoinpage.enabled': false, // go straight to the meeting and do not show the pre-join page
    //       'recording.enabled': false, // disable as it requires Dropbox integration
    //       'live-streaming.enabled': false, // 'sign in on Google' button not yet functional
    //       'android.screensharing.enabled': false, // experimental feature, not fully production ready
    //   },
    // })


    // if(this.platform.is('android')){
    //   const roomId =item.link.substr(item.link.lastIndexOf('/') + 1);
    
    //   // const roomId = 'VqyaB3flP';
    //   jitsiplugin.loadURL('https://meet.jit.si/' + roomId, roomId, false, function (data) {
    //     if (data === "CONFERENCE_WILL_LEAVE") {
    //         jitsiplugin.destroy(function (data) {
    //             // call finished
    //         }, function (err) {
    //             console.log(err);
    //         });
    //     }
    // }, function (err) {
    //     console.log(err);
    // });
    //  }else {
      let url=item.link.substr(item.link.lastIndexOf(' ') + 1);
      //   console.log("jitsi call:",url);
      let options:InAppBrowserOptions ={
        location:'no',
        hideurlbar:'yes',
        zoom:'no'
      }
      
      const browser = this.iab.create(url,'_system',options);
      
    //  }
  


  }
  async showPeople(item){
    const modal = await this.modalCntl.create({
      component: MoreCallbookComponent,
      backdropDismiss:true,
      componentProps:{
        data:item.family,
        call:1
      },
      cssClass:'morepop'
      
      
    });
    return await modal.present();
  }
  async cancel(ev,id){

    const popover = await this.popCntlr.create({
      component: CancelComponent,
      event:ev,
      backdropDismiss:true,
      cssClass:'cancelpop',
      componentProps:{
        call_id:id
      },
      
    });
    popover.onDidDismiss().then(()=>{
      this.ionViewWillEnter();
    })
    return await popover.present();
  }
  async showComment(item){
    const popover = await this.popCntlr.create({
      component: CommentComponent,
      backdropDismiss:true,
      componentProps:{
        data:item.comment
      },
      cssClass:'comments-popover'
      
      
    });
    return await popover.present();
  }

}

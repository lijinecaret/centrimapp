import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookedcallListPageRoutingModule } from './bookedcall-list-routing.module';

import { BookedcallListPage } from './bookedcall-list.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookedcallListPageRoutingModule,
    ApplicationPipesModule,
    
  ],
  declarations: [BookedcallListPage]
})
export class BookedcallListPageModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmCheckinPageRoutingModule } from './confirm-checkin-routing.module';

import { ConfirmCheckinPage } from './confirm-checkin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmCheckinPageRoutingModule
  ],
  declarations: [ConfirmCheckinPage]
})
export class ConfirmCheckinPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmCheckinPage } from './confirm-checkin.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmCheckinPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmCheckinPageRoutingModule {}

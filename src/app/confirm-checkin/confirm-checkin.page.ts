import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import momenttz from 'moment-timezone';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Subscription } from 'rxjs';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-confirm-checkin',
  templateUrl: './confirm-checkin.page.html',
  styleUrls: ['./confirm-checkin.page.scss'],
})
export class ConfirmCheckinPage implements OnInit {
name:any;
time:any;
gov:any={};
subscription:Subscription;
additional:any;
timeoutHandle:any;
  constructor(private storage:Storage,private router:Router,private iab:InAppBrowser,private platform:Platform,
    private route:ActivatedRoute) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.additional=this.route.snapshot.paramMap.get('additional');
    if(this.additional=='null'||this.additional==0){
      this.additional=null
    }
    // this.storage.ready().then(()=>{
      const tz=await this.storage.get('TIMEZONE');
        this.time=momenttz().tz(tz).format('hh:mm A');
      // })
      const data=await this.storage.get('NAME');
        this.name=data
      // })
      this.storage.set('CHECKIN',1);
    // })
    
    
        
      const gov=await this.storage.get('GOV-CHECKIN');
        this.gov=JSON.parse(gov);
        
        console.log('gov:',this.gov);
        
        if(this.gov.required=='1'){
          this.timeoutHandle=setTimeout(()=>{
            this.openLink();
            this.router.navigate(['/menu'],{replaceUrl:true})
          },10000)
        }else{
          this.timeoutHandle=setTimeout(() => { 
     
            this.router.navigate(['/menu'],{replaceUrl:true})
           
          }, 3000)
        }
      // })
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        if(this.timeoutHandle){
          clearTimeout(this.timeoutHandle);
          this.timeoutHandle = null;
      }
        this.router.navigate(['/menu'],{replaceUrl:true})
        // this.back();
      
        });
  }

  openLink(){
    let options:InAppBrowserOptions ={
      location:'yes',
    hidenavigationbuttons:'yes',
    hideurlbar:'yes',
    zoom:'no'
    }
    const browser = this.iab.create(this.gov.url,'_blank',options);
    
  }
  ionViewWillLeave(){
    if(this.timeoutHandle){
      clearTimeout(this.timeoutHandle);
      this.timeoutHandle = null;
  }
  }
  ionViewDidLeave(){
    if(this.timeoutHandle){
      clearTimeout(this.timeoutHandle);
      this.timeoutHandle = null;
  }
  }
}

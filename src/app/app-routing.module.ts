import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  // { path: '', redirectTo: 'login', pathMatch: 'full' },
  // { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'stories',
    loadChildren: () => import('./stories/stories.module').then( m => m.StoriesPageModule),
    
  },
  {
    path: 'activities',
    loadChildren: () => import('./activities/activities.module').then( m => m.ActivitiesPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'message',
    loadChildren: () => import('./message/message.module').then( m => m.MessagePageModule)
  },
  {
    path: 'user-profile',
    loadChildren: () => import('./user-profile/user-profile.module').then( m => m.UserProfilePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'story-details',
    loadChildren: () => import('./story-details/story-details.module').then( m => m.StoryDetailsPageModule)
  },
  {
    path: 'dining',
    loadChildren: () => import('./dining/dining.module').then( m => m.DiningPageModule)
  },
  {
    path: 'petty-cash',
    loadChildren: () => import('./petty-cash/petty-cash.module').then( m => m.PettyCashPageModule)
  },
  
  {
    path: 'notifications',
    loadChildren: () => import('./notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'resources',
    loadChildren: () => import('./resources/resources.module').then( m => m.ResourcesPageModule)
  },
  {
    path: 'resources-details',
    loadChildren: () => import('./resources-details/resources-details.module').then( m => m.ResourcesDetailsPageModule)
  },
  {
    path: 'newsletter',
    loadChildren: () => import('./newsletter/newsletter.module').then( m => m.NewsletterPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'facility-details',
    loadChildren: () => import('./facility-details/facility-details.module').then( m => m.FacilityDetailsPageModule)
  },
 
  {
    path: 'relative-profile',
    loadChildren: () => import('./relative-profile/relative-profile.module').then( m => m.RelativeProfilePageModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('./edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
 
  {
    path: 'activity-details',
    loadChildren: () => import('./activity-details/activity-details.module').then( m => m.ActivityDetailsPageModule)
  },

  
  // {
  //   path: 'survey',
  //   loadChildren: () => import('./survey/survey.module').then( m => m.SurveyPageModule)
  // },

  {
    path: 'survey',
    loadChildren: () => import('./survey/survey.module').then( m => m.SurveyPageModule)
  },

  {
    path: 'maintenance',
    loadChildren: () => import('./maintenance/maintenance.module').then( m => m.MaintenancePageModule)
  },
  {
    path: 'reset-password',
    loadChildren: () => import('./reset-password/reset-password.module').then( m => m.ResetPasswordPageModule)
  },
  {
    path: 'feedback',
    loadChildren: () => import('./feedback/feedback.module').then( m => m.FeedbackPageModule)
  },
  {
    path: 'entertainment',
    loadChildren: () => import('./entertainment/entertainment.module').then( m => m.EntertainmentPageModule)
  },
  {
    path: 'ebooks',
    loadChildren: () => import('./ebooks/ebooks.module').then( m => m.EbooksPageModule)
  },
  {
    path: 'games',
    loadChildren: () => import('./games/games.module').then( m => m.GamesPageModule)
  },
  {
    path: 'movies',
    loadChildren: () => import('./movies/movies.module').then( m => m.MoviesPageModule)
  },
  {
    path: 'threesixty-videos',
    loadChildren: () => import('./threesixty-videos/threesixty-videos.module').then( m => m.ThreesixtyVideosPageModule)
  },
  {
    path: 'videos',
    loadChildren: () => import('./videos/videos.module').then( m => m.VideosPageModule)
  },
  {
    path: 'createmessage',
    loadChildren: () => import('./createmessage/createmessage.module').then( m => m.CreatemessagePageModule)
  },
  {
    path: 'new-personal-message',
    loadChildren: () => import('./new-personal-message/new-personal-message.module').then( m => m.NewPersonalMessagePageModule)
  },
 
  {
    path: 'feedback-fivesmiley',
    loadChildren: () => import('./feedback-fivesmiley/feedback-fivesmiley.module').then( m => m.FeedbackFivesmileyPageModule)
  },
  
  {
    path: 'play-game',
    loadChildren: () => import('./play-game/play-game.module').then( m => m.PlayGamePageModule)
  },
  {
    path: 'select-language',
    loadChildren: () => import('./select-language/select-language.module').then( m => m.SelectLanguagePageModule)
  },
  {
    path: 'survey-fivesmiley',
    loadChildren: () => import('./survey-fivesmiley/survey-fivesmiley.module').then( m => m.SurveyFivesmileyPageModule)
  },
  {
    path: 'others',
    loadChildren: () => import('./others/others.module').then( m => m.OthersPageModule)
  },
  {
    path: 'music',
    loadChildren: () => import('./music/music.module').then( m => m.MusicPageModule)
  },
  {
    path: 'open-media',
    loadChildren: () => import('./open-media/open-media.module').then( m => m.OpenMediaPageModule)
  },
  
  {
    path: 'choose-consumer',
    loadChildren: () => import('./choose-consumer/choose-consumer.module').then( m => m.ChooseConsumerPageModule)
  },
  {
    path: 'callbooking',
    loadChildren: () => import('./callbooking/callbooking.module').then( m => m.CallbookingPageModule)
  },
  {
    path: 'subfolderfiles',
    loadChildren: () => import('./subfolderfiles/subfolderfiles.module').then( m => m.SubfolderfilesPageModule)
  },
  {
    path: 'choosedate',
    loadChildren: () => import('./choosedate/choosedate.module').then( m => m.ChoosedatePageModule)
  },
  {
    path: 'callbooking-success',
    loadChildren: () => import('./callbooking-success/callbooking-success.module').then( m => m.CallbookingSuccessPageModule)
  },
  {
    path: 'confirm-callbooking',
    loadChildren: () => import('./confirm-callbooking/confirm-callbooking.module').then( m => m.ConfirmCallbookingPageModule)
  },
  {
    path: 'bookedcall-list',
    loadChildren: () => import('./bookedcall-list/bookedcall-list.module').then( m => m.BookedcallListPageModule)
  },
  {
    path: 'maintenance-list',
    loadChildren: () => import('./maintenance-list/maintenance-list.module').then( m => m.MaintenanceListPageModule)
  },
  {
    path: 'maintenance-details',
    loadChildren: () => import('./maintenance-details/maintenance-details.module').then( m => m.MaintenanceDetailsPageModule)
  },
  {
    path: 'maintenance-count',
    loadChildren: () => import('./maintenance-count/maintenance-count.module').then( m => m.MaintenanceCountPageModule)
  },
  
  {
    path: 'create-maintenance-request',
    loadChildren: () => import('./create-maintenance-request/create-maintenance-request.module').then( m => m.CreateMaintenanceRequestPageModule)
  },
  {
    path: 'consumer-maintenance-details',
    loadChildren: () => import('./consumer-maintenance-details/consumer-maintenance-details.module').then( m => m.ConsumerMaintenanceDetailsPageModule)
  },
  {
    path: 'maintenance-comment',
    loadChildren: () => import('./maintenance-comment/maintenance-comment.module').then( m => m.MaintenanceCommentPageModule)
  },
  {
    path: 'technician-new-request',
    loadChildren: () => import('./technician-new-request/technician-new-request.module').then( m => m.TechnicianNewRequestPageModule)
  },
  {
    path: 'select-category',
    loadChildren: () => import('./select-category/select-category.module').then( m => m.SelectCategoryPageModule)
  },
  {
    path: 'select-feedback-type',
    loadChildren: () => import('./select-feedback-type/select-feedback-type.module').then( m => m.SelectFeedbackTypePageModule)
  },
  
  {
    path: 'poll',
    loadChildren: () => import('./poll/poll.module').then( m => m.PollPageModule)
  },
  {
    path: 'newfeedback',
    loadChildren: () => import('./newfeedback/newfeedback.module').then( m => m.NewfeedbackPageModule)
  },
  
  {
    path: 'checkout-ffmember-name',
    loadChildren: () => import('./checkout-ffmember-name/checkout-ffmember-name.module').then( m => m.CheckoutFfmemberNamePageModule)
  },
  {
    path: 'checkout-declaration',
    loadChildren: () => import('./checkout-declaration/checkout-declaration.module').then( m => m.CheckoutDeclarationPageModule)
  },
  
  {
    path: 'checkout-thanks',
    loadChildren: () => import('./checkout-thanks/checkout-thanks.module').then( m => m.CheckoutThanksPageModule)
  },
  
  {
    path: 'checkin-choose-consumer',
    loadChildren: () => import('./checkin-choose-consumer/checkin-choose-consumer.module').then( m => m.CheckinChooseConsumerPageModule)
  },
  {
    path: 'checkin-scan-qr',
    loadChildren: () => import('./checkin-scan-qr/checkin-scan-qr.module').then( m => m.CheckinScanQrPageModule)
  },
  {
    path: 'checkin-visit-purpose',
    loadChildren: () => import('./checkin-visit-purpose/checkin-visit-purpose.module').then( m => m.CheckinVisitPurposePageModule)
  },
  {
    path: 'checkin-additional-visitors',
    loadChildren: () => import('./checkin-additional-visitors/checkin-additional-visitors.module').then( m => m.CheckinAdditionalVisitorsPageModule)
  },
  {
    path: 'checkin-add-additional-visitors',
    loadChildren: () => import('./checkin-add-additional-visitors/checkin-add-additional-visitors.module').then( m => m.CheckinAddAdditionalVisitorsPageModule)
  },
  {
    path: 'checkin-health-declaration',
    loadChildren: () => import('./checkin-health-declaration/checkin-health-declaration.module').then( m => m.CheckinHealthDeclarationPageModule)
  },
  {
    path: 'checkin-vis-guidelines',
    loadChildren: () => import('./checkin-vis-guidelines/checkin-vis-guidelines.module').then( m => m.CheckinVisGuidelinesPageModule)
  },
  {
    path: 'confirm-checkin',
    loadChildren: () => import('./confirm-checkin/confirm-checkin.module').then( m => m.ConfirmCheckinPageModule)
  },
  {
    path: 'feedback-type',
    loadChildren: () => import('./feedback-type/feedback-type.module').then( m => m.FeedbackTypePageModule)
  },
  
  {
    path: 'visit-booking',
    loadChildren: () => import('./visit-booking/visit-booking.module').then( m => m.VisitBookingPageModule)
  },
  {
    path: 'create-visit-booking',
    loadChildren: () => import('./create-visit-booking/create-visit-booking.module').then( m => m.CreateVisitBookingPageModule)
  },
  {
    path: 'checkin-vaccine-info',
    loadChildren: () => import('./checkin-vaccine-info/checkin-vaccine-info.module').then( m => m.CheckinVaccineInfoPageModule)
  },
  {
    path: 'checkin-additional-info',
    loadChildren: () => import('./checkin-additional-info/checkin-additional-info.module').then( m => m.CheckinAdditionalInfoPageModule)
  },
  {
    path: 'checkin-additional-vaccine-info',
    loadChildren: () => import('./checkin-additional-vaccine-info/checkin-additional-vaccine-info.module').then( m => m.CheckinAdditionalVaccineInfoPageModule)
  },
  {
    path: 'menu-booking',
    loadChildren: () => import('./menu-booking/menu-booking.module').then( m => m.MenuBookingPageModule)
  },
  
  {
    path: 'checkin-phone-num',
    loadChildren: () => import('./checkin-phone-num/checkin-phone-num.module').then( m => m.CheckinPhoneNumPageModule)
  },
  {
    path: 'consumer-contact',
    loadChildren: () => import('./consumer-contact/consumer-contact.module').then( m => m.ConsumerContactPageModule)
  },
  {
    path: 'consumer-gallery',
    loadChildren: () => import('./consumer-gallery/consumer-gallery.module').then( m => m.ConsumerGalleryPageModule)
  },
  {
    path: 'overview',
    loadChildren: () => import('./overview/overview.module').then( m => m.OverviewPageModule)
  },
  {
    path: 'resident-overview',
    loadChildren: () => import('./resident-overview/resident-overview.module').then( m => m.ResidentOverviewPageModule)
  },
  {
    path: 'dining-menu',
    loadChildren: () => import('./dining-menu/dining-menu.module').then( m => m.DiningMenuPageModule)
  },
  {
    path: 'dining-view-menu',
    loadChildren: () => import('./dining-view-menu/dining-view-menu.module').then( m => m.DiningViewMenuPageModule)
  },
  {
    path: 'dining-selectedmenu-list',
    loadChildren: () => import('./dining-selectedmenu-list/dining-selectedmenu-list.module').then( m => m.DiningSelectedmenuListPageModule)
  },
  {
    path: 'dining-take-order',
    loadChildren: () => import('./dining-take-order/dining-take-order.module').then( m => m.DiningTakeOrderPageModule)
  },
  {
    path: 'dining-menu-fi-details',
    loadChildren: () => import('./dining-menu-fi-details/dining-menu-fi-details.module').then( m => m.DiningMenuFiDetailsPageModule)
  },
  {
    path: 'din-continental-breakfast',
    loadChildren: () => import('./din-continental-breakfast/din-continental-breakfast.module').then( m => m.DinContinentalBreakfastPageModule)
  },
  {
    path: 'din-continental-fi-details',
    loadChildren: () => import('./din-continental-fi-details/din-continental-fi-details.module').then( m => m.DinContinentalFiDetailsPageModule)
  },
  {
    path: 'din-choose-season-date',
    loadChildren: () => import('./din-choose-season-date/din-choose-season-date.module').then( m => m.DinChooseSeasonDatePageModule)
  },
  {
    path: 'onboarding-p1',
    loadChildren: () => import('./onboarding-p1/onboarding-p1.module').then( m => m.OnboardingP1PageModule)
  },
  {
    path: 'onboarding-p2',
    loadChildren: () => import('./onboarding-p2/onboarding-p2.module').then( m => m.OnboardingP2PageModule)
  },
  {
    path: 'onboarding-p3',
    loadChildren: () => import('./onboarding-p3/onboarding-p3.module').then( m => m.OnboardingP3PageModule)
  },
  {
    path: 'onboarding-p4',
    loadChildren: () => import('./onboarding-p4/onboarding-p4.module').then( m => m.OnboardingP4PageModule)
  },
  {
    path: 'onboarding-welcome',
    loadChildren: () => import('./onboarding-welcome/onboarding-welcome.module').then( m => m.OnboardingWelcomePageModule)
  },
  {
    path: 'resident-directory',
    loadChildren: () => import('./resident-directory/resident-directory.module').then( m => m.ResidentDirectoryPageModule)
  },

  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-createmessage',
  templateUrl: './createmessage.page.html',
  styleUrls: ['./createmessage.page.scss'],
})
export class CreatemessagePage implements OnInit {
contacts:any=[];
staff:any=[];
user_type:any;
subscription:Subscription;
branch:any;
offset:any = 0;
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,private router:Router,
    private platform:Platform) { }

  ngOnInit() {
  }


async ionViewWillEnter(){
  // this.storage.ready().then(()=>{
    const name=await this.storage.get('BRANCH_NAME');
      this.branch=name
    // })
    const data=await this.storage.get('USER_TYPE');
      this.user_type=data;
      if(data==6){
        this.getFamily();
        
      }else if(data==5){
        this.getContacts();
        
      }
    // })
    this.staff=[];
    this.getStaffContacts(false,'');
  // });
  
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/message']) ;
  }); 

}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}


// get contacts for family login
async getFamily(){
  this.contacts=[];
  // this.storage.ready().then(()=>{
    const uid=await this.storage.get('USER_ID');
      const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
        let url=this.config.domain_url+'showfamily/'+uid;
        let headers=await this.config.getHeader();
      // let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log("fam:",res,url);
        
        // res.data.forEach(ele=>{
          res.data[0].family.forEach(element => {
            if(element.parent_details[0].user.status!=0){
            this.contacts.push({'primary':0,'rel':element.relation,'user':element.parent_details[0].user});
            }
          //   let url1=this.config.domain_url+'showcontacts/'+element.parent_details[0].user.user_id;
          
          // this.http.get(url1,{headers}).subscribe((res:any)=>{
          //   console.log("confam:",res,url);
          //   res.data.forEach(ele => {
          //     if(ele.user&&ele.user.user_id!=uid&&this.contacts.map(x=>x.user.user_id).indexOf(ele.user.user_id)<0&&ele.user.status!=0){
          //     this.contacts.push({'primary':ele.is_primary,'rel':ele.relation,'user':ele.user});
          //     }
              
          //   });
          // })
          });



          


        // })
            
        
        })
  //     })
  //     })
  //   })
  // })
  console.log("arr:",this.contacts);
  
}
// get contacts for resident login
async getContacts(){
  this.contacts=[];
  // this.storage.ready().then(()=>{
    const uid=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'showcontacts/'+uid;
      let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log("con:",res,url);
        res.data.forEach(element => {
          if(element.user&&element.user.status!=0){
          this.contacts.push({'primary':element.is_primary,'rel':element.relation,'user':element.user});
          }
        });
        console.log("arr:",this.contacts);
      })
  //   })
  // })
  
}
doInfinite(event) {
   
 
    this.getStaffContacts(true,event);
  
  }

// get staff for resident login
async getStaffContacts(isFirstLoad,event){

  // this.storage.ready().then(()=>{
    const uid=await this.storage.get('USER_ID');
      const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
        let url=this.config.domain_url+'showstaff';
        let headers= await this.config.getHeader();
        let body;
        body={
          user_one:uid,
          nw_id:cid,
          branch_id:bid,
          skip:this.offset,
            take:20
        }
        // if(this.terms){
        //   body.keyword=this.terms
        // }
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log("staff:",res,body);
          for (let i = 0; i < res.data.length; i++) {
            if(res.data[i].user_type_id!=4&&res.data[i].user_type_id!=9&&res.data[i].user_type_id!=13&&res.data[i].user_type_id!=14){
              this.staff.push(res.data[i]);
            }
          }
          if (isFirstLoad)
    event.target.complete();
   
    this.offset=this.offset+20;    
        
    },error=>{
    
      console.log(error);
    });
        

}






openChat(item){
    this.router.navigate(['/new-personal-message',{user_two:item.user.user_id,pic:item.user.profile_pic,name:item.user.name,role:item.rel}]);
}
openStaffChat(item){
  let role=null
  if(item.user_type_id==1){
    role='Admin'
  }else if(item.user_details!=null){
    role=item.user_details.title
  }
  this.router.navigate(['/new-personal-message',{user_two:item.user_id,pic:item.profile_pic,name:item.name,role:role}]);
}

}

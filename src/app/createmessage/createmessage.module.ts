import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreatemessagePageRoutingModule } from './createmessage-routing.module';

import { CreatemessagePage } from './createmessage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreatemessagePageRoutingModule
  ],
  declarations: [CreatemessagePage]
})
export class CreatemessagePageModule {}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-onboarding-p2',
  templateUrl: './onboarding-p2.page.html',
  styleUrls: ['./onboarding-p2.page.scss'],
})
export class OnboardingP2Page implements OnInit {
info:any;
password:any;
confirmPassword:any;
passwordForm:FormGroup;
subscription:Subscription;
  constructor(private router:Router,private route:ActivatedRoute,private toast:ToastController,
    private storage:Storage,private platform:Platform) { }

  ngOnInit() {

    this.passwordForm=new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!,%,&,@,#,$,^,*,?,_,~])/)]),
     
      confirmPassword : new FormControl('',[Validators.required, Validators.minLength(8),Validators.pattern(/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!,%,&,@,#,$,^,*,?,_,~])/)])
    });
  }
  ionViewWillEnter(){
      this.info=JSON.parse(this.route.snapshot.paramMap.get('info'));
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    
        this.back();
      });     
    }
    ionViewDidLeave(){
       
        this.subscription.unsubscribe();
    }
    back(){
      this.router.navigate(['/onboarding-p1',{info:JSON.stringify(this.info)}])
    }

  async next(){
    const data=await this.storage.get('USER_TYPE');
    console.log('pass:',this.password);
   
   
    if(!this.password){
      this.presentToast('Please enter your password.');
    }else if(this.password&&!this.confirmPassword){
      this.presentToast('Please re enter your password.');
    }else if(this.password==this.confirmPassword){
      if(this.passwordForm.valid){
      this.info.password=this.password;
      if(data==6){
      this.router.navigate(['/onboarding-p3',{info:JSON.stringify(this.info)}]);
      }else{
        this.router.navigate(['/onboarding-p4',{info:JSON.stringify(this.info)}]);
      }
    }else{
      this.presentToast("Incorrect password format.")
    }
    }else{
      this.presentToast("Confirmed password doesn't match with new password")
    }
  
  }

  async presentToast(mes) {
    const alert = await this.toast.create({
      message: mes,
      duration: 3000,
      cssClass: 'toast-mess',
      position: 'top'
    });
    alert.present();
  }
  async skip(){
    const data=await this.storage.get('USER_TYPE');
    this.info.password=null;
      if(data==6){
      this.router.navigate(['/onboarding-p3',{info:JSON.stringify(this.info)}]);
      }else{
        this.router.navigate(['/onboarding-p4',{info:JSON.stringify(this.info)}]);
      }
  }

//   checkStrength(password) {
//     var strength = 0;
//     var text=document.getElementById('result');
//     var bar=document.getElementById('password-strength');
//     var character=document.getElementById('one-char').children[0];
//     var num=document.getElementById('one-num').children[0];
//     var special=document.getElementById('one-special-char').children[0];
//     var len=document.getElementById('eight-char').children[0];
   



//    //If password contains both lower and uppercase characters, increase strength value.
//    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
//        strength += 1;
//         character.setAttribute('name','checkmark')
      
//    } else{
//     character.setAttribute('name','ellipse-outline')
//    }

//    //If it has numbers and characters, increase strength value.
//    if (password.match(/([0-9])/)) {
//        strength += 1;
//        num.setAttribute('name','checkmark')
      
//       } else{
//         num.setAttribute('name','ellipse-outline')
//       }

//    //If it has one special character, increase strength value.
//    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
//        strength += 1;
//        special.setAttribute('name','checkmark')
      
//       } else{
//         special.setAttribute('name','ellipse-outline')
//       }

//    if (password.length > 7) {
//        strength += 1;
//        len.setAttribute('name','checkmark')
      
//       } else{
//         len.setAttribute('name','ellipse-outline')
//       }

//    // If value is less than 2
//    if (strength < 2) {
//       text.classList.remove('weak-pswd', 'strong-pswd');
     
//       bar.classList.remove('pswd-strong', 'p-strong', 'pswd-weak', 'p-weak');
//        bar.classList.add('pswd-very-weak', 'p-v-weak');
//        text.classList.add('vweak-pswd');
//        text.textContent='Very weak';
      
//    } else if (strength == 2) {
//       //  $('#result').addClass('good');
//        bar.classList.remove('pswd-very-weak', 'p-v-weak', 'pswd-strong', 'p-strong');
//        bar.classList.add('pswd-weak', 'p-weak');
//        text.classList.remove('vweak-pswd', 'strong-pswd')
//        text.classList.add('weak-pswd');
//        text.textContent='Weak';
//    } else if (strength == 4) {
//     text.classList.remove('vweak-pswd', 'weak-pswd')
//     text.classList.add('strong-pswd');
//     bar.classList.remove('pswd-very-weak', 'p-v-weak', 'pswd-weak', 'p-weak');
//     bar.classList.add('pswd-strong', 'p-strong');
//     text.textContent='Strong';

//        return true;
//    }

//    return false;
// }


checkStrength(password) {
  var strength = 0;
  let passed:any = {
   'special':false,
   'alpa':false,
   'num':false,
   'len':false,
  }
  var text = document.getElementById('result');
  var bar = document.getElementById('password-strength');
  var character = document.getElementById('one-char').children[0];
  var num = document.getElementById('one-num').children[0];
  var special = document.getElementById('one-special-char').children[0];
  var len = document.getElementById('eight-char').children[0];

  //If password contains both lower and uppercase characters, increase strength value.
  if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
   // strength += 1;
   passed.alpa =true
   character.setAttribute('name', 'checkmark');
  } else {
   passed.alpa =false
   character.setAttribute('name', 'ellipse-outline');
  }

  //If it has numbers and characters, increase strength value.
  if (password.match(/([0-9])/)) {
   // strength += 1;
   passed.num = true
   num.setAttribute('name', 'checkmark');
  } else {
   passed.num = false
   num.setAttribute('name', 'ellipse-outline');
  }

  //If it has one special character, increase strength value.
  if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
   // strength += 1;
   passed.special = true
   special.setAttribute('name', 'checkmark');
  } else {
   passed.special = false
   special.setAttribute('name', 'ellipse-outline');
  }

  if (password.length > 7) {
   // strength += 1;
   passed.len =true
   len.setAttribute('name', 'checkmark');
  } else {
   passed.len =false
   len.setAttribute('name', 'ellipse-outline');
  }

  strength = 0;
  for (const key in passed) {
   if (passed.hasOwnProperty(key) && passed[key] === true) {
      strength++;
   }
  }
  console.log(strength)

  if(strength<=0 ){
   text.textContent =''
   text.classList.value =''
   bar.classList.value =''
  }
  // }else if(strength ==0 && password.length>0){
  //  text.textContent = 'Very weak';
  //  text.classList.value ='';
  //  text.classList.add('vweak-pswd');
  //  bar.classList.value ='';
  // }
  else if(strength ==1){
   text.classList.value =''
   bar.classList.value='';
   bar.classList.add('pswd-very-weak', 'p-v-weak');
   text.classList.add('vweak-pswd')
   text.textContent = 'Very weak';
  }else if(strength ==2){
   text.classList.value =''
   bar.classList.value='';
   bar.classList.add('pswd-weak', 'p-weak');
   text.classList.add('weak-pswd')
   text.textContent = 'Weak';
  }
  else if(strength ==3){
   text.classList.value =''
   bar.classList.value='';
   bar.classList.add('pswd-strong', 'p-strong');
   text.classList.add('strong-pswd')
   text.textContent = 'Strong';
  }
  else if(strength ==4){
   text.classList.value =''
   bar.classList.value='';
   bar.classList.add('pswd-very-strong', 'p-v-strong');
   text.classList.add('vstrong-pswd')
   text.textContent = 'Very strong';
   return true;
  }


  return false;
}
}

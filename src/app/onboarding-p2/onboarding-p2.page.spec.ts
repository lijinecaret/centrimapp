import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OnboardingP2Page } from './onboarding-p2.page';

describe('OnboardingP2Page', () => {
  let component: OnboardingP2Page;
  let fixture: ComponentFixture<OnboardingP2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnboardingP2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OnboardingP2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

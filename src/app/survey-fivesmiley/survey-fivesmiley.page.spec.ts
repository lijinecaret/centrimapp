import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SurveyFivesmileyPage } from './survey-fivesmiley.page';

describe('SurveyFivesmileyPage', () => {
  let component: SurveyFivesmileyPage;
  let fixture: ComponentFixture<SurveyFivesmileyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyFivesmileyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SurveyFivesmileyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SurveyFivesmileyPageRoutingModule } from './survey-fivesmiley-routing.module';

import { SurveyFivesmileyPage } from './survey-fivesmiley.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SurveyFivesmileyPageRoutingModule
  ],
  declarations: [SurveyFivesmileyPage]
})
export class SurveyFivesmileyPageModule {}

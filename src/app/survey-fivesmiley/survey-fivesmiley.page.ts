import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Platform, ModalController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Storage } from '@ionic/storage-angular';
import { ThankyouComponent } from '../components/thankyou/thankyou.component';

@Component({
  selector: 'app-survey-fivesmiley',
  templateUrl: './survey-fivesmiley.page.html',
  styleUrls: ['./survey-fivesmiley.page.scss'],
})
export class SurveyFivesmileyPage implements OnInit {
  sid:any;
  lid:any;
  cid:any;
  id:any;
  questions:any=[];
  subscription:Subscription;
  noOfItem:number=1;
  que_id:string='';
  opt_id:string='';
  selected:any;
  feedback:any;
  suggestion:boolean=true;
  feed_suggestion:any=[];
    constructor(private router:Router,private orient:ScreenOrientation,private route:ActivatedRoute,private storage:Storage,
      private http:HttpClient,private config:HttpConfigService,private platform:Platform,private modalCntlr:ModalController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.orient.lock(this.orient.ORIENTATIONS.LANDSCAPE);
   
      this.sid=this.route.snapshot.paramMap.get("sid");
      this.lid=this.route.snapshot.paramMap.get("lid");
      // this.storage.ready().then(()=>{
        const dat=await this.storage.get('USER_ID');
          this.id=dat.toString();
        const res=await this.storage.get('SURVEY_CID');
          this.cid=res.toString();

          let url=this.config.feedback_url+'get_survey_questions';
          // let body={
          //   company_id:14,
          //   user_id:3,
          //   survey_bid:8,
          //   language_id:1
          // }
          let body = `company_id=${this.cid}&user_id=${this.id}&survey_bid=${this.sid}&language_id=${this.lid}`;
         
          console.log("body:",body);
          
          let headers=new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          })

          this.http.post(url,body,{headers}).subscribe((data:any)=>{
            this.questions=data.qanda
            console.log(data);
            
          })
      //   })
      //   })
      // })

      
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/survey']) ;
    });               

  }
  next(){
    if(this.noOfItem <= this.questions.length){ 
      
      this.noOfItem += 1; 
      if(this.noOfItem>this.questions.length){
        this.suggestion=true;
        this.getSuggestion();
              
      }
    } else { 
      console.log('END OF DAY 1') 
    }
    // console.log("no:",this.noOfItem,"len:",this.questions.length)
  }
  previous(){
    if(this.noOfItem <= this.questions.length){ 
      if(this.noOfItem!=1){
      this.noOfItem += -1; 
      }else{
        this.noOfItem=1;
      }
    } else { 
      console.log('END OF DAY 1') 
    }
    console.log("no:",this.noOfItem,"len:",this.questions.length)
  }
  ionViewDidLeave(){
    this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
  
      this.subscription.unsubscribe();
  }



  getOption(item,id){
    if(this.que_id==''){
      this.que_id=id;
    }else{
      this.que_id=this.que_id+'*'+id;
    }
    if(this.opt_id==''){
      this.opt_id=item.id;
    }else{
      this.opt_id=this.opt_id+'*'+item.id;
    }
    if(this.noOfItem <= this.questions.length){ 
      
      this.noOfItem += 1; 
      if(this.noOfItem>this.questions.length){
        this.suggestion=false;
        this.getSuggestion();
              console.log("sug:",this.suggestion);
      }
    } else { 
      console.log('END OF DAY 1') 
    }
    this.selected=item.id;
    console.log("que:",this.que_id,"opt:",this.opt_id);
    
  }
async getSuggestion(){
  // this.storage.ready().then(()=>{
         
    const res=await this.storage.get('SURVEY_CID');
      this.cid=res;

      let url=this.config.feedback_url+'get_suggestion';
      // let body={
      //   company_id:14,
      //   user_id:3,
      //   survey_bid:8,
      //   language_id:1
      // }
      let body = `company_id=${this.cid}&question_id=${this.que_id}&option_id=${this.opt_id}`;
     
      console.log("body:",body);
      
      let headers=new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      })

      this.http.post(url,body,{headers}).subscribe((data:any)=>{
        this.feed_suggestion=data.feedback_text
        console.log(data);
        
    
  })
//   })
// })
}


setSuggestion(item){
  this.feedback=item;
}
async submit(){
  // this.storage.ready().then(()=>{
         
    const res=await this.storage.get('SURVEY_CID');
      this.cid=res;
      const dat=await this.storage.get('USER_ID');
        this.id=dat;
      let url=this.config.feedback_url+'saveSurveyResponse';
      // let body={
      //   company_id:14,
      //   user_id:3,
      //   survey_bid:8,
      //   language_id:1
      // }
      let body = `company_id=${this.cid}&feedback=${this.feedback}&question_id=${this.que_id}&option_id=${this.opt_id}&user_id=${this.id}&survey_branch_id=${this.sid}`;
     
      console.log("body:",body);
      
      let headers=new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      })

      this.http.post(url,body,{headers}).subscribe((data:any)=>{
        
        console.log(data);
        this.openModal();
        
      })
    
//   })
//   })
// })
}


async openModal() {
  const modal = await this.modalCntlr.create({
    component: ThankyouComponent,
    componentProps: {
      "flag":1
    }
  });
  return await modal.present();
}

}

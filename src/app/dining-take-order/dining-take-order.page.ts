import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { AdditionalItemDetailsComponent } from '../components/dining/additional-item-details/additional-item-details.component';
import { AllergicInfoComponent } from '../components/dining/allergic-info/allergic-info.component';

import { ChooseDateComponent } from '../components/dining/choose-date/choose-date.component';
import { ContinentalItemComponent } from '../components/dining/continental-item/continental-item.component';
import { DietaryPreferencesComponent } from '../components/dining/dietary-preferences/dietary-preferences.component';
import { MarkLeaveSingledayComponent } from '../components/dining/mark-leave-singleday/mark-leave-singleday.component';
import { MenuItemDetailsComponent } from '../components/dining/menu-item-details/menu-item-details.component';
import { MenuOptionsComponent } from '../components/dining/menu-options/menu-options.component';
import { OrderConfirmationComponent } from '../components/dining/order-confirmation/order-confirmation.component';
import { OrderSuccessMessageComponent } from '../components/dining/order-success-message/order-success-message.component';
import { HttpConfigService } from '../services/http-config.service';
import { NoItemAlertComponent } from '../components/dining/no-item-alert/no-item-alert.component';

@Component({
  selector: 'app-dining-take-order',
  templateUrl: './dining-take-order.page.html',
  styleUrls: ['./dining-take-order.page.scss'],
})
export class DiningTakeOrderPage implements OnInit {
  subscription:Subscription;
  menu:any;
  currentDate:any;
  start:any;
  end:any;
  dates:any[]=[];
  date:any;
  currentIdx:any;
  sliderOne: any;
// slidenum:any;
// slideOptsOne = {
//   initialSlide: 0,
//   slidesPerView: this.checkScreen(),
  
// };
// progress:any=[];
// viewEntered:boolean=false;
sel_additional_item:any[]=[];
  hide_footer:boolean=false;
  selected:any[]=[];
  menuItems:any[]=[];
  skipped:boolean=false;
  leaveMarked:boolean=false;
  editOrder:boolean=false;
  preOrderedItems:any[]=[];
  serve_area:any[]=[];
  additional_by_ser_time:any=[];
  details:any;
  allergies:any[]=[];
  mealSize:any;
  consumer_name:any;
  tz:any;
 show_allergies:boolean=false;
  constructor(private router:Router,private platform:Platform,private route:ActivatedRoute,private http:HttpClient,
    private storage:Storage,private config:HttpConfigService,private loadingCtrl:LoadingController,
    private modalCntrl:ModalController,private popCntrl:PopoverController,private toastCntlr:ToastController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.dates=[];
    let menu=this.route.snapshot.paramMap.get('menu')
    this.menu=JSON.parse(menu);
    this.date = this.route.snapshot.paramMap.get('date');
    
    this.tz = await this.storage.get('TIMEZONE');

    this.showLoading();
    // this.getDiningDetails();
    this.getDietaryProfile();
    console.log('menu:',this.menu,menu);
    this.currentDate=moment().format('DD MMM YYYY');
    // this.date=this.currentDate;
    // this.showLoading();
    if (moment(this.date).isSameOrAfter(moment(this.currentDate))) {
      this.hide_footer = false
    } else {
      this.hide_footer = true
    }

    this.start=moment(this.menu.start_date);
    this.end=moment(this.menu.enddate);
    var start=this.start.clone();
    while(start.isSameOrBefore(this.end)){
      this.dates.push(start.format('DD MMM YYYY'));
      start.add(1,'days');
    }
    let date = moment(this.date).format('DD MMM YYYY')
    this.currentIdx = this.dates.map(item => item).indexOf(date);

    console.log('dates:',this.dates);
    
    // this.sliderOne =
    // {
    //   isBeginningSlide: true,
    //   isEndSlide: false,
    //   isActive:false,
    //   dates:[]=this.dates
    // };
   
    this.getDetails();
    console.log(this.leaveMarked);
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
   this.back();
   }); 
   
   }
   ionViewWillLeave() { 
   this.subscription.unsubscribe();
   }
   back(){
    // this.router.navigate(['/dining-menu']);
    this.router.navigate(['din-choose-season-date',{menu:JSON.stringify(this.menu),flag:1}],{replaceUrl:true})
   }

  //  checkScreen(){
  //   if(window.innerWidth>=700){
  //       return 6;
  //   }else{
  //       return 3;
  //   }
  // }

  // async getDiningDetails(){
   
  //   const cid=await this.storage.get('COMPANY_ID');
  //   const bid=await this.storage.get('BRANCH');
  //   const tz=await this.storage.get('TIMEZONE');
  //   let headers= await this.config.getHeader();
  //   let url=this.config.domain_url+'get_dining_ordering_details';
  //   const type=await this.storage.get('USER_TYPE');
  //   const uid=await this.storage.get('USER_ID');
  //   let consumer;
  //   if(type==5){
  //     consumer=uid
  //   }else{
  //     consumer= await this.storage.get('RESIDENT_ID');
  //   }
  //   let body={
  //     company_id:cid,
  //     dining_id:this.menu.id,
  //     period_status:true,
  //     consumer_user_id:consumer
  //   }
  //   this.http.post(url,body,{headers}).subscribe((res:any)=>{
  //     console.log('dining:',res);
  //     this.progress=res.data.period_progress;
  //     this.viewEntered=true;
  //   },error=>{
      
  //   });
  // }
  async showLoading() {
      
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      
      // message: 'Please wait...',
      spinner: null,
      // duration: 3000,
    });
    return await loading.present();
  }
  
  async dismissLoader() {
    
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  // SlideDidChange(object, slideView) {
  //   this.checkIfNavDisabled(object, slideView);
  //   object.isActive= true;
  // }
  // checkIfNavDisabled(object, slideView) {
  //   this.checkisBeginning(object, slideView);
  //   this.checkisEnd(object, slideView);
  // }
  
  // checkisBeginning(object, slideView) {
  //   slideView.isBeginning().then((istrue) => {
  //     object.isBeginningSlide = istrue;
  //   });
  // }
  // checkisEnd(object, slideView) {
  //   slideView.isEnd().then((istrue) => {
  //     object.isEndSlide = istrue;
  //   });
  // }

  // slidesLoaded($event) {
  //   //move to slide number 2
    
  //   $event.target.slideTo(this.currentIdx);
  //   }

  // setIndicatorLine(item){
  //   let date=moment(item).format('yyyy-MM-DD');
    
  //   const idx=this.progress.map(x=>x.date).indexOf(date);
    
  //   let i;
  //   if(this.progress[idx].color=='x-danger'||this.progress[idx].color=='x-info'){
  //     i=0
  //   }else if(this.progress[idx].color=='x-success'){
  //     i=1
  //   }else if(this.progress[idx].color=='x-warning'){
  //     i=2
  //   }
     
  //   return i;
  // }

  async chooseDate() {
    const modal = await this.modalCntrl.create({
      component: ChooseDateComponent,
      cssClass: 'din-choosedate-modal',
      componentProps: {
        menu: this.menu
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data) {
        this.date = dataReturned.data;
        
       
        this.showLoading();
        
        
        this.getDetails();
        let currentDate = moment().format('DD MMM YYYY');
        if (moment(this.date).isSameOrAfter(moment(currentDate))) {
          this.hide_footer = false
        } else {
          this.hide_footer = true
        }

      }
    });
    return await modal.present();
  }
  setDate(item,index){
    
    this.date=item;
    this.currentIdx=index;
    this.showLoading();
        
        
    this.getDetails();
    let currentDate = moment().format('DD MMM YYYY');
    if (moment(this.date).isSameOrAfter(moment(currentDate))) {
      this.hide_footer = false
    } else {
      this.hide_footer = true
    }
  }
  previous() {
    if (this.currentIdx == 0) {
      console.log('reached first item')
    } else {
      console.log('curr:', this.currentIdx, this.dates[this.currentIdx]);
      this.currentIdx--;
      console.log('changedcur:', this.currentIdx, this.dates[this.currentIdx])
      this.date = this.dates[this.currentIdx];
      
      this.selected=[];
        this.sel_additional_item=[];
      this.showLoading();
      
      this.getDetails();

      let currentDate = moment().format('DD MMM YYYY');
      if (moment(this.date).isSameOrAfter(moment(currentDate))) {
        this.hide_footer = false
      } else {
        this.hide_footer = true
      }
    }
  }
  next() {
    if (this.currentIdx == (this.dates.length - 1)) {
      console.log('reached end')
    } else {
      this.currentIdx++;
      this.date = this.dates[this.currentIdx];
     
      this.selected=[];
        this.sel_additional_item=[];
      this.showLoading();
      
      this.getDetails();

      let currentDate = moment().format('DD MMM YYYY');
      if (moment(this.date).isSameOrAfter(moment(currentDate))) {
        this.hide_footer = false
      } else {
        this.hide_footer = true
      }
    }
  }

  async getDetails(){
    this.additional_by_ser_time=[];
  
    let url = this.config.domain_url + 'day_ordering_menu_listing';
    console.log(url);
    const cid = await this.storage.get('COMPANY_ID');
    const bid = await this.storage.get('BRANCH');
    const uid = await this.storage.get('USER_ID');
    const type=await this.storage.get('USER_TYPE');
    let consumer;
    if(type==5){
      consumer=uid
    }else{
      consumer= await this.storage.get('RESIDENT_ID');
    }
    let headers=await this.config.getHeader();
    let body = {
      company_id: cid,
      consumer_id: consumer,
      dining_id: this.menu.id,
      date: moment(this.date).format('yyyy-MM-DD'),
      created_by: uid
    }
    this.http.post(url, body, { headers }).subscribe((res: any) => {

      console.log('newData:',res)
      this.menuItems = res.data.meals_with_items;
      this.consumer_name=res.data.consumer_name;
      let skipped =[];
      this.menuItems.forEach(el => {
       
        this.additional_by_ser_time.push({ item: el.additional_items, ser_time: el.serving_time_id, show: false });
        if(el.is_skipped){
          skipped.push(el);
        }
      })
      if(skipped.length==this.menuItems.length){
      this.skipped = true;
      }else{
        this.skipped=false;
      }
      this.serve_area = res.data.serve_area;
      console.log('res:', res);
      if (res.data.is_leave) {
        this.leaveMarked = true;
        this.hide_footer = true;
      } else {
        this.leaveMarked = false;
        this.hide_footer = false;
      }
      if (res.data.is_edit) {
        this.editOrder = true
      } else {
        this.editOrder = false;
      }
      this.preOrderedItems = res.data.meals_with_items;
      
      if (this.preOrderedItems.length) {
        this.preOrderedItems.map(x => {
          if (x.pre_item_orders.length) {
            x.pre_item_orders.map(y => {
              if(!this.selected.includes(y.menu_item_id)){
              this.selected.push(y.menu_item_id)
              }
            })
          }
          if (x.additional_items.length) {
            x.additional_items.map(y => {
              if (y.selected_data) {
                if(!this.sel_additional_item.includes(y.selected_data.item_id)){
                this.sel_additional_item.push(y.selected_data.item_id);
                }
              }
            })
          }
          if (x.all_coloumns.length) {
            x.all_coloumns.map(y => {
              if(y.items.length==1){
              if (y.items[0].is_ordered==true) {
                if(!this.selected.includes(y.items[0].id)){
                this.selected.push(y.items[0].id)
                }
              }
            }else{
              y.items.map(z=>{
                if(z.is_ordered==true){
                  if(!this.selected.includes(z.id)){
                  this.selected.push(z.id)
                  }
                }
              })
            }
            })
          }
        })
      }
      console.log('sele:',this.selected,this.editOrder);
      let currentDate = moment().format('DD MMM YYYY');
        if (moment(this.date).isSameOrAfter(moment(currentDate))) {
          this.hide_footer = false
        } else {
          this.hide_footer = true
        }
      this.dismissLoader();
    },error=>{
      console.log(error);
      this.dismissLoader()
    })
  }

  async getDietaryProfile(){
    const uid = await this.storage.get('USER_ID');
    const type=await this.storage.get('USER_TYPE');
    let consumer;
    if(type==5){
      consumer=uid
      console.log('consumer uid:',consumer);
    }else{
      consumer= await this.storage.get('RESIDENT_ID');
      console.log('consumer res id:',consumer);
    }
    let url = this.config.domain_url + 'ordering_consumer_profile';
      this.additional_by_ser_time = [];
      this.selected=[];
      this.sel_additional_item=[];
      const cid = await this.storage.get('COMPANY_ID');
     
      console.log('consumer:',consumer);
      let headers=await this.config.getHeader();
      let body = {
        user_id: consumer,
        company_id: cid
      }
      this.http.post(url, body, { headers }).subscribe((res: any) => {
        console.log('pro:',res);
        this.details = res.data;
       
  
        // if (this.details.selected_diets) {
        //   this.diet_type = this.details.selected_diets.split(/\, +/);
        // }
        if (this.details&&this.details.allergies) {
          this.allergies = this.details.allergies.split(/\, +/);
        }
        // if (this.details.fluid_cap_target) {
        //   this.fluid_cap_target = this.details.fluid_cap_target
        // } else {
        //   this.fluid_cap_target = 0
        // }
        if (this.details&&this.details.meal_size) {
          this.mealSize = this.details.meal_size.id;
        }
      })
  }

  async dietaryPreference() {
    const modal = await this.modalCntrl.create({
      component: DietaryPreferencesComponent,
      cssClass: 'dining-dpreference-modal',
      componentProps: {
        details: this.details
      }
    });
    modal.onDidDismiss().then((dataReturned) => {

    });
    return await modal.present();
  }

  async options(ev, item) {
  

    console.log('add:', this.additional_by_ser_time)

    let add;
    this.additional_by_ser_time.forEach(el=>{
      if(el.ser_time==item.serving_time_id){
        add=el
      }
    })
    const uid = await this.storage.get('USER_ID');
        const type=await this.storage.get('USER_TYPE');
        let consumer;
        if(type==5){
          consumer=uid
        }else{
          consumer= await this.storage.get('RESIDENT_ID');
        }

    const popover = await this.popCntrl.create({
      component: MenuOptionsComponent,
      id: 'menu_options',
      event: ev,
      backdropDismiss: true,
      cssClass: 'dining-more-options-popover',
      componentProps: {
        menu: item,
        additional:add,
        
        date: this.date,
       
        consumer_id: consumer,
       
        serve_area: this.details.serving_area_id,
        
      },
    });
    popover.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data == 1) {

      
        this.showLoading();
       
        this.getDetails();
      } else if (dataReturned.data == 2) {
      
      }

    });


    return await popover.present();
  }
  async markLeave(ev) {
    const uid = await this.storage.get('USER_ID');
        const type=await this.storage.get('USER_TYPE');
        let consumer;
        if(type==5){
          consumer=uid
        }else{
          consumer= await this.storage.get('RESIDENT_ID');
        }
    const popover = await this.popCntrl.create({
      component: MarkLeaveSingledayComponent,
      event: ev,
      backdropDismiss: true,
      cssClass: 'dining-more-options-popover',
      componentProps: {

        date: this.date,
        menu_id: this.menu.id,
        consumer_id: consumer,
        show_allergy:this.show_allergies

      },
    });
    popover.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data == 1) {
        this.leaveMarked = true;
        this.hide_footer = true;
      }else if(dataReturned.data==2){
        this.showAllergies();
      }
    });


    return await popover.present();
  }

  cutoffTime(menu) {
  
    
    
    let c;
    let d = moment.tz(new Date(), this.tz).format('YYYY-MM-DD HH:mm:ss');
    let cutoff = moment(menu.serving_date + ' ' + menu.end_time);
    let current = moment(d);

    if (current.diff(cutoff) > 0) {
      c = true
    } else {
      c = false
    }
   
    return c;
  }

  preSavingItem(i, serving_id, or, item, menu) {
    console.log('sk:',!menu.is_skipped)
        if (i.subitem == 1 && !this.cutoffTime(menu)) {
          this.openContinentalDetails(i, serving_id,menu,or,item);
        } else {
          if (!this.editOrder && !menu.is_skipped) {
            if (!this.availableItem(i)||i.visibility==0) {
            } else {
              if (this.cutoffTime(menu)) {
                console.log('Cutoff time over')
              } else {
                console.log('item:', item)
               
                  if (this.selected.includes(i.id)) {
                  
                    this.selectItem(i, serving_id, 2,or,item)
                  } else {
                    if (this.allergicIcon(i)) {
                      this.allergicAlert(i.itemdetails.alergies, i, serving_id,or,item);
                    } else {
                      this.selectItem(i, serving_id, 1,or,item);
                     
                    }
                  }
               
              }
            }
          }
        }
      }

      async selectItem(i, serving_id, u,or,item) {
  
  
        const cid = await this.storage.get('COMPANY_ID');
        const bid = await this.storage.get('BRANCH');
        const uid = await this.storage.get('USER_ID');
        const type=await this.storage.get('USER_TYPE');
        let consumer;
        if(type==5){
          consumer=uid
        }else{
          consumer= await this.storage.get('RESIDENT_ID');
        }
        let url = this.config.domain_url + 'presave_ordering_item';
        let headers=await this.config.getHeader();
        let body;
        body = {
          consumer_id: consumer,
          company_id: cid,
          dining_id: this.menu.id,
          date: moment(this.date).format('YYYY-MM-DD'),
          created_by: uid,
          menu_item_id: i.id,
          serving_time_id: serving_id
        }
        if (u == 2) {
          body.uncheck = 1
        }
       
        this.http.post(url, body, { headers }).subscribe((res: any) => {
          console.log('presave:', res, body,u,i.id);
          if (res.status == 'success') {
            if (or == 2) {
             
                item.items.map(x => {
                  if (this.selected.includes(x.id)) {
                    var index = this.selected.indexOf(x.id);
                    this.selected.splice(index, 1);
                  }
                })
              }
            if (u == 1) {
              if (!this.selected.includes(i.id))
                this.selected.push(i.id)
            } else if (u == 2) {
              const idx = this.selected.indexOf(i.id);
              this.selected.splice(idx, 1);
            }
            this.getDetails();
          }
    
        }, error => {
    
        })
      }

      incompatable(item, serving_time_id) {
        let inc = false;
        const idx = this.preOrderedItems.map(x => x.serving_time_id).indexOf(serving_time_id);
        const Idx = this.preOrderedItems[idx].all_coloumns.map(x => x.id).indexOf(item.id);
        if (Idx < 0) {
          inc = true
        }
        return inc;
    
    
      }
    
      async openContinentalDetails(item, serving_id,menu,or,i) {
        const uid = await this.storage.get('USER_ID');
        const type=await this.storage.get('USER_TYPE');
        let consumer;
        if(type==5){
          consumer=uid
        }else{
          consumer= await this.storage.get('RESIDENT_ID');
        }
        let component;
       
        component = ContinentalItemComponent;
       
        let fdtexture,fltexture=null;
        if(this.details.food_texture){
          fdtexture=this.details.food_texture.id
        }
        if(this.details.fluid_texture){
          fltexture=this.details.fluid_texture.id
        }
    
        console.log(component)
        const modal = await this.modalCntrl.create({
    
          component: component,
          cssClass: 'full-width-modal',
          componentProps: {
            menu: item,
            date: this.date,
            menu_id: this.menu.id,
            consumer: consumer,
            con_name: this.consumer_name,
            serving_time_id: serving_id,
            fdtexture: fdtexture,
            fltexture: fltexture,
            preOrderedItems: this.preOrderedItems,
            allergies: this.allergies,
            edit: this.editOrder,
            skipped:menu.is_skipped
          }
        });
        modal.onDidDismiss().then((dataReturned) => {
          console.log(dataReturned)
          if (dataReturned.data) {
           
            if (or == 2) {
             
                i.items.map(x => {
                  if (this.selected.includes(x.id)) {
                    var index = this.selected.indexOf(x.id);
                    this.selected.splice(index, 1);
                  }
                })
              }
    if(dataReturned.role=='1'){
            this.selected.push(dataReturned.data);
    }else{
      const idx = this.selected.indexOf(dataReturned.data);
      if(idx>=0)
              this.selected.splice(idx, 1);
      
    }
    
          }
          this.getDetails();
        });
    
        return await modal.present();
      }

      async allergicAlert(allergies, i, serving_id,or,item) {
        const modal = await this.modalCntrl.create({
          component: AllergicInfoComponent,
          cssClass: 'dining-allergicinfo-modal',
          componentProps: {
            consumer: this.consumer_name,
            allergies: allergies
          }
        });
        modal.onDidDismiss().then((dataReturned) => {
          if (dataReturned.data == 2) {
            this.selectItem(i, serving_id, 1,or,item);
           
          }
        });
        return await modal.present();
      }

      allergicIcon(i) {
        let c;
        if (i.itemdetails.alergies) {
          i.itemdetails.alergies.map(x => {
    
            if (this.allergies.includes(x.alergy.alergy)) {
              c = true;
            }
           
          })
        }
        return c;
      }

      availableItem(i) {
  
        let avl;
        
        if (i.itemdetails.texture_array && i.itemdetails.texture_array.length) {
          let idx,Idx=-1;
          if(this.details.food_texture&&this.details.food_texture.id){
          idx = i.itemdetails.texture_array.map(x => x.food_texture_id).indexOf(this.details.food_texture.id);
          }
          if(this.details.fluid_texture&&this.details.fluid_texture.id){
          Idx = i.itemdetails.texture_array.map(x => x.food_texture_id).indexOf(this.details.fluid_texture.id);
          }
          if (idx >= 0 || Idx >= 0) {
            avl = true
          } else {
            avl = false
          }
    
        } else {
          avl = true
        }
    
        return avl;
      }

      setMealSize(i, servetime) {
        let m, Idx;
       
        if (this.selected.includes(i.id)) {
    
     
          const idx = this.preOrderedItems.map(x => x.serving_time_id).indexOf(servetime);
          if (this.preOrderedItems[idx].pre_item_orders.length) {
            Idx = this.preOrderedItems[idx].pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
            if (Idx >= 0) {
              if (this.preOrderedItems[idx].pre_item_orders[Idx].meal_size == 1) {
                m = 'S';
              } else if (this.preOrderedItems[idx].pre_item_orders[Idx].meal_size == 2) {
                m = 'M'
              } else if (this.preOrderedItems[idx].pre_item_orders[Idx].meal_size == 3) {
                m = 'L'
              } else if (this.preOrderedItems[idx].pre_item_orders[Idx].meal_size == 4) {
                m = 'N'
              } else {
                m = null
              }
            } else {
              m = null
            }
          }
        }
        return m;
      }
    
      setQuantity(i, servetime) {
        let q, Idx;
        
        if (this.selected.includes(i.id)) {
         
          const idx = this.preOrderedItems.map(x => x.serving_time_id).indexOf(servetime);
          if (this.preOrderedItems[idx].pre_item_orders.length) {
            Idx = this.preOrderedItems[idx].pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
            if (Idx >= 0) {
              q = this.preOrderedItems[idx].pre_item_orders[Idx].quantity;
            } else {
              q = null
            }
          }
        
    
        }
        
    
        return q;
      }
    
      setOptions(i, servetime) {
        let q, Idx;
       
        if (this.selected.includes(i.id)) {
         
          const idx = this.preOrderedItems.map(x => x.serving_time_id).indexOf(servetime);
          if (this.preOrderedItems[idx].pre_item_orders.length) {
            Idx = this.preOrderedItems[idx].pre_item_orders.map(item => item.menu_item_id).indexOf(i.id);
            if (Idx >= 0 && (this.preOrderedItems[idx].pre_item_orders[Idx].items_selected_options && this.preOrderedItems[idx].pre_item_orders[Idx].items_selected_options.length)) {
              q = this.preOrderedItems[idx].pre_item_orders[Idx].items_selected_options;
            } else {
              q = null
            }
          }
    
    
        }
    
    
        return q;
      }

      async showItemdetails(item, st, serving_id,menu) {
        const uid = await this.storage.get('USER_ID');
        const type=await this.storage.get('USER_TYPE');
        let consumer;
        if(type==5){
          consumer=uid
        }else{
          consumer= await this.storage.get('RESIDENT_ID');
        }
        if (item.subitem == 1) {
          this.openContinentalDetails(item, serving_id,menu,1,item)
        } else {
          if (!this.availableItem(item) || this.cutoffTime(menu) || menu.is_skipped||item.visibility==0) {
          } else {
          
            const modal = await this.modalCntrl.create({
              component: MenuItemDetailsComponent,
              cssClass: 'full-width-modal',
              componentProps: {
                item: item,
                date: this.date,
                serving_time: st,
                menu_id: this.menu.id,
                consumer: consumer,
                serving_time_id: serving_id,
                flag:2
               
              }
            });
            modal.onDidDismiss().then((dataReturned) => {
              if (dataReturned.data) {
              
                this.getDetails();
              }
            });
    
            return await modal.present();
          }
        }
      }

      async edit() {
        this.editOrder = false;
        const cid = await this.storage.get('COMPANY_ID');
        const bid = await this.storage.get('BRANCH');
        const uid = await this.storage.get('USER_ID');
        const type=await this.storage.get('USER_TYPE');
        let consumer;
        if(type==5){
          consumer=uid
        }else{
          consumer= await this.storage.get('RESIDENT_ID');
        }
        let headers=await this.config.getHeader();
    
    
    
    
        let url = this.config.domain_url + 'enable_dining_order_via_date';
    
        let body;
        body = {
          consumer_id:consumer,
          dining_id: this.menu.id,
          date: moment(this.date).format('YYYY-MM-DD'),
          created_by: uid,
    
        }
    
    
        console.log('body:', body)
        this.http.post(url, body, { headers }).subscribe((res: any) => {
          console.log('edit:', res, body);
    
    
        }, error => {
          console.log('err:', error)
        })
    
      }

      async confirm() {
        if (!this.editOrder) {
        //  console.log('confirm:',this.selected,this.menuItems.map(x=>x.all_coloumns.length>0));
        //  let no_items=true;
        //  this.menuItems.map(x=>{if(x.all_coloumns.length>0){
        //   x.all_coloumns.forEach(element => {
        //     if(element.items&&element.items.length){
        //       no_items=false
        //     }
        //   });
          
        //  }});
        //   if((!this.selected||!this.selected.length)&&!no_items){
        //     this.presentAlert('Please select atleast one item.')
        //   }else{
            this.checkStatus();
        //   }
         
        }
      }
  
      async checkStatus(){
        const cid = await this.storage.get('COMPANY_ID');
            const bid = await this.storage.get('BRANCH');
            const uid = await this.storage.get('USER_ID');
            const type=await this.storage.get('USER_TYPE');
        let consumer;
        if(type==5){
          consumer=uid
        }else{
          consumer= await this.storage.get('RESIDENT_ID');
        }
            let url = this.config.domain_url + 'dining_order_confirmation_info';
            let headers= await this.config.getHeader()
            let body = {
              consumer_id: consumer,
              dining_id: this.menu.id,
              date: moment(this.date).format('YYYY-MM-DD'),
              logged_userid:uid
            }
            this.http.post(url, body, { headers }).subscribe((res: any) => {
              console.log('confirminfo:', res, body);
        //       let no_items=true;
        //  this.menuItems.map(x=>{if(x.all_coloumns.length>0){
        //   x.all_coloumns.forEach(element => {
        //     if(element.items&&element.items.length){
        //       no_items=false
        //     }
        //   });
          
        //  }});
        //  console.log('no item status:', no_items)
              if(res.data.no_selection){
                this.presentAlert('Please select atleast one item.')
              }else if(res.data.show_alert){
                this.confirmAlert(res.data.stat)
              }else if(res.data.no_items){
                this.noItemAlert();
              }else{
                this.confirmsingledayorder();
              }
              
            }, error => {
    
            })
      }
  
      async confirmsingledayorder(){
        const cid = await this.storage.get('COMPANY_ID');
            const bid = await this.storage.get('BRANCH');
            const uid = await this.storage.get('USER_ID');
            const type=await this.storage.get('USER_TYPE');
        let consumer;
        if(type==5){
          consumer=uid
        }else{
          consumer= await this.storage.get('RESIDENT_ID');
        }
            let url = this.config.domain_url + 'confirm_single_day_order';
            let headers= await this.config.getHeader()
            let body = {
              consumer_id: consumer,
              dining_id: this.menu.id,
              date: moment(this.date).format('YYYY-MM-DD'),
              created_by: uid,
    
            }
            this.http.post(url, body, { headers }).subscribe((res: any) => {
              console.log('confirm:', res, body);
              
              this.showSuccessMessage();
              
            }, error => {
    
            })
      }

      async confirmAlert(stat) {
        const modal = await this.modalCntrl.create({
          component: OrderConfirmationComponent,
          cssClass: 'din-order-confirmation-modal',
          componentProps:{
            status:stat
          }
        });
        modal.onDidDismiss().then((dataReturned) => {
            if(dataReturned.data){
              this.hide_footer=true;
              this.editOrder=true;
              this.confirmsingledayorder();
            }
        });
        return await modal.present();
      }

      async showSuccessMessage(){
        const modal = await this.modalCntrl.create({
          component: OrderSuccessMessageComponent,
          cssClass:'full-width-modal',
          // cssClass: 'dining-allergicinfo-modal',
          componentProps: {
            
          }
        });
        modal.onDidDismiss().then((dataReturned) => {
          this.back();
        });
        return await modal.present();
      }

      async selectAdditionalItem(i, item, menu) {
        const cid = await this.storage.get('COMPANY_ID');
        const bid = await this.storage.get('BRANCH');
        const uid = await this.storage.get('USER_ID');
        let headers= await this.config.getHeader()
    
    
        
        const type=await this.storage.get('USER_TYPE');
    let consumer;
    if(type==5){
      consumer=uid
    }else{
      consumer= await this.storage.get('RESIDENT_ID');
    }
    
        let url = this.config.domain_url + 'presave_additional_ordering_item';
    
        let body;
        body = {
          consumer_id: consumer,
          dining_id: this.menu.id,
          date: moment(this.date).format('YYYY-MM-DD'),
          created_by: uid,
          item_id: i.item.id,
          serving_time_id: item.ser_time
        }
    
        if (this.sel_additional_item.includes(i.item.id)) {
          body.uncheck = 1
        }
        console.log('body:', body,this.sel_additional_item,i,item)
        this.http.post(url, body, { headers }).subscribe((res: any) => {
          const idx=this.additional_by_ser_time.findIndex(x=>x.ser_time==item.ser_time);
              const Idx=this.additional_by_ser_time[idx].item.findIndex(x=>x.id==i.id);
          console.log('presave:', res);
          if (res.status == 'success') {
            if (res.message == 'created') {

              // const idx=this.additional_by_ser_time.findIndex(x=>x.ser_time==item.ser_time);
              // const Idx=this.additional_by_ser_time[idx].item.findIndex(x=>x.id==i.id);
              console.log('slcadd:',this.additional_by_ser_time[idx],this.additional_by_ser_time[idx].item[Idx],i,item)
              this.additional_by_ser_time[idx].item[Idx].is_ordered=true;
              this.sel_additional_item.push(i.item.id);
             
            } else {
              // const idx=this.additional_by_ser_time.findIndex(x=>x.ser_time==item.ser_time);
              // const Idx=this.additional_by_ser_time[idx].item.findIndex(x=>x.id==i.id);
              console.log('slcadd:',this.additional_by_ser_time[idx],this.additional_by_ser_time[idx].item[Idx],i,item)
              this.additional_by_ser_time[idx].item[Idx].is_ordered=false;
              const id = this.sel_additional_item.indexOf(i.item.id);
      if(id>=0)
              this.sel_additional_item.splice(id, 1);
            }
     
          }
          this.getDetails();
          setTimeout(() => {
            this.additional_by_ser_time[idx].show=true
          }, 1300);
          
         
        }, error => {
          console.log('err:', error)
        })
    
      }

      async unmarkLeave() {

       
      
        const cid = await this.storage.get('COMPANY_ID');
        const bid = await this.storage.get('BRANCH');
        const uid = await this.storage.get('USER_ID');
        const type=await this.storage.get('USER_TYPE');
    let consumer;
    if(type==5){
      consumer=uid
    }else{
      consumer= await this.storage.get('RESIDENT_ID');
    }
        let url = this.config.domain_url + 'remove_leave_via_ordering';
        let headers= await this.config.getHeader()
        let body = {
          consumer_id: consumer,
          date: moment(this.date).format('YYYY-MM-DD'),
          updated_by: uid
        }
        this.http.post(url, body, { headers }).subscribe((res: any) => {

          console.log('unmark:',res);
          if(res.status=='error'){
            this.presentAlert(res.message);
          }else{
          
          this.presentAlert('Leave deleted successfully.');
          this.leaveMarked = false;
          this.hide_footer = false;
         
          this.selected=[];
            this.sel_additional_item=[];
          this.showLoading();
         
          this.getDetails();
          }
    
        }, error => {
    
        })
      
      }
      async presentAlert(mes) {
        const alert = await this.toastCntlr.create({
          message: mes,
          cssClass: 'toastStyle',
          duration: 3000,
          position:'top' 
        });
        alert.present(); //update
      }

      async skipFullMeal(){
        const cid = await this.storage.get('COMPANY_ID');
        const bid = await this.storage.get('BRANCH');
        const uid = await this.storage.get('USER_ID');
        let headers= await this.config.getHeader()
    
    
        
        const type=await this.storage.get('USER_TYPE');
    let consumer;
    if(type==5){
      consumer=uid
    }else{
      consumer= await this.storage.get('RESIDENT_ID');
    }
    
        let url = this.config.domain_url + 'skipping_day_wise_full_meal';
    
        let body;
        body = {
          company_id:cid,
          consumer_id: consumer,
          dining_id: this.menu.id,
          date: moment(this.date).format('YYYY-MM-DD'),
          created_by: uid,
          
        }
    
       
       
        this.http.post(url, body, { headers }).subscribe((res: any) => {
          console.log('skip:', res);
          this.skipped=true;
          this.showLoading();
          this.getDetails();
        })
      }

      async showAllergies(){
        // const modal = await this.modalCntrl.create({
        //   component: AllergiesComponent,
         
        //   componentProps: {
        //     allergies:this.allergies
        //   }
        // });
        // modal.onDidDismiss().then((dataReturned) => {
          
        // });
        // return await modal.present();
        this.show_allergies=!this.show_allergies;
      }

      async showAdditionalItemdetails(item, st, serving_id,menu) {
        let consumer;
        const type=await this.storage.get('USER_TYPE');
        const uid = await this.storage.get('USER_ID');
        if(type==5){
          consumer=uid
        }else{
          consumer= await this.storage.get('RESIDENT_ID');
        }
        if (item.subitem == 1) {
          this.openContinentalDetails(item, serving_id,menu,1,item)
        } else {
          if ( this.cutoffTime(menu) || menu.is_skipped) {
          } else {
            // console.log('getCom:', this.getComment(item, serving_id), this.setMealSize(item, serving_id))
            const modal = await this.modalCntrl.create({
              component: AdditionalItemDetailsComponent,
              cssClass: 'full-width-modal',
              componentProps: {
                item: item,
                date: this.date,
                serving_time: st,
                menu_id: this.menu.id,
                consumer: consumer,
                serving_time_id: serving_id,
                
               
              }
            });
            modal.onDidDismiss().then((dataReturned) => {
              if (dataReturned.data) {
                
                const id = this.sel_additional_item.indexOf(dataReturned.data);
                if(id>=0){
                       
                      }else{
                this.sel_additional_item.push(dataReturned.data);
                      }
              
              }
            });
    
            return await modal.present();
          }
        }
      }

      async noItemAlert(){
        const modal = await this.modalCntrl.create({
          component: NoItemAlertComponent,
          cssClass: 'din-order-confirmation-modal',
          componentProps:{
            date: this.date
          }
        });
        modal.onDidDismiss().then((dataReturned) => {
            if(dataReturned.data){
              this.hide_footer=true;
              this.editOrder=true;
              this.confirmsingledayorder();
            }
        });
        return await modal.present();
      }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningTakeOrderPageRoutingModule } from './dining-take-order-routing.module';

import { DiningTakeOrderPage } from './dining-take-order.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { AllergicInfoComponent } from '../components/dining/allergic-info/allergic-info.component';
import { MarkLeaveSingledayComponent } from '../components/dining/mark-leave-singleday/mark-leave-singleday.component';
import { OrderConfirmationComponent } from '../components/dining/order-confirmation/order-confirmation.component';
import { OrderSuccessMessageComponent } from '../components/dining/order-success-message/order-success-message.component';
import { MenuOptionsComponent } from '../components/dining/menu-options/menu-options.component';
import { DiningAreaComponent } from '../components/dining/dining-area/dining-area.component';
import { DietaryPreferencesComponent } from '../components/dining/dietary-preferences/dietary-preferences.component';
import { AllergiesComponent } from '../components/dining/allergies/allergies.component';
import { AdditionalItemDetailsComponent } from '../components/dining/additional-item-details/additional-item-details.component';
import { ContinentalItemDetailsComponent } from '../components/dining/continental-item-details/continental-item-details.component';
import { ContinentalItemComponent } from '../components/dining/continental-item/continental-item.component';
import { MenuItemDetailsComponent } from '../components/dining/menu-item-details/menu-item-details.component';
import { NoItemAlertComponent } from '../components/dining/no-item-alert/no-item-alert.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningTakeOrderPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [
    DiningTakeOrderPage,
    AllergicInfoComponent,
    MarkLeaveSingledayComponent,
    OrderConfirmationComponent,
    OrderSuccessMessageComponent,
    MenuOptionsComponent,
    DiningAreaComponent,
    DietaryPreferencesComponent,
    AllergiesComponent,
    AdditionalItemDetailsComponent,
    ContinentalItemComponent,
    ContinentalItemDetailsComponent,
    MenuItemDetailsComponent,
    NoItemAlertComponent
  ],
  entryComponents: [
    AllergicInfoComponent,
    MarkLeaveSingledayComponent,
    OrderConfirmationComponent,
    OrderSuccessMessageComponent,
    MenuOptionsComponent,
    DiningAreaComponent,
    DietaryPreferencesComponent,
    AllergiesComponent,
    AdditionalItemDetailsComponent,
    ContinentalItemComponent,
    ContinentalItemDetailsComponent,
    MenuItemDetailsComponent,
    NoItemAlertComponent
  ]
})
export class DiningTakeOrderPageModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningTakeOrderPage } from './dining-take-order.page';

describe('DiningTakeOrderPage', () => {
  let component: DiningTakeOrderPage;
  let fixture: ComponentFixture<DiningTakeOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningTakeOrderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningTakeOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

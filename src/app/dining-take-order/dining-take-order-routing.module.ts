import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningTakeOrderPage } from './dining-take-order.page';

const routes: Routes = [
  {
    path: '',
    component: DiningTakeOrderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningTakeOrderPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-checkin-phone-num',
  templateUrl: './checkin-phone-num.page.html',
  styleUrls: ['./checkin-phone-num.page.scss'],
})
export class CheckinPhoneNumPage implements OnInit {
phone:any;
disable:boolean=false;
res_id: any;
 from:any;
  onPremises: any[] = [];
  subscription: Subscription;
  constructor(private router:Router,private route:ActivatedRoute,private platform:Platform,private storage:Storage,
    private toast:ToastController) { }

  ngOnInit() {
  }
  async ionViewWillEnter() {
    this.disable=false;
    this.res_id = this.route.snapshot.paramMap.get('res_id');
    this.onPremises = JSON.parse(this.route.snapshot.paramMap.get('onPremises'));
    this.from = this.route.snapshot.paramMap.get('from');
    // this.storage.ready().then(() => {
      const ph=await this.storage.get('PHONE');
        if(ph==null||ph=='+61'||ph.toString().length<10){
          this.storage.set('PHONE',null);
          this.storage.remove('checkinPhone');
   
        }else{
          let phone='0'+ph.substring(3);
          this.phone=phone
        }
    //   })
    // })
    
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.back();
    });

  }
  back() {
    if (this.from == 1 || this.onPremises == null) {
      this.router.navigate(['/checkin-scan-qr']);
    } else {
      this.router.navigate(['/checkin-choose-consumer', { onPremises: JSON.stringify(this.onPremises), res_id: this.res_id }]);
    }
    // this.router.navigate(['/checkin-visit-purpose',{res_id:this.res_id,onPremises:JSON.stringify(this.onPremises),purpose:this.purpose}])
  }

  async next(){
    this.disable=true;
    if(this.phone==undefined || this.phone==''){
      this.presentToast('Please enter your phone number');
      this.disable=false;
    }else if(this.phone.toString().length!=10){
      this.presentToast('Please enter a 10 digit phone number');
      this.disable=false;
    }else if(this.phone.charAt( 0 )!=='0'){
      this.presentToast('Phone number should starts with 0');
      this.disable=false;
    }else{
    // this.storage.ready().then(() => {
      let phone='+61'+this.phone.substring(1);
      this.storage.set('PHONE',phone);
      this.storage.set('checkinPhone',1);
      const temp=await this.storage.get('VIS-TEMPARATURE');
       
    
    const vac=await this.storage.get('VIS-VACCINATION');
      if(temp==1||vac==1){
      this.router.navigate(['/checkin-vaccine-info',{res_id:this.res_id,onPremises:JSON.stringify(this.onPremises),from:3,phone:this.phone}])
      }else{
        this.router.navigate(['/checkin-additional-visitors', {res_id:this.res_id,onPremises:JSON.stringify(this.onPremises),from:3,phone:this.phone}])
      }
  //   })
  // })
  // })
}
  }
  async presentToast(mes){
    const alert = await this.toast.create({
      message: mes,
      duration: 3000,
      cssClass:'toast-mess',
      position:'top'       
    });
    alert.present();
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckinPhoneNumPage } from './checkin-phone-num.page';

describe('CheckinPhoneNumPage', () => {
  let component: CheckinPhoneNumPage;
  let fixture: ComponentFixture<CheckinPhoneNumPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinPhoneNumPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckinPhoneNumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckinPhoneNumPage } from './checkin-phone-num.page';

const routes: Routes = [
  {
    path: '',
    component: CheckinPhoneNumPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckinPhoneNumPageRoutingModule {}

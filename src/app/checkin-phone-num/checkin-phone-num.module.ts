import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckinPhoneNumPageRoutingModule } from './checkin-phone-num-routing.module';

import { CheckinPhoneNumPage } from './checkin-phone-num.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckinPhoneNumPageRoutingModule
  ],
  declarations: [CheckinPhoneNumPage]
})
export class CheckinPhoneNumPageModule {}

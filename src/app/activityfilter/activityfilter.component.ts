import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-activityfilter',
  templateUrl: './activityfilter.component.html',
  styleUrls: ['./activityfilter.component.scss'],
})
export class ActivityfilterComponent implements OnInit {
type:any;
wingArray:any=[];
wing:any;
  constructor(private popCntl:PopoverController,private storage:Storage,private config:HttpConfigService,private http:HttpClient) { }

  ngOnInit() {}
ionViewWillEnter(){
 
}
  selectAll(){
    this.type=0;
    this.closeModal();
  }

  activity(){
    this.type=1;
    this.closeModal();
  }

  event(){
    this.type=8;
    this.closeModal();
  }

  meeting(){
    this.type=9;
    this.closeModal();
  }

  assigned(){
    this.type=11;
    this.closeModal();
  }

  async closeModal() {
    const onClosedData: any = this.type;
    await this.popCntl.dismiss(onClosedData);
  }

}

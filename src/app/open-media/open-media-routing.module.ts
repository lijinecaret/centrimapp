import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OpenMediaPage } from './open-media.page';

const routes: Routes = [
  {
    path: '',
    component: OpenMediaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OpenMediaPageRoutingModule {}

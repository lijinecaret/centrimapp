import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OpenMediaPage } from './open-media.page';

describe('OpenMediaPage', () => {
  let component: OpenMediaPage;
  let fixture: ComponentFixture<OpenMediaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenMediaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OpenMediaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

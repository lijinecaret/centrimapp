import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OpenMediaPageRoutingModule } from './open-media-routing.module';

import { OpenMediaPage } from './open-media.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OpenMediaPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [OpenMediaPage]
})
export class OpenMediaPageModule {}

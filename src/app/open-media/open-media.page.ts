import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Platform, ModalController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { ShowfileComponent } from '../components/showfile/showfile.component';
import { Storage } from '@ionic/storage-angular';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';
@Component({
  selector: 'app-open-media',
  templateUrl: './open-media.page.html',
  styleUrls: ['./open-media.page.scss'],
})
export class OpenMediaPage implements OnInit {
flag:any;
title:any;
id:any;
subscription:Subscription;
 au_url:any;
 trustedVideoUrl: SafeResourceUrl;
 vid=[];
 folder:any=[];
 files:any=[];
 v_url:any;
 img:any;
 link:any;
 other=[];
 mid:any;
  constructor(private domSanitizer: DomSanitizer,private route:ActivatedRoute,private http:HttpClient,private config:HttpConfigService,private platform:Platform,
    private router:Router,private orient:ScreenOrientation,private iab: InAppBrowser,
    private modalCntlr:ModalController,private storage:Storage) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.flag=this.route.snapshot.paramMap.get("flag");
    this.id=this.route.snapshot.paramMap.get("id");
    this.mid=this.route.snapshot.paramMap.get("mid");
    this.title=this.route.snapshot.paramMap.get("title");
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('TOKEN');
        let token=data;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    let headers= await this.config.getHeader()
    this.vid=[];
    
console.log(this.flag);

    let url=this.config.domain_url+'media/'+this.id;
    console.log(url);
    // let headers= await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log(res);
      
      this.folder=res.media_fldr;
      this.files=res.media_files;

      if(this.flag!=6 || this.flag!=7 || this.flag!=5 || this.flag!=8){
      for(let i of this.files){
        let type;
        if((i.url).includes('www.youtube.com')){
          console.log("you",i.url);
          let yurl=(i.url).replace('watch?v=','embed/');
          if(yurl.includes('&')){
            yurl=yurl.substr(0,yurl.indexOf('&'));
           }
          this.v_url=yurl+'?enablejsapi=1'
          let img;
          if((i.url).includes('&')){
            img=(this.v_url).substring((this.v_url).lastIndexOf("/") + 1, (this.v_url).lastIndexOf("?"));
            // img=(i.url).substr(0,(i.url).indexOf('&'));
            // img=img.substr(i.url.lastIndexOf('=') + 1)
           }else{
            img=(i.url).substr(i.url.lastIndexOf('=') + 1);
           }
          this.img='http://img.youtube.com/vi/'+img+'/default.jpg'
        type=0;
      }else{
        this.v_url=i.url
        type=1;
      }
        // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.v_url);
        let video={url:this.v_url,title:i.title,id:i.id,img:this.img,type:type};
        this.vid.push(video);
        console.log('video:',this.vid);
      }
    }
    if(this.flag==5){
      this.vid=[];
      console.log("flag:",this.flag)
      for(let i of this.files){
        let type;
        if(i.url!="null"){
        if((i.url).includes('www.youtube.com')){
          console.log("you",i.url);
          let yurl=(i.url).replace('watch?v=','embed/');
          if(yurl.includes('&')){
            yurl=yurl.substr(0,yurl.indexOf('&'));
           }
          this.v_url=yurl+'?enablejsapi=1'
          let img;
          if((i.url).includes('&')){
            img=(this.v_url).substring((this.v_url).lastIndexOf("/") + 1, (this.v_url).lastIndexOf("?"));
            // img=img.substr(img.lastIndexOf('=') + 1)
            // console.log("vdoId:",img);
            
           }else{
            img=(i.url).substr(i.url.lastIndexOf('=') + 1);
           }
          this.img='http://img.youtube.com/vi/'+img+'/default.jpg';
          this.link='null';
          type=0;
        }else{
          this.v_url=i.url;
          type=1;
        }
        // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.v_url);
        
      }else if(i.link!=null){
        this.v_url='null';
        this.link=i.link;
        this.img="assets/entertainment/music/mp3.svg"
      }else if(i.url=='null' && i.link==null){
        console.log("im:",i.image[0]);
        
        this.v_url=i.image[0].media_image;
        this.link='null';
        this.img="assets/entertainment/music/mp3.svg"
      }
      let video={url:this.v_url,title:i.title,id:i.id,img:this.img,link:this.link,type:type};
        this.vid.push(video);
      }
      console.log("video:",this.vid);
      
    
    }
    if(this.flag==8){
      this.other=[];
      for(let i of this.files){
      let url;
        let link;
        let i_url;
        let type;
        if(i.url!='null'){
          if((i.url).includes('www.youtube.com')){
          console.log("you",i.url);
          
          let yurl=(i.url).replace('watch?v=','embed/');
          if(yurl.includes('&')){
            yurl=yurl.substr(0,yurl.indexOf('&'))+'?enablejsapi=1';
           }
          i_url=yurl;
          this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(yurl);
        // this.v_url=yurl+'?enablejsapi=1'
        //   let img=(i.url).substr(i.url.lastIndexOf('=') + 1);
        //   this.img='http://img.youtube.com/vi/'+img+'/default.jpg'
        // }else{
        //   this.v_url=i.url
        }else {
          
          i_url=i.url;
          type=1;
          this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(i.url);
          }
        // this.trustedVideoUrl = this.domSanitizer.sanitize(SecurityContext.URL,i.url)
        url=this.trustedVideoUrl;
        link='null';

        }else{
          url='null';
          if(i.link!=null){
            type=1;
          link=i.link
          }else{
            link=i.image[0].media_image;
            type=1;
          }
        }
        let video={url:url,link:link,title:i.title,id:i.id,play:i_url,type:type};
        this.other.push(video);
        console.log("other:",this.other);
      }
      
    }
    })
    if(this.flag==9){
      this.au_url=this.route.snapshot.paramMap.get("url");
    }
    if(this.flag==10){
      this.au_url=this.route.snapshot.paramMap.get("url");
      // this.orient.lock(this.orient.ORIENTATIONS.LANDSCAPE);
    }
//   })
// })
// })
    if(this.flag==1 ){
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/videos',{id:this.mid}]) ;
      });
    }
    else if(this.flag==2){
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/movies',{id:this.mid}]) ;
      });
    }
    else if(this.flag==3){
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/threesixty-videos',{id:this.mid}]) ;
      });
    }
    else if(this.flag==4){
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/games',{id:this.mid}]) ;
      });
    }
    else if(this.flag==5){
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/music',{id:this.mid}]) ;
      });
    }
    else if(this.flag==6 || this.flag==9){
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/ebooks',{flag:1,id:this.mid}]) ;
      });
    }
    else if(this.flag==7){
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/ebooks',{flag:2,id:this.mid}]) ;
      });
    }
    else if(this.flag==8){
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
        this.router.navigate(['/others',{id:this.mid}]) ;
      });
    }
  }
      
  play(item){
    // window.open(encodeURI(url),"_system","location=yes");
    console.log(item.url);
    let options:InAppBrowserOptions ={
      location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'no'
    }
    screen.orientation.lock('landscape');
        
      const browser = this.iab.create(item.url,'_blank',options);
      browser.on('exit').subscribe(()=>{
        screen.orientation.unlock();
        screen.orientation.lock('portrait');
      })
  }
  

  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    // let listaFrames = document.getElementsByTagName("iframe");
    // for (var index = 0; index < listaFrames.length; index++) {
    //   let iframe = listaFrames[index].contentWindow;
    //   iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    // }
  }


back(){
  if(this.flag==1 ){
       
      this.router.navigate(['/videos',{id:this.mid}]) ;
   
  }
  else if(this.flag==2){
    
      this.router.navigate(['/movies',{id:this.mid}]) ;
    
  }
  else if(this.flag==3){
    
      this.router.navigate(['/threesixty-videos',{id:this.mid}]) ;
   
  }
  else if(this.flag==4){
     
      this.router.navigate(['/games',{id:this.mid}]) ;
    
  }
  else if(this.flag==5){
     
      this.router.navigate(['/music',{id:this.mid}]) ;
    
  }
  else if(this.flag==6 || this.flag==9){
     
      this.router.navigate(['/ebooks',{flag:1,id:this.mid}]) ;
    
  }
  else if(this.flag==7){
      
      this.router.navigate(['/ebooks',{flag:2,id:this.mid}]) ;
    
  }
  else if(this.flag==8){
       
      this.router.navigate(['/others',{id:this.mid}]) ;
    
  }
}
read(item){
  console.log("url:",item.url);
  
  console.log("url:",item.url);
  if(item.url!='null'){
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'no'
  }
  const browser = this.iab.create(item.url,'_blank',options);
}
else if(item.link!=null){
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'yes'
  }
  const browser = this.iab.create('https://docs.google.com/viewer?url='+item.link+'&embedded=true','_blank',options);
}else if(item.url=="null" && item.link==null){
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'no'
  }
  const browser = this.iab.create('https://docs.google.com/viewer?url='+item.image[0].media_image+'&embedded=true','_blank',options);
}
}
playAudio(item){
  if(item.url!='null'){
  console.log(item.url);
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'no'
  }
 
    const browser = this.iab.create(item.url,'_blank',options);
}else{
  this.openModal(item.link);
}
}
async openModal(link){
  const modal = await this.modalCntlr.create({
    component: ShowfileComponent,
    componentProps: {
      link:link,
      
       
    },
    showBackdrop:false,
    cssClass:'audioModal'
  });
  return await modal.present();
}

view(item){
  console.log("url:",item.url);
  if(item.url!='null'){
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'no'
  }
  const browser = this.iab.create(item.url,'_blank',options);
}
else if(item.link!=null){
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'yes'
  }
  if(item.link.includes('.pdf')){
    this.showpdf(item.link)
}else{
  const browser = this.iab.create('https://docs.google.com/viewer?url='+item.link+'&embedded=true','_blank',options);
}
}else if(item.url=="null" && item.link==null){
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'no'
  }
  const browser = this.iab.create('https://docs.google.com/viewer?url='+item.media[0].media_image+'&embedded=true','_blank',options);
}
}
playOther(item){
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'no'
  }
  screen.orientation.lock('landscape');
  const browser = this.iab.create(item.play,'_blank',options);
  browser.on('exit').subscribe(()=>{
    screen.orientation.unlock();
    screen.orientation.lock('portrait');
  })
}
async showpdf(file){
  const modal = await this.modalCntlr.create({
    component: ViewpdfComponent,
    cssClass:'fullWidthModal',
    componentProps: {
      
      data:file,
      
       
    },
    
    
  });
  return await modal.present();
}
}

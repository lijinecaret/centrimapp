import { Component, OnInit } from '@angular/core';
import { Platform, ModalController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SafeResourceUrl } from '@angular/platform-browser';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { ShowfileComponent } from '../components/showfile/showfile.component';
import { Storage } from '@ionic/storage-angular';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';


@Component({
  selector: 'app-ebooks',
  templateUrl: './ebooks.page.html',
  styleUrls: ['./ebooks.page.scss'],
})
export class EbooksPage implements OnInit {
  trustedVideoUrl: SafeResourceUrl;
  folder:any=[];
  files:any=[];
  
  flag:any;
  subscription:Subscription;
  id:any;
  constructor(private platform:Platform,private http:HttpClient,private config:HttpConfigService,
    private route:ActivatedRoute,private router:Router,private iab: InAppBrowser,private modalCntlr:ModalController,
    private storage:Storage) { }

  ngOnInit() {
    
  }
  async ionViewWillEnter(){
    this.id=this.route.snapshot.paramMap.get('id');
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('TOKEN');
        let token=data;
        const bid=await this.storage.get('BRANCH');
         
    let headers= await this.config.getHeader()
    this.flag=this.route.snapshot.paramMap.get("flag");
    this.folder=[];
    this.files=[];
    if(this.flag==1){
    let url=this.config.domain_url+'media/'+this.id;
    // let headers= await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      this.folder=res.media_fldr;
      this.files=res.media_files;
      console.log(res)
    })
  }
  else{
    
      let url=this.config.domain_url+'media/'+this.id;
      // let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        this.folder=res.media_fldr;
        this.files=res.media_files;
        console.log(res)
      })
  }
// })
//       })
//     })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/entertainment']) ;
  }); 
  }
  


ionViewWillLeave() { 
  this.subscription.unsubscribe();
}
  read(item){
    console.log("url:",item.url);
    if(item.url!='null'){
    let options:InAppBrowserOptions ={
      location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'no'
    }
    
    const browser = this.iab.create(item.url,'_blank',options);
  }
  else if(item.link!=null){
    let options:InAppBrowserOptions ={
      location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'yes'
    }
    if(item.link.includes('.pdf')){
      this.showpdf(item.link)
  }else{
    const browser = this.iab.create('https://docs.google.com/viewer?url='+item.link+'&embedded=true','_blank',options);
  }
  }else if(item.url=="null" && item.link==null){
    let options:InAppBrowserOptions ={
      location:'yes',
      hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'no'
    }

    const browser = this.iab.create('https://docs.google.com/viewer?url='+item.image[0].media_image+'&embedded=true','_blank',options);
  }
    // const options: DocumentViewerOptions = {
    //   title: item.title
    // }
    // this.viewer.viewDocument(item.url, 'application/pdf', options)
    // let path = null;
    //   if(this.platform.is('ios')){
    //     path=this.file.documentsDirectory;
    //   }
    //   else if(this.platform.is('android')){
    //     path=this.file.externalDataDirectory;
    //   }
        
    //     const fileTransfer: FileTransferObject=this.transfer.create();
    //     fileTransfer.download('assets/ebook/the-success-principles.pdf',path+'TheSuccessPrinciples').then(entry=>{
    //       console.log("download");
          
    //       let url=entry.toURL();
    //       console.log("url",url);
          
          // this.fileOpener.open('assets/ebook/the-success-principles.pdf','application/pdf');
         
        // },error=>{
        //   console.log(error);
          
        // });
        
  }
  play(item){
    // window.open(encodeURI(item.url),"_system","location=no");


    // this.nativeAudio.preloadComplex(item.id,item.url,1,1,0).then(()=>{
    //   this.nativeAudio.play(item.id).then(()=>{
    //     console.log("playing");
        
    //   })
    // })
    // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(item.url);
    // this.router.navigate(['/open-media',{url:this.trustedVideoUrl,flag:9}]);



    if(item.url!='null'){

    console.log("url:",item.url);
    let options:InAppBrowserOptions ={
      location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'no'
    }
    const browser = this.iab.create(item.url,'_blank',options);
  }else{
    
    // let options:InAppBrowserOptions ={
    //   location:'no',
    //     hideurlbar:'yes',
    //     zoom:'no'
    // }
    // const browser = this.iab.create(item.link,'_blank',options);
    this.openModal(item.link);
  }
  }

  openFolder(item){
    if(this.flag==1){
    this.router.navigate(['open-media',{flag:6,id:item.id,title:item.title,mid:this.id}])
    }
    else{
      this.router.navigate(['open-media',{flag:7,id:item.id,title:item.title,mid:this.id}])
      }
  }
  async openModal(link){
    const modal = await this.modalCntlr.create({
      component: ShowfileComponent,
      componentProps: {
        link:link,
        
         
      },
      showBackdrop:false,
      cssClass:'audioModal'
    });
    return await modal.present();
  }

  async showpdf(file){
    const modal = await this.modalCntlr.create({
      component: ViewpdfComponent,
      cssClass:'fullWidthModal',
      componentProps: {
        
        data:file,
        
         
      },
      
      
    });
    return await modal.present();
  }
}

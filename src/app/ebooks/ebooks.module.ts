import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EbooksPageRoutingModule } from './ebooks-routing.module';

import { EbooksPage } from './ebooks.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EbooksPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [EbooksPage]
})
export class EbooksPageModule {}

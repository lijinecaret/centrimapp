import { Component, OnInit, ViewChildren } from '@angular/core';
import { IonSlides, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { GetObjectService } from '../services/get-object.service';
import { Subscription } from 'rxjs';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';


@Component({
  selector: 'app-facility-details',
  templateUrl: './facility-details.page.html',
  styleUrls: ['./facility-details.page.scss'],
})
export class FacilityDetailsPage implements OnInit {
  @ViewChildren('slideWithNav') slideWithNav: IonSlides;
  content:any=[];
  img:any=[];
  map:any;
  c_name:any;
  sliderOne: any;

  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
  };
subscription:Subscription;
contentStyle={top:'75px'};
viewEntered = false;
  constructor(private route:ActivatedRoute,private objService:GetObjectService,private router:Router,
    private platform:Platform,private iab: InAppBrowser) { 
    
      if(this.platform.is('android')){
        this.contentStyle.top='75px'
      }else if(this.platform.is('ios')){
        this.contentStyle.top='95px'
      }

      
  }
  
  ngOnInit() {




    this.c_name=this.route.snapshot.paramMap.get('com_name');
    this.content=this.objService.getExtras();
    console.log("cont:",this.content);
    this.img=this.content.branchimages;
    this.map='http://www.google.com/maps/place/'+this.content.latitude+','+this.content.longitude;
    console.log("img:",this.img);

    this.sliderOne =
    {
      isBeginningSlide: true,
      isEndSlide: false,
      isActive:false,
      slidesItems: []=this.img
    };

  }
ionViewWillEnter(){
  
    this.ngOnInit();
    
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/about']) ;
  }); 

}
ionViewDidEnter() {
  this.viewEntered = true;
}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}
openMap(){
  let options:InAppBrowserOptions ={
    location:'no',
    hideurlbar:'yes'
  }
  const browser = this.iab.create(this.map,'_blank',options);
}


slideNext(object,slideView) {
  slideView.slideNext(500).then(() => {
    this.checkIfNavDisabled(object, slideView);
  });
}

//Move to previous slide
slidePrev(object,slideView) {
  slideView.slidePrev(500).then(() => {
    this.checkIfNavDisabled(object, slideView);
  });;
}
  SlideDidChange(object,slideView) {
    this.checkIfNavDisabled(object, slideView);
    object.isActive= true;
  }
  checkIfNavDisabled(object, slideView) {
    this.checkisBeginning(object, slideView);
    this.checkisEnd(object, slideView);
  }
 
  checkisBeginning(object, slideView) {
    slideView.isBeginning().then((istrue) => {
      object.isBeginningSlide = istrue;
    });
  }
  checkisEnd(object, slideView) {
    slideView.isEnd().then((istrue) => {
      object.isEndSlide = istrue;
    });
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';

import { IonicStorageModule } from '@ionic/storage-angular';
import { HttpClientModule } from '@angular/common/http';


import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Market } from '@ionic-native/market/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { Network } from '@ionic-native/network/ngx';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
// import { ImagePicker } from '@ionic-native/image-picker/ngx';
// import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';
// import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Media } from '@ionic-native/media/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { InvitationComponent } from './invitation/invitation.component';
import { ActivityfilterComponent } from './activityfilter/activityfilter.component';
import { HideHeaderDirective } from './directives/hide-header.directive';
import { ViewImageComponent } from './components/ImageViewer/view-image/view-image.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ShowfileComponent } from './components/showfile/showfile.component';
import { NonetworkComponent } from './components/nonetwork/nonetwork.component';
import { ThankyouComponent } from './components/thankyou/thankyou.component';
import { ReceivecallComponent } from './components/receivecall/receivecall.component';
import { OncallComponent } from './components/oncall/oncall.component';
import { VideocallComponent } from './components/videocall/videocall.component';
import { MoreCallbookComponent } from './components/more-callbook/more-callbook.component';
import { CancelComponent } from './components/cancel/cancel.component';
import { OpenimageComponent } from './components/openimage/openimage.component';
import { CommentComponent } from './components/comment/comment.component';
import { MaintenanceFilterComponent } from './components/maintenance-filter/maintenance-filter.component';
import { MultiFileUploadComponent } from './components/multi-file-upload/multi-file-upload.component';
import { MessageoptionsComponent } from './components/messageoptions/messageoptions.component';
import { MessageAttachmentComponent } from './components/message-attachment/message-attachment.component';
import { ActivityImageComponent } from './components/activity-image/activity-image.component';
import { TaggedConsumerInfoComponent } from './components/tagged-consumer-info/tagged-consumer-info.component';
import { PettycashConsumersComponent } from './components/pettycash-consumers/pettycash-consumers.component';
import { ScanerrorComponent } from './components/scanerror/scanerror.component';
import { TemperatureWarningComponent } from './components/temperature-warning/temperature-warning.component';
import { LocationsComponent } from './components/locations/locations.component';
import { ApplicationPipesModule } from './application-pipes.module';
import { OtherVisitorsComponent } from './components/other-visitors/other-visitors.component';
import { VisitBookingWarningComponent } from './components/visit-booking-warning/visit-booking-warning.component';
import { VisitBookingConfirmationComponent } from './components/visit-booking-confirmation/visit-booking-confirmation.component';
import { VisitBookingRescheduleComponent } from './components/visit-booking-reschedule/visit-booking-reschedule.component';
import { VisitBookingOptionsComponent } from './components/visit-booking-options/visit-booking-options.component';
import { SetResidentProfileComponent } from './components/set-resident-profile/set-resident-profile.component';
import { OverviewActivityComponent } from './components/overview-activity/overview-activity.component';
import { ShowVisitorsComponent } from './components/show-visitors/show-visitors.component';
import { ConfirmSignupComponent } from './components/confirm-signup/confirm-signup.component';
import { OverviewDiningComponent } from './components/overview-dining/overview-dining.component';
import { OverviewFoodoptionsComponent } from './components/overview-foodoptions/overview-foodoptions.component';
import { ViewpdfComponent } from './components/viewpdf/viewpdf.component';
import { NotificationComponent } from './components/notification/notification.component';
import { ReportAbuseComponent } from './components/report-abuse/report-abuse.component';
import { ReportReasonComponent } from './components/report-reason/report-reason.component';
import { ChooseDateComponent } from './components/dining/choose-date/choose-date.component';



// import { PdfViewerModule } from 'ng2-pdf-viewer';
// import { MessagesearchPipe } from './pipes/messagesearch.pipe';
// import { DateformatPipe } from './pipes/dateformat.pipe';


@NgModule({
  declarations: [AppComponent,
    InvitationComponent,
    ActivityfilterComponent,
    ViewImageComponent,
    ShowfileComponent,
    HideHeaderDirective,
    NonetworkComponent,
    ThankyouComponent,
    // ReceivecallPage,
   
    ReceivecallComponent,
    OncallComponent,
    VideocallComponent,
    MoreCallbookComponent,
    CancelComponent,
    OpenimageComponent,
    CommentComponent,
    MaintenanceFilterComponent,
    MultiFileUploadComponent,
    MessageoptionsComponent,
    MessageAttachmentComponent,
    ActivityImageComponent,
    TaggedConsumerInfoComponent,
    PettycashConsumersComponent,
    ScanerrorComponent,
    TemperatureWarningComponent,
    LocationsComponent,
    OtherVisitorsComponent,
    VisitBookingWarningComponent,
    VisitBookingConfirmationComponent,
    VisitBookingRescheduleComponent,
    VisitBookingOptionsComponent,
    SetResidentProfileComponent,
    OverviewActivityComponent,
    OverviewDiningComponent,
    OverviewFoodoptionsComponent,
    ShowVisitorsComponent,
    ConfirmSignupComponent,
    ViewpdfComponent,
    NotificationComponent,
    ReportAbuseComponent,
    ReportReasonComponent,
    ChooseDateComponent,
    
    
    ],
  entryComponents: [InvitationComponent,
    ActivityfilterComponent,
    ViewImageComponent,
    ShowfileComponent,
   
    // ReceivecallPage,
    NonetworkComponent,
    ThankyouComponent,
    ReceivecallComponent,
    OncallComponent,
    VideocallComponent,
    MoreCallbookComponent,
    CancelComponent,
    OpenimageComponent,
    CommentComponent,
    MaintenanceFilterComponent,
    MultiFileUploadComponent,
    MessageoptionsComponent,
    MessageAttachmentComponent,
    ActivityImageComponent,
    TaggedConsumerInfoComponent,
    PettycashConsumersComponent,
    ScanerrorComponent,
    TemperatureWarningComponent,
    LocationsComponent,
    OtherVisitorsComponent,
    VisitBookingWarningComponent,
    VisitBookingConfirmationComponent,
    VisitBookingRescheduleComponent,
    VisitBookingOptionsComponent,
    SetResidentProfileComponent,
    OverviewActivityComponent,
    OverviewDiningComponent,
    OverviewFoodoptionsComponent,
    ShowVisitorsComponent,
    ConfirmSignupComponent,
    ViewpdfComponent,
    NotificationComponent,
    ReportAbuseComponent,
    ReportReasonComponent,
    ChooseDateComponent,
    
  ],
  imports: [BrowserModule, 
            IonicModule.forRoot(), 
            AppRoutingModule,
            IonicStorageModule.forRoot(),
            HttpClientModule,
            FormsModule,
            ReactiveFormsModule,
            ApplicationPipesModule,
            // PdfViewerModule
          ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    Camera,
    File,
    HTTP,
    
    InAppBrowser,
   
    ScreenOrientation,
   
    FilePath,
    NativeAudio,
    Network,
    StreamingMedia,
    WebView,
    AndroidPermissions,
    Market,
    AppVersion,
    // ImagePicker,
    Base64,
    Media,
    SpinnerDialog,
   
    // BarcodeScanner,
    Chooser,
    // FirebaseX,
   
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

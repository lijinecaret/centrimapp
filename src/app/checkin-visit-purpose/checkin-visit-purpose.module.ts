import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckinVisitPurposePageRoutingModule } from './checkin-visit-purpose-routing.module';

import { CheckinVisitPurposePage } from './checkin-visit-purpose.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckinVisitPurposePageRoutingModule
  ],
  declarations: [CheckinVisitPurposePage]
})
export class CheckinVisitPurposePageModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-checkin-visit-purpose',
  templateUrl: './checkin-visit-purpose.page.html',
  styleUrls: ['./checkin-visit-purpose.page.scss'],
})
export class CheckinVisitPurposePage implements OnInit {
res_id:any;
purpose:any;
flag:any;
onPremises:any[]=[];
subscription:Subscription;
  constructor(private router:Router,private route:ActivatedRoute,private toastCntlr:ToastController,
    private platform:Platform,private storage:Storage) { }

  ngOnInit() {
  }


  ionViewWillEnter(){
    this.res_id=this.route.snapshot.paramMap.get('res_id');
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.onPremises=JSON.parse(this.route.snapshot.paramMap.get('onPremises'));
    this.purpose=this.route.snapshot.paramMap.get('purpose');
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      // this.router.navigate(['/checkin-scan-qr']) ;
      this.back() ;
   
    }); 
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
  async next(){
    if(this.purpose==undefined||this.purpose==null){
          this.presentAlert('Please specify your purpose.')
    }else{
      // this.storage.ready().then(() => {
        
      const temp=await this.storage.get('VIS-TEMPARATURE');
       
    
    const vac=await this.storage.get('VIS-VACCINATION');
      if(temp==1||vac==1){
      this.router.navigate(['/checkin-vaccine-info',{res_id:this.res_id,purpose:this.purpose,onPremises:JSON.stringify(this.onPremises)}])
      }else{
        this.router.navigate(['/checkin-additional-visitors', {res_id:this.res_id,purpose:this.purpose,onPremises:JSON.stringify(this.onPremises)}])
      }
//     })
//   })
// })
    }
  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'       
    });
    alert.present(); //update
  }
  back(){
    this.purpose='';
    if(this.flag==1||this.onPremises==null){
    this.router.navigate(['/checkin-scan-qr']);
    }else{
      this.router.navigate(['/checkin-choose-consumer',{onPremises:JSON.stringify(this.onPremises),res_id:this.res_id}]);
    }
  }
}

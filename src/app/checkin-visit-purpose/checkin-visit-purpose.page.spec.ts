import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckinVisitPurposePage } from './checkin-visit-purpose.page';

describe('CheckinVisitPurposePage', () => {
  let component: CheckinVisitPurposePage;
  let fixture: ComponentFixture<CheckinVisitPurposePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinVisitPurposePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckinVisitPurposePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckinVisitPurposePage } from './checkin-visit-purpose.page';

const routes: Routes = [
  {
    path: '',
    component: CheckinVisitPurposePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckinVisitPurposePageRoutingModule {}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, IonContent, PopoverController, ModalController, LoadingController, AlertController, ToastController, ActionSheetController } from '@ionic/angular';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Subscription } from 'rxjs';
import { File } from '@ionic-native/file/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { VideocallComponent } from '../components/videocall/videocall.component';
import { OpenimageComponent } from '../components/openimage/openimage.component';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { MessageAttachmentComponent } from '../components/message-attachment/message-attachment.component';
import { MessageoptionsComponent } from '../components/messageoptions/messageoptions.component';
import moment from 'moment';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';
import { ReportReasonComponent } from '../components/report-reason/report-reason.component';
// import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { Chooser } from '@ionic-native/chooser/ngx';
// import { Jitsi } from 'capacitor-jitsi-meet';
declare var ScaleDrone;
// declare var jitsiplugin;
var drone;
@Component({
  selector: 'app-new-personal-message',
  templateUrl: './new-personal-message.page.html',
  styleUrls: ['./new-personal-message.page.scss'],
})
export class NewPersonalMessagePage implements OnInit {
  @ViewChild('content', { static: false }) content: IonContent;
  // @ViewChild('content', { static: false })content: Content;;
  trustedVideoUrl: SafeResourceUrl;
  drone:any;
  inputMessage:string;
  msg:any=[];
  message:any=[];
  id:any;
  pic:any;
  name:any;
  user_two:any;
  roomid:any;
  attach:any;
  ext:any;
  updated_at:any;
  att_name:any;
  subscription:Subscription;
  keyboardStyle = { width: '100%', height: '0px' };
  scroll:boolean=true;
  mess_date:any;
  key:boolean;
  chat:HTMLElement;
  text:HTMLElement;
  MAX_FILE_SIZE = 24 * 1024 * 1024;
  timer:any;
  longpress = 1000;
  user_name:any;
  base64File:any;
  last_date:any;
  role:any;
  disable:boolean=false;
  deleteStatus:boolean=false;
  constructor(private http:HttpClient,private storage:Storage,public config:HttpConfigService,
    private route:ActivatedRoute,private router:Router,private actionsheetCntlr:ActionSheetController,
    private platform:Platform,private filePath: FilePath,private iab: InAppBrowser,
    private file:File,private keyboard:Keyboard,private popoverController:PopoverController,
    private permission:AndroidPermissions,private modalCntl:ModalController,private loadingCtrl:LoadingController,
    public elRef: ElementRef,private alertCntlr:AlertController,private domSanitizer: DomSanitizer,private streamingMedia:StreamingMedia,
    private toastCntlr:ToastController,private base64:Base64,private chooser:Chooser) { }

  ngOnInit() {
    // setTimeout(() => {
    //   for (let i = 0; i < this.msg.length; i++) {
    //     this.msg[i] = i
    //   }
    // }, 300);
    
    if(this.platform.is('android')){
      this.key=true;
    }else if(this.platform.is('ios')){
      this.key=false;
    }
    this.keyboard.onKeyboardWillShow().subscribe( {
      next: x => {
        this.keyboardStyle.height = x.keyboardHeight + 'px';
      },
      error: e => {
        console.log(e);
      }
    });
    this.keyboard.onKeyboardWillHide().subscribe( {
      next: x => {
        this.keyboardStyle.height = '0px';
      },
      error: e => {
        console.log(e);
      }
    });


  }
  async ionViewWillEnter(){
    this.chat=this.elRef.nativeElement.querySelector('#chatArea');
   
    // let test=new Date('2020-06-21T15:26:40.000000Z')
    // let x=new Date(test.getTime() - test.getTimezoneOffset()*60000)
    // var hours = x.getHours();
    //               let minutes = x.getMinutes();
    //               var ampm = hours >= 12 ? 'pm' : 'am';
    //               hours = hours % 12;
    //               hours = hours ? hours : 12; // the hour '0' should be '12'
    //               var minute = minutes < 10 ? '0'+minutes : minutes;
    //               let updated_at= hours + ':' + minute + ' ' + ampm;
    // console.log("test:",updated_at);

      this.user_two=this.route.snapshot.paramMap.get('user_two');
      this.pic=this.route.snapshot.paramMap.get('pic');
      this.name=this.route.snapshot.paramMap.get('name');
      this.role=this.route.snapshot.paramMap.get('role');
      this.text=this.elRef.nativeElement.querySelector('#textmsg');
    
      // this.createRoom();

     
      // this.storage.ready().then(()=>{
        const name=await this.storage.get('NAME');
          this.user_name=name;
        const data=await this.storage.get('USER_ID');
            this.id=data;
    
            let body={
              user_one:this.id,
              user_two:this.user_two
            }
            console.log("body:",body);
            let url=this.config.domain_url+'createroom';
            let headers= await this.config.getHeader();
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log("room:",res);
              this.roomid=res.roomid;

              // document.clear();

              this.getMessage();
             
              // this.callFunction();
              
             drone = new ScaleDrone('OQSaMd3By2dEaqy8');
              // this.drone = new ScaleDrone('2HajVkkzeDQi10x8');
              let iii=this.id;
              let pic=this.pic;
              // let msgbox=this.text;
              let that=this;
             

              drone.on('open', function (error) {
                if (error) return console.log(error);
                this.roomid=res.roomid;
                  console.log("roomid2:",''+this.roomid+'');
                  
                this.room = drone.subscribe(''+this.roomid+'');
                // this.room = new ScaleDrone('2HajVkkzeDQi10x8').subscribe(''+this.roomid+'');
                // this.room.on('open', function (error) {
                //   if (error) return console.error(error);
                //   console.log('Connected to room');
                // });
                

                var pattern = new RegExp('(https?:\\/\\/)?'+ // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
                '(\\#[-a-z\\d_]*)?$','i');

                this.room.on('data', message =>{
                  
                  that.dismissLoader();
                  that.deleteStatus=true;
                    console.log("mes:",message)
                    let current=new Date();
                    let date=new Date(message.time.replace(' ','T'));
                    let ti=moment.utc(message.time).local().format();
                    let time=new Date(ti);
                  
                      
                  var hours = time.getHours();
                  let minutes = time.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  var minute = minutes < 10 ? '0'+minutes : minutes;
                  let updated_at= hours + ':' + minute + ' ' + ampm;
                  
                  console.log("id1",iii);
                 
                  var div = document.createElement('ion-row');

                  var div1 = document.createElement('ion-row');

                  if(message.user_id==that.user_two){
                    console.log("user2");
                    
                  if(message.msg!=='' && message.msg!==null){
                    console.log("msg");
                    
                    div1.innerHTML = '<ion-col class="col_fit"><ion-avatar><img src="'+pic+'"></ion-avatar></ion-col><ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation">'+message.msg+'</p><ion-label class="conversation_time">'+updated_at+'</ion-label></ion-col>';
                    div1.addEventListener('click',()=>{
                      if(message.msg.length<160){
                      console.log("long text check")
                      if(pattern.test(message.msg)){
                      // if(t<=300000){
                        console.log("terminated here2");
                      that.jitsiCall(message.msg);
                      // }else{
                      //   this.notActiveAlert()
                      }
                      }
                    })
                    }
                    if(message.attachment!=='' && message.attachment !== null){
                      console.log("attachment");
                      
                      let fileExtension=(message.attachment).substr((message.attachment).lastIndexOf('.') + 1);
                      if((fileExtension=='jpg')||(fileExtension=='jpeg')||(fileExtension=='png')){
  
                        div.innerHTML = '<ion-col class="col_fit" ><ion-avatar><img src="'+pic+'"></ion-avatar></ion-col><ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation_bordered"><img src="'+message.attachment+'" style="height: 180px;"></p><p class="conversation_time">'+updated_at+'</p></ion-col>';
                        div.addEventListener('click',()=>{
                          that.openDoc(message.attachment)
                      })
                    }else if(fileExtension=='mp4'||fileExtension=='mkv'||fileExtension=='avi'||fileExtension=='mov'||fileExtension=='x-flv' || fileExtension=='x-mpegURL'|| fileExtension=="MP2T" || fileExtension=="3gpp"|| fileExtension=="quicktime"|| fileExtension=="x-msvideo"|| fileExtension=="x-ms-wmv"){
                      // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(element.attachment);
                      div.innerHTML='<ion-col class="col_fit" ><ion-avatar><img src="'+pic+'"></ion-avatar></ion-col><ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;"class="conversation_bordered"><video src="'+message.attachment+'#t=0.05" preload="metadata" style="height:150px;"></p><div class="video-overlay_msg"></div><div class="play-button-holder"><ion-icon name="play"  class="play_icon_msg" ></ion-icon></div><p class="conversation_time">'+updated_at+'</p></ion-col>';
                      div.addEventListener('click',()=>{
                        that.playVdo(message.attachment)
                      })
                    }else{
                      let filename=(message.attachment).substr((message.attachment).lastIndexOf('/') + 1);
                      let doc_icon;
                          if((fileExtension=='pdf')){
                            doc_icon='<ion-icon src="assets/pdf_icon.svg"></ion-icon><span style="margin-left:6px">'
                          }else if(fileExtension=='doc'||fileExtension=="msword" || fileExtension=="ms-doc" || fileExtension=="document"){
                            doc_icon='<ion-icon src="assets/ms_word_doc_icon.svg"></ion-icon><span style="margin-left:6px">'
                          }else if(fileExtension=='sheet'||fileExtension=="excel"|| fileExtension=="ms-excel"|| fileExtension=="x-excel"|| fileExtension=="x-msexcel"){
                            doc_icon='<ion-icon src="assets/excel-icon.svg"></ion-icon><span style="margin-left:6px">'
                          }
                          div.innerHTML='<ion-col class="col_fit"><ion-avatar><img src="'+pic+'"></ion-avatar></ion-col><ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:10px;"class="conversation">'+doc_icon+filename+'</span></p><p class="conversation_time">'+updated_at+'</p></ion-col>';
                          div.addEventListener('click',()=>{
                            that.openPdf(message.attachment)
                      })
                    
                    }
                    }
                  // }else if(message.user_id==iii){
                  //   console.log("loggedin user");
                    
                  //   if(message.msg!='null'&& message.msg!=null){
                  //     console.log("msg");
                      
                  //     div1.innerHTML ='<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation">'+message.msg+'</p><p class="conversation_time">'+updated_at+'</p></ion-col>';
                     

                  //   div1.addEventListener('click',()=>{
                  //     if(message.msg.length<160){
                  //     console.log("long text check")
                  //     if(pattern.test(message.msg)){
                  //     // if(t<=300000){
                  //       console.log("terminated here2");
                  //     that.jitsiCall(message.msg);
                  //     // }else{
                  //     //   that.notActiveAlert()
                  //     // }
                  //   }
                  //     }
                  //   })
                  
                  // }
                  // if(message.attachment!=='' && message.attachment !== null){
                  //   let fileExtension=(message.attachment).substr((message.attachment).lastIndexOf('.') + 1);
                  //   if((fileExtension=='jpg')||(fileExtension=='jpeg')||(fileExtension=='png')){
  
                  //     div.innerHTML ='<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation_bordered"><img src="'+message.attachment+'" style="height: 180px;"></p><p class="conversation_time">'+updated_at+'</p></ion-col>';
                  //     div.addEventListener('click',()=>{
                  //       that.openDoc(message.attachment)
                  //     })
                  //   }else if(fileExtension=='mp4'||fileExtension=='mkv'||fileExtension=='avi'||fileExtension=='mov'||fileExtension=='x-flv' || fileExtension=='x-mpegURL'|| fileExtension=="MP2T" || fileExtension=="3gpp"|| fileExtension=="quicktime"|| fileExtension=="x-msvideo"|| fileExtension=="x-ms-wmv"){
                  //     // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(element.attachment);
                  //     div.innerHTML='<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation_bordered"><video src="'+message.attachment+'#t=0.05" preload="metadata" style="height:150px;"></p><div class="video-overlay_msg"></div><div class="play-button-holder"><ion-icon name="play"  class="play_icon_msg" ></ion-icon></div><p class="conversation_time">'+updated_at+'</p></ion-col>';
                  //     div.addEventListener('click',()=>{
                  //       that.playVdo(message.attachment)
                  //     })
                  //   }else{
                  //     let filename=(message.attachment).substr((message.attachment).lastIndexOf('/') + 1);
                  //     let doc_icon;
                  //         if((fileExtension=='pdf')){
                  //           doc_icon='<ion-icon src="assets/pdf_icon.svg"></ion-icon><span style="margin-left:6px">'
                  //         }else if(fileExtension=='doc'||fileExtension=="msword" || fileExtension=="ms-doc" || fileExtension=="document"){
                  //           doc_icon='<ion-icon src="assets/ms_word_doc_icon.svg"></ion-icon><span style="margin-left:6px">'
                  //         }else if(fileExtension=='sheet'||fileExtension=="excel"|| fileExtension=="ms-excel"|| fileExtension=="x-excel"|| fileExtension=="x-msexcel"){
                  //           doc_icon='<ion-icon src="assets/excel-icon.svg"></ion-icon><span style="margin-left:6px">'
                  //         }
                  //         div.innerHTML='<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:10px;" class="conversation">'+doc_icon+filename+'</span></p><p class="conversation_time">'+updated_at+'</p></ion-col>';
                  //         div.addEventListener('click',()=>{
                  //           that.openPdf(message.attachment)
                  //     })
                  //   }
                  //   }
                  }


                    // if(message.user_id==iii){
                    //   console.log("right",message.user_id,iii)
                    //  if(message.msg!==''&&message.msg!==null){
                    //   div1.classList.add('conversation_item_right');
                    //  }
                    //  if(message.attachment!==''&&message.attachment!==null){
                    //   div.classList.add('conversation_item_right');
                    //  }
                    // }else 
                    if(message.user_id!=iii){
                      console.log("left",message.user_id,iii)
                      if(message.msg!==''&&message.msg!==null){
                        div1.classList.add('conversation_item_left');
                       }
                       if(message.attachment!==''&&message.attachment!==null){
                      div.classList.add('conversation_item_left');
                       }
                    }
                    if(message.msg!==''&&message.msg!==null){
                    that.text.appendChild(div1);
                    }
                    if(message.attachment!==''&&message.attachment!==null){
                      that.text.appendChild(div);
                    }

                    // if(message.user_id==that.id){
                    //   div.addEventListener('touchstart',()=>{
                    //     that.touchstart(message.cr_id,div);
                    //     })
                    //     div.addEventListener('touchend',()=>{
                    //       that.touchend();
                    //     })
                    //     div1.addEventListener('touchstart',()=>{
                    //       that.touchstart(message.cr_id,div1);
                    //      })
                    //      div1.addEventListener('touchend',()=>{
                    //       that.touchend();
                    //      })
                    //   }
                    console.log("sca:",message)
                    setTimeout(() => {
                      // window.scrollTo(0,document.body.scrollHeight);
                      that.content.scrollToBottom(0);
                  }, 0);
                  });
                });
               });
            
               
        // })
      // })
      // })



      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{  
        this.router.navigate(['/message']) ;
        
      })
      
      
  }
  
  ionViewDidLeave(){
   
    // document.clear();
    this.chat.innerHTML="";
    this.text.innerHTML="";
    drone.close();
        this.subscription.unsubscribe();
       
        // this.chat.remove();
        
        
  }
// create room
createRoom(){
  
  
}


// get previous messages
  async getMessage(){
    this.msg=[];
    
    
   this.showLoading();

    let body={
      roomid:this.roomid
    }
    console.log("get:",body);
    
    let url=this.config.domain_url+'showconersation';
    let headers= await this.config.getHeader();
    this.http.post(url,body,{headers}).subscribe((data:any)=>{
      
       console.log(data);
       
        
        this.message=data.conversations;
        if(this.message.length>0){
          console.log('deletestatus:',this.message.map(x=>x.delete_status==this.id));
          
         this.message.map(x=>{
            if(x.delete_status!==this.id){
              this.deleteStatus=true
            }
          })
          
          
        let da=this.message[0].created_at;
        let d=new Date();
        // this.mess_date=this.fixDigit(d.getDate())+' '+this.fixDigit(d.getMonth()+1)+' '+d.getFullYear();
        this.message.forEach(element=>{

          if(element.attachment&&element.attachment!=''){
            this.ext=element.attachment.substring(element.attachment.lastIndexOf('.') + 1);
            this.att_name=element.attachment.substring(element.attachment.lastIndexOf('/') + 1);
          }else{
            this.ext='';
            this.att_name='';
          }
          let current=new Date();
          var yest = new Date();
          yest.setDate(yest.getDate() - 1);
          let date=new Date(element.created_at);
          let time= new Date( (date.getTime() - 
          date.getTimezoneOffset()*60000)+current.getTimezoneOffset()*60000);
          
          let t=(current.getTime()-(new Date(time).getTime()));
                // let min = Math.floor(t/ 60000);
                // let today=this.fixDigit(current.getDate())+' '+this.fixDigit(current.getMonth()+1)+' '+current.getFullYear();
                this.mess_date=this.fixDigit(time.getDate())+' '+this.getMon(time.getMonth())+' '+time.getFullYear();
                // if(hr>24){
                //   this.updated_at=time.getFullYear()+'/' + this.fixDigit(time.getMonth()+1) + '/'+this.fixDigit(time.getDate());
                // }else{
                  
                  if (d.toLocaleDateString() === date.toLocaleDateString()) {
                    console.log("datecomshere");
                    console.log("date:",d.toLocaleDateString(),date.toLocaleDateString())
                  }else{
                    d=date;
                  var item = document.createElement('ion-row');
                  console.log("item created");
                  
                  if(current.toLocaleDateString()==d.toLocaleDateString()){
                    console.log("today");
                    
                  item.innerHTML = '<div class="conversation_day no_mt"> Today</div>';
                  }else if(yest.toLocaleDateString()==d.toLocaleDateString()){
                    console.log("yesterday");
                    
                    item.innerHTML = '<div class="conversation_day no_mt"> Yesterday</div>'
                  }else{
                    console.log("dateee:",this.mess_date);
                    console.log("test:",d);
                    
                    item.innerHTML = '<div class="conversation_day no_mt">'+this.mess_date+'</div>'
                  }
                  item.classList.add('chat_date');
                  // document.querySelector('.msgarea').appendChild(item);
                  if(element.delete_status==this.id||element.delete_status==1){
                    console.log("chat deleted");
                    
                  }else{
                  this.chat.appendChild(item);
                  }
                }
          var hours = time.getHours();
                  let minutes = time.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                 var hour= hours < 10 ? '0'+hours:hours
                  var minute = minutes < 10 ? '0'+minutes : minutes;
                  this.updated_at= hour + ':' + minute + ' ' + ampm;
                // }

                var pattern = new RegExp('(https?:\\/\\/)?'+ // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
                '(\\#[-a-z\\d_]*)?$','i');

                var row=document.createElement('ion-row');
                  console.log("row created");
                  var row1=document.createElement('ion-row');
                if(element.user.user_id==this.user_two){
                  
                  if(element.msg!==''&&element.msg!==null){
                    console.log("msggggg:",element.msg);
                    
                  row1.innerHTML = '<ion-col class="col_fit"><ion-avatar><img src="'+element.user.profile_pic+'"></ion-avatar></ion-col><ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation">'+element.msg+'</p><ion-label class="conversation_time">'+this.updated_at+'</ion-label></ion-col>';
                     
                  
                    
                    row1.addEventListener('click',()=>{
                      if(element.msg.length<160){
                      
                      if(pattern.test(element.msg)){
                      // if(t<=300000){
                      
                      this.jitsiCall(element.msg);
                      // }else{
                      //   this.notActiveAlert()
                      }
                      }
                    })
                  
                  
                  
                  
                  }
                  if(element.attachment!=='' && element.attachment !== null){
                    
                    let fileExtension=(element.attachment).substr((element.attachment).lastIndexOf('.') + 1);
                    if((fileExtension=='jpg')||(fileExtension=='jpeg')||(fileExtension=='png')){

                  row.innerHTML = '<ion-col class="col_fit"><ion-avatar><img src="'+element.user.profile_pic+'"></ion-avatar></ion-col><ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation_bordered"><img src="'+element.attachment+'" style="height: 180px;"></p><p class="conversation_time">'+this.updated_at+'</p></ion-col>';
                  
                  row.addEventListener('click',()=>{
                      this.openDoc(element.attachment)
                    })
                  }else if(fileExtension=='mp4'||fileExtension=='mkv'||fileExtension=='avi'||fileExtension=='mov'||fileExtension=='x-flv' || fileExtension=='x-mpegURL'|| fileExtension=="MP2T" || fileExtension=="3gpp"|| fileExtension=="quicktime"|| fileExtension=="x-msvideo"|| fileExtension=="x-ms-wmv"){
                   
                    // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(element.attachment);
                    row.innerHTML='<ion-col class="col_fit"><ion-avatar><img src="'+element.user.profile_pic+'"></ion-avatar></ion-col><ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;"class="conversation_bordered"><video src="'+element.attachment+'#t=0.05" preload="metadata" style="height:100%; width:100%;"></p><div class="video-overlay_msg"></div><div class="play-button-holder"><ion-icon name="play"  class="play_icon_msg" ></ion-icon></div><p class="conversation_time">'+this.updated_at+'</p></ion-col>';
                    row.addEventListener('click',()=>{
                      this.playVdo(element.attachment)
                    })
                  }else{
                    let filename=(element.attachment).substr((element.attachment).lastIndexOf('/') + 1);
                    let doc_icon;
                   
                        if((fileExtension=='pdf')){
                          doc_icon='<ion-icon src="assets/pdf_icon.svg"></ion-icon><span style="margin-left:6px">'
                        }else if(fileExtension=='doc'||fileExtension=="msword" || fileExtension=="ms-doc" || fileExtension=="document"){
                          doc_icon='<ion-icon src="assets/ms_word_doc_icon.svg"></ion-icon><span style="margin-left:6px">'
                        }else if(fileExtension=='sheet'||fileExtension=="excel"|| fileExtension=="ms-excel"|| fileExtension=="x-excel"|| fileExtension=="x-msexcel"){
                          doc_icon='<ion-icon src="assets/excel-icon.svg"></ion-icon><span style="margin-left:6px">'
                        }
                    row.innerHTML='<ion-col class="col_fit"><ion-avatar><img src="'+element.user.profile_pic+'"></ion-avatar></ion-col><ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:10px;"class="conversation">'+doc_icon+filename+'</span></p><p class="conversation_time">'+this.updated_at+'</p></ion-col>';
                    row.addEventListener('click',()=>{
                      this.openPdf(element.attachment)
                    })
                  
                  }
                  }
                }else if(element.user.user_id==this.id){
                 
                  
                  if(element.msg!==''&&element.msg!==null){
                   
                    
                  row1.innerHTML ='<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation">'+element.msg+'</p><p class="conversation_time">'+this.updated_at+'</p></ion-col>';
                  console.log("innerHtml added:",element.msg);
                  
                  row1.addEventListener('click',()=>{
                    if(element.msg.length<160){
                    
                    if(pattern.test(element.msg)){
                    // if(t<=300000){
                     
                    this.jitsiCall(element.msg);
                    // }else{
                    //   this.notActiveAlert()
                    }
                    }
                  })
                }
                if(element.attachment!=='' && element.attachment !== null){
                  let fileExtension=(element.attachment).substr((element.attachment).lastIndexOf('.') + 1);
                  if((fileExtension=='jpg')||(fileExtension=='jpeg')||(fileExtension=='png')){

                    row.innerHTML ='<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation_bordered"><img src="'+element.attachment+'" style="height: 180px;"></p><p class="conversation_time">'+this.updated_at+'</p></ion-col>';
                    row.addEventListener('click',()=>{
                      this.openDoc(element.attachment)
                    })
                  }else if(fileExtension=='mp4'||fileExtension=='mkv'||fileExtension=='avi'||fileExtension=='mov'||fileExtension=='x-flv' || fileExtension=='x-mpegURL'|| fileExtension=="MP2T" || fileExtension=="3gpp"|| fileExtension=="quicktime"|| fileExtension=="x-msvideo"|| fileExtension=="x-ms-wmv"){
                    // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(element.attachment);
                    row.innerHTML='<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation_bordered"><video src="'+element.attachment+'#t=0.05" preload="metadata" style="height:100%; width:100%;"></p><div class="video-overlay_msg"></div><div class="play-button-holder"><ion-icon name="play"  class="play_icon_msg" ></ion-icon></div><p class="conversation_time">'+this.updated_at+'</p></ion-col>';
                    row.addEventListener('click',()=>{
                      this.playVdo(element.attachment)
                    })
                  }else{
                    let filename=(element.attachment).substr((element.attachment).lastIndexOf('/') + 1);
                    let doc_icon;
                        if((fileExtension=='pdf')){
                          doc_icon='<ion-icon src="assets/pdf_icon.svg"></ion-icon><span style="margin-left:6px">'
                        }else if(fileExtension=='doc'||fileExtension=="msword" || fileExtension=="ms-doc" || fileExtension=="document"){
                          doc_icon='<ion-icon src="assets/ms_word_doc_icon.svg"></ion-icon><span style="margin-left:6px">'
                        }else if(fileExtension=='sheet'||fileExtension=="excel"|| fileExtension=="ms-excel"|| fileExtension=="x-excel"|| fileExtension=="x-msexcel"){
                          doc_icon='<ion-icon src="assets/excel-icon.svg"></ion-icon><span style="margin-left:6px">'
                        }
                    row.innerHTML='<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation">'+doc_icon+filename+'</span></p><p class="conversation_time">'+this.updated_at+'</p></ion-col>';
                    row.addEventListener('click',()=>{
                      this.openPdf(element.attachment)
                    })
                  }
                  }
                }
               console.log("ids:",element.user.user_id,this.user_two,this.id);
                if(element.user.user_id==this.user_two){
                  console.log("item on left");
                  if(element.msg!==''&&element.msg!==null){
                    row1.classList.add('conversation_item_left');
                  }
                  if(element.attachment!==''&&element.attachment!==null){
                  row.classList.add('conversation_item_left');
                  }
                }else if(element.user.user_id==this.id){
                  console.log("item on right");
                  if(element.msg!==''&&element.msg!==null){
                    row1.classList.add('conversation_item_right');
                  }
                  if(element.attachment!==''&&element.attachment!==null){
                  row.classList.add('conversation_item_right');
                  }
                }
                // document.querySelector('.msgarea').appendChild(row);
                // document.getElementById('chatArea').appendChild(row);
                if(element.delete_status==this.id||element.delete_status==1){
                  console.log("chat deleted");
                  
                }else{
                  console.log("row appended,",element.msg,element.attachment);
                  if(element.msg!==''&&element.msg!==null){
                this.chat.appendChild(row1);
                  }
                  if(element.attachment!==''&&element.attachment!==null){
                    this.chat.appendChild(row)
                  }
                }
                if(element.user.user_id==this.id){
                row.addEventListener('touchstart',()=>{
                   this.touchstart(element.cr_id,row);
                  })
                  row.addEventListener('touchend',()=>{
                    this.touchend();
                  })
                  row1.addEventListener('touchstart',()=>{
                    this.touchstart(element.cr_id,row1);
                   })
                   row1.addEventListener('touchend',()=>{
                     this.touchend();
                   })
                }
                // this.msg.push({'attachment':element.attachment,'msg':element.msg,'updated_at':this.updated_at,'user':element.user,'ext':this.ext,'att_name':this.att_name});
          
                // 
                setTimeout(() => {
                  // window.scrollTo(0,document.body.scrollHeight);
                  this.content.scrollToBottom(0);
              }, 0);
              
        })
        this.last_date=new Date(this.message[this.message.length-1].created_at);
        // this.content.scrollToBottom(0);
        this.dismissLoader();
      }
    })
  

  }
  fixDigit(val){
    return val.toString().length === 1 ? "0" + val : val;
  }
  getMon(mon){
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    return month[mon];
  }
  async sendMessage(){
    
    // this.sendMessageToScaleDrone(this.inputMessage);
    console.log("id:",this.id)
    
    // this.storage.ready().then(()=>{
    //   const data=await this.storage.get('USER_ID');
    //       this.id=data;
    if(this.inputMessage==undefined||this.inputMessage=="" ){
      console.log("null msg");
      
    }else{
      this.disable=true
          console.log("push message:",this.inputMessage);
          let  url = this.config.domain_url+'messaging';
          let body={
            roomid:this.roomid,
            message:this.inputMessage,
            user_id:this.id,
            attachment:''
          }
          let headers= await this.config.getHeader();
          console.log("messbody:",body);
          
         
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log("mess:",res);
              var pattern = new RegExp('(https?:\\/\\/)?'+ // protocol
                  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
                  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
                  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
                  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
                  '(\\#[-a-z\\d_]*)?$','i');
  
              let ti=moment.utc(res.roomid.created_time).local().format();
                      let time=new Date(ti);
                      var hours = time.getHours();
                    let minutes = time.getMinutes();
                    var ampm = hours >= 12 ? 'pm' : 'am';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    var minute = minutes < 10 ? '0'+minutes : minutes;
                    let updated_at= hours + ':' + minute + ' ' + ampm;
             
  
  
                    var div1 = document.createElement('ion-row');
                    if(res.roomid.msg!=''&& res.roomid.msg!=null){
                      console.log("msg");
                      
                      div1.innerHTML = '<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation">'+res.roomid.msg+'</p><p class="conversation_time">'+updated_at+'</p></ion-col>';
                        div1.addEventListener('click',()=>{
                          if(res.roomid.msg.length<160){
                          console.log("long text check")
                          if(pattern.test(res.roomid.msg)){
                          
                          this.jitsiCall(res.roomid.msg);
                          
                        }
                          }
                        })
  
                        div1.addEventListener('touchstart',()=>{
                          this.touchstart(res.roomid.id,div1);
                         })
                         div1.addEventListener('touchend',()=>{
                           this.touchend();
                         })
  
                        
                          div1.classList.add('conversation_item_right');
                         
                         this.text.appendChild(div1);
                         setTimeout(() => {
                          // window.scrollTo(0,document.body.scrollHeight);
                          this.content.scrollToBottom(0);
                      }, 0);
                      
            this.disable=false
                    }
          },error=>{
            console.log(error);
            this.disable=false
          })
        
  this.inputMessage = '';
          
  }
  }

// scaledrone configuration

// sendMessageToScaleDrone(content) {
//   console.log("send invoked");
//   this.storage.ready().then(()=>{
//     const data=await this.storage.get('USER_ID');
//         this.id=data;
//   this.drone.publish({
//     room: ''+this.roomid+'',
//     message: {
     
//       content: content,
//       user_id: this.id
     
//     }
//   });
//   console.log("message send");
// })
//   })
// }

callFunction(){
  // this.content.scrollToBottom(0);
  // setTimeout(() => {
    // if(this.scroll) {
      this.content.scrollToBottom(0);
      // this.scroll = false;
  // }
// }, 1000);
}
ionViewDidEnter(){
  this.content.scrollToBottom(0);
}

// start videocall
videoCall(){
  this.permission.checkPermission(this.permission.PERMISSION.CAMERA).then(
    result => console.log('Has permission?',result.hasPermission),
    err => this.permission.requestPermission(this.permission.PERMISSION.CAMERA),
    
  );
  // this.permission.checkPermission(this.permission.PERMISSION.MICROPHONE).then(
  //   result => console.log('Has permission?',result.hasPermission),
  //   err => this.permission.requestPermission(this.permission.PERMISSION.MICROPHONE),
    
  // );
  
  this.permission.requestPermissions([this.permission.PERMISSION.CAMERA,this.permission.PERMISSION.RECORD_AUDIO]);
 
    // this.router.navigate(['/video-call',{user_one:this.id,user_two:this.user_two,pic:this.pic,name:this.name}]);
    
  this.startCall();
}



// take picture with camera
async openCamera(){
  // const options: CameraOptions = {
  //   quality: 100,
  //   sourceType: this.camera.PictureSourceType.CAMERA,
  //   destinationType: this.camera.DestinationType.FILE_URI,
  //   encodingType: this.camera.EncodingType.JPEG,
  //   mediaType: this.camera.MediaType.PICTURE
  // }
  // this.camera.getPicture(options).then((imageData) => {
   
  //   this.attach=imageData;
  //   console.log("att:",this.attach);
   
  //   this.attachment(this.attach,'jpg');
  
  // }, (err) => {
   
  // });

  const image = await Camera.getPhoto({
    quality: 90,
    allowEditing: false,
    resultType: CameraResultType.Base64,
    correctOrientation:true,
    source:CameraSource.Camera
  });

 
  var imageUrl = image.base64String;

  // Can be set to the src of an image now
  let base64Image = 'data:image/jpeg;base64,' + imageUrl;
  console.log('image:',imageUrl);
  this.attach=base64Image;
  //   console.log("att:",this.attach);
   
    this.attachment(this.attach,'jpg',this.attach);
}

async selectAttach(){
  

  const actionSheet = await this.actionsheetCntlr.create({
    header: 'Select',
    mode:'md',
    buttons: [{
      text: 'Image',
      // role: 'destructive',
      icon: 'image-outline',
      handler: () => {
        this.selectImage();
        console.log('Delete clicked');
      }
    }, {
      text: 'Video',
      // role: 'destructive',
      icon: 'videocam-outline',
      handler: () => {
        this.selectVdo();
        console.log('Delete clicked');
      }
    },{
      text: 'Document',
      icon: 'document-outline',
      handler: () => {
        this.selectDoc();
        console.log('Favorite clicked');
      }
    }, {
      text: 'Cancel',
      // icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  });
  await actionSheet.present();




  
  // const options: CameraOptions = {
  //   quality: 100,
  //   sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
  //   destinationType: this.camera.DestinationType.FILE_URI,
    
  //   mediaType: this.camera.MediaType.ALLMEDIA
  // }
  
  // this.camera.getPicture(options).then((imageData) => {
   
  //   this.attach=imageData;
  //   console.log("a:",this.attach);
  //   if((this.attach).includes('.png')||(this.attach).includes('.jpg')||(this.attach).includes('image')){
  //   console.log('attach img');
  //  if(this.attach.includes('file://')||this.attach.includes('content://')){
  //   this.attachment(this.attach,'jpg');
  //   }else{
  //     this.attachment('file://'+this.attach,'jpg');
  //   }
    
  //   }else if((this.attach).includes('.mp4')||(this.attach).includes('.mkv')||(this.attach).includes('.avi')||(this.attach).includes('.mov')){
  //     console.log('attach vdo');
  //     let fileUrl='file://'+this.attach;
      
      
  //     this.file.resolveLocalFilesystemUrl(fileUrl).then(fileEntry => {
       
  //       fileEntry.getMetadata((metadata) => {
  //           console.log("sizee:",metadata.size);//metadata.size is the size in bytes
  //          if(metadata.size>this.MAX_FILE_SIZE){
  //            this.alert('File is larger than 24MB');
  //            console.log('large size');
             
  //          }
  //          else{
  //           let ext;
  //           if((this.attach).includes('.mp4')){
  //             ext='mp4'
  //           }else if((this.attach).includes('.mkv')){
  //             ext='mkv'
  //           }else if((this.attach).includes('.mov')){
  //             ext='mov'
  //           }else{
  //             ext='avi'
  //           }
            
  //             console.log("vdo:",this.attach);
              
  //             this.attachment(fileUrl,ext);
           
  //          }
  //       })
       
  //   })

    
  
  //   }else if((this.attach).includes('.pdf')||(this.attach).includes('.doc')){
  //     console.log("send doc:",this.attach);
  //     let fileUrl=this.attach;
    
  //     console.log('fileuri:',fileUrl)
  //     let ext;
  //           if((this.attach).includes('.pdf')){
  //             ext='pdf'
  //           }else if((this.attach).includes('.doc')){
  //             ext='doc'
  //           }else if((this.attach).includes('.xls')){
  //             ext='xls'
  //           }
  //     this.sendDoc(fileUrl,ext);
  //   }else{
  //     console.log('not supported');
      
  //     this.alert('File format not supported');
  //   }
  // }, (err) => {
   
  // });

}
async selectImage(){
  const image = await Camera.pickImages({
    quality: 100,
    correctOrientation:true,
    limit:1
    
  });


  for (var i = 0; i < image.photos.length; i++) {
     
          
          
          const contents = await Filesystem.readFile({
            path: image.photos[i].path
          });
          
            
          this.attach='data:image/jpeg;base64,' + contents.data
          //   console.log("att:",this.attach);
           
            this.attachment(this.attach,'jpg',this.attach);
            
       
           
      }
}
async selectVdo(){
  this.chooser.getFile()
  .then(file => {
   
    if(file){
      console.log('type:',file,file.mediaType);
    
      if(file.mediaType=="video/x-flv" || file.mediaType=="video/mp4" || file.mediaType=="application/x-mpegURL"
      || file.mediaType=="video/MP2T" || file.mediaType=="video/3gpp"
      || file.mediaType=="video/quicktime"|| file.mediaType=="video/x-msvideo"|| file.mediaType=="video/x-ms-wmv"){
        let ext='.'+file.mediaType.split('/')[1];
       
        this.attachment(file.uri,ext,file.dataURI);
    }else{
      this.alert('File format not supported');
    }
  }
  })
  .catch((error: any) => console.error(error));
}
async selectDoc(){
  this.chooser.getFile()
  .then(file => {
   
    if(file){
      console.log('type:',file.mediaType);
    
      if(file.mediaType=="application/pdf" || file.mediaType=="application/msword" || file.mediaType=="application/doc"
      || file.mediaType=="application/ms-doc" || file.mediaType=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"
      || file.mediaType=="application/excel"|| file.mediaType=="application/vnd.ms-excel"|| file.mediaType=="application/x-excel"
      || file.mediaType=="application/x-msexcel"|| file.mediaType=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
      this.sendDoc(file.dataURI,file.name);
    }else{
      this.alert('File format not supported');
    }
  }
  })
  .catch((error: any) => console.error(error));
  
}
async sendImg(item){
  let msg;
  if(this.inputMessage==undefined){
    msg=''
  }else{
    msg=this.inputMessage
  }
  console.log("attachsend:",this.attach);


  let headers= await this.config.getHeader()
   

    // const contents = await Filesystem.readFile({
    //   path: item
    // });

    // let base64File='data:image/jpeg;base64,'+contents.data;
           
            let  url = this.config.domain_url+'messaging';
            let body ={
              roomid:this.roomid,
              message:msg,
              user_id:this.id,
              img_bs:item
          }
            console.log('body:',body)
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log(res);
              this.inputMessage='';
              let ti=moment.utc(res.roomid.created_time).local().format();
                    let time=new Date(ti);
                  
                      
                  var hours = time.getHours();
                  let minutes = time.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  var minute = minutes < 10 ? '0'+minutes : minutes;
                  let updated_at= hours + ':' + minute + ' ' + ampm;
                  
                
                 
                  var div = document.createElement('ion-row');

                  
                  if(res.roomid.attachment!=='' && res.roomid.attachment !== null){
                    
  
                      div.innerHTML ='<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation_bordered"><img src="'+res.roomid.attachment+'" style="height: 180px;"></p><p class="conversation_time">'+updated_at+'</p></ion-col>';
                      div.addEventListener('click',()=>{
                        this.openDoc(res.roomid.attachment)
                      })
                  
                      div.addEventListener('touchstart',()=>{
                        this.touchstart(res.roomid.id,div);
                       })
                       div.addEventListener('touchend',()=>{
                         this.touchend();
                       })
                   
                     div.classList.add('conversation_item_right');
          this.text.appendChild(div);
          
          setTimeout(() => {
            // window.scrollTo(0,document.body.scrollHeight);
            this.content.scrollToBottom(0);
        }, 0);
      }
              this.dismissLoader();
            },error=>{
              console.log(error);
              this.dismissLoader();
            })
  
    // console.log('content:', contents);
  

}


async sendVdo(file,ext){
  let msg;
  if(this.inputMessage==undefined){
    msg=''
  }else{
    msg=this.inputMessage
  }


  // const contents = await Filesystem.readFile({
  //   path: file
  // });
  
  // let base64File='data:video/'+ext+';base64,'+contents.data;
  let headers= await this.config.getHeader()
  let  url = this.config.domain_url+'messaging';
  let body ={
    roomid:this.roomid,
    message:msg,
    user_id:this.id,
    video_bs:file
}
console.log('body:',body)
this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log(res);
    this.inputMessage='';
    let ti=moment.utc(res.roomid.created_time).local().format();
                    let time=new Date(ti);
                  
                      
                  var hours = time.getHours();
                  let minutes = time.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  var minute = minutes < 10 ? '0'+minutes : minutes;
                  let updated_at= hours + ':' + minute + ' ' + ampm;
                  
                
                 
                  var div = document.createElement('ion-row');
                  if(res.roomid.attachment!=='' && res.roomid.attachment !== null){
                    div.innerHTML='<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:0px;" class="conversation_bordered"><video src="'+res.roomid.attachment+'#t=0.05" preload="metadata" style="height:150px;"></p><div class="video-overlay_msg"></div><div class="play-button-holder"><ion-icon name="play"  class="play_icon_msg" ></ion-icon></div><p class="conversation_time">'+updated_at+'</p></ion-col>';
                    div.addEventListener('click',()=>{
                      this.playVdo(res.roomid.attachment)
                    })
                  
                    div.addEventListener('touchstart',()=>{
                      this.touchstart(res.roomid.id,div);
                     })
                     div.addEventListener('touchend',()=>{
                       this.touchend();
                     })
                 
                   div.classList.add('conversation_item_right');
        this.text.appendChild(div);
        
        setTimeout(() => {
          // window.scrollTo(0,document.body.scrollHeight);
          this.content.scrollToBottom(0);
      }, 0);
    }
    this.dismissLoader();
  },error=>{
    console.log(error);
    this.dismissLoader();
  })

// console.log('content:', contents);

}


async sendDoc(file,ext){
  let msg;
  if(this.inputMessage==undefined){
    msg=''
  }else{
    msg=this.inputMessage
  }
  let headers= await this.config.getHeader()
    
  // const contents = await Filesystem.readFile({
  //   path: file
  // });
  // console.log('content:',ext);
  
  // let base64File='data:application/'+ext+';base64,'+contents.data;
  
 
           console.log('doc:',file)
  let  url = this.config.domain_url+'messaging';
  let body ={
    roomid:this.roomid,
    message:msg,
    user_id:this.id,
    doc_bs:file
}
console.log('body:',body)
this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log(res);
    this.inputMessage='';
    let ti=moment.utc(res.roomid.created_time).local().format();
    let time=new Date(ti);
  
      
  var hours = time.getHours();
  let minutes = time.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  var minute = minutes < 10 ? '0'+minutes : minutes;
  let updated_at= hours + ':' + minute + ' ' + ampm;
  

 
  var div = document.createElement('ion-row');
  if(res.roomid.attachment!=='' && res.roomid.attachment !== null){
  let fileExtension=(res.roomid.attachment).substr((res.roomid.attachment).lastIndexOf('.') + 1);
    let filename=(res.roomid.attachment).substr((res.roomid.attachment).lastIndexOf('/') + 1);
                      let doc_icon;
                          if((fileExtension=='pdf')){
                            doc_icon='<ion-icon src="assets/pdf_icon.svg"></ion-icon><span style="margin-left:6px">'
                          }else if(fileExtension=='doc'||fileExtension=="msword" || fileExtension=="ms-doc" || fileExtension=="document"){
                            doc_icon='<ion-icon src="assets/ms_word_doc_icon.svg"></ion-icon><span style="margin-left:6px">'
                          }else if(fileExtension=='sheet'||fileExtension=="excel"|| fileExtension=="ms-excel"|| fileExtension=="x-excel"|| fileExtension=="x-msexcel"){
                            doc_icon='<ion-icon src="assets/excel-icon.svg"></ion-icon><span style="margin-left:6px">'
                          }
                          div.innerHTML='<ion-col class="col_fit" style="padding-left:15px;"><p style="margin-bottom:10px;" class="conversation">'+doc_icon+filename+'</span></p><p class="conversation_time">'+updated_at+'</p></ion-col>';
                          div.addEventListener('click',()=>{
                            this.openPdf(res.roomid.attachment)
                      })
                      div.addEventListener('touchstart',()=>{
                        this.touchstart(res.roomid.id,div);
                       })
                       div.addEventListener('touchend',()=>{
                         this.touchend();
                       })
                   
                     div.classList.add('conversation_item_right');
          this.text.appendChild(div);
          
          setTimeout(() => {
            // window.scrollTo(0,document.body.scrollHeight);
            this.content.scrollToBottom(0);
        }, 0);
      }
    this.dismissLoader();
  },error=>{
    console.log(error);
    this.dismissLoader();
  })


  
}

async alert(mes){
  // let message;
  // if(type==2){
  //   message='File format not supported'
  // }
  const alert = await this.alertCntlr.create({
    
    message: mes,
    backdropDismiss:true
  });

  await alert.present();
}


async jitsiCall(link){



  // const roomId =link.substr(link.lastIndexOf('/') + 1);

  // const result = await Jitsi.joinConference({
  //   // required parameters
    
  //   roomName: roomId, // room identifier for the conference
  //   url: 'https://meet.jit.si', // endpoint of the Jitsi Meet video bridge

  //   // recommended settings for production build. see full list of featureFlags in the official Jitsi Meet SDK documentation
  //   featureFlags: {
  //       'prejoinpage.enabled': false, // go straight to the meeting and do not show the pre-join page
  //       'recording.enabled': false, // disable as it requires Dropbox integration
  //       'live-streaming.enabled': false, // 'sign in on Google' button not yet functional
  //       'android.screensharing.enabled': false, // experimental feature, not fully production ready
  //   },
  // })
    // if(this.platform.is('android')){
  //   const roomId =link.substr(link.lastIndexOf('/') + 1);
  
  //   // const roomId = 'VqyaB3flP';
  //   jitsiplugin.loadURL('https://meet.jit.si/' + roomId, roomId, false, function (data) {
  //     if (data === "CONFERENCE_WILL_LEAVE") {
  //         jitsiplugin.destroy(function (data) {
  //             // call finished
  //         }, function (err) {
  //             console.log(err);
  //         });
  //     }
  // }, function (err) {
  //     console.log(err);
  // });
  //  }else {
    let url=link.substr(link.lastIndexOf(' ') + 1);
    //   console.log("jitsi call:",url);
    let options:InAppBrowserOptions ={
      location:'no',
      hideurlbar:'yes',
      zoom:'no'
    }
    
    const browser = this.iab.create(url,'_system',options);
    
  //  }
}

// // for files except images

// sendFile(uri){
//   let fileExtension=uri.substr(uri.lastIndexOf('.') + 1);
//   let fileMIMEType=this.getMIMEtype(fileExtension);
//   let filename=uri.substr(uri.lastIndexOf('/') + 1);
  
//   // if(fileMIMEType==undefined){
//   //   fileMIMEType='image/jpg'
//   // }
//   console.log("ext:",fileMIMEType);
//   const fileTransfer: FileTransferObject=this.transfer.create();
//         let options: FileUploadOptions = {
//           fileKey: 'attachment',
//           fileName: filename,
//           chunkedMode:false,
//           httpMethod:'post',
//           mimeType:fileMIMEType,
//           headers:{},
//           params:{
//             roomid:this.roomid,
//             message:'',
//             user_id:this.id}
//         }
//         let  url = this.config.domain_url+'messaging';
//         console.log("trans:",options)
//         fileTransfer.upload(this.attach,url,options).then((data)=>{
//           console.log("success");
//           console.log(data);
          
          
//         },error=>{
//           console.log(error);
          
//         })
// }


// getMIMEtype(extn){
//   let ext=extn.toLowerCase();
//   let MIMETypes={
//     'txt' :'text/plain',
//     'docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
//     'doc' : 'application/msword',
//     'pdf' : 'application/pdf',
//     'jpg' : 'image/jpeg',
//     'bmp' : 'image/bmp',
//     'png' : 'image/png',
//     'xls' : 'application/vnd.ms-excel',
//     'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
//     'rtf' : 'application/rtf',
//     'ppt' : 'application/vnd.ms-powerpoint',
//     'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
//   }
//   return MIMETypes[ext];
// }



async openDoc(item) {
console.log("openDoc");

  const popover = await this.popoverController.create({
    component: OpenimageComponent,
    backdropDismiss:true,
    componentProps:{
      data:item
    },
    cssClass:'msg_attach'
  });
  return await popover.present();
}

async startCall(){
  const modal = await this.modalCntl.create({
    component: VideocallComponent,
    componentProps: {
      user_one:this.id,
      user_two:this.user_two,
      pic:this.pic,
      name:this.name
        
    }
  });
  return await modal.present();
}

async showLoading() {
 
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    duration: 3000
  });
  return await loading.present();
}
async dismissLoader() {
  return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}

touchstart(crid,row) {
  let that=this;
 
  this.timer = setTimeout(()=>{
    // console.log(that.presentAlertConfirm(crid));
    
     that.presentAlertConfirm(crid,row);
    // this.storage.ready().then(()=>{
    //   const data=await this.storage.get('USER_ID');
  
        // let body={
        //   crid:crid,
        //   user_one:that.id
        // }
  
        // let headers=that.config.head;
        // let url=that.config.domain_url+'deletemessagesingle';
        // that.http.post(url,body,{headers}).subscribe((res:any)=>{
        //   console.log(res);
          
        // })
  
  
    //   })
    // })
  }, that.longpress); 
 
}

touchend() {

  //stops short touches from firing the event
  if (this.timer)
      clearTimeout(this.timer); // clearTimeout, not cleartimeout..
}



async presentAlertConfirm(cid,row) {
  
  const alert = await this.alertCntlr.create({
    header: 'Confirm',
    backdropDismiss:true,
    message: 'Delete this message?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Delete',
        handler: () => {
         
          
         this.deleteSingleMsg(cid,row);

        }
      }
    ]
  });
  
  await alert.present();
 
}

async deleteSingleMsg(id,row){
 

      let body={
        crid:id,
        user_one:this.id
      }
      
      let headers= await this.config.getHeader();
      let url=this.config.domain_url+'deletemessagesingle';
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        if(res.status=='success'){
          this.notActiveAlert('Deleted successfully.')
          row.remove();
        }
        
      })

   
   
}
generateRoom(){
  let outString: string = '';
  let inOptions: string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

  for (let i = 0; i < 9; i++) {

    outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

    
  }
  return outString;
}
async virtualMeet(){
  let jitsiRoom=this.generateRoom();
//  this.storage.ready().then(()=>{
   const data=await this.storage.get('COMPANY_ID');
     let url=this.config.domain_url+'generatecallurl';
     let headers= await this.config.getHeader();
     let body={company_id:data}
     this.http.post(url,body,{headers}).subscribe((res:any)=>{
       let callLink=res.vdocalllink+jitsiRoom;
      //  this.jitsiCall(callLink);
      //  this.sendLink(callLink);
      this.callPrompt(callLink);
       
     })
//    })
//  })

}

async callPrompt(callLink) {
  
  const alert = await this.alertCntlr.create({
    header: 'Send video call link to contact?',
    backdropDismiss:true,
    message: callLink+'<br/> Once you click on join call button, please wait until other person joins the call by clicking on the link.',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Share & Join',
        handler: () => {
         
          this.jitsiCall(callLink);
         this.sendLink(callLink);

        }
      }
    ]
  });
  
  await alert.present();
 
}
async sendLink(callLink){
  let  url = this.config.domain_url+'messaging';
          let body={
            roomid:this.roomid,
            message:this.user_name+' has requested a video call with you.\nPlease click link below to start the call\n'+callLink,
            user_id:this.id,
            attachment:''
          }
          let headers= await this.config.getHeader();
          console.log("messbody:",body);
          
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log("mess:",res);
            
          },error=>{
            console.log(error);
            
          })
}
playVdo(attachment){
  let options: StreamingVideoOptions = {
    successCallback: () => { console.log('Video played') },
    errorCallback: (e) => { console.log('Error streaming') },
    orientation: 'landscape',
    shouldAutoClose: true,
    controls: true
  };
  
  this.streamingMedia.playVideo(attachment, options);
}

async notActiveAlert(mes){
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}


openPdf(attachment){
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
  hideurlbar:'yes',
  zoom:'yes',
  beforeload:'yes',
      clearcache:'yes'
  }
  if(attachment.includes('.pdf')){
    this.showpdf(attachment)
}else{
  const browser = this.iab.create(encodeURI('https://docs.google.com/gview?embedded=true&url='+attachment),'_blank',options);
}
}
async options(ev){

  const popover = await this.popoverController.create({
    component: MessageoptionsComponent,
    event:ev,
    backdropDismiss:true,
    cssClass:'message-options',
    componentProps:{
      deleteStatus:this.deleteStatus
    }
    
  });
  popover.onDidDismiss().then((data)=>{
   if(data.data==1){
      this.deleteChatAlert();
      // this.chat.innerHTML='';
      // this.text.innerHTML='';
   }else if(data.data==2){
    this.reason();
   }
  })
  return await popover.present();
}

async deleteChatAlert(){

  const alert = await this.alertCntlr.create({
    header: 'Delete chat',
    backdropDismiss:true,
    message: 'Are you sure you want to delete this chat?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Delete',
        handler: () => {
         
          
          this.deleteChat();
          this.chat.innerHTML='';
              this.text.innerHTML='';

        }
      }
    ]
  });
  
  await alert.present();

 
}

async reason(){
  const modal = await this.modalCntl.create({
    component: ReportReasonComponent,
    cssClass:'report-reason-modal',
    backdropDismiss:true,
    
    
  });
  modal.onDidDismiss().then((data)=>{
    let reason;
    if(data.role=='1'){
      if(data.data){
     reason=data.data
    }else{
      reason='';
    }
      this.block(reason);
  }
  })
  
  return await modal.present();
}
async deleteChat(){
  // this.storage.ready().then(()=>{
    let headers= await this.config.getHeader();
    const data=await this.storage.get('USER_ID');
  let url=this.config.domain_url+'deletechat';
  let body={
    roomid:this.roomid,
    user_one:data,
    user_two:this.user_two
  }
  console.log("bo:",body);
  
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log(res);
    this.notActiveAlert('Chat deleted successfully.')
  })
// })
//   })
}


async attachment(file,ext,dataUri){
// drone.close();
  const modal = await this.modalCntl.create({
    component: MessageAttachmentComponent,
    cssClass:'message_attachment',
    backdropDismiss:true,
    componentProps:{
      data:file,
      ext:ext,
      roomid:this.roomid,
      user:this.id
    }
    
    
  });
  modal.onDidDismiss().then((data)=>{
  //  this.ionViewWillEnter();
  if(data.data!=undefined){
    if(data.role=='jpg'){
      this.sendImg(dataUri);
      this.showLoading();
    }else{
      this.sendVdo(dataUri,data.role);
      this.showLoading();
    }
  }
  })
  return await modal.present();
}

async showpdf(file){
  const modal = await this.modalCntl.create({
    component: ViewpdfComponent,
    cssClass:'fullWidthModal',
    componentProps: {
      
      data:file,
      
       
    },
    
    
  });
  return await modal.present();
}
async block(reason){
  const uid=await this.storage.get('USER_ID');
  const bid=await this.storage.get('BRANCH');
 const cid=await this.storage.get('COMPANY_ID');
  let url=this.config.domain_url+'report-issue';
  let headers= await this.config.getHeader()
  let body;
  body={
    user_id:uid,
    reason:reason,
    content_type:'CONVERSATION',
    request_id:this.user_two
  }
  console.log('report:',body);
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log(res);
    
      this.notActiveAlert('Request to block user has been submitted. We shall verify and take appropriate action');
    
  },error=>{
    this.notActiveAlert('Something went wrong.Please try again later');
  })
}


  

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewPersonalMessagePage } from './new-personal-message.page';

const routes: Routes = [
  {
    path: '',
    component: NewPersonalMessagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewPersonalMessagePageRoutingModule {}

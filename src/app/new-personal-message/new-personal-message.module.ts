import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewPersonalMessagePageRoutingModule } from './new-personal-message-routing.module';

import { NewPersonalMessagePage } from './new-personal-message.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewPersonalMessagePageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [NewPersonalMessagePage]
})
export class NewPersonalMessagePageModule {}

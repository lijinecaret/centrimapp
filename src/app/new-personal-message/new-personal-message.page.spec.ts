import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewPersonalMessagePage } from './new-personal-message.page';

describe('NewPersonalMessagePage', () => {
  let component: NewPersonalMessagePage;
  let fixture: ComponentFixture<NewPersonalMessagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPersonalMessagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewPersonalMessagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

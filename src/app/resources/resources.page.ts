import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Router } from '@angular/router';
import { HttpConfigService } from '../services/http-config.service';
import { ModalController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Storage } from '@ionic/storage-angular';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
@Component({
  selector: 'app-resources',
  templateUrl: './resources.page.html',
  styleUrls: ['./resources.page.scss'],
})
export class ResourcesPage implements OnInit {
token:any;
resource:any=[];
subscription:Subscription;
hide:boolean=false;
srch:boolean=false;
terms:any;
doc:any;
folder:any;
  constructor(private http:HttpClient,private router:Router,private iab: InAppBrowser,private modalCntlr:ModalController,private config:HttpConfigService,private platform:Platform,
    private storage:Storage,private dialog:SpinnerDialog) { }

  ngOnInit() {
    
    
    
  }
  ionViewWillEnter(){
    this.getContent();
    this.terms='';
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu']) ;
  });  
  
  }
  
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
  }
  async search1()
  {
    //console.log("search keyword:",keyword);
    if(this.terms){
    this.srch=true;
    const data=await this.storage.get('TOKEN');
      let token=data;
      const bid=await this.storage.get('BRANCH');
      const cid=await this.storage.get("COMPANY_ID");
    
    let url=this.config.domain_url+'resource_search';
  let headers= await this.config.getHeader();
  let body={
    type:0,
    search:this.terms
  }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      //this.resource=res.resources.data;
     this.doc=res.resource_files;
      this.resource=res.resource_fldr;
    },error=>{
      console.log(error);
      
    })
  }else{
    this.getContent();
    this.doc=[];
  }

  }
  openDoc(item){
    if(item.url=="null"){
      // window.open(encodeURI(item.file),"_system","location=yes");
      let options:InAppBrowserOptions ={
        location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'yes',
      beforeload:'yes',
      clearcache:'yes'
      }
      this.platform.ready().then(() => {
        if(item.file.includes('.pdf')){
          this.showpdf(item.file)
      }else{
        let browser;
        if(this.platform.is('ios')){
          browser = this.iab.create(item.file,'_blank',options);
        }else{
         browser = this.iab.create(encodeURI('https://docs.google.com/gview?embedded=true&url='+item.file),'_blank',options);
        }
      this.dialog.show();  
      browser.on('loadstart').subscribe(() => {
    
        this.dialog.hide();  
       
      }, err => {
        this.dialog.hide();
      })
    
      browser.on('loadstop').subscribe(()=>{
        this.dialog.hide();;
      }, err =>{
        this.dialog.hide();
      })
    
      browser.on('loaderror').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
      
      browser.on('exit').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
    }
    })
      }else{
        let options:InAppBrowserOptions ={
          location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'no'
        }
        const browser = this.iab.create(item.url,'_blank',options);
      
      }


  }

// fetch content
async getContent(){
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('TOKEN');
      let token=data;
      const bid=await this.storage.get('BRANCH');
      const cid=await this.storage.get("COMPANY_ID");
        let branch=bid.toString();
  
  let url=this.config.domain_url+'resource?type=0';
  let headers= await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log(res);
      this.resource=res.resources.data;
    
      
    },error=>{
      console.log(error);
      
    })
//   })
// })
//   })
}
// open resource folder
openFolder(item){
  let res=[{id:item.id,title:item.title}]
  this.router.navigate(['/resources-details',{count:0,res:JSON.stringify(res)}])
}
cancel(){
  this.hide=false;
  this.terms='';
  
}
search(){
    this.hide=true;
  }
  async showpdf(file){
    const modal = await this.modalCntlr.create({
      component: ViewpdfComponent,
      cssClass:'fullWidthModal',
      componentProps: {
        
        data:file,
        
         
      },
      
      
    });
    return await modal.present();
  }
}

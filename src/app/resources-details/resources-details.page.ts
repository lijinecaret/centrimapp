import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { ModalController, Platform } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { HttpConfigService } from '../services/http-config.service';
import { Subscription } from 'rxjs';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';

@Component({
  selector: 'app-resources-details',
  templateUrl: './resources-details.page.html',
  styleUrls: ['./resources-details.page.scss'],
})
export class ResourcesDetailsPage implements OnInit {
res_id:any;
resource:any=[];
token:any;
title:any;
subscription:Subscription;
searchTerm:any;
hide:boolean=false;
terms:any;
folder:any=[];
count:any=0;
res:any=[];
  constructor(private route:ActivatedRoute,private http:HttpClient,private platform:Platform,
    private file:File,private iab: InAppBrowser,private modalCntlr:ModalController,
    private config:HttpConfigService,private router:Router,private storage:Storage,private dialog:SpinnerDialog) { }

  ngOnInit() {
    
  }
  ionViewWillEnter(){
    this.res=JSON.parse(this.route.snapshot.paramMap.get('res'));
    this.res_id=this.res[this.res.length-1].id;
    this.title=this.res[this.res.length-1].title;
    this.count=this.route.snapshot.paramMap.get('count');
    this.getContent();
    this.terms='';
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/resources']) ;

      if(this.count==0){
        this.router.navigate(['/resources']) ;
        
        
      }else{
        this.count--;
        this.res.pop();
        this.router.navigate(['/resources-details',{count:this.count,res:JSON.stringify(this.res)}])
      }
    });  
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    }
  async getContent(){
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('TOKEN');
        let token=data;
        const bid=await this.storage.get('BRANCH');
        const cid=await this.storage.get("COMPANY_ID");
          let branch=bid.toString();
    
    let url=this.config.domain_url+'resource/'+this.res_id;
    let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.resource=res.resource_files;
        this.folder=res.resource_fldr;
       
        
      },error=>{
        console.log(error);
        
      })
//     })
//   })
// })
  }


  openDoc(item){
    if(item.url=="null"){
      // window.open(encodeURI(item.file),"_system","location=yes");
      
      this.platform.ready().then(() => {
        if(item.file.includes('.pdf')){
          this.showpdf(item.file)
      }else{
        let browser;
        if(this.platform.is('ios')){
          let options:InAppBrowserOptions ={
            location:'yes',
        hidenavigationbuttons:'yes',
          hideurlbar:'yes',
          zoom:'yes',
         
          }
          browser = this.iab.create(item.file,'_blank',options);
        }else{
          let options:InAppBrowserOptions ={
            location:'yes',
        hidenavigationbuttons:'yes',
          hideurlbar:'yes',
          zoom:'yes',
          beforeload:'yes',
          clearcache:'yes'
          }
         browser = this.iab.create(encodeURI('https://docs.google.com/gview?embedded=true&url='+item.file),'_blank',options);
        }
      
        
      this.dialog.show();  
      browser.on('loadstart').subscribe(() => {
    
        this.dialog.hide();  
       
      }, err => {
        this.dialog.hide();
      })
    
      browser.on('loadstop').subscribe(()=>{
        this.dialog.hide();;
      }, err =>{
        this.dialog.hide();
      })
    
      browser.on('loaderror').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
      
      browser.on('exit').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
    }
    })
      }else{
        let options:InAppBrowserOptions ={
          location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'no'
        }
        const browser = this.iab.create(item.url,'_blank',options);
      
      }


  }
  search1(){
    this.hide=true;
  }

  // filterItems(searchTerm) {
  //   // if(!items) return [];
  //   if(!searchTerm) return this.resource;
  //   return this.resource.filter(item => {
  //     return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
  //   });
  // }
  // setFilteredItems() {
  //   this.resource = this.filterItems(this.searchTerm);
  // }
  cancel(){
    this.hide=false;
    this.terms='';
  }

  openFolder(item){

    this.count++;
    this.res.push({id:item.id,title:item.title});
      this.router.navigate(['/resources-details',{count:this.count,res:JSON.stringify(this.res)}])
    // this.router.navigate(['subfolderfiles',{title:item.title,fl_id:item.id,res_id:this.res_id,res_title:this.title}])
  }

  async showpdf(file){
    const modal = await this.modalCntlr.create({
      component: ViewpdfComponent,
      cssClass:'fullWidthModal',
      componentProps: {
        
        data:file,
        
         
      },
      
      
    });
    return await modal.present();
  }

  async search()
  {
   if(this.terms){
    const data=await this.storage.get('TOKEN');
      let token=data;
      const bid=await this.storage.get('BRANCH');
      const cid=await this.storage.get("COMPANY_ID");
      
   let headers= await this.config.getHeader()
   
    let url=this.config.domain_url+'resource_search';
  // let headers= await this.config.getHeader();
  let body={
    type:0,
    search:this.terms,
    parent_id:this.res_id
  }
  
  
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      //this.resource=res.resources.data;
     this.resource=res.resource_files;
      this.folder=res.resource_fldr;
    },error=>{
      console.log(error);
      
    })
  }else{
    this.getContent();
  }

  }

  back(){
    if(this.count==0){
      this.router.navigate(['/resources']) ;
      
      
    }else{
      this.count--;
      this.res.pop();
      this.router.navigate(['/resources-details',{count:this.count,res:JSON.stringify(this.res)}])
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VisitBookingPageRoutingModule } from './visit-booking-routing.module';

import { VisitBookingPage } from './visit-booking.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { CalendarModule } from 'ion2-calendar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisitBookingPageRoutingModule,
    ApplicationPipesModule,
    CalendarModule
  ],
  declarations: [VisitBookingPage]
})
export class VisitBookingPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisitBookingPage } from './visit-booking.page';

const routes: Routes = [
  {
    path: '',
    component: VisitBookingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisitBookingPageRoutingModule {}

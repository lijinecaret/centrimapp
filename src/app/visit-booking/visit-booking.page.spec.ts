import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisitBookingPage } from './visit-booking.page';

describe('VisitBookingPage', () => {
  let component: VisitBookingPage;
  let fixture: ComponentFixture<VisitBookingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitBookingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisitBookingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

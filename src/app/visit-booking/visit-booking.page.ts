import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { CommentComponent } from '../components/comment/comment.component';
import { MoreCallbookComponent } from '../components/more-callbook/more-callbook.component';
import { VisitBookingOptionsComponent } from '../components/visit-booking-options/visit-booking-options.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-visit-booking',
  templateUrl: './visit-booking.page.html',
  styleUrls: ['./visit-booking.page.scss'],
})
export class VisitBookingPage implements OnInit {
  subscription:Subscription;
  date:Date;
  date_1:any;
  today:any;
  rv:any;
  current:Date=new Date();
  callList:any[]=[];
  timezone:any;
  // terms:any;
  staffArray:any[]=[];
staff:any;
staff_id:any;
type:any='all';
wingArray:any[]=[];
wing:any;
wingId:any;
  constructor(private platform:Platform,private router:Router,private http:HttpClient,private config:HttpConfigService,
    private popCntlr:PopoverController,private storage:Storage,private modalCntl:ModalController) { }

  ngOnInit() {
    this.date=this.current;
    this.today=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
    this.date_1=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
  }
 async ionViewWillEnter(){
  // this.terms='';

  this.rv=await this.storage.get('RVSETTINGS');
//   this.wingArray=[];
//   this.getWing();
// this.storage.ready().then(()=>{
//   const bid=await this.storage.get('BRANCH');
//   const utype=await this.storage.get('USER_TYPE');
//     this.storage.get('USER_ID').then(id=>{

//       if(utype==3){
//         let  url=this.config.domain_url+'get_staff_wing/'+id;
//         // let body={
//         //   user_id:id
//         // }
//         // console.log('staff wing:',body);
        
//         this.http.get(url).subscribe((res:any)=>{
//           console.log('wing:',res);
//           this.wingId=res[0].wing_id;
//           let url1=this.config.domain_url+'branch_wings/'+bid;
//           this.http.get(url1).subscribe((res:any)=>{
//             res.data.details.forEach(ele=>{
//               console.log("el:",ele.id,this.wingId);
              
//               if(ele.id==this.wingId){
//                 this.wing=ele.name;
//               }
//             })
//             console.log("stafffff:",this.wing);
//           })
      
//         })
        
        
//       }else{
//         this.wingId='all';
//         this.wing="All wings";
//       }


//     })
//   })
// })
// })




  this.getContent();
  
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/menu']) ;
  }); 

}

selecttype(){
    this.getContent();
}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}

async getContent(){
  // this.storage.ready().then(()=>{
    const bid=await this.storage.get('BRANCH');
      const uid=await this.storage.get('USER_ID');
  let url=this.config.domain_url+'visit_booking_by_user';
  // let head=this.config.getHeader();
  
  
  let headers=await this.config.getHeader();
  let body={
    user_id:uid,
    date:this.date_1,
    type:this.type
  }
  console.log("body:",body);
  
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log("res:",res);
    this.callList=res.data;
    
  })
// })
 
// })
// })
}


// getWing(){
//   this.wingArray=[];
//   this.wingArray.push({id:'all',wing:'All wings'});
// //  this.wingArray.push({id:0,wing:'All facilities'});
//  this.storage.ready().then(()=>{
//    const bid=await this.storage.get('BRANCH');
//      const utype=await this.storage.get('USER_TYPE');
//      let branch=bid.toString();
//      let body={company_id:branch}
//      let url=this.config.domain_url+'branch_wings/'+bid;
//      this.http.get(url).subscribe((res:any)=>{
//       //  console.log("wing:",res);
//        for(let i in res.data.details){
//         // console.log("data:",res.data.details[i]);
        
//       let obj={id:res.data.details[i].id,wing:res.data.details[i].name};
   
//          this.wingArray.push(obj);
         
       
//        }
//        console.log("array:",this.wingArray);
       
       
//      },error=>{
//        console.log(error);
//       })
//      })
//    })
//  })
//  }
//  setWing(wing){
//   this.wingArray.forEach(element => {
//     if(element.wing==wing){
//       this.wingId=element.id;
//     }
//   });
//   console.log("wingId:",this.wingId);
//   this.getContent();
// }

onChange(ev){
console.log("cdate:",this.date);
this.date_1=this.date;
this.getContent();
}
 //  method to fix month and date in two digits
fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
}
getTimeZone(timezone){

  return moment().tz(timezone).format('hh:mm');
}



async showComment(item,ev){
  const popover = await this.popCntlr.create({
    component: CommentComponent,
    event:ev,
    backdropDismiss:true,
    cssClass:'comments-popover',
    componentProps:{
      data:item.comment
    },
   
    
    
  });
  return await popover.present();
}


async showInfo(ev,item){
  const popover = await this.modalCntl.create({
    component: MoreCallbookComponent,
   
    backdropDismiss:true,
    componentProps:{
      data:item
    },
 
    
    
  });
  return await popover.present();
}


async showPeople(item){
  console.log("item:",item)
  let data=[];
  let other=[];
  if(item.family.length>0){
    data=item.family;
    // fam=1;
  }
   if(item.other_visitor_list.length>0){
    other=item.other_visitor_list;
    // fam=0
  }
  const popover = await this.modalCntl.create({
    component: MoreCallbookComponent,
    backdropDismiss:true,
    componentProps:{
      data:data,
      other:other,
      call:0
    },
    cssClass:'morepop'
    
    
  });
  return await popover.present();
}

async showOptions(ev,item){
  const popover = await this.popCntlr.create({
    event:ev,
    component: VisitBookingOptionsComponent,
    backdropDismiss:true,
    componentProps:{
      data:item
    },
    cssClass:'morepop'
    
    
  });
  popover.onDidDismiss().then((dataReturned)=>{
    if(dataReturned.data){
    this.getContent();
    }
  })
  return await popover.present();
}
create(){
  this.router.navigate(['/create-visit-booking'],{replaceUrl:true})
}
}

import { Component, OnInit, ViewChildren } from '@angular/core';
import { PopoverController, IonSlides, Platform, LoadingController, ToastController, ModalController } from '@ionic/angular';
import { ActivityfilterComponent } from '../activityfilter/activityfilter.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { Router } from '@angular/router';
import { HttpConfigService } from '../services/http-config.service';
import { Subscription } from 'rxjs';
import { ConfirmSignupComponent } from '../components/confirm-signup/confirm-signup.component';


@Component({
  selector: 'app-activities',
  templateUrl: './activities.page.html',
  styleUrls: ['./activities.page.scss'],
})
export class ActivitiesPage implements OnInit{
  @ViewChildren('slideWithNav') slideWithNav: IonSlides;
view:boolean=false;
date:Date;
current:Date=new Date();
token:string;
activity:any[]=[];
week=[];
day:string;
year:any;
month:any;
numAct:any;
act:boolean;
currentDate:any=0;
date_1:string;
subscription:Subscription;
sliderOne: any;
slidenum:any;
slideOptsOne = {
  initialSlide: 0,
  slidesPerView: this.checkScreen(),
  
};
    type:any;
    contentStyle ={top:'70px'};
    plat:boolean;
    wingArray:any=[];
    sel_wing:any;
    wing:any;
    utype:any;
    cid:any=[];
    act_filter:any;
    selectedList:any='1';
    rvsettings:any;

    residents:any=[];
    uid:any;
    // actCal={margintop:'350px'};
  constructor(public popoverController:PopoverController,public http:HttpClient,public storage:Storage,private router:Router,
    private config:HttpConfigService,private platform:Platform,private loadingCtrl:LoadingController,private toastCntlr:ToastController,
    private modalCntl:ModalController) { 
     
      
      
      
    }
    checkScreen(){
      if(window.innerWidth>=700){
          return 6;
      }else{
          return 3;
      }
  }
    //Move to Next slide
  slideNext(object, slideView) {
    slideView.slideNext(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });
  }

  //Move to previous slide
  slidePrev(object, slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }
    SlideDidChange(object, slideView) {
      this.checkIfNavDisabled(object, slideView);
      object.isActive= true;
    }
    checkIfNavDisabled(object, slideView) {
      this.checkisBeginning(object, slideView);
      this.checkisEnd(object, slideView);
    }
   
    checkisBeginning(object, slideView) {
      slideView.isBeginning().then((istrue) => {
        object.isBeginningSlide = istrue;
      });
    }
    checkisEnd(object, slideView) {
      slideView.isEnd().then((istrue) => {
        object.isEndSlide = istrue;
      });
    }
   
  ngOnInit() {

    if(this.platform.is('android')){
      this.contentStyle.top='75px';
      this.plat=true;
      // this.actCal.margintop='350px';
    }else if(this.platform.is('ios')){
      this.contentStyle.top='60px';
      this.plat=false;
      // this.actCal.margintop='370px';
    }

    this.date=this.current;
    this.date_1=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
    var mon = new Array();
    mon[0] = "January";
    mon[1] = "February";
    mon[2] = "March";
    mon[3] = "April";
    mon[4] = "May";
    mon[5] = "June";
    mon[6] = "July";
    mon[7] = "August";
    mon[8] = "September";
    mon[9] = "October";
    mon[10] = "November";
    mon[11] = "December";
    this.year=this.date.getFullYear();
    this.month=mon[this.date.getMonth()];

    
    

    let m=this.current.getMonth()+1
  // get current week
    for (let i = 0; i <= 6; i++) {

      // let first = this.current .getTime() - (7 * 24 * 60 * 60 * 1000)

      let mon=m;
      let first = this.current.getDate()+ i;
      if(m==1||m==3||m==5||m==7||m==8||m==10||m==12){
        if (first>31){
          first=first-31;
          mon=m+1
          
        }
      }else if(m==2){
        if(first>28){
          first=first-28;
          mon=m+1
        }
      }else{
        if(first>30){
          first=first-30;
          mon=m+1
        }
      }
      let x= this.current.getDay()+i;
     if(x>6){
       x=x-7;
     }
        if(x==0){
        this.day="Sun";
      }
      else if(x==1){
        this.day='Mon';
      }
      else if(x==2){
        this.day='Tue';
      }
      else if(x==3){
        this.day='Wed';
      }
      else if(x==4){
        this.day='Thu';
      }
      else if(x==5){
        this.day='Fri';
      }
      else if(x==6){
        this.day='Sat';
      }
    

      
      var con={'day':this.day,'date':first,'mon':mon};
      
      this.week.push(con);
      this.sliderOne =
      {
        isBeginningSlide: true,
        isEndSlide: false,
        isActive:false,
        week:[]=this.week
      };
    }

  }


  async ionViewWillEnter(){
    this.selectedList='1';
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('USER_TYPE');
        this.utype=data;
    //   })
    // })
    const uid=await this.storage.get('USER_ID');
        this.uid=uid;
    const rv=await this.storage.get('RVSETTINGS');
    this.rvsettings=rv
    
    this.sel_wing=0;
    this.wing='0';
    this.getContent();
    this.getWing();
    if(this.utype==6){
      this.getFamily();
    }
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu']) ;
    }); 


    
  }
// open filter
  async filter(myEvent) {
    const popover = await this.popoverController.create({
      component: ActivityfilterComponent,
      event: myEvent,
      backdropDismiss:true,
      cssClass:'filterpop'
      
      // translucent: true
    });
    popover.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.type = dataReturned.data;
        
        console.log("sel_wing:",this.sel_wing);
        
        //alert('Modal Sent Data :'+ dataReturned);
        if(this.sel_wing==0){
        this.filterContent();
        }else{
          this.filterTypeWing();
        }
        if(this.type==1){
          this.act_filter="Activity"
          this.selectedList='1';
          }else if(this.type==9){
            this.act_filter="Meeting"
            this.selectedList='1';
          }else if(this.type==8){
            this.act_filter="Event"
            this.selectedList='1';
          }else if(this.type==11){
            this.act_filter='Assigned';
            this.selectedList='2';
          }
      }
    });
    return await popover.present();
  }
// selecting date from calendar
  onChange(dat) {
    this.selectedList='1';
   
    this.date_1=dat.slice(6)+'-' + dat.slice(3,5)+'-'+dat.slice(0,2);
    let d=dat.slice(3,5)+'/' +dat.slice(0,2)+'/'+ dat.slice(6)
    this.date=new Date(d);
    console.log(dat," ",this.date);
    console.log("sel_wing:",this.sel_wing);
    if(this.sel_wing==0){
    this.getContent();
    }else{
      this.filterByWing(this.sel_wing);
    }
  }

// switch between calendar and week view
 calendarView(){
   this.view=!this.view;
   this.selectedList='1';
 }


 setDate(item,index){
  //  let x=item.date+' '+this.current.toUTCString().slice(0,3)+this.current.toUTCString().slice(7);
  //  let y=item.date.toString();
  //  this.date=x;
  this.selectedList='1';
  var setMonth = new Array();
  setMonth[1] = "January";
  setMonth[2] = "February";
  setMonth[3] = "March";
  setMonth[4] = "April";
  setMonth[5] = "May";
  setMonth[6] = "June";
  setMonth[7] = "July";
  setMonth[8] = "August";
  setMonth[9] = "September";
  setMonth[10] = "October";
  setMonth[11] = "November";
  setMonth[12] = "December";


   this.date_1=this.current.getFullYear()+'-' + this.fixDigit(item.mon)+'-'+this.fixDigit(item.date);
   this.month=setMonth[item.mon];
    this.currentDate=index;
    console.log("index:",this.currentDate);
    console.log("sel_wing:",this.sel_wing);
    if(this.sel_wing==0){
   this.getContent();
    }else{
      this.filterByWing(this.sel_wing)
    }

 }

//  method to fix month and date in two digits
 fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
}

// fetch activity list
 async getContent(){
   this.cid=[];
  // this.storage.ready().then(()=>{
    let cid;
  const utype=await this.storage.get('USER_TYPE');
    const data=await this.storage.get('USER_ID');
      const tok=await this.storage.get('TOKEN');
        let token=tok;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    let headers= await this.config.getHeader()
    
      if(utype==5){
        cid=null
        this.cid=null;
        this.getActivity();
      }else{
        let url=this.config.domain_url+'profile/'+data;
        let headers= await this.config.getHeader();
          this.http.get(url,{headers}).subscribe((res:any)=>{
            console.log("fam:",res,res.data.family[0].parent_details);
            res.data.family.forEach(element => {
              this.cid.push(element.resident_user_id);
            });
            // cid=res.data.family[0].parent_details[0].user_id
            //   this.cid=cid;
              this.getActivity();
          })
      }
   console.log("date:",this.date_1);
  
    
 
  //  let body={
  //    date:'daily',
  //    date_1: this.date_1,
  //    user_id:data,
  //    consumer_id:cid

  //  }
  //  let url=this.config.domain_url+'all_activities';
  // //  let headers= await this.config.getHeader();
  //  console.log("head:",body)
  //  this.http.post(url,body,{headers}).subscribe((res:any)=>{
  //    this.activity=res.data;
  //    this.activity.sort(this.compare);
  //    console.log("res:",res);

  //    this.numAct=this.activity.length;
  //    if(this.numAct==0){
  //      this.act=false;
  //    }else{
  //      this.act=true;
  //    }
  //  },error=>{
  //    console.log(error);
     
  //  })
//   })
// })
//     })
//   })
// })
 }


async getActivity(){
  // this.storage.ready().then(()=>{
    
  
    const data=await this.storage.get('USER_ID');
      const tok=await this.storage.get('TOKEN');
        let token=tok;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    let headers= await this.config.getHeader()
    
  //    let cid=[];
  // if(this.cid!=null){
  //   cid.push(this.cid);
  // }else{
  //   cid=null;
  // }
    
 
   let body={
     date:'daily',
     date_1: this.date_1,
     user_id:data,
     consumer_id:this.cid

   }
   let url=this.config.domain_url+'all_activities';
  //  let headers= await this.config.getHeader();
   console.log("head:",body)
   this.http.post(url,body,{headers}).subscribe((res:any)=>{
     this.activity=res.data;
     this.activity.sort(this.compare);
     console.log("res:",res);

     this.numAct=this.activity.length;
     if(this.numAct==0){
       this.act=false;
     }else{
       this.act=true;
     }
   },error=>{
     console.log(error);
     
   })

   
//   })
// })
//     })
//   })

 }
 compare(i,j){
  let c=i.activity_start_date.slice(0,10)+' '+i.start_time;
  let d= j.activity_start_date.slice(0,10)+' '+j.start_time;
  let a=new Date(c).getTime() - new Date(d).getTime();
  return a;
}
//  view single activity
 async gotoDetails(id){
   this.router.navigate(['/activity-details',{act_id:id}]);
 }
 
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }


 async filterContent(){
  console.log("date:",this.date_1);
  //  this.storage.ready().then(()=>{
      
          const data=await this.storage.get('USER_ID');
            const tok=await this.storage.get('TOKEN');
              let token=tok;
              const bid=await this.storage.get('BRANCH');
                let branch=bid.toString();
          let headers=new HttpHeaders({
            'Authorization': 'Bearer'+token ,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'branch_id':branch
          });
              
            let body={
              date:'daily',
              date_1: this.date_1,
              user_id:data,
              type:this.type,
              consumer_id:this.cid

            }
            let url=this.config.domain_url+'all_activities';
            // let headers= await this.config.getHeader();
            console.log("head:",headers)
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              this.activity=res.data;
              this.activity.sort(this.compare);
              console.log("res:",res);

              this.numAct=this.activity.length;
              if(this.numAct==0){
                this.act=false;
              }else{
                this.act=true;
              }
            },error=>{
              console.log(error);
              
            })
    //       })
    //     })
    //       })
       
        
    // })
 }

 async getWing(){
   this.wingArray=[];
  this.wingArray.push({id:0,wing:'All'});
  // this.storage.ready().then(()=>{
    const bid=await this.storage.get('COMPANY_ID');
      let branch=bid.toString();
      let body={company_id:branch}
      let url=this.config.domain_url+'get_calendar_types';
      this.http.post(url,body).subscribe((res:any)=>{
        console.log("wing:",res);
        for(let i in res.data){
            console.log("data:",res.data[i]);
            
          let obj={id:res.data[i].calendar_id,wing:res.data[i].calendar_types.type};
          this.wingArray.push(obj)
        
        }
        console.log("array:",this.wingArray);
        
        
      },error=>{
        console.log(error);
        
      })
  //   })
  // })
 }
 changeWing(wing){
  this.selectedList='1';
   if(wing==0){
     this.getContent();
     this.sel_wing=0;
     this.wing='0';
   }else{
     console.log("winggg:",wing);
     this.sel_wing=wing;
     this.wing=wing;
     this.filterByWing(wing)
   }
 }


 async filterByWing(wing){
  // this.storage.ready().then(()=>{
      
    // const data=await this.storage.get('USER_ID');
   console.log("date:",this.date_1);
  
    // const tok=await this.storage.get('TOKEN');
    //   let token=tok;
      const bid=await this.storage.get('BRANCH');
        let branch=bid.toString();
  let headers=new HttpHeaders({
    
    'branch_id':branch
  });
   let body={
     date:'daily',
     date_1: this.date_1,
     calendar:wing

   }
   let url=this.config.domain_url+'activity_by_calendar_type';
  //  let headers= await this.config.getHeader();
   console.log("head:",headers)
   this.http.post(url,body,{headers}).subscribe((res:any)=>{
     this.activity=res.data;
     this.activity.sort(this.compare);
     console.log("res:",res);

     this.numAct=this.activity.length;
     if(this.numAct==0){
       this.act=false;
     }else{
       this.act=true;
     }
   },error=>{
     console.log(error);
     
   })
//   })
// })
    // })
  // })
 }


async filterTypeWing(){
  // this.storage.ready().then(()=>{
      
    // const data=await this.storage.get('USER_ID');
   console.log("date:",this.date_1);
  
    const tok=await this.storage.get('TOKEN');
      let token=tok;
      const bid=await this.storage.get('BRANCH');
        let branch=bid.toString();
  let headers=new HttpHeaders({
    
    'branch_id':branch
  });
   let body={
     date:'daily',
     date_1: this.date_1,
     calendar:this.sel_wing,
     type:this.type

   }
   let url=this.config.domain_url+'activity_by_calendar_type';
  //  let headers= await this.config.getHeader();
   console.log("head:",headers)
   this.http.post(url,body,{headers}).subscribe((res:any)=>{
     this.activity=res.data;
     this.activity.sort(this.compare);
     console.log("res:",res);

     this.numAct=this.activity.length;
     if(this.numAct==0){
       this.act=false;
     }else{
       this.act=true;
     }
   },error=>{
     console.log(error);
     
   })
//   })
// })
//     })
}
async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}
changeView(i){
  if(i==1){
      this.type=1;
      this.act_filter="Activity"
  }else{
    this.type=11;
    this.act_filter='Assigned'
  }
  if(this.sel_wing==0){
    this.filterContent();
    }else{
      this.filterTypeWing();
    }
    
}

async signupAlert(item){
  const utype=await this.storage.get('USER_TYPE');
  const data=await this.storage.get('USER_ID');
  if(utype==5){
      let cid=[data];
      this.signup(item,cid)
  }else{
  const modal = await this.modalCntl.create({
    component: ConfirmSignupComponent,
    
    componentProps: {
     
      item:item
        
    }
  });
  modal.onDidDismiss().then((dataReturned)=>{
    if(dataReturned.data){
      
      this.signup(item,dataReturned.data)
    }
  })
  return await modal.present();
}
}
signup(item,cid){
  

  let url=this.config.domain_url+'add_attendee';
    let body={
      activity_id:item.id,
      user_id:cid,
      action:0
    }
    this.http.post(url,body).subscribe((res:any)=>{
      console.log('signedup:',res);
       if(res.status=='success'){
       
        this.presentAlert('signed up successfully.');
       this.gotoDetails(item.id);
        // this.router.navigate(['/activity-details',{act_id:item.id}]);
       
     
      }else{
      
        this.presentAlert('Something went wrong. Please try again later.')
      }
    },err=>{
      
    })
}
async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'    
  });
  alert.present(); //update
}

async getFamily(){
  // this.storage.ready().then(()=>{
    const uid=await this.storage.get('USER_ID');
      const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
        let url=this.config.domain_url+'showfamily/'+uid;
        let headers=new HttpHeaders({'branch':bid,'company_id':cid.toString()});
      this.http.get(url,{headers}).subscribe((res:any)=>{
       
        
        res.data.forEach(ele=>{
          ele.family.forEach(element => {
            if(element.parent_details[0].branches.user_branch.retirement_living==1&&element.share_details==1){
            this.residents.push(element.parent_details[0].user.user_id.toString())
            }
          });
        })
       
      })

      console.log("parent:",this.residents);
}

signupStatus(item){
  // const utype=await this.storage.get('USER_TYPE');
  // const data=await this.storage.get('USER_ID');
  
  if(item.signup_list.length){
    let arr=[];
    
  item.signup_list.map(x => {
    arr.push(x.user_id.toString());
  })
  console.log('res:',this.residents,'list:',arr)
    if(this.utype==5){
      if(arr.includes(this.uid.toString())){
        return false
      }else{
        return true
      }
    }else{
      
      if(this.residents.every(ele=>arr.includes(ele))){
       
        return false
      }else{
        return true
        
      }
     
    }
  // });
}else{
  return true
}
 
  
}

waitinglistStatus(item){

  if(item.signup_list.length){
   
    
  
  
  
  
    if(this.utype==5){
      const idx=item.signup_list.map(x => x.user_id).indexOf(this.uid)
      if(idx>=0&&item.signup_list[idx].action==0){
       
        return true
      }else{
        return false
      }
    }else{
      
      let arr=[];
       item.signup_list.map(x=>{
        if(this.residents.includes(x.user_id.toString())){
            arr.push(x)
          }
        })
       
      
   
    let a;
    arr.map(x=>{
      if(x.action==0){
        a=true
        // return true
      }else{
        a=false;
        // return false
      }
      
    })
    console.log('wait:',arr,a);
    return a
    }
  
}else{
  return false
}
 
}
}

import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-callbooking-success',
  templateUrl: './callbooking-success.page.html',
  styleUrls: ['./callbooking-success.page.scss'],
})
export class CallbookingSuccessPage implements OnInit {
subscription:Subscription;
slot:any;
date:any;
duration:any;
  constructor(private platform:Platform,private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {
  }
ionViewWillEnter(){
  this.slot=this.route.snapshot.paramMap.get('slot');
  this.duration=this.route.snapshot.paramMap.get('duration');
  let date=this.route.snapshot.paramMap.get('date');
  this.date=new Date(date);
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/menu']) ;
  }); 

}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}

}

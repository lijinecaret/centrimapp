import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FeedbackFivesmileyPage } from './feedback-fivesmiley.page';

describe('FeedbackFivesmileyPage', () => {
  let component: FeedbackFivesmileyPage;
  let fixture: ComponentFixture<FeedbackFivesmileyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbackFivesmileyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FeedbackFivesmileyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { Platform, ModalController, ActionSheetController, PopoverController, LoadingController, ToastController } from '@ionic/angular';
import { ThankyouComponent } from '../components/thankyou/thankyou.component';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { ActivityImageComponent } from '../components/activity-image/activity-image.component';
// import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
@Component({
  selector: 'app-feedback-fivesmiley',
  templateUrl: './feedback-fivesmiley.page.html',
  styleUrls: ['./feedback-fivesmiley.page.scss'],
})
export class FeedbackFivesmileyPage implements OnInit {
// numArray:any=[];
sid:any;
lid:any;
cid:any;
id:any;
cat_id:any;
flag:any;
questions:any=[];
subscription:Subscription;
noOfItem:number=1;
que_id:any;
opt_id:any;
selected:any;
feedback:any;
suggestion:boolean=true;
feed_suggestion:any=[];
name:any;
mail:any;
phone:any;
nextQue:boolean=false;
preQue:boolean=false;
public surveyData : {QuestionId : number , OptionId : number}[];
keyboardStyle = { width: '100%', height: '0px' };
feedtext:any;
feednexthide:boolean;
img:any=[];
imageResponse:any=[];
opt:any;
type:any;
res:any;
disabled:boolean=false;
branch:any;
disableSkip:boolean=false;
isLoading:boolean=false;
  constructor(private router:Router,private orient:ScreenOrientation,private route:ActivatedRoute,private storage:Storage,
    private http:HttpClient,private config:HttpConfigService,private platform:Platform,private modalCntlr:ModalController,
    private keyboard:Keyboard,private actionsheetCntlr:ActionSheetController,
    private popCntrl:PopoverController,private loadingCtrl:LoadingController,private toast:ToastController) { }

  ngOnInit() {
    // this.imagePicker.requestReadPermission();
    this.keyboard.onKeyboardWillShow().subscribe( {
      next: x => {
        this.keyboardStyle.height = x.keyboardHeight + 'px';
      },
      error: e => {
        console.log(e);
      }
    });
    this.keyboard.onKeyboardWillHide().subscribe( {
      next: x => {
        this.keyboardStyle.height = '0px';
      },
      error: e => {
        console.log(e);
      }
    });
    
    
  }
  inputChange(){
    if(this.feedtext==undefined || this.feedtext==''){
      this.feednexthide=true;
    }else{
      this.feednexthide=false;
    }
  }
  async ionViewWillEnter(){
    this.disabled=false;
    this.disableSkip=false;
    this.surveyData = [{QuestionId: 0 ,OptionId : 0  }];
    
    // this.orient.lock(this.orient.ORIENTATIONS.LANDSCAPE);
   
      
      this.sid=this.route.snapshot.paramMap.get("sid");
      this.lid=this.route.snapshot.paramMap.get("lid");
      this.flag=this.route.snapshot.paramMap.get("flag");
      this.cat_id=this.route.snapshot.paramMap.get("cat_id");
      this.res=this.route.snapshot.paramMap.get("res");
      this.type=this.route.snapshot.paramMap.get("type");
      this.branch=this.route.snapshot.paramMap.get('branch');
      if(this.type==2){
        this.getSuggestion();
        this.suggestion=false;
      }else{
        this.suggestion=true;
        this.noOfItem=1;
      // this.storage.ready().then(()=>{
        const dat=await this.storage.get('BRANCH');
          if(this.branch){
            this.id=this.branch
          }else{
          this.id=dat;
          }
        const res=await this.storage.get('SURVEY_CID');
          this.cid=res;

          let url=this.config.feedback_url+'get_review_questions';
          // let body={
          //   company_id:14,
          //   user_id:3,
          //   survey_bid:8,
          //   language_id:1
          // }
          let body = `company_id=${this.cid}&branch_id=${this.id}&review_bid=${this.sid}&language_id=${this.lid}&category_id=${this.cat_id}`;
         
          console.log("body:",body);
          
          let headers=new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          })

          this.http.post(url,body,{headers}).subscribe((data:any)=>{
            this.questions=data.qanda
            console.log('aues:',data,this.questions,this.noOfItem);
            
          })
      //   })
      //   })
      // })
    }
      
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
     this.back();
    });               

  }
  back(){
    // if(this.flag==1){
    //   this.router.navigate(['/menu']) ;
    // } else{  
    //   this.router.navigate(['/feedback']) ;
    // }
    // this.router.navigate(['/select-category',{sid:this.sid,flag:this.flag}]);
    this.router.navigate(['/feedback-type',{sid:this.sid,flag:1,cat_id:this.cat_id,type:this.type,res:this.res,branch:this.branch}],{replaceUrl:true})
  }
  next(){
    if(this.noOfItem <= this.questions.length){ 
      this.nextQue=false;
      this.noOfItem += 1; 
      this.preQue=true;
      if(this.noOfItem>this.questions.length){
        this.suggestion=true;
        this.getSuggestion();
              
      }
    } else { 
      console.log('END OF DAY 1') 
    }
    // console.log("no:",this.noOfItem,"len:",this.questions.length)
  }
  previous(){
    if(this.type==2||this.noOfItem==1){
      this.back();
      console.log('first q')
    }else if(this.type!==2&&!this.suggestion){
      this.suggestion=true;
      this.noOfItem+=-1;
     
      this.surveyData.unshift({ QuestionId: 0, OptionId: 0 });
    }else{
    if(this.noOfItem <= this.questions.length){
     
        this.nextQue=true;
     
      if(this.noOfItem!=1){
      this.noOfItem += -1; 
      }else{
        this.noOfItem=1;
      }
      if(this.noOfItem==1){
        this.preQue=false;
      }else{
        this.preQue=true;
      }
    } else { 
      console.log('END OF DAY 1') 
    }
  }
    console.log("no:",this.noOfItem,"len:",this.questions.length)
  }
  ionViewDidLeave(){
 
    
      this.subscription.unsubscribe();
  }



  getOption(item,ques){

    const targetIdx = this.surveyData.map(item => item.QuestionId).indexOf(ques.id);
    if(targetIdx>=0)
      {

        this.surveyData[targetIdx].OptionId = item.id;
        
      }
else{
     if(ques.type==7){
       this.surveyData.push({ QuestionId:ques.id, OptionId: this.feedtext })
     }else{
      this.surveyData.push({ QuestionId: ques.id, OptionId: item.id });
     }
}
if(this.noOfItem==this.questions.length){
  console.log("initial:",this.surveyData);
  
  this.surveyData.splice(0,1);
  console.log("data:",this.surveyData);
  for(let data of this.surveyData){
    //  
    if(this.que_id==''||this.que_id==undefined)
    this.que_id = data.QuestionId;
    if(this.que_id==data.QuestionId){
        console.log("firstQue");
        
    }else{
     this.que_id += "*" + data.QuestionId;
    }
   }
  
   for(let data of this.surveyData){
    //  
    if(this.opt_id==''||this.opt_id==undefined)
    this.opt_id = data.OptionId;
    if(this.opt==''||this.opt==undefined)
    this.opt = data.OptionId;
    if(this.opt_id==data.OptionId){
      console.log("firstOpt");
      
  }else{
     this.opt_id += "*_*" + data.OptionId;
     this.opt +="*"+data.OptionId;
  }
   }
}

    // if(this.que_id==''){
    //   this.que_id=id;
    // }else{
    //   this.que_id=this.que_id+'*'+id;
    // }
    // if(this.opt_id==''){
    //   this.opt_id=item.id;
    // }else{
    //   this.opt_id=this.opt_id+'*'+item.id;
    // }
    if(this.noOfItem>=1){
      this.preQue=true;
    }
    if(this.noOfItem <= this.questions.length){ 
      
      this.noOfItem += 1; 
      if(this.noOfItem>this.questions.length){
        this.suggestion=false;
        // this.surveyData.splice(0  ,1); 
        
        
        this.getSuggestion();
              console.log("sug:",this.suggestion);
      }
    } else { 
      console.log('END OF DAY 1') 
    }
    this.selected=item.id;
    console.log("que:",this.que_id,"opt:",this.opt_id);
    
  }
async getSuggestion(){
  this.feed_suggestion=[];
  // this.storage.ready().then(()=>{
         
    const res=await this.storage.get('SURVEY_CID');
      this.cid=res;

      let url=this.config.feedback_url+'get_rsuggestion';
      // let body={
      //   company_id:14,
      //   user_id:3,
      //   survey_bid:8,
      //   language_id:1
      // }
      let body = `company_id=${this.cid}&question_id=${this.que_id}&option_id=${this.opt}`;
     
      console.log("body:",body);
      
      let headers=new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      })

      this.http.post(url,body,{headers}).subscribe((data:any)=>{
        // this.feed_suggestion=data.feedback_text
        // console.log(data);
        for(let i in data.feedback_text){
          this.feed_suggestion.push(data.feedback_text[i])
        }
    
  })
//   })
// })
}


setSuggestion(item,i){
  if(this.feedback==undefined||this.feedback==''){
    this.feedback=item;
  }else{
  this.feedback +=','+item;
  }
  this.feed_suggestion.splice(i,1)
}
async submit(){
  this.disabled=true;
  // this.storage.ready().then(()=>{
         
    const res=await this.storage.get('SURVEY_CID');
      this.cid=res;
      const name=await this.storage.get('NAME');
        this.name=name;
        const mail=await this.storage.get('EMAIL');
          this.mail=mail;
          const phone=await this.storage.get('PHONE');
            this.phone=phone;
            let utype;
            const type=await this.storage.get('USER_TYPE');
              if(type==6){
                utype=2
              }else if(type==5){
                utype=1
                this.res=name;
              }else{
                utype=0
              }
           
      let url=this.config.feedback_url+'saveReviewResponse';
      var s="";
      for (var i=0;i< this.imageResponse.length;i++)
      {
        s+=`&images[]=${this.imageResponse[i]}`;
      }
      let body = `company_id=${this.cid}&question_id=${this.que_id}&option_id=${this.opt_id}&feedback=${this.feedback}&review_branch_id=${this.sid}&name=${this.name}&email=${this.mail}&phone=${this.phone}`+s+`&feedback_type=${this.type}&category_id=${this.cat_id}&resident_name=${this.res}&usertype=${utype}`;
    //  let body=new FormData();
    //  body.append('company_id',this.cid);
    //  body.append('question_id',this.que_id);
    //  body.append('option_id',this.opt_id);
    //  body.append('feedback',this.feedback);
    //  body.append('review_branch_id',this.sid);
    //  body.append('name',this.name);
    //  body.append('email',this.mail);
    //  body.append('phone',this.phone);
    //  body.append('images',this.imageResponse);
      console.log("body:",body);

      // let data={
      //   company_id:this.cid,
      //   question_id:this.que_id,
      //   option_id:this.opt_id,
      //   feedback:this.feedback,
      //   review_branch_id:this.sid,
      //   name:this.name,
      //   email:this.mail,
      //   phone:this.phone,
      //   images:this.imageResponse
      // }
      // console.log("datttaaa:",data);
      let headers=new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      })

      this.http.post(url,body,{headers}).subscribe((data:any)=>{
        
        console.log(data);
        
        this.openModal();

      },error=>{
        console.log(error);
        this.disabled=false;
        this.disableSkip=false;
      })
//           })
//   })
//   })
// })
//     })
//   })
}



async openModal() {
  const modal = await this.modalCntlr.create({
    component: ThankyouComponent,
    cssClass: 'fb-thanks-modal',
    backdropDismiss:false,
    componentProps: {
      "flag":2
    }
  });
  return await modal.present();
}



async addImage(){
  // let options;
  //   options={
  //     maximumImagesCount: 5,
  //     outputType: 1
  //   }
  //   if(this.platform.is('ios')){
  //     options.disable_popover=true
  //   }
    // this.imagePicker.getPictures(options).then((results) => {
    //   for (var i = 0; i < results.length; i++) {
    //       // console.log('Image URI: ' + results[i]);
    //       if((results[i]==='O')||results[i]==='K'){
    //         console.log("no img");
            
    //         }else{
    //           this.img.push(results[i]);
    //        let im='data:image/jpeg;base64,'+results[i]
    //       //  this.imageResponse.push(im);
    //       this.uploadImage(im);
    //         }
          
           
    //   }
    // }, (err) => { });

    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:5
      
    });
  
  
    for (var i = 0; i < image.photos.length; i++) {
            console.log('Image URI: ' + image.photos[i]);
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[i].path
            });
            
              // this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
              this.uploadImage('data:image/jpeg;base64,' + contents.data)
            
              
         
             
        }
  

  
  console.log("imagg:",image.photos,image.photos[0].path);
  
  
}


async selectImage() {
  const actionSheet = await this.actionsheetCntlr.create({
    header: "Select Image source",
    mode:'md',
    buttons: [{
      text: 'Load from Library',
      handler: () => {
        this.addImage();
      }
    },
    {
      text: 'Use Camera',
      handler: () => {
        this.pickImage();
      }
    },
    {
      text: 'Cancel',
      role: 'cancel'
    }
    ]
  });
  await actionSheet.present();
 
}


async pickImage(){
  // const options: CameraOptions = {
  //   quality: 100,
  //   sourceType: this.camera.PictureSourceType.CAMERA,
  //   destinationType: this.camera.DestinationType.DATA_URL,
  //   encodingType: this.camera.EncodingType.JPEG,
  //   mediaType: this.camera.MediaType.PICTURE
  // }
  // this.camera.getPicture(options).then((imageData) => {
  //   // imageData is either a base64 encoded string or a file URI
  //   // If it's base64 (DATA_URL):
  //   let base64Image = 'data:image/jpeg;base64,' + imageData;
    
  //   // this.imageResponse.push(base64Image);
  //   this.uploadImage(base64Image);

     
  // }, (err) => {
  //   // Handle error
  // });
  const image = await Camera.getPhoto({
    quality: 90,
    allowEditing: false,
    resultType: CameraResultType.Base64,
    correctOrientation:true,
    source:CameraSource.Camera
  });

  // image.webPath will contain a path that can be set as an image src.
  // You can access the original file using image.path, which can be
  // passed to the Filesystem API to read the raw data of the image,
  // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
  var imageUrl = image.base64String;

  // Can be set to the src of an image now
  let base64Image = 'data:image/jpeg;base64,' + imageUrl;
  console.log('image:',imageUrl);
 
  this.uploadImage(base64Image);
}


removeImg(i){
  this.imageResponse.splice(i,1);
}

async viewImg(){
  const popover = await this.popCntrl.create({
    component: ActivityImageComponent,
    // event: ev,
    componentProps:{
      data:this.imageResponse,
      flag:1
    },
    cssClass:'image_pop'
  });
  return await popover.present();
}
skip(){
  this.disableSkip=true;
  this.feedback==null
 
    this.submit();
  
}

uploadImage(img){
  if(this.isLoading==false){
    this.showLoading();
    }
  let url=this.config.feedback_url+'upload_feedback_image_app';
  let headers = new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
  })
  // let body={file:img}
  let body= `file=${img}`
console.log("body:",body);

  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    
      console.log('img:',res)
      if(res.data){
        this.imageResponse.push(res.data);
          }else{
            this.presentAlert('Upload failed. Please try again')
          }
    this.dismissLoader();
    console.log('resp:',this.imageResponse)
  },error=>{
    console.log(error);
    this.dismissLoader();
  })
}

async showLoading() {
  this.isLoading=true;
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
  this.isLoading=false;
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}
async presentAlert(message) {
  const alert = await this.toast.create({
    message: message,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FeedbackFivesmileyPageRoutingModule } from './feedback-fivesmiley-routing.module';

import { FeedbackFivesmileyPage } from './feedback-fivesmiley.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FeedbackFivesmileyPageRoutingModule
  ],
  declarations: [FeedbackFivesmileyPage]
})
export class FeedbackFivesmileyPageModule {}

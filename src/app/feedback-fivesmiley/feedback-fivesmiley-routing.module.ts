import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeedbackFivesmileyPage } from './feedback-fivesmiley.page';

const routes: Routes = [
  {
    path: '',
    component: FeedbackFivesmileyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeedbackFivesmileyPageRoutingModule {}

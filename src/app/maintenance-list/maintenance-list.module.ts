import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaintenanceListPageRoutingModule } from './maintenance-list-routing.module';

import { MaintenanceListPage } from './maintenance-list.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaintenanceListPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [MaintenanceListPage]
})
export class MaintenanceListPageModule {}

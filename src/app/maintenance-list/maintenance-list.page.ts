import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform, PopoverController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { MaintenanceFilterComponent } from '../components/maintenance-filter/maintenance-filter.component';

@Component({
  selector: 'app-maintenance-list',
  templateUrl: './maintenance-list.page.html',
  styleUrls: ['./maintenance-list.page.scss'],
})
export class MaintenanceListPage implements OnInit {
request:any=[];
id:any;
filter:any;
sel_filter:any;
sel_id:any;
subscription:Subscription;
tec:any=[];
branch:any=[];
bname:any;
sel_branch:any;
  constructor(private http:HttpClient,private storage:Storage,private config:HttpConfigService,private router:Router,
    private route:ActivatedRoute,private platform:Platform,private popoverCntlr:PopoverController) { }

  ngOnInit() {
  }
async ionViewWillEnter(){
  this.filter=this.route.snapshot.paramMap.get('filter');
  if(this.filter!=1){
  this.id=this.route.snapshot.paramMap.get('data');
  }
  this.request=[];
  this.branch=[];
  this.getBranch();
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');
      const bid=await this.storage.get('BRANCH');

      this.sel_branch=bid;
      let url;
      if(this.filter==1){
      url=this.config.domain_url+'get_maintenance_request_technician?user_id='+data;
      }else if(this.filter==2){
        url=this.config.domain_url+'get_maintenance_request_technician?status='+this.id+'&branch='+bid+'&user_id='+data;
      }else if(this.filter==3){
        url=this.config.domain_url+'filter_by_priority?priority='+this.id+'&branch='+bid+'&user_id='+data;
      }
      let id=data.toString();
      let headers= await this.config.getHeader()
      console.log("head:",url);
      
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.request=res.data;
        
          this.tec=res.technician;
        
      },error=>{
        console.log(error);
      })
  //     })
  //   })
  // })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/maintenance-count']) ;
  }); 
}
gotoDetails(id){
  this.router.navigate(['/maintenance-details',{req_id:id,filter:this.filter}])
}


async filterRequest(ev){
// open filter
 
  const popover = await this.popoverCntlr.create({
    component: MaintenanceFilterComponent,
    event: ev,
    backdropDismiss:true,
    cssClass:'maintenance_filter',
    componentProps:{
      data:this.tec,
      flag:1
    },
    
    // translucent: true
  });
  popover.onDidDismiss().then((dataReturned) => {
    if (dataReturned !== null) {
      this.sel_filter = dataReturned.data.filter;
      this.sel_id = dataReturned.data.id;
      console.log("fil_data:",dataReturned);
      //alert('Modal Sent Data :'+ dataReturned);
      this.filterContent();
    }
  });
  return await popover.present();
}
async filterContent(){
  // this.storage.ready().then(()=>{
    // this.storage.get('BRANCH').then(data=>{
      const uid=await this.storage.get('USER_ID');

      
      let url;
      console.log("filtering");
       if(this.sel_filter=="Status"){
        url=this.config.domain_url+'get_maintenance_request_technician?status='+this.sel_id+'&branch='+this.sel_branch+'&user_id='+uid;
      }else if(this.sel_filter=="Risk rating"){
        url=this.config.domain_url+'get_maintenance_request_technician?risk_rating='+this.sel_id+'&branch='+this.sel_branch+'&user_id='+uid;
      }else if(this.sel_filter=="Date"){
        let current=new Date();
        let later;
        if(this.sel_id==0){
          later=current
        }else if(this.sel_id==1){
          later=new Date(Date.now()-1*24*60*60*1000);
        }else if(this.sel_id==2){
          later=new Date(Date.now()-7*24*60*60*1000);
        }else if(this.sel_id==3){
          later=new Date(Date.now()-30*24*60*60*1000);
        }
        let to_date=current.getFullYear()+'-'+this.fixDigit((current.getMonth()+1))+'-'+this.fixDigit(current.getDate());
        let from_date=later.getFullYear()+'-'+this.fixDigit((later.getMonth()+1))+'-'+this.fixDigit(later.getDate());

        url=this.config.domain_url+'get_maintenance_request_technician?sdate='+from_date+'&edate='+to_date+'&branch='+this.sel_branch+'&user_id='+uid;;

      }else if(this.sel_filter=="Technician"){
        url=this.config.domain_url+'get_maintenance_request_technician?technician='+this.sel_id+'&branch='+this.sel_branch+'&user_id='+uid;
      }else if(this.sel_filter=="Branch"){
        url=this.config.domain_url+'get_maintenance_request_technician?branch='+this.sel_id;
      }

      // let id=data.toString();
      // let headers=new HttpHeaders({'branch_id':id});
      console.log("head:",url);
      
      this.http.get(url).subscribe((res:any)=>{
        console.log(res);
        this.request=res.data;
      },error=>{
        console.log(error);
        
      })

    // })
    // })
  // })
  
}
fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
}



async getBranch(){
  this.branch.push({id:0,status:'All'});
  // this.storage.ready().then(()=>{
    // const data=await this.storage.get('COMPANY_ID');
      const data=await this.storage.get('USER_ID');
      // let url=this.config.domain_url+'branch_viatype';
      let url=this.config.domain_url+'technician_branches'
      let body={user_id:data}
      // let body={company_id:data,
      //           role_id:4}
      let headers=new HttpHeaders({company_id:data.toString()});
      this.http.post(url,body).subscribe((res:any)=>{
        console.log("res:",res);
        res.forEach(element=>{
          let branch={id:element.branch_id,status:element.branches.name}
          this.branch.push(branch);
        })
        
      })
  //   })
  // })
}
changeStatus(i){
  console.log("item:",i);
  if(i==0){
    this.ionViewWillEnter();
    // this.sel_branch=this.sel_branch;
  }else{
    this.sel_branch=i;
  let url=this.config.domain_url+'get_maintenance_request_technician?branch='+i;
  this.http.get(url).subscribe((res:any)=>{
    console.log(res);
    this.request=res.data;
  },error=>{
    console.log(error);
    
  })
}
}
}

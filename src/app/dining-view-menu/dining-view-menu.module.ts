import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningViewMenuPageRoutingModule } from './dining-view-menu-routing.module';

import { DiningViewMenuPage } from './dining-view-menu.page';
import { ApplicationPipesModule } from '../application-pipes.module';
import { ViewMenuAdditionalComponent } from '../components/dining/view-menu-additional/view-menu-additional.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningViewMenuPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DiningViewMenuPage,ViewMenuAdditionalComponent],
  entryComponents:[ViewMenuAdditionalComponent]
})
export class DiningViewMenuPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningViewMenuPage } from './dining-view-menu.page';

const routes: Routes = [
  {
    path: '',
    component: DiningViewMenuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningViewMenuPageRoutingModule {}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, ModalController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { ChooseDateComponent } from '../components/dining/choose-date/choose-date.component';
import { ViewMenuAdditionalComponent } from '../components/dining/view-menu-additional/view-menu-additional.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-dining-view-menu',
  templateUrl: './dining-view-menu.page.html',
  styleUrls: ['./dining-view-menu.page.scss'],
})
export class DiningViewMenuPage implements OnInit {
  subscription:Subscription;
  menu:any;
  currentDate:any;
  start:any;
  end:any;
  dates:any[]=[];
  sliderOne: any;
// slidenum:any;
// slideOptsOne = {
//   initialSlide: 0,
//   slidesPerView: this.checkScreen(),
  
// };
// progress:any=[];
// viewEntered:boolean=false;

additionalItems:any[]=[];
additional_by_ser_time:any=[];
menuItems:any[]=[];
date:any;
currentIdx:any;

  constructor(private router:Router,private platform:Platform,private route:ActivatedRoute,private http:HttpClient,
    private storage:Storage,private config:HttpConfigService,private loadingCtrl:LoadingController,
    private modalCntrl:ModalController,private popCntrl:PopoverController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.dates=[];
    let menu=this.route.snapshot.paramMap.get('menu')
    this.menu=JSON.parse(menu);
    console.log('menu:',this.menu,menu);
    this.date = this.route.snapshot.paramMap.get('date');
   
    this.currentDate=moment().format('DD MMM YYYY');
    // this.date=this.currentDate;
    this.start=moment(this.menu.start_date);
    this.end=moment(this.menu.enddate);
    var start=this.start.clone();
    let date = moment(this.date).format('DD MMM YYYY')
    
    while(start.isSameOrBefore(this.end)){
      this.dates.push(start.format('DD MMM YYYY'));
      start.add(1,'days');
    }
    this.currentIdx = this.dates.map(item => item).indexOf(date);
    console.log('dates:',this.dates);
    
    // this.sliderOne =
    // {
    //   isBeginningSlide: true,
    //   isEndSlide: false,
    //   isActive:false,
    //   dates:[]=this.dates
    // };
    
    this.getList();
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
   this.back();
   }); 
   
   }
   ionViewWillLeave() { 
   this.subscription.unsubscribe();
   }
   back(){
    // this.router.navigate(['/dining-menu']);
    this.router.navigate(['din-choose-season-date',{menu:JSON.stringify(this.menu),flag:2}],{replaceUrl:true})
   }
  // checkScreen(){
  //   if(window.innerWidth>=700){
  //       return 6;
  //   }else{
  //       return 3;
  //   }
  // }
  // SlideDidChange(object, slideView) {
  //   this.checkIfNavDisabled(object, slideView);
  //   object.isActive= true;
  // }
  // checkIfNavDisabled(object, slideView) {
  //   this.checkisBeginning(object, slideView);
  //   this.checkisEnd(object, slideView);
  // }
  
  // checkisBeginning(object, slideView) {
  //   slideView.isBeginning().then((istrue) => {
  //     object.isBeginningSlide = istrue;
  //   });
  // }
  // checkisEnd(object, slideView) {
  //   slideView.isEnd().then((istrue) => {
  //     object.isEndSlide = istrue;
  //   });
  // }
  // async getDiningDetails(){
   
  //   const cid=await this.storage.get('COMPANY_ID');
  //   const bid=await this.storage.get('BRANCH');
  //   const tz=await this.storage.get('TIMEZONE');
  //   let headers= await this.config.getHeader();
  //   let url=this.config.domain_url+'get_dining_ordering_details';
  //   const type=await this.storage.get('USER_TYPE');
  //   const uid=await this.storage.get('USER_ID');
  //   let consumer;
  //   if(type==5){
  //     consumer=uid
  //   }else{
  //     consumer= await this.storage.get('RESIDENT_ID');
  //   }
  //   let body={
  //     company_id:cid,
  //     dining_id:this.menu.id,
  //     period_status:true,
  //     consumer_user_id:consumer
  //   }
  //   this.http.post(url,body,{headers}).subscribe((res:any)=>{
  //     console.log('dining:',res);
  //     this.progress=res.data.period_progress;
  //     this.viewEntered=true;
  //   },error=>{
      
  //   });
  // }
  // setIndicatorLine(item){
  //   let date=moment(item).format('yyyy-MM-DD');
    
  //   const idx=this.progress.map(x=>x.date).indexOf(date);
  //   console.log('date:',date,idx)
  //   let i;
  //   if(this.progress[idx].color=='x-danger'||this.progress[idx].color=='x-info'){
  //     i=0
  //   }else if(this.progress[idx].color=='x-success'){
  //     i=1
  //   }else if(this.progress[idx].color=='x-warning'){
  //     i=2
  //   }
     
  //   return i;
  // }

  async getList(){
    this.showLoading();
    this.additional_by_ser_time=[];
    let url=this.config.domain_url+'get_menu_details_bydate/'+this.menu.id+'/'+moment(this.date).format('yyyy-MM-DD');
  console.log(url);
  const cid=await this.storage.get('COMPANY_ID');
  const bid=await this.storage.get('BRANCH');
  let headers= await this.config.getHeader();
  this.http.get(url,{headers}).subscribe((res:any)=>{
   
    this.menuItems=res.data.menu_details;
    this.additionalItems=res.data.additionalitems;
    this.menuItems.forEach(el=>{
      this.setAdditionalitem(el.serving_time_id);
    })
    this.dismissLoader();
  })
  }

  async showLoading() {
     
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: 'Please wait...',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  
  async dismissLoader() {
   
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  setAdditionalitem(id){
   
    let item=[];
    this.additionalItems.forEach(element => {
      element.servingtime.map(x => {
        if(id==x.servingtime[0].id){
          item.push(element.item)
          
          // this.servingTime=x.servingtime[0].servingtime
        }
      })
      
    });
    this.additional_by_ser_time.push({item:item,ser_time:id,show:false});
   
  }

  async chooseDate(){
    const modal = await this.modalCntrl.create({
      component: ChooseDateComponent,
      cssClass:'din-choosedate-modal',
      componentProps:{
        menu:this.menu
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if(dataReturned.data){
        this.date=dataReturned.data
        this.getList();
      }
    });
    return await modal.present();
  }

  showDetails(i,st,st_id,f){
    if(i.subitem==1){
      this.router.navigate(['/din-continental-breakfast',{menu:JSON.stringify(this.menu),date:this.date,item:JSON.stringify(i),ser_time:st,ser_time_id:st_id}])
    }else{
    this.router.navigate(['/dining-menu-fi-details',{menu:JSON.stringify(this.menu),date:this.date,item:JSON.stringify(i),ser_time:st,ser_time_id:st_id,flag:f}])
    }
  }

  async options(ev,id) {
     
      
    let item;
    this.additional_by_ser_time.forEach(el=>{
      if(el.ser_time==id){
        item=el
      }
    })

    const popover = await this.popCntrl.create({
      component: ViewMenuAdditionalComponent,
      id:'menu_options',
      event: ev,
      backdropDismiss: true,
      cssClass: 'dining-more-options-popover',
      componentProps: {
        menu:item,
       
        
      },
    });
      popover.onDidDismiss().then((dataReturned) => {
     
          if(dataReturned.data){
         const idx= this.additional_by_ser_time.map(x=>x.ser_time==id).indexOf(id);
         this.additional_by_ser_time[idx]=dataReturned.role;
         console.log('change:',this.additional_by_ser_time)
       
        }

      });

    
    return await popover.present();
  }


  previous() {
    if (this.currentIdx == 0) {
      console.log('reached first item')
    } else {
      console.log('curr:', this.currentIdx, this.dates[this.currentIdx]);
      this.currentIdx--;
      console.log('changedcur:', this.currentIdx, this.dates[this.currentIdx])
      this.date = this.dates[this.currentIdx];
      this.getList();
      // this.showLoading();
      

      let currentDate = moment().format('DD MMM YYYY');
      
    }
  }
  next() {
    if (this.currentIdx == (this.dates.length - 1)) {
      console.log('reached end')
    } else {
      this.currentIdx++;
      this.date = this.dates[this.currentIdx];
      this.getList();
      

      let currentDate = moment().format('DD MMM YYYY');
      
    }
  }

  showOption(menu){
    let op=false;
    let item=[];
    this.additionalItems.forEach(x=>{
      x.servingtime.map(y=>{
        if(y.serving_time_id==menu.serving_time_id){
          op=true;
          
        }
      })
    })
    
    return op;
  }
}

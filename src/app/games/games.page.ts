import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-games',
  templateUrl: './games.page.html',
  styleUrls: ['./games.page.scss'],
})
export class GamesPage implements OnInit {
subscription:Subscription;
  constructor(private router:Router,private platform:Platform) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/entertainment']) ;
  });  
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
play(){
  this.router.navigate(['/play-game']);
}
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-select-category',
  templateUrl: './select-category.page.html',
  styleUrls: ['./select-category.page.scss'],
})
export class SelectCategoryPage implements OnInit {
sid:any;
flag:any;
type:any;
subscription:Subscription;
category:any=[];

  constructor(private storage:Storage,private http:HttpClient,private router:Router,private route:ActivatedRoute,
    private config:HttpConfigService,private platform:Platform,private orient:ScreenOrientation) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.orient.lock(this.orient.ORIENTATIONS.LANDSCAPE);
  
    this.sid=this.route.snapshot.paramMap.get("sid");
    this.flag=this.route.snapshot.paramMap.get("flag");
        this.type=this.route.snapshot.paramMap.get('type');
        
    // this.storage.ready().then(()=>{
      
    //   const res=await this.storage.get('SURVEY_CID');
        

    //     let url=this.config.feedback_url+'get_review_categories';
       
    //     let body=new FormData();
    //     body.append('company_id',res);
    //     body.append('review_bid',this.sid);
    //     body.append('language_id','1');
       
    //     console.log("body:",body);
        
        

    //     this.http.post(url,body).subscribe((data:any)=>{
    //       this.category=data.display_all_review_languages;
          // console.log(this.config.head);
          
    //     })
    //   })
    //   })
    let url=this.config.domain_url+'feedback_categories'
    // let url='http://52.65.155.193/centrim_api/api/feedback_categories';
    this.http.get(url).subscribe((res:any)=>{
      this.category=res.data
    })
      this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
        if(this.flag==1){
          // this.router.navigate(['/select-feedback-type']) ;
          this.router.navigate(['/menu']) ;
        } else{  
          this.router.navigate(['/feedback',{flag:2}]) ;
        }
      });               
  
    }
    back(){
      if(this.flag==1){
        this.router.navigate(['/menu']) ;
      } else{  
        this.router.navigate(['/feedback',{flag:2}]) ;
      }
    }


    gotoFeedback(item){
      // if(item.id==6){
      
        
      //     this.getOtherCat();
      //   }else{
      this.router.navigate(['/feedback-fivesmiley',{sid:this.sid,lid:1,flag:this.flag,cat_id:item.id}])
        // }
      // this.router.navigate(['/newfeedback',{type:this.type,cat_id:item.id}])
    }
    ionViewDidLeave(){
      // this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
      
        this.subscription.unsubscribe();
    }
    getOtherCat(){

    }
}

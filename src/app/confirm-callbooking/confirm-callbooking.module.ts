import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmCallbookingPageRoutingModule } from './confirm-callbooking-routing.module';

import { ConfirmCallbookingPage } from './confirm-callbooking.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmCallbookingPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [ConfirmCallbookingPage]
})
export class ConfirmCallbookingPageModule {}

import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Platform, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { GetObjectService } from '../services/get-object.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'app-confirm-callbooking',
  templateUrl: './confirm-callbooking.page.html',
  styleUrls: ['./confirm-callbooking.page.scss'],
})
export class ConfirmCallbookingPage implements OnInit {
name:any;
consumer_id:any;
date:any;
date_1:any;
slot:any;
duration:any;
img:any;
rec_pic:any;
subscription:Subscription;
family:any=[];
user_name:any;
disableButton:boolean=false;
comment:any;
keyboardStyle = { width: '100%', height: '0px' };
sel_branch:any;
  constructor(private storage:Storage,private route:ActivatedRoute,private http:HttpClient,private config:HttpConfigService,
    private router:Router,private platform:Platform,private objService:GetObjectService,
    private toast:ToastController,private keyboard:Keyboard,private loadingCtrl:LoadingController) { }

  ngOnInit() {
    this.keyboard.onKeyboardWillShow().subscribe( {
      next: x => {
        this.keyboardStyle.height = x.keyboardHeight + 'px';
      },
      error: e => {
        console.log(e);
      }
    });
    this.keyboard.onKeyboardWillHide().subscribe( {
      next: x => {
        this.keyboardStyle.height = '0px';
      },
      error: e => {
        console.log(e);
      }
    });
  }
async ionViewWillEnter(){
  this.name=this.route.snapshot.paramMap.get('name');
  this.consumer_id=this.route.snapshot.paramMap.get('consumer');
  let date=this.route.snapshot.paramMap.get('date');
  this.sel_branch=this.route.snapshot.paramMap.get('branch');
  this.date=new Date(date);
  this.date_1=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
  this.slot=this.route.snapshot.paramMap.get('slot');
  this.duration=this.route.snapshot.paramMap.get('duration');
  this.rec_pic=this.route.snapshot.paramMap.get('pic');
  this.family=this.objService.getExtras();
  console.log("fam:",this.family);
  // this.storage.ready().then(()=>{
    const img=await this.storage.get('PRO_IMG');
      this.img=img
    // });
    const name=await this.storage.get('NAME');
      this.user_name=name
  //   })
  // })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{   
    this.objService.setExtras(this.family) 
    this.router.navigate(['/callbooking',{date:this.date,cid:this.consumer_id,name:this.name,pic:this.rec_pic,branch:this.sel_branch}]) ;
  }); 

}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}

cancel(){
  this.objService.setExtras(this.family)
  this.router.navigate(['/callbooking',{date:this.date,cid:this.consumer_id,name:this.name,pic:this.rec_pic,branch:this.sel_branch}])
}
async confirm(){
  this.disableButton = true;
  this.showLoading();
  // this.storage.ready().then(()=>{
    const cid=await this.storage.get('COMPANY_ID');
      const uid=await this.storage.get('USER_ID');
       
        let url=this.config.domain_url+'add_callbooking';
        let headers=await this.config.getHeader();
        let participants=[];
        participants.push(uid);
        this.family.forEach(element => {
          participants.push(element.cid);
        });
        console.log("par:",participants);
        let d;
          if(this.duration=='30'){
            d=1
          }else{
            d=2
          }
        let body={
          company_id:cid,
          participants:participants,
          consumer_id:this.consumer_id,
          date:this.date_1,
          start_time:this.slot,
          duration:d,
          comment:this.comment
        }
        console.log("bookingdata:",body);
        
        this.http.post(url,body,{headers}).subscribe((data:any)=>{
          
          console.log(data);
          if(data.status=="error"){
            this.dismissLoader();
              this.presentAlert(data.message);
          }else{
            this.dismissLoader();
          this.router.navigate(['/callbooking-success',{date:this.date,slot:this.slot,duration:this.duration}])
          }
          
        })
//       })
//     })
//   })
// })
}

//  method to fix month and date in two digits
fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
}

back(){
  this.router.navigate(['/callbooking',{date:this.date,cid:this.consumer_id,name:this.name,pic:this.rec_pic,branch:this.sel_branch}]);
}
async presentAlert(message) {
  const alert = await this.toast.create({
    message: message,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}

async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}
async dismissLoader() {
  return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}

}

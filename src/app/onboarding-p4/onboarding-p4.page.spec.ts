import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OnboardingP4Page } from './onboarding-p4.page';

describe('OnboardingP4Page', () => {
  let component: OnboardingP4Page;
  let fixture: ComponentFixture<OnboardingP4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnboardingP4Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OnboardingP4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnboardingP4PageRoutingModule } from './onboarding-p4-routing.module';

import { OnboardingP4Page } from './onboarding-p4.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OnboardingP4PageRoutingModule
  ],
  declarations: [OnboardingP4Page]
})
export class OnboardingP4PageModule {}

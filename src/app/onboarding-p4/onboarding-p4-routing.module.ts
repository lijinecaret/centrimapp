import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnboardingP4Page } from './onboarding-p4.page';

const routes: Routes = [
  {
    path: '',
    component: OnboardingP4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OnboardingP4PageRoutingModule {}

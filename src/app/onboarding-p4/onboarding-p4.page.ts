import { HttpClient } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-onboarding-p4',
  templateUrl: './onboarding-p4.page.html',
  styleUrls: ['./onboarding-p4.page.scss'],
})
export class OnboardingP4Page implements OnInit {
  info:any;
  email:any=0;
  myStory:any=0;
  myStoryComment:any=0;
  videoCall:any=0;
  visit:any=0;
  message:any=0;
  maintenance:any=0;
  agree:any=1;
  isLoading:boolean=true;
  subscription:Subscription;
  constructor(private router:Router,private route:ActivatedRoute,private iab:InAppBrowser,private http:HttpClient,
    private config:HttpConfigService,private storage:Storage,private toast:ToastController,private platform:Platform) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.info=JSON.parse(this.route.snapshot.paramMap.get('info'));
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    
      this.back();
    });     
  }
  ionViewDidLeave(){
     
      this.subscription.unsubscribe();
  }
  async back(){
    const data=await this.storage.get('USER_TYPE');
    if(data==6){
      this.router.navigate(['/onboarding-p3',{info:JSON.stringify(this.info)}]);
      }else{
        this.router.navigate(['/onboarding-p2',{info:JSON.stringify(this.info)}]);
      }
  }
  emailStatus(i){
    if(this.isLoading==false){
    if(i.currentTarget.checked==true){
      this.email=1;
    }else{
      this.email=0;
    }
    }
    }
    myStoryStatus(i){
      if(this.isLoading==false){
      if(i.currentTarget.checked==true){
        this.myStory=1;
      }else{
        this.myStory=0;
      }
      }
      }
      myStoryCommentStatus(i){
        if(this.isLoading==false){
        if(i.currentTarget.checked==true){
          this.myStoryComment=1;
        }else{
          this.myStoryComment=0;
        }
        }
        }
        videoCallStatus(i){
          if(this.isLoading==false){
          if(i.currentTarget.checked==true){
            this.videoCall=1;
          }else{
            this.videoCall=0;
          }
          }
          }
          visitStatus(i){
            if(this.isLoading==false){
            if(i.currentTarget.checked==true){
              this.visit=1;
            }else{
              this.visit=0;
            }
            }
            }
            messageStatus(i){
              if(this.isLoading==false){
              if(i.currentTarget.checked==true){
                this.message=1;
              }else{
                this.message=0;
              }
              }
              }
              maintenanceStatus(i){
                if(this.isLoading==false){
                if(i.currentTarget.checked==true){
                  this.maintenance=1;
                }else{
                  this.maintenance=0;
                }
                }
                }

  async next(){
    if(this.agree==0){
      this.presentToast('Please agree to terms & conditions.')
    }else{
    let url=this.config.domain_url+'update_onboarding_details';
    const uid=await this.storage.get('USER_ID');
    const bid=await this.storage.get('BRANCH');
    let body;
    body={
      user_id:uid,
      user_name:this.info.uname,
      name:this.info.name,   
      profile_pic:this.info.img,
      phone:this.info.phone,
      mobile:this.info.mobile,     
      notify_email:this.email

    }
    if(this.info.password){
      body.password=this.info.password;
    }
    if(this.email==1){
      body.my_story=this.myStory;
      body.my_story_comment=this.myStoryComment;
      body.video_call=this.videoCall;
      body.visit_booking=this.visit;
      body.message=this.message;
      body.maintenance_comment=this.maintenance;
      

    }
    console.log('onbody:',body);
    
    this.http.post(url,body).subscribe((res:any)=>{
      console.log(res);
      this.storage.set('USERTOKEN',res.data.token);
      this.storage.set("loggedIn",1);
    this.router.navigate(['/menu']);
    let update_url=this.config.domain_url+'update_login_info';
    let update_body={
      user_id:uid,
      branch_id:bid,
      
    }
    this.http.post(update_url,update_body).subscribe((res:any)=>{
      console.log('info:',res);
      
    })
    },err=>{
      console.log(err)
      this.presentToast('Something went wrong. Please try again later.')
    })
  }
  }

  openTerms(){
  
    let options:InAppBrowserOptions ={
      location:'yes',
    hideurlbar:'yes',
    zoom:'no',
    hidenavigationbuttons:'yes'
    }
    // const browser = this.iab.create('https://app.centrim.life/terms-and-conditions','_blank',options);
    const browser=this.iab.create('https://app.centrim.life/legal','_blank',options)
  
}
async presentToast(mes) {
  const alert = await this.toast.create({
    message: mes,
    duration: 3000,
    cssClass: 'toast-mess',
    position: 'top'
  });
  alert.present();
}
terms(i){
  // if(status.currentTarget.checked==true)
  if(this.isLoading==false){
  if(i.currentTarget.checked==true){
    this.agree=1;
  }else{
    this.agree=0;
  }
}
}
@HostListener('touchstart')
onTouchStart() {

this.isLoading=false;
}
}

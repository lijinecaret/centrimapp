import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { ActivatedRoute, Router } from '@angular/router';

import { ActionSheetController, AlertController, LoadingController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { TemperatureWarningComponent } from '../components/temperature-warning/temperature-warning.component';
import { OpenimageComponent } from '../components/openimage/openimage.component';
import { Subscription } from 'rxjs';
import { Chooser } from '@ionic-native/chooser/ngx';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { Filesystem } from '@capacitor/filesystem';
@Component({
  selector: 'app-checkin-vaccine-info',
  templateUrl: './checkin-vaccine-info.page.html',
  styleUrls: ['./checkin-vaccine-info.page.scss'],
})
export class CheckinVaccineInfoPage implements OnInit {
  users: any = {};
  vaccine: any = {};
  temp: any;
  testDate: any;
  firstDose: any;
  secondDose: any;
  viewEntered: boolean = false;
  tempStatus: any;
  vac_proof: any;
  inf_proof: any;
  dose: any;
  user: any = {};
  vacArray: any[] = [];
  vaccine_id: any;
  vaccination: any;
  vacOrNot: any;
  inf_status: any;
  // multiple_user:boolean;
  residents: any = [];
  user_vaccine: any = [];
  vac_status: any = 0;
  inf_vac_status: any = 0;
  minDate: any = new Date().toISOString();
  vaccinated: any = 0;
  covidVaccinated: any = 0;
  show_covid_vac: boolean = true;
  show_inf_vac: boolean = true;
  expiryDate: any;
  uploaded_date: any;
  loaded: boolean = false;
  repeat_covid: boolean = false;
  covid_second: boolean = false;
  res_id: any;
  purpose: any;
  onPremises: any[] = [];
  subscription: Subscription;
  name: any;
  flag: any;

  from: any;
  constructor(private storage: Storage, private http: HttpClient, private config: HttpConfigService,
    private dialog: SpinnerDialog, private iab: InAppBrowser, private router: Router, 
    private modalCntl: ModalController, private toast: ToastController, private loadingCtrl: LoadingController,
    private popCntl: PopoverController, private route: ActivatedRoute, private platform: Platform,
    private actionsheetCntlr: ActionSheetController, private chooser: Chooser, private alertCntrl: AlertController) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.res_id = this.route.snapshot.paramMap.get('res_id');
    this.purpose = this.route.snapshot.paramMap.get('purpose');
    this.onPremises = JSON.parse(this.route.snapshot.paramMap.get('onPremises'));
    this.flag = this.route.snapshot.paramMap.get('flag');
    this.from = this.route.snapshot.paramMap.get('from');
    this.getDetails();

    // this.storage.ready().then(() => {
      const data=await this.storage.get('NAME');
        this.name = data;
      // })
      const temp=await this.storage.get('VIS-TEMPARATURE');
        this.tempStatus = temp;
      // })

      const vac=await this.storage.get('VIS-VACCINATION');
        this.vaccination = vac;
        if (this.vaccination == 1) {
          const vacArray=await this.storage.get('VACCINES');
            let vac = JSON.parse(vacArray);
            vac.forEach(element => {
              this.vacArray.push(element.vaccine_id)
            });
          // })

        }
    //   })
    // })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.back();
    });

  }
  back() {
    if (this.from == 1 || this.onPremises == null) {
      this.router.navigate(['/checkin-scan-qr']);
    } else if(this.from==3){
      this.router.navigate(['/checkin-phone-num',{res_id:this.res_id,onPremises:JSON.stringify(this.onPremises)}])
    } else {
      this.router.navigate(['/checkin-choose-consumer', { onPremises: JSON.stringify(this.onPremises), res_id: this.res_id }]);
    }
    // this.router.navigate(['/checkin-visit-purpose',{res_id:this.res_id,onPremises:JSON.stringify(this.onPremises),purpose:this.purpose}])
  }
  async getDetails() {
    // this.users=[];
    // let phone = this.user.phone.substring(1);
    // this.storage.ready().then(() => {
      const name=await this.storage.get('NAME');
        const ph=await this.storage.get('PHONE');
          const bid=await this.storage.get('BRANCH');
            const cid=await this.storage.get('COMPANY_ID');
              let url = this.config.domain_url + 'search_uservaccination_app';
              let body = {
                mobile: ph,
                name: name,
                visitor_type: 0,
                branch_id: bid,
                company_id: cid
              }
              let headers = await this.config.getHeader();
              this.http.post(url, body, { headers }).subscribe((res: any) => {
                console.log('vac-info:', res, url, body);
                this.viewEntered = true;
                this.users = res.data
                this.vaccine = res.vaccines;
                this.residents = res.resident_user_id;
                if (this.flag == 1) {
                  this.user = JSON.parse(this.route.snapshot.paramMap.get('user_info'));
                  if(this.user&&this.user.temp){
                  this.temp = this.user.temp;
                  }
                  this.vacOrNot = this.user.vaccine.vacOrNot;
                  this.inf_status = this.user.vaccine.inf_status;
                  this.vaccine_id = this.user.vaccine.id;
                  this.vaccinated = this.user.vaccine.vaccinated;
                  this.covidVaccinated = this.user.vaccine.covidVaccinated
                  this.uploaded_date = this.user.vaccine.uploaded_date;
                  this.repeat_covid = this.user.vaccine.repeat_covid;
                  this.covid_second = this.user.vaccine.covid_second;
                  if (this.user.vaccine.vac_date == null) {
                    this.testDate = undefined
                  } else {
                    this.testDate = this.user.vaccine.vac_date
                  }

                  this.vac_proof = this.user.vaccine.proof;
                  this.vac_status = this.user.vaccine.vac_status;

                  if (this.user.vaccine.firstDose == null) {
                    this.firstDose = undefined
                  } else {
                    this.firstDose = this.user.vaccine.firstDose
                  }
                  if (this.repeat_covid && this.user.vaccine.firstDoseCopy) {
                    this.firstDose = this.user.vaccine.firstDoseCopy
                  }

                  if (this.user.vaccine.secondDose == null) {
                    this.secondDose = undefined
                  } else {
                    this.secondDose = this.user.vaccine.secondDose
                  }
                  if (this.repeat_covid && this.user.vaccine.secondDoseCopy) {
                    this.secondDose = this.user.vaccine.secondDoseCopy
                  }
                  this.inf_vac_status = this.user.vaccine.inf_vaccine_status

                  this.inf_proof = this.user.vaccine.inf_vaccine_proof;
                  this.show_covid_vac = this.user.vaccine.show_vac;
                  this.show_inf_vac = this.user.vaccine.show_inf;
                  this.user_vaccine = this.user.vaccine.user_vaccine;
                } else {
                  if (this.vaccine && this.vaccine.length) {
                    this.vaccine.forEach(element => {
                      this.user_vaccine.push(element.vaccine_id)
                      this.uploaded_date = element.uploaded_date;
                      if (element.vaccine && element.vaccine.id == 1) {
                        this.inf_vac_status = 1;
                        this.inf_status = 1;
                        this.vaccinated = 1;

                        this.testDate = moment(element.vaccine_date).format();
                        this.expiryDate = new Date(element.expiry_date).getTime();


                      } else if (element.vaccine && element.vaccine.id == 2) {
                        this.vac_status = 1;
                        this.covidVaccinated = 1;
                        this.vacOrNot = 1;
                        this.vaccinated = 1;
                        this.vaccine_id = element.vaccine.id

                        if (element.firstdose_date != null) {
                          this.firstDose = moment(element.firstdose_date).format();
                        }
                        if (element.seconddose_date != null) {
                          this.secondDose = moment(element.seconddose_date).format();
                        }

                      }

                    });
                    if (this.user_vaccine.includes(2)) {
                      if (this.secondDose != undefined) {
                        this.show_covid_vac = false;

                      }
                      if (this.firstDose != undefined && this.secondDose == undefined) {
                        this.repeat_covid = true;
                      }
                    }
                    if (this.user_vaccine.includes(1)) {
                      let days = this.expiryDate - new Date().getTime();;
                      let day = Math.floor(days / (60 * 60 * 24 * 1000));
                      if (day >= -1) {
                        this.show_inf_vac = false;
                      }
                      console.log('days:', day)
                    }

                    console.log('covac:', this.vac_proof, this.secondDose, this.firstDose, this.vacOrNot);
                    console.log('ifvac:', this.inf_status, this.inf_proof, this.testDate)
                  }
                }

              })
    //         })
    //       })
    //     })
    //   })
    // })
  }



  async gotoNext() {
    // this.storage.ready().then(() => {
      const temp=await this.storage.get('VIS-TEMPARATURE');
        this.tempStatus = temp;
        const vac=await this.storage.get('VIS-VACCINATION');
          if (this.tempStatus == 1 && (this.temp == undefined || this.temp == '')) {

            this.presentToast('Please enter your body temperature.');

          } else if (this.tempStatus == 1 && (parseFloat(this.temp) > 37.4||parseFloat(this.temp)<30)) {

            this.warning();


          } else {
            // if(vac == 1 && this.vacOrNot !== 1 && this.inf_status !== 1){

            // }
            let firstDose, secondDose, testDate, firstDoseCopy, secondDoseCopy;
            if (!this.firstDose) {
              firstDose = null
              firstDoseCopy = null
            } else {
              firstDose = moment(this.firstDose).format('yyyy-MM-DD HH:mm:ss');
              firstDoseCopy = firstDose
            }
            if (!this.secondDose) {
              secondDose = null
              secondDoseCopy = null
            } else {
              secondDose = moment(this.secondDose).format('yyyy-MM-DD HH:mm:ss');
              secondDoseCopy = secondDose
              // if(this.repeat_covid){
              //   firstDose=null
              // }
            }
            if (!this.testDate) {
              testDate = null
            } else {
              testDate = moment(this.testDate).format('yyyy-MM-DD')
            }
            if (!this.vac_proof) {
              this.vac_proof = null
            }
            if (!this.inf_proof) {
              this.inf_proof = null
            }
            if (!this.uploaded_date) {
              this.uploaded_date = null;
            }
            if (this.repeat_covid) {
              firstDose = null;
              // secondDose=null;
            }
            if (this.covid_second) {
              secondDose = null
            }
            this.user.vaccine = {
              id: this.vaccine_id,
              vac_date: testDate,
              dose: this.dose,
              proof: this.vac_proof,
              vac_status: this.vac_status,
              firstDose: firstDose,
              firstDoseCopy: firstDoseCopy,
              secondDose: secondDose,
              inf_vaccine_status: this.inf_vac_status,
              inf_vaccine_date: testDate,
              inf_vaccine_proof: this.inf_proof,
              vaccinated: this.vaccinated,
              covidVaccinated: this.covidVaccinated,
              show_vac: this.show_covid_vac,
              show_inf: this.show_inf_vac,
              user_vaccine: this.user_vaccine,
              vacOrNot: this.vacOrNot,
              inf_status: this.inf_status,
              uploaded_date: this.uploaded_date,
              repeat_covid: this.repeat_covid,
              covid_second: this.covid_second
            }
            this.user.temp = this.temp;




            this.router.navigate(['/checkin-additional-visitors', { res_id: this.res_id, purpose: this.purpose, onPremises: JSON.stringify(this.onPremises), user_info: JSON.stringify(this.user), from: 3 }])




          }
    //     })
    //   })
    // })
  }


  async warning() {
    const modal = await this.modalCntl.create({
      component: TemperatureWarningComponent,
      cssClass: 'warningModal',
      componentProps: {
        // name: this.user.name,


      },
      id: 'warning'
    });

    return await modal.present();
  }

  async presentToast(mes) {
    const alert = await this.toast.create({
      message: mes,
      duration: 3000,
      cssClass: 'toast-mess',
      position: 'top'
    });
    alert.present();
  }

  async getVaccineList() {
    let headers=await this.config.getHeader();
    this.vacArray = [];
    let url = this.config.domain_url + 'get_vaccine';
    this.http.get(url,{headers}).subscribe((res: any) => {

      this.vacArray = res.data;

      console.log('vac:', res, this.vaccine_id);


    })
  }

  async capturePhoto(i) {

    // if(this.platform.is('android')){
    //   this.presentAlert('Please rotate to front camera.',i)
    // }else{
    // this.takePhoto(i)
    // }
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(i)
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.takePhoto(i);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();

  }
  async uploadImage(img, i) {
    // if(this.isLoading==false){
    this.showLoading();
    // }
    let headers=await this.config.getHeader();
    let url = this.config.domain_url + 'upload_file';
    let body = { file: img }
    console.log("body:", img);

    this.http.post(url, body,{headers}).subscribe((res: any) => {
      console.log("uploaded:", res);
      if (i == 2) {
        this.vac_proof = res.data;
        // this.vacOrNot=1
        this.vaccine_id = 2
      } else {
        this.inf_proof = res.data;
        // this.inf_status=1
      }
      this.dismissLoader();
    }, error => {
      // this.imageResponse.slice(0,1);
      console.log(error);
      this.dismissLoader();
      this.presentToast('Upload failed.Please try again.')

    })
  }

  async showImg(img) {
    const popover = await this.popCntl.create({
      component: OpenimageComponent,
      cssClass: 'msg_attach',
      id: 'viewImg',
      // event: ev,
      componentProps: {
        data: img
      },
      // cssClass:'image_pop'
    });
    return await popover.present();
  }
  async showLoading() {

    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      message: 'Please wait...',
      spinner: null,
      id: 'loader'
      // duration: 3000
    });
    return await loading.present();
  }

  async dismissLoader() {

    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  clearDate(i) {
    if (i == 1) {
      this.firstDose = undefined
      if (this.secondDose == undefined) {
        this.vacOrNot = undefined;
        this.vaccine_id = null;
        this.vac_status = 0;
      }
    } else if (i == 2) {
      this.secondDose = undefined
      if (this.firstDose == undefined) {
        this.vacOrNot = undefined;
        this.vaccine_id = null;
        this.vac_status = 0;
      }
    } else if (i == 3) {
      this.testDate = undefined
      this.inf_status = undefined;
      this.inf_vac_status = 0;
    }
  }
  removeImg(i) {
    if (i == 1) {
      this.vac_proof = undefined
    } else {
      this.inf_proof = undefined
    }
  }

  changeStatus(i) {
    if (this.loaded) {
      if (i == 1 || i == 2) {
        this.vacOrNot = 1;

        this.covidVaccinated = 0;
        this.vaccine_id = 2;
        this.vac_status = 1;
        this.vaccinated = 0;
      } else if (i == 3) {
        this.inf_status = 1;
        this.inf_vac_status = 0;
        this.vaccinated = 0;
      }
      this.uploaded_date = moment().format('yyyy-MM-DD')
    }
  }
  // async presentAlert(mes,i){
  //   const alert = await this.alertCntl.create({
  //     // header: 'Log out',
  //     backdropDismiss: true,
  //     message: mes,
  //     buttons: [
  //       {
  //         text: 'Ok',
  //         role: 'cancel',
  //         // cssClass: 'secondary',
  //         handler: (blah) => {
  //           this.takePhoto(i)
  //         }
  //       }
  //     ]
  //   });

  //   await alert.present();
  // }
  async pickImage(i) {
    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:1
      
    });
  
  
    for (var j = 0; j < image.photos.length; j++) {
         
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[j].path
            });
            
            
              this.uploadImage('data:image/jpeg;base64,' + contents.data,i)
            
              
         
             
        }
       
  }
  async takePhoto(i) {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    var imageUrl = image.base64String;
   
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    
    this.uploadImage(base64Image,i);
  
  }
  @HostListener('touchstart')
  onTouchStart() {

    this.loaded = true;
  }


  openDoc(item) {


    // window.open(encodeURI(item.post_attachment),"_system","location=yes");

    // if(item.attachment.length>0){
    let options: InAppBrowserOptions = {
      location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar: 'yes',
      zoom: 'yes'
    }
    if(item.includes('.pdf')){
      this.showpdf(item)
  }else{
    const browser = this.iab.create('https://docs.google.com/viewer?url=' + item + '&embedded=true', '_blank', options);
    browser.on('loadstart').subscribe(() => {
      console.log('start');
      this.dialog.show();

    }, err => {
      console.log(err);

      this.dialog.hide();
    })

    browser.on('loadstop').subscribe(() => {
      console.log('stop');

      this.dialog.hide();;
    }, err => {
      this.dialog.hide();
    })

    browser.on('loaderror').subscribe(() => {
      this.dialog.hide();
    }, err => {
      this.dialog.hide();
    })

    browser.on('exit').subscribe(() => {
      this.dialog.hide();
    }, err => {
      this.dialog.hide();
    })
  }
  }


  async attachfile(i) {
    const actionSheet = await this.actionsheetCntlr.create({
      header: 'Select',
      mode:'ios',
      buttons: [{
        text: 'Image',
        // role: 'destructive',
        icon: 'image-outline',
        handler: () => {
          this.capturePhoto(i);
          console.log('Delete clicked');
        }
      }, {
        text: 'Document',
        icon: 'document-outline',
        handler: () => {
          this.selectDoc(i);
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancel',
        // icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  async selectDoc(i) {
    this.chooser.getFile()
      .then(file => {
        console.log(file ? file : 'canceled');
        if (file) {
          if (file.mediaType == "application/pdf" || file.mediaType == "application/msword") {
            let data;
            if (file.mediaType == "application/msword") {
              let type = /msword/
              data = file.dataURI.replace(type, 'doc')
            } else {
              data = file.dataURI
            }
            this.uploadImage(data, i);
          } else {
            this.alert('File format not supported');
          }
        }
      })
      .catch((error: any) => console.error(error));
  }

  async alert(mes) {

    const alert = await this.alertCntrl.create({

      message: mes,
      backdropDismiss: true
    });

    await alert.present();
  }

  async showpdf(file){
    const modal = await this.modalCntl.create({
      component: ViewpdfComponent,
      cssClass:'fullWidthModal',
      componentProps: {
        
        data:file,
        
         
      },
      
      
    });
    return await modal.present();
  }
}

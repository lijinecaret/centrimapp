import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckinVaccineInfoPageRoutingModule } from './checkin-vaccine-info-routing.module';

import { CheckinVaccineInfoPage } from './checkin-vaccine-info.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckinVaccineInfoPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [CheckinVaccineInfoPage]
})
export class CheckinVaccineInfoPageModule {}

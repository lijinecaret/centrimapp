import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckinVaccineInfoPage } from './checkin-vaccine-info.page';

const routes: Routes = [
  {
    path: '',
    component: CheckinVaccineInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckinVaccineInfoPageRoutingModule {}

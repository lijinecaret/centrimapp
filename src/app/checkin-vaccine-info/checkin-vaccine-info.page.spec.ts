import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckinVaccineInfoPage } from './checkin-vaccine-info.page';

describe('CheckinVaccineInfoPage', () => {
  let component: CheckinVaccineInfoPage;
  let fixture: ComponentFixture<CheckinVaccineInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinVaccineInfoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckinVaccineInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { AlertController, ModalController, Platform, ToastController,LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { ScanerrorComponent } from '../components/scanerror/scanerror.component';
import { Subscription } from 'rxjs';
import { BarcodeScanner } from '@capacitor-community/barcode-scanner';
@Component({
  selector: 'app-checkin-scan-qr',
  templateUrl: './checkin-scan-qr.page.html',
  styleUrls: ['./checkin-scan-qr.page.scss'],
})
export class CheckinScanQrPage implements OnInit {
  scannedData:any;
  members:any[]=[];
  res_id:any;
  checkedout_members:any[]=[];
  all_members:any[]=[];
  subscription:Subscription;
  // opened:boolean=false;

  scanActive: boolean = false;
  constructor(private loadingCtrl:LoadingController,private config:HttpConfigService,private http:HttpClient,
    private storage:Storage,private router:Router,private toastCntlr:ToastController,
    private alertCntlr:AlertController,private modalCntl:ModalController,private platform:Platform) { }

  ngOnInit() {
  }

ionViewWillEnter(){
  // this.scan();
  // this.opened=false;

  this.getFamily();
  
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    
    

    if(!this.scanActive)  {
    this.router.navigate(['/menu']) ;
    }else{
      this.stopScanner();
    }
    // this.back() ;
 
  }); 
}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
  BarcodeScanner.stopScan();
    this.scanActive = false;
    document.querySelector('body').classList.remove('scanner-active');
}
  // async scan(){
   
  //   this.opened=true;
  //   const option:BarcodeScannerOptions={
      
  //     // preferFrontCamera : true, 
  //     prompt : "Place a qrcode inside the scan area", 
  //     resultDisplayDuration: 700,
 
  // }
  // this.barcode
  //   .scan(option)
  //   .then(barcodeData => {
      
  //     this.scannedData =barcodeData;
  //     console.log("scanned:",this.scannedData);
      
  //     let url=this.scannedData.text;
  //     console.log('url:',url);
  //     if(this.scannedData.cancelled==true){
  //       setTimeout(()=>{
  //         this.opened=false;
  //       },600)
      
  //   }
  //   if(url){
  //     // if(url.includes('https://'+dom+'.centrim.life/')){

  //       let id=url.substr(url.lastIndexOf('/')+1);
  //           this.permission();
  //       this.checkin(id);
        
        
  //     // }else{
        
  //     //   this.openModal();
  //     // }
      
  //   }
    
  //   })
  //   .catch(err => {
  //     this.opened=false;
  //     console.log("Error", err,this.opened);
      
  //   })

        
  // }

  async checkPermission() {
    return new Promise(async (resolve, reject) => {
      const status = await BarcodeScanner.checkPermission({ force: true });
      if (status.granted) {
        resolve(true);
      } else if (status.denied) {
        BarcodeScanner.openAppSettings();
        resolve(false);
      }
    });
  }

  async startScanner() {
    
    document.head.style.background= 'transparent';
    document.querySelector('body').classList.add('scanner-active');
    const allowed = await this.checkPermission();

    if (allowed) {
      this.scanActive = true;
      BarcodeScanner.hideBackground();

      const result = await BarcodeScanner.startScan();
      if (result.hasContent) {
        this.scanActive = false;
        // alert(result.content); //The QR content will come out here
        //Handle the data as your heart desires here
        let url=result.content;
        console.log('url:',url);
        
        
      
      
  
          let id=url.substring(url.lastIndexOf('/')+1);
          
              this.permission(); 
             
          this.checkin(id);
      } else {
      
    this.openModal();   
   
      }
    } else {
    this.scanActive = false;
  
    this.openModal();

    }
  }

  stopScanner() {
    BarcodeScanner.stopScan();
    this.scanActive = false;
    document.querySelector('body').classList.remove('scanner-active');
  }




  async showLoading() {
      
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      
      // message: 'Please wait...',
      spinner: null,
      // duration: 3000,
    });
    return await loading.present();
  }
  
  async dismissLoader() {
    
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

async checkin(id){
  // this.dismissLoader();
  let headers=await this.config.getHeader();
  this.members=[];
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');
      const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
      let url=this.config.domain_url+'checkin_details/'+id+'/'+data;
      this.showLoading(); 
      this.http.get(url,{headers}).subscribe((res:any)=>{
        
        console.log("res:",res,url);
        if(res.branch_details.branch.id==bid&&res.branch_details.branch.company_id==cid){
          this.checkedout_members=res.checkedout_residents;
          res.residents_on_premises.forEach(element => {
            console.log("user_id:",element.user_id);
            
            this.members.push(element.user_id);
            
          });
         
          setTimeout(()=>{
          this.next();
        },500)
        }else {
          
          this.openModal();
        }
        this.dismissLoader();
        // this.members=res.residents_on_premises;
        
      },error=>{
        console.log(error);
        this.dismissLoader();
      })
//     })
//   })
// })
//   })

}

  async getFamily(){
    this.all_members=[];
    // this.storage.ready().then(()=>{
      const id=await this.storage.get('USER_ID');
        const cid=await this.storage.get('COMPANY_ID');
          const tok=await this.storage.get('TOKEN');
            let token=tok;
            const bid=await this.storage.get('BRANCH');
              let branch=bid.toString();
        
      
      let url=this.config.domain_url+'myfamily/'+id;
      let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log("fam:",res)
         
     
      
          res.data[0].family.forEach(ele => {
            this.all_members.push(ele.parent_details[0]);
          })
        })
//       })
//       })
//     })
//   })
// })
  }

  async next(){
   
    if(this.members.length==0){
      console.log('not in premises:',this.all_members);
      this.presentAlert("The resident you are visiting is not in the premise right now.")
    }
    else if(this.members.length>0){
      console.log(' in premises');
      if(this.all_members.length>1){
        console.log('multi:',this.members);
      this.router.navigate(['/checkin-choose-consumer',{onPremises:JSON.stringify(this.members)}]);
    }else{
      console.log('single');
      this.res_id=this.all_members[0].user_id;
      // this.router.navigate(['/checkin-visit-purpose',{res_id:this.res_id,flag:1}]);

      // this.storage.ready().then(() => {
        const ph=await this.storage.get('PHONE');
          if(ph==null||ph=='+61'||ph.toString().length<10){
            this.router.navigate(['/checkin-phone-num',{res_id:this.res_id,onPremises:JSON.stringify(this.members),from:1}])
          }else{
        const temp=await this.storage.get('VIS-TEMPARATURE');
         
      
      const vac=await this.storage.get('VIS-VACCINATION');
        if(temp==1||vac==1){
        this.router.navigate(['/checkin-vaccine-info',{res_id:this.res_id,onPremises:JSON.stringify(this.members),from:1}])
        }else{
          this.router.navigate(['/checkin-additional-visitors', {res_id:this.res_id,onPremises:JSON.stringify(this.members),from:1}])
        }
    //   })
    // })
  }
// })
//   })
    }
  }
  }
  async presentToast(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'       
    });
    alert.present(); //update
  }
  

  async presentAlert(mes){
    const alert = await this.alertCntlr.create({
      // header: 'New version available',
      message: mes,
      
      backdropDismiss:true
    });
  
    await alert.present();
  }

  async permission(){
    // this.storage.ready().then(()=>{
      const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
    let url=this.config.domain_url+'visitor_settings';
    let headers=await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log("vis:",res);

      this.storage.set("VIS-ADDITIONAL-VISITOR-INFO",res.data.additional_visitor_info);
      // this.storage.set("VIS-ADDRESS-REQUIRED",res.data.address_required);
      this.storage.set("VIS-HEALTH-DECLARATION",res.data.health_declaration);
      // this.storage.set("VIS-PHOTO-REQUIRED",res.data.photo_required);
      this.storage.set("VIS-RECENT-TEST-DETAILS",res.data.recent_test_details);
      // this.storage.set("VIS-SIGNATURE",res.data.signature);
      this.storage.set("VIS-TEMPARATURE",res.data.temparature);
      this.storage.set("VIS-TERMS",res.data.terms_conditions);
      this.storage.set("VIS-VACCINATION",res.data.vaccination_details);
      this.storage.set("GOV-CHECKIN",JSON.stringify(res.data.govt_checkin));
      this.storage.set("VACCINES",JSON.stringify(res.data.selected_vaccines))
    })
//   })
// })
//     })
  }

  async openModal(){
  
   
    const modal = await this.modalCntl.create({
      component: ScanerrorComponent,
     cssClass:'warningModal',
     backdropDismiss:false,
     id:'scan'
    });
   modal.onDidDismiss().then((data)=>{
     
    
  })
    return await modal.present();
  }
}

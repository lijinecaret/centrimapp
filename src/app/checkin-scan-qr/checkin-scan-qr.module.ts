import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckinScanQrPageRoutingModule } from './checkin-scan-qr-routing.module';

import { CheckinScanQrPage } from './checkin-scan-qr.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckinScanQrPageRoutingModule
  ],
  declarations: [CheckinScanQrPage]
})
export class CheckinScanQrPageModule {}

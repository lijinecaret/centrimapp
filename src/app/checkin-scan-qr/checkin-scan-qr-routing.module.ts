import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckinScanQrPage } from './checkin-scan-qr.page';

const routes: Routes = [
  {
    path: '',
    component: CheckinScanQrPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckinScanQrPageRoutingModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckinScanQrPage } from './checkin-scan-qr.page';

describe('CheckinScanQrPage', () => {
  let component: CheckinScanQrPage;
  let fixture: ComponentFixture<CheckinScanQrPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinScanQrPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckinScanQrPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient } from '@angular/common/http';
import { ActionSheetController, AlertController, LoadingController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';

// import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { Chooser } from '@ionic-native/chooser/ngx';
import { ActivityImageComponent } from '../components/activity-image/activity-image.component';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';

@Component({
  selector: 'app-maintenance-comment',
  templateUrl: './maintenance-comment.page.html',
  styleUrls: ['./maintenance-comment.page.scss'],
})
export class MaintenanceCommentPage implements OnInit {
flag:any;
req_id:any;
comment:any;
subscription:Subscription;
// files:FileLikeObject[];
img:any=[];
imageResponse:any=[];
docResponse:any=[];
  docs:any=[];
isLoading:boolean=false;
// @ViewChild(MultiFileUploadComponent,{static:false}) fileField: MultiFileUploadComponent;
  constructor(private route:ActivatedRoute,private storage:Storage,private config:HttpConfigService,private http:HttpClient,
    private router:Router,private platform:Platform,private toast:ToastController,private loadingCtrl:LoadingController,
    private actionsheetCntlr:ActionSheetController,private alertCntrl:AlertController,
    private chooser:Chooser,private popCntrl:PopoverController) { }

  ngOnInit() {
    // this.imagePicker.requestReadPermission();
  }
ionViewWillEnter(){
  this.img=[];
  this.imageResponse=[];
  this.docs=[];
  this.docResponse=[];
  this.flag=this.route.snapshot.paramMap.get('flag');
  this.req_id=this.route.snapshot.paramMap.get('req_id');
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    if(this.flag==1){
      this.router.navigate(['/maintenance-details',{req_id:this.req_id}],{replaceUrl:true}) ;
    }
    else if(this.flag==2){
      this.router.navigate(['/consumer-maintenance-details',{req_id:this.req_id}],{replaceUrl:true});
    }
    }); 
}
async selectImage() {
  const actionSheet = await this.actionsheetCntlr.create({
    header: "Select Image source",
    buttons: [{
      text: 'Load from Library',
      handler: () => {
        this.addImage();
      }
    },
    {
      text: 'Use Camera',
      handler: () => {
        this.pickImage();
      }
    },
    {
      text: 'Cancel',
      role: 'cancel'
    }
    ]
  });
  await actionSheet.present();
  
}


async pickImage(){
  // const options: CameraOptions = {
  //   quality: 100,
  //   sourceType: this.camera.PictureSourceType.CAMERA,
  //   destinationType: this.camera.DestinationType.DATA_URL,
  //   encodingType: this.camera.EncodingType.JPEG,
  //   mediaType: this.camera.MediaType.PICTURE
  // }
  // this.camera.getPicture(options).then((imageData) => {
  //   // imageData is either a base64 encoded string or a file URI
  //   // If it's base64 (DATA_URL):
  //   let base64Image = 'data:image/jpeg;base64,' + imageData;
    
  //   this.imageResponse.push(base64Image);
  //   this.uploadImage(base64Image);
  //   //  this.img.push(imageData);
  // }, (err) => {
  //   // Handle error
  // });
  const image = await Camera.getPhoto({
    quality: 90,
    allowEditing: false,
    resultType: CameraResultType.Base64,
    correctOrientation:true,
    source:CameraSource.Camera
  });

  // image.webPath will contain a path that can be set as an image src.
  // You can access the original file using image.path, which can be
  // passed to the Filesystem API to read the raw data of the image,
  // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
  var imageUrl = image.base64String;

  // Can be set to the src of an image now
  let base64Image = 'data:image/jpeg;base64,' + imageUrl;
  console.log('image:',imageUrl);
  this.imageResponse.push(base64Image);
  this.uploadImage(base64Image);
}
async addImage(){
  
  // let options;
  // options={
  //   maximumImagesCount: 5,
  //   outputType: 1
  // }
  // if(this.platform.is('ios')){
  //   options.disable_popover=true
  // }
  // this.imagePicker.getPictures(options).then((results) => {
  //   for (var i = 0; i < results.length; i++) {
  //       console.log('Image URI: ' + results[i]);
        
        
  //       if((results[i]==='O')||results[i]==='K'){
  //         console.log("no img");
          
  //         }else{
  //           // this.img.push(results[i]);
  //           this.uploadImage('data:image/jpeg;base64,' + results[i]);
  //         this.imageResponse.push('data:image/jpeg;base64,' + results[i])
  //         }
  //   }
  // }, (err) => { });

  const image = await Camera.pickImages({
    quality: 90,
    correctOrientation:true,
    limit:5
    
  });


  for (var i = 0; i < image.photos.length; i++) {
          console.log('Image URI: ' + image.photos[i]);
          
          
          const contents = await Filesystem.readFile({
            path: image.photos[i].path
          });
          
            this.imageResponse.push('data:image/jpeg;base64,' + contents.data);
            this.uploadImage('data:image/jpeg;base64,' + contents.data)
          
            
       
           
      }



console.log("imagg:",image.photos,image.photos[0].path);

}
removeImg(i){
  this.imageResponse.splice(i,1);
  this.img.splice(i,1);
}
async submit(){


  
  
    
console.log('com:',this.comment)
    
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');
      
      let url=this.config.domain_url+'store_comments';
      if(this.comment==undefined || this.comment=='' || this.comment==' '){
        // this.dismissLoader();
        this.presentAlert('Please enter your comment');
        // this.dismissLoader();
      }else{
        this.showLoading();
        if(this.docs.length>0){
          this.docs.forEach(element => {
            this.img.push(element)
          });
        }
    //     this.files = this.fileField.getFiles();
    //     console.log(this.files);
    //   let formData = new FormData();
    // formData.append('from_id', data); // Add any other data you want to send
    // formData.append('request_id', this.req_id);
    // formData.append('comment', this.comment);
    // this.files.forEach((file) => {
    //   formData.append('images[]', file.rawFile);
    // });
      let body={
        from_id:data,
        request_id:this.req_id,
        comment:this.comment,
        images:this.img,
        visibility:1
      }
      console.log("body:",body);
      let headers= await this.config.getHeader();
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.comment="";
        this.presentAlert('Comment added successfully.');
          this.dismissLoader();
        this.back();
        
      },error=>{
        this.dismissLoader();
          this.presentAlert('Something went wrong.Please try again.');
        console.log(error);
        
      })
      }
  //   })
  // })
}
back(){
  if(this.flag==1){
    this.router.navigate(['/maintenance-details',{req_id:this.req_id}])
  }
  else if(this.flag==2){
    this.router.navigate(['/consumer-maintenance-details',{req_id:this.req_id,flag:1}])
  }
  this.comment='';
  this.dismissLoader();
}
async presentAlert(message) {
  const alert = await this.toast.create({
    message: message,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}
async showLoading() {
  this.isLoading=true;
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}
async dismissLoader() {
  this.isLoading=false;
  return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}



// private getBlob(b64Data:string, contentType:string, sliceSize:number= 512) {
//   contentType = contentType || '';
//   sliceSize = sliceSize || 512;
//   let byteCharacters = atob(b64Data);
//   let byteArrays = [];

//   for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
//       let slice = byteCharacters.slice(offset, offset + sliceSize);

//       let byteNumbers = new Array(slice.length);
//       for (let i = 0; i < slice.length; i++) {
//           byteNumbers[i] = slice.charCodeAt(i);
//       }

//       let byteArray = new Uint8Array(byteNumbers);

//       byteArrays.push(byteArray);
//   }
//   let blob = new Blob(byteArrays, {type: contentType});
//   return blob;
// }
async uploadImage(img){
  if(this.isLoading==false){
    this.showLoading();
    }
    let headers= await this.config.getHeader()
  let url=this.config.domain_url+'upload_file';
  let body={file:img}
console.log("body:",img);

  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    
    // this.img.push(res.data);
    this.img.push(res.data);
    this.dismissLoader();
    // console.log("uploaded:",this.img);
  },error=>{
    console.log(error);
    this.dismissLoader();
  })
}

async selectDoc(){
  this.chooser.getFile()
.then(file => {
  console.log(file ? file : 'canceled');
  if(file){
  if(file.mediaType=="application/pdf" || file.mediaType=="application/msword"){
    let data;
    if(file.mediaType=="application/msword"){
      let type=/msword/
      data=file.dataURI.replace(type,'doc')
    }else{
      data=file.dataURI
    }
    this.uploadDoc(data,file.name);
  }else{
    this.alert('File format not supported');
  }
}
})
.catch((error: any) => console.error(error));
}

async alert(mes){
  
  const alert = await this.alertCntrl.create({
    
    message: mes,
    backdropDismiss:true
  });

  await alert.present();
}

async uploadDoc(file,fname){
  let fileName ;
  let headers= await this.config.getHeader()
  let name=fname.split('.',1)
  if(name.toString().length>3){
    fileName=name.toString().substring(0,3)+'...'+fname.substring(fname.lastIndexOf('.')+1);
  }
  let url=this.config.domain_url+'upload_file';
    let body={file:file}
  console.log("body:",body);
  if(!this.isLoading){
    this.showLoading();;
    this.isLoading=true;
    }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      
      // this.img.push(res.data);
      this.docs.push(res.data);
      this.dismissLoader();
      this.isLoading=false;
      this.docResponse.push({name:fileName});
      // console.log("uploaded:",this.img);
    },error=>{
      console.log(error);
      this.dismissLoader();
      this.isLoading=false;
      this.presentAlert('Something went wrong')
    })
  
}

removeDoc(i){
  this.docResponse.splice(i,1);
  this.docs.splice(i,1);
}


async presentActionSheet() {
  const actionSheet = await this.actionsheetCntlr.create({
    header: 'Select',
    buttons: [{
      text: 'Image',
      // role: 'destructive',
      icon: 'image-outline',
      handler: () => {
        this.selectImage();
        console.log('Delete clicked');
      }
    }, {
      text: 'Document',
      icon: 'document-outline',
      handler: () => {
        this.selectDoc();
        console.log('Favorite clicked');
      }
    }, {
      text: 'Cancel',
      // icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  });
  await actionSheet.present();
}
async showImage(){
  const popover = await this.popCntrl.create({
    component: ActivityImageComponent,
    // event: ev,
    componentProps:{
      data:this.img,
      flag:1
    },
    cssClass:'image_pop'
  });
  return await popover.present();
}
}

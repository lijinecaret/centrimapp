import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MaintenanceCommentPage } from './maintenance-comment.page';

const routes: Routes = [
  {
    path: '',
    component: MaintenanceCommentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MaintenanceCommentPageRoutingModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsumerContactPage } from './consumer-contact.page';

const routes: Routes = [
  {
    path: '',
    component: ConsumerContactPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsumerContactPageRoutingModule {}

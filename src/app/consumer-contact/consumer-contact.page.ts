import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { InvitationComponent } from '../invitation/invitation.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-consumer-contact',
  templateUrl: './consumer-contact.page.html',
  styleUrls: ['./consumer-contact.page.scss'],
})
export class ConsumerContactPage implements OnInit {
  cid:any;
  subscription:Subscription;
  flag:any;
  contacts:any=[];
pending:any=[];
hideInvite:boolean=false;
res_branch:any;
res_id:any;

rv:any;
  constructor(private router:Router,private route:ActivatedRoute,public popoverController:PopoverController,private http:HttpClient,
    private config:HttpConfigService,private platform:Platform,private storage:Storage) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.res_branch=this.route.snapshot.paramMap.get('branch');
    this.rv=await this.storage.get('RVSETTINGS');
    this.getContacts();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/resident-overview',{flag:this.flag}],{replaceUrl:true}) ;
    }); 

  }


  ionViewWillLeave() { 
    this.subscription.unsubscribe();
   
 }
 async getContacts(){
  const type=await this.storage.get('USER_TYPE');
  const id=await this.storage.get('USER_ID');
  const cid=await this.storage.get('COMPANY_ID');
  let url=this.config.domain_url+'consumer/'+this.cid;
                console.log(url);
                let headers=await this.config.getHeader();
                // let headers= await this.config.getHeader();
                this.http.get(url,{headers}).subscribe((data:any)=>{
                console.log('res:',data)
                  this.contacts=data.data.contacts;
                  this.pending=data.data.pending_contacts;
                  this.res_id=data.data.user_id;
                  if(type==5){
                    this.hideInvite=true;
                  }else{
                    this.contacts.forEach(ele=>{
                      if(ele.is_primary==1){
                        let primaryId=ele.contact_user_id;
                        // this.storage.ready().then(()=>{
                        //   this.storage.get("USER_ID").then(usr_id=>{
                            if(id==primaryId){
                              this.hideInvite=true;
                            }else{
                              this.hideInvite=false;
                            }
                          }
                        })
                  }
                })
 }


 // open invitation  
 async invite() {
  // console.log(ev);
  const popover = await this.popoverController.create({
    component: InvitationComponent,
   
    cssClass:'invite_pop-1',
    componentProps:{
      id:this.res_id
    }
  });

  popover.onDidDismiss().then((dataReturned)=>{
    if(dataReturned.data){
      this.getContacts();
    }
    else
    {
      this.getContacts();
    }
  })
 
  return await popover.present();
}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsumerContactPageRoutingModule } from './consumer-contact-routing.module';

import { ConsumerContactPage } from './consumer-contact.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsumerContactPageRoutingModule
  ],
  declarations: [ConsumerContactPage]
})
export class ConsumerContactPageModule {}

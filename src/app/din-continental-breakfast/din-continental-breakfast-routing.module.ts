import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinContinentalBreakfastPage } from './din-continental-breakfast.page';

const routes: Routes = [
  {
    path: '',
    component: DinContinentalBreakfastPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinContinentalBreakfastPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinContinentalBreakfastPageRoutingModule } from './din-continental-breakfast-routing.module';

import { DinContinentalBreakfastPage } from './din-continental-breakfast.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinContinentalBreakfastPageRoutingModule
  ],
  declarations: [DinContinentalBreakfastPage]
})
export class DinContinentalBreakfastPageModule {}

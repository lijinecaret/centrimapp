import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-din-continental-breakfast',
  templateUrl: './din-continental-breakfast.page.html',
  styleUrls: ['./din-continental-breakfast.page.scss'],
})
export class DinContinentalBreakfastPage implements OnInit {
  subscription:Subscription;
  menu:any;
  date:any;
  item:any;
  details:any=[];
  ser_time:any;
  ser_time_id:any;
  constructor(private route:ActivatedRoute,private router:Router,private platform:Platform,private storage:Storage,
    private config:HttpConfigService,private http:HttpClient) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    let menu=this.route.snapshot.paramMap.get('menu')
      this.menu=JSON.parse(menu);
      this.date=this.route.snapshot.paramMap.get('date');
      this.ser_time=this.route.snapshot.paramMap.get('ser_time')
      this.ser_time_id=this.route.snapshot.paramMap.get('ser_time_id')
    let item=this.route.snapshot.paramMap.get('item');
    this.item=JSON.parse(item)
    console.log('item:',this.item,this.menu);
    const cid=await this.storage.get('COMPANY_ID');
      const bid=await this.storage.get('BRANCH');
      const uid=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'get_menu_item_coloumn_wise/'+this.item.datails_column_id;
      let headers=await this.config.getHeader();
      let body={
        consumer_id:uid,
        menu_item_id:this.item.id,
        date:moment(this.date).format('YYYY-MM-DD'),
        meal_time_id:this.ser_time_id
       
      }
      console.log('cbody:',body)
      this.http.get(url,{headers}).subscribe((res:any)=>{
       console.log('confirm:',res);
       
       this.details=res.data.individual_items[0].sub_items;
      })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      this.back();
      }); 
      
      }
      ionViewWillLeave() { 
      this.subscription.unsubscribe();
      }
      
        back(){
          this.router.navigate(['/dining-view-menu',{menu:JSON.stringify(this.menu),date:this.date}])
        }

        showItem(sub){
          this.router.navigate(['/din-continental-fi-details',{menu:JSON.stringify(this.menu),date:this.date,item:JSON.stringify(this.item),ser_time:this.ser_time,ser_time_id:this.ser_time_id,sub:JSON.stringify(sub)}])
        }
}

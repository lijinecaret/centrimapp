import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinContinentalBreakfastPage } from './din-continental-breakfast.page';

describe('DinContinentalBreakfastPage', () => {
  let component: DinContinentalBreakfastPage;
  let fixture: ComponentFixture<DinContinentalBreakfastPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinContinentalBreakfastPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinContinentalBreakfastPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

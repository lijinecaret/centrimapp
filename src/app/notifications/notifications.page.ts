import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController, Platform, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {
  subscription:Subscription;
  notification:any=[];
  currentDate:number;
  date:number;
  time:any;
  flag:any;
  offset:any;
  constructor(private router:Router,private platform:Platform,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private route:ActivatedRoute,private alertCntlr:AlertController,private toastCntl:ToastController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.notification=[];
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.currentDate=(new Date()).getTime();
    this.notification=[];
      this.getNotification(false,'');
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{  
    if(this.flag==1){  
    this.router.navigate(['/stories']) ;
    }else{
      this.router.navigate(['/menu']);
    }
});  

}

async getNotification(isFirstLoad,event){
  const data=await this.storage.get('USER_ID');
  let url=this.config.domain_url+'notification/'+data;
  let body={
   id:data,
   offset:this.offset,
   limit:20
 }
  let headers= await this.config.getHeader();
  console.log("head:",headers);
  this.http.get(url,{headers}).subscribe((res:any)=>{
    console.log(res);
  
   res.data.forEach(element => {
     this.date=(new Date(element.created_at)).getTime();
     let t=this.currentDate-this.date;
     let min=Math.floor(t/ (60*1000));
     let hr = Math.floor(t/ 3600 / 1000); // round to nearest second
     let days= Math.floor(t/ (60*60*24*1000));
     let month=Math.floor(t/ (60*60*24*1000*31));
     let yr=Math.floor(t/ (60*60*24*1000*31*12))
     // console.log("time:",this.com_time);
     if(month>12){
       this.time=yr+ ' years ago';
     }
     else if(days>31){
       this.time=month+ ' months ago ';
     }
     else if(hr>24){
       
       this.time=days+ ' days ago ';
     }else if(min>60){
       this.time=hr+' hours ago ';
     }
     else if(min<=60 && min>1){
       this.time=min+' minutes ago ';
     }
     else {
       this.time="Now";
     }
     console.log("time:",this.time);
     this.notification.push({'id': element.id,
     'user_id': element.user_id,
     'not_type': element.not_type,
     'link_id': element.link_id,
     'notification_icon': element.notificaiton_icon,
     'title':element.title,
     'payload': element.payload,
     'readstatus':element.readstatus,
     'created_at': this.time})
    });
    console.log("not:",this.notification);
  //   if (isFirstLoad)
  //   event.target.complete();

  // this.offset=this.offset+20;
  })
}
back(){
  if(this.flag==1){
    this.router.navigate(['/stories'])
  }else{
    this.router.navigate(['/menu']);
  }
}
ionViewWillLeave() { 
this.subscription.unsubscribe();
}

async gotoDetails(item,idx){
  // this.readItem(item.id);
  // this.storage.ready().then(()=>{
  const id=await this.storage.get('USER_TYPE');
  if(item.not_type==1 ||item.not_type==2 ||item.not_type==3 || item.not_type==5 || item.not_type==6 ||item.not_type==14){
    let flag;
    if(item.not_type==5){
      flag=1
    }else{
      flag=2
    }
    this.router.navigate(['/story-details',{post_id:item.link_id,flag:flag}])
  }
  else if(item.not_type==4 ){
    this.router.navigate(['/activity-details',{act_id:item.link_id}]);
  }
  else if(item.not_type==7){
    this.router.navigate(['/activity-details',{act_id:item.link_id}]);
  }
  else if(item.not_type==8){
    this.router.navigate(['/message']);
  }
  else if(item.not_type==9){
    this.router.navigate(['/bookedcall-list']);
  }else if(item.not_type==21){
    this.router.navigate(['/visit-booking']);
  }else if(item.not_type==11){
    this.router.navigate(['/newsletter']);
  }else if(item.not_type==12||item.not_type==13){
    if(id==4){
      this.router.navigate(['/maintenance-details',{filter:0,req_id:item.link_id}]);
    }else{
      this.router.navigate(['/consumer-maintenance-details',{req_id:item.link_id}])
    }
  }else if(item.not_type==20){
    this.router.navigate(['/poll']);
  }else if(item.not_type==17){
    this.router.navigate(['/dining-menu'])
  }
  this.readItem(item.id,idx)
// })
// })
}


async readItem(id,idx){
  console.log("delete");
  
  let url=this.config.domain_url+'readnotification';
  let body={not_id:id}
  let headers= await this.config.getHeader();
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log(res);
    this.notification.splice(idx,1);
  })
}
async readAll(){
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'readallnotification';
      let body={user_id:data}
      let headers= await this.config.getHeader();
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.notification=[];
        this.presentAlert('You have cleared all notifications')
      })
  //   })
  // })
}

async readAllAlert(){
  const alert = await this.alertCntlr.create({
    mode:'ios',
    header: 'Clear Notification',
    message: 'Are you sure you want to clear all the notifications.',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Clear',
        handler: () => {
          this.readAll();
        }
      }
    ]
  });

  await alert.present();
}
async presentAlert(mes){
  const alert = await this.toastCntl.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'      
  });
  alert.present();
}

// doInfinite(ev){
//   this.getNotification(true,ev)
// }
}

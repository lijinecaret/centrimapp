import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FeedbackTypePageRoutingModule } from './feedback-type-routing.module';

import { FeedbackTypePage } from './feedback-type.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FeedbackTypePageRoutingModule
  ],
  declarations: [FeedbackTypePage]
})
export class FeedbackTypePageModule {}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { IonContent, LoadingController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-feedback-type',
  templateUrl: './feedback-type.page.html',
  styleUrls: ['./feedback-type.page.scss'],
})
export class FeedbackTypePage implements OnInit {
  subscription:Subscription;
  category:any=[];
  type:any;
  cat_id:any;
cat:HTMLElement;
  
  show=4;
  contacts:any[]=[];
  con_id:any;
  utype:any;
  sid:any;
  @ViewChild(IonContent, { static: false }) content: IonContent;
  flag:any;
  branch:any;
  constructor(private router:Router,private orient:ScreenOrientation,private platform:Platform,
    private config:HttpConfigService,private http:HttpClient,private loadingCtrl:LoadingController,
    private storage:Storage,private toast:ToastController,public elRef: ElementRef,private route:ActivatedRoute) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    // this.orient.lock(this.orient.ORIENTATIONS.LANDSCAPE);
    this.contacts=[];
    this.sid=this.route.snapshot.paramMap.get("sid");
    this.flag=this.route.snapshot.paramMap.get("flag");
        // this.ftype=this.route.snapshot.paramMap.get('type');
        if(this.flag==1){
      this.branch=this.route.snapshot.paramMap.get("branch");
        }

    // this.storage.ready().then(()=>{
      const utype=await this.storage.get('USER_TYPE');
       
        this.utype=utype;
        if(utype==6){
          this.getFamily();
        }else{
          this.show=0;
          this.con_id=await this.storage.get('USER_TYPE');
              this.branch=await this.storage.get('PCFBRANCH');
        }
        this.flag = this.route.snapshot.paramMap.get("flag");
        if(this.flag==1){
        this.show=1
        
      this.con_id=this.route.snapshot.paramMap.get('res');
      
      this.cat_id = this.route.snapshot.paramMap.get("cat_id");
      
      this.type = this.route.snapshot.paramMap.get("type");
        }else{
      
      if(utype==6){
        this.show=4;
        }else{
          this.show=0;
        }
        }
      this.getCategories();
    // this.orient.lock(this.orient.ORIENTATIONS.LANDSCAPE);
     
        this.cat=this.elRef.nativeElement.querySelector('#cat');
       
        this.type;
        this.cat_id;
        
      
        // let body={
        //   company_id:cid,
        //   review_bid:this.sid,
        //   language_id:1
        // }
       
        this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
          
          // if(this.flag==1){
            this.router.navigate(['/menu']) ;
          // }else{
          //   this.router.navigate(['/feedback'])
          // }
         
        });      
  //     })
  //   })
  // }) 
  }
  next(id){
    this.type=id;
    this.show=1;
    // this.content.scrollToPoint(0, this.cat.offsetTop, 1000);
    // this.router.navigate(['/select-category',{type:id}])
  }

  async getCategories(){
    const cid=await this.storage.get("SURVEY_CID");
    let body=new FormData();
    body.append('company_id',cid);
    body.append('review_bid',this.sid);
    body.append('language_id','1');
   
    let url=this.config.feedback_url+'get_review_categories';
  // let url='http://52.65.155.193/centrim_api/api/feedback_categories';
  console.log('catbody:',body,cid,this.sid);
  
  this.http.post(url,body).subscribe((res:any)=>{
    console.log('cat:',res);
    
    this.category=res.display_all_review_languages
  })
  }
 
  
  gotoFeedback(cat){
    this.cat_id=cat.id;
    // this.show=2
    
    this.router.navigate(['/feedback-fivesmiley',{sid:this.sid,lid:1,cat_id:this.cat_id,res:this.con_id,branch:this.branch,type:this.type}],{replaceUrl:true})
    
    // this.content.scrollToPoint(0, this.feed.offsetTop, 1000);
  }
  
  
  
  
  
  
  ionViewDidLeave(){
    this.type='';
    this.cat_id='';
   
    this.show=4;
  }
  
  
  
  
  
  async presentAlert(message) {
    const alert = await this.toast.create({
      message: message,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'       
    });
    alert.present(); //update
  }
  
  
  
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }
  previous(){
    if(this.show==1){
      this.show=0
    }else if(this.show==2){
      this.show=1
    }else if(this.show==0){
      this.show=4
    }
  }
  
  
  async getFamily(){
    this.showLoading();
    // this.storage.ready().then(()=>{
      const uid=await this.storage.get('USER_ID');
      const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
        let url=this.config.domain_url+'showfamily/'+uid;
        let headers= await this.config.getHeader()
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log("parent:",res);
          
          res.data[0].family.forEach(element => {
            
              this.contacts.push(element.parent_details[0])
            });
         
            console.log("arr:",this.contacts);
            if(this.contacts.length<=1){
              if(this.contacts[0].status==0){
                this.show=5;
              }else{
              this.show=0;
              this.con_id=this.contacts[0].user.name;
              this.branch=this.contacts[0].branches.user_branch.pcs_branch_id;
              }
            }
            this.dismissLoader();
        },err=>{
          this.dismissLoader();
        })
  
    //   })
    // })
    //   })
    // })
    
    
  }
  selectConsumer(id,item){
    if(item.status==0){
      this.presentAlert("This resident is inactive.")
    }else{
    this.con_id=item.user.name;
    this.branch=item.branches.user_branch.pcs_branch_id;
    this.show=0;
    this.feedback(this.branch);
    }
  }

  back(){
    // if(this.flag==1){
      this.router.navigate(['/menu'],{replaceUrl:true}) ;
    // }else{
    //   this.router.navigate(['/feedback'])
    // }
  }


  async feedback(data){
  
   
      // const dat=await this.storage.get('PCFBRANCH');
        let id=data.toString();
      
        const x=await this.storage.get('SURVEY_CID');
          let cid=x.toString();
         
          let url=this.config.feedback_url+'get_defaulted_review';
          let body = `company_id=${cid}&branch_id=${id}`;
         
          console.log("body:",body);
          
          let headers=new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          })
          this.http.post(url,body,{headers}).subscribe((data:any)=>{
            console.log('default:',data);
            
            this.sid=data.data.review_branch_id;
            this.getCategories();
          })
        }

}

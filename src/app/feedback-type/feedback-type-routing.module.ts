import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeedbackTypePage } from './feedback-type.page';

const routes: Routes = [
  {
    path: '',
    component: FeedbackTypePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeedbackTypePageRoutingModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OnboardingP3Page } from './onboarding-p3.page';

describe('OnboardingP3Page', () => {
  let component: OnboardingP3Page;
  let fixture: ComponentFixture<OnboardingP3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnboardingP3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OnboardingP3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

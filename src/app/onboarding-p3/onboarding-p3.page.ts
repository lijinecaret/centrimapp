import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-onboarding-p3',
  templateUrl: './onboarding-p3.page.html',
  styleUrls: ['./onboarding-p3.page.scss'],
})
export class OnboardingP3Page implements OnInit {
  info:any;
  parent:any=[];
  selected:any;
  subscription:Subscription;
  constructor(private router:Router,private route:ActivatedRoute,private toast:ToastController,
    private config:HttpConfigService,private http:HttpClient,private storage:Storage,private platform:Platform) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.info=JSON.parse(this.route.snapshot.paramMap.get('info'));
    this.getResidents();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
    
      this.back();
    });     
  }
  ionViewDidLeave(){
     
      this.subscription.unsubscribe();
  }

  back(){
    this.router.navigate(['/onboarding-p2',{info:JSON.stringify(this.info)}]);
  }

  async getResidents(){
    this.parent=[];
  
      const data=await this.storage.get('USER_ID');
     
        let url=this.config.domain_url+'profile/'+data;
        let headers= await this.config.getHeader();
          this.http.get(url,{headers}).subscribe((res:any)=>{
            console.log("get:",res,url);
            
                res.data.family.forEach(element=>{
                  if(element.parent_details[0].status==1){
                 
                  this.parent.push({parent:element.parent_details[0],share:element.share_details,relation:element.relation});
                  }
                })
               
                console.log("parent",this.parent);
               
                  this.selected=this.parent[0].parent.user.user_id;
               
                      this.storage.set('BRANCH',this.parent[0].parent.wing.branch_id);
                      this.storage.set('RESIDENT_ID',this.parent[0].parent.user.user_id);
                      this.storage.set('RESIDENT_USER_ID',this.parent[0].parent.id);
                      this.storage.set('RESIDENT_NAME',this.parent[0].parent.user.name)
                      this.storage.set('RESIDENT_IMG',this.parent[0].parent.user.profile_pic);
                      this.storage.set('RES-RV-SETTING',this.parent[0].parent.branches.user_branch.retirement_living);
                      this.storage.set('COMAPANY_NAME',this.parent[0].parent.user.company.c_name);
                     
              
                
          },error=>{
            console.log(error);
            
          });
    
  
  }
  selectResident(item){
    this.selected=item.parent.user.user_id;
    this.storage.set('BRANCH',item.parent.wing.branch_id);
    this.storage.set('RESIDENT_ID',item.parent.user.user_id);
    this.storage.set('RESIDENT_USER_ID',item.parent.id);
    this.storage.set('RESIDENT_NAME',item.parent.user.name)
    this.storage.set('RESIDENT_IMG',item.parent.user.profile_pic);
    this.storage.set('RES-RV-SETTING',item.parent.branches.user_branch.retirement_living);
    this.storage.set('COMAPANY_NAME',item.parent.user.company.c_name);

  }
  next(){
    this.router.navigate(['/onboarding-p4',{info:JSON.stringify(this.info)}]);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnboardingP3Page } from './onboarding-p3.page';

const routes: Routes = [
  {
    path: '',
    component: OnboardingP3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OnboardingP3PageRoutingModule {}

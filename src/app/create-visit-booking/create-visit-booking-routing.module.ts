import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateVisitBookingPage } from './create-visit-booking.page';

const routes: Routes = [
  {
    path: '',
    component: CreateVisitBookingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateVisitBookingPageRoutingModule {}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ModalController, Platform, ToastController } from '@ionic/angular';
import { GetObjectService } from '../services/get-object.service';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { VisitBookingWarningComponent } from '../components/visit-booking-warning/visit-booking-warning.component';
import { OtherVisitorsComponent } from '../components/other-visitors/other-visitors.component';
import { VisitBookingConfirmationComponent } from '../components/visit-booking-confirmation/visit-booking-confirmation.component';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-create-visit-booking',
  templateUrl: './create-visit-booking.page.html',
  styleUrls: ['./create-visit-booking.page.scss'],
})
export class CreateVisitBookingPage implements OnInit {
  content: any = 0;
  type: any;

  contacts: any = [];
  family: any = [];
  user_id: any;
  next: boolean = false;
  cid: any;
  c_name: any;
  c_pic: any;
  fam: any = [];
  fam_mem: any = [];
  sel_con: any;
  sel_fam: boolean = false;
  duration: any = '1';
  slot: any = [];
  start: any;
  end: any;
  hours;
  minutes;
  not_available: any = [];
  ar_avl: Array<string>;
  date: any;
  wing: any;
  sel_slot: any;
  other:any;
  subscription:Subscription;
  comment:any;
  sel_branch:any;
  constructor(private storage: Storage, private config: HttpConfigService, private http: HttpClient, private router: Router,
    private platform: Platform, private objService: GetObjectService, private toastCntlr: ToastController,
    private modalCntlr: ModalController,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }
  selectType(i) {
    this.type=i;
    this.content = 1
  }
  async ionViewWillEnter() {
    this.sel_con = (-1);
    this.fam = [];
    this.fam_mem = [];
    this.family = [];
    this.contacts = [];
    this.date = moment().add(1, 'days').format('yyyy-MM-DD');
  
    // this.storage.ready().then(() => {
      const data=await this.storage.get('USER_TYPE');
        const id=await this.storage.get('USER_ID');
          this.user_id = id
        // })

        if (data == 6) {
          this.getFamily();

        } else if (data == 5) {
          this.getContacts();

        }
    //   })
    // })
    this.start = '05:00:00';
    this.end = '23:00:00';

    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.back() ;
    }); 
  }

  async getFamily() {
    // this.storage.ready().then(() => {
      const uid=await this.storage.get('USER_ID');
        
          
          let headers=await this.config.getHeader();
        let url = this.config.domain_url + 'showfamily/' + uid;
       
        this.http.get(url, { headers }).subscribe((res: any) => {
          console.log("parent:", res);

          res.data.forEach(ele => {
            ele.family.forEach(element => {
              this.contacts.push({ parent: element.parent_details[0], share: element.share_details })
            });
            if(this.contacts[0].share!==0||this.contacts[0].parent.status!==0){
              this.choose(this.contacts[0],0);
            }
          })

        })
    //   })
    // })

    //   })
    // })
    console.log("arr:", this.contacts);

  }

  // get contacts for resident login
  async getContacts() {
    this.contacts = [];
    // this.storage.ready().then(() => {
      const uid=await this.storage.get('USER_ID');
        let url = this.config.domain_url + 'showcontacts/' + uid;
        let headers=await this.config.getHeader();
        this.http.get(url, { headers }).subscribe((res: any) => {
          console.log("con:", res);
          res.data.forEach(element => {
            this.contacts.push({ 'primary': element.is_primary, 'rel': element.relation, 'user': element.user });

          });
          console.log("arr:", this.contacts);
        })



    //   })
    // })

  }
  async choose(item, idx) {
    console.log("choose:", item)
    if (item.share == 0) {
      this.presentAlert("You don't have access to this profile right now.")
    } else if(item.parent.status==0){
      this.presentAlert("This resident is inactive.")
    }else {
      this.sel_con = idx;
      this.fam = [];
      this.fam_mem = [];
      this.family = [];
      this.next = true;
      this.cid = item.parent.user.user_id;
      this.c_name = item.parent.user.name;
      this.c_pic = item.parent.user.profile_pic;
      this.wing = item.parent.wing.id;
      this.sel_branch= item.parent.wing.branch_id;
      this.getAvailableTimeSlots();
      let f_url = this.config.domain_url + 'bookingConsumer/' + item.parent.user.user_id;
      console.log("url:", f_url);

      let headers=await this.config.getHeader();
      this.http.get(f_url, { headers }).subscribe((data: any) => {
        console.log("fam:", data);
        let fam = data.data.contacts;
        fam.forEach(element => {
          if (element.user.user_id != this.user_id) {
            let ele = { fam: element, selected: false }
            this.family.push(ele)
          }

        });

      })
    }

  }
  chooseFam(item) {
    if(item.fam.user.status==0){
      this.presentAlert('Inactive family member.')
    }else{
    item.selected = !item.selected;
    this.fam.push({ cid: item.fam.user.user_id, name: item.fam.user.name, pic: item.fam.user.profile_pic });
    this.fam_mem = this.fam.reduce((arr, x) => {
      let exists = !!arr.find(y => y.cid === x.cid);
      if (!exists) {
        arr.push(x);
      } else {
        var index = arr.indexOf(x);
        arr.splice(index, 1);
      }

      return arr;
    }, []);


    console.log(this.fam_mem);
    // if(this.fam_mem.includes())

  }
  }

  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top' 
    });
    alert.present(); //update
  }

  selectVisitors() {
    if (this.c_name == undefined || this.c_name == ' ' || this.c_name == null) {
      this.presentAlert('Please choose a person.')
    } else {
      // this.objService.setExtras(this.fam_mem);
      this.content = 2;

    }
  }

  onChange(ev) {

    let current = moment();
    let date = moment(this.date);
    console.log('date:', current, date, date.diff(current, 'days'),current.isBefore(date))
    // if (date.diff(current, 'days') < 1) {
    //   this.openWarning(1)
    // }
   if(date.isBefore(current)){
    this.openWarning(1);
   }
    else if ((date.diff(current, 'days') > 14)) {
      this.openWarning(2)
    } else {

      this.getAvailableTimeSlots();
    }

  }


  selectDuration() {
    console.log("dur:", this.duration);
    // this.slot=this.timeArray(this.start,this.end,30);
    this.getAvailableTimeSlots();
    // this.slot=this.timeArray(this.start,this.end,30);

    console.log("slots:", this.slot);

  }

  async getAvailableTimeSlots() {
    this.not_available = [];
    // this.storage.ready().then(() => {
      const data=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');

        let headers=await this.config.getHeader();
          // let headers= await this.config.getHeader();
          let url = this.config.domain_url + 'visiting_timeslots';
          // let d;
          // if(this.duration=='30'){
          //   d=1
          // }else{
          //   d=2
          // }
          let body = {
            company_id: data,
            date: moment(this.date).format('yyyy-MM-DD'),
            duration: this.duration,
            wing_id: this.wing,
            type: this.type
          }
          console.log("body:", body);

          this.http.post(url, body, { headers }).subscribe((res: any) => {
            console.log("res:", res)
            res.data.forEach(element => {
              console.log(element);
              let t = { slots: element }
              this.not_available.push(t);


            });
            this.ar_avl = res.data
            // this.not_available.push(res.data));
            console.log("fff:", this.ar_avl)
            this.slot = this.timeArray(this.start, this.end, 30);
          }, error => {
            console.log(error);
          })
    //     })
    //   })
    // })
    console.log("not:", this.not_available);
    // this.ar_avl= this.not_available.map(p => p.slots)



  }


  timeArray(start, end, duration) {
    var start = start.split(":");
    var end = end.split(":");
    duration = parseInt(duration);
    start = parseInt(start[0]) * 60 + parseInt(start[1]);
    end = parseInt(end[0]) * 60 + parseInt(end[1]);
    console.log(start, end);

    var result = [];

    for (let time = start; time <= end; time += duration) {
      var slot = this.timeString(time);
      let disable;

      // alligator.includes("thick scales");
      // console.log("arraaaa:",this.ar_avl,"sl:",slot)
      // console.log("incl:",this.ar_avl.includes(slot));
      if (this.ar_avl.length > 0) {
        if (this.ar_avl.includes(slot)) {
          disable = false;
        } else {
          disable = true;
        }
      } else {
        disable = true
      }
      // this.not_available.forEach(el => {


      //   console.log("loop executed");

      //   if(el.slots===slot){
      //     disable== true;
      //   }
      //   else{
      //     disable== false;
      //   }
      //   console.log("dis:",disable);

      // })
      result.push({ 'slot': slot, 'disable': disable });

    }
    console.log("result:", result);

    return result;
  }

  timeString(time) {
    this.hours = Math.floor(time / 60);
    let h = Math.floor(time / 60);
    this.minutes = time % 60;
    if (this.hours > 12) {
      this.hours = this.hours - 12;
    }
    if (this.hours < 10) {
      this.hours = "0" + this.hours; //optional
    }

    if (this.minutes < 10) {
      this.minutes = "0" + this.minutes;
    }

    if (h >= 12) {
      return this.hours + ":" + this.minutes + ' PM';
    } else {
      return this.hours + ":" + this.minutes + ' AM';
    }
  }
  back() {
    this.router.navigate(['/visit-booking'], { replaceUrl: true });
  }


  async openWarning(i) {
    const popover = await this.modalCntlr.create({
      component: VisitBookingWarningComponent,
      backdropDismiss: true,
      componentProps: {
        data: i
      },
      cssClass: 'morepop'


    });
    popover.onDidDismiss().then((dataReturned)=>{
      this.date = moment().add(1, 'days').format('yyyy-MM-DD');
      this.getAvailableTimeSlots()
    })
    return await popover.present();
  }

  async otherVisitor() {
    const popover = await this.modalCntlr.create({
      component: OtherVisitorsComponent,
      backdropDismiss: true,
      componentProps: {
        more: this.other
      },
      cssClass: 'morepop'


    });
    popover.onDidDismiss().then((dataReturned)=>{
      if(dataReturned.data){
      this.other=dataReturned.data;
      }
    })
    return await popover.present();
  }

  bookvisit(item) {
   
    if (item.disable) {
      this.presentAlert('This time slot is not available.Please select another.');

    } else {
      this.sel_slot = item.slot;
      this.confirmAlert();
      // this.confirm();
    }
 
  }


  async confirmAlert() {
    
    let dur;
    if(this.duration==1){
      dur='30 Minutes'
    }else if(this.duration==2){
      dur='60 Minutes'
    }else{
      dur='2 Hours'
    }
    console.log('slo:',this.sel_slot);
  
    const popover = await this.modalCntlr.create({
      component: VisitBookingConfirmationComponent,
      backdropDismiss: true,
      componentProps: {
        fam: this.fam_mem,
        other:this.other,
        name:this.c_name,
        date:this.date,
        slot:this.sel_slot,
        duration:dur,
        pic:this.c_pic
      },
      cssClass: 'visit_booking_confirmation'


    });
    popover.onDidDismiss().then((dataReturned)=>{
      console.log('sl:',this.sel_slot);
      if(dataReturned.data){
        this.comment=dataReturned.data.comment
        this.showLoading();
        this.confirm();
      }
    })
    return await popover.present();
    
  }

  async confirm() {
    // this.storage.ready().then(() => {
      const data=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
          const uid=await this.storage.get('USER_ID');
          let headers=await this.config.getHeader();
          
          let url = this.config.domain_url + 'add_visiting_booking';
         let other,other_type;
         if(this.other==undefined){
           other=null;
           other_type=0
         }else{
           other=this.other[0];
           if(this.other.length>1){
           this.other.forEach(element => {
             if(element==other){

             }else{
               other=other+','+element
             }
           });
          }
           other_type=1
         }
         let participants=[];
        participants.push(uid);
        if(this.fam_mem&&this.fam_mem.length){
        this.fam_mem.forEach(element => {
          participants.push(element.cid);
        });
      }
          let body = {
            company_id: data,
            date: moment(this.date).format('yyyy-MM-DD'),
            duration: this.duration,
            start_time: this.sel_slot,
            consumer_id: this.cid,
            participants:participants,
            wing_id:this.wing,
            type:this.type,
            other_visitor_type:other_type,
            other_visitor_list:other,
            comment:this.comment
          }
          console.log("body:", body);

          this.http.post(url, body, { headers }).subscribe((res: any) => {
            console.log("res:", res)
            if(data.status=="error"){
              this.dismissLoader();
                this.presentAlert(data.message);
            }else{
            this.dismissLoader();
            this.presentAlert('Booking successfull.')
            this.back();
            }
          }, error => {
            this.dismissLoader();
            this.presentAlert('Something went wrong. Please try again later')
            console.log(error);
          })
  //       })
  //     })
  //   })
  // })
  }
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      duration: 3000
    });
    return await loading.present();
}

  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  removeOtherVisitor(idx){
    this.other.splice(idx,1);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateVisitBookingPageRoutingModule } from './create-visit-booking-routing.module';

import { CreateVisitBookingPage } from './create-visit-booking.page';
import { CalendarModule } from 'ion2-calendar';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateVisitBookingPageRoutingModule,
    CalendarModule,
    ApplicationPipesModule
  ],
  declarations: [CreateVisitBookingPage]
})
export class CreateVisitBookingPageModule {}

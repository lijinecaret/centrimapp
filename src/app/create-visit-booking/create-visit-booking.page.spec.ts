import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateVisitBookingPage } from './create-visit-booking.page';

describe('CreateVisitBookingPage', () => {
  let component: CreateVisitBookingPage;
  let fixture: ComponentFixture<CreateVisitBookingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateVisitBookingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateVisitBookingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

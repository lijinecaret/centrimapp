import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CallbookingPage } from './callbooking.page';

const routes: Routes = [
  {
    path: '',
    component: CallbookingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CallbookingPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CallbookingPageRoutingModule } from './callbooking-routing.module';

import { CallbookingPage } from './callbooking.page';
import { ApplicationPipesModule } from '../application-pipes.module';
// import { CalendarModule } from 'ion2-calendar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // CalendarModule,
    CallbookingPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [CallbookingPage]
})
export class CallbookingPageModule {}

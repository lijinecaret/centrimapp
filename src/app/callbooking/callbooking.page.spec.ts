import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CallbookingPage } from './callbooking.page';

describe('CallbookingPage', () => {
  let component: CallbookingPage;
  let fixture: ComponentFixture<CallbookingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallbookingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CallbookingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

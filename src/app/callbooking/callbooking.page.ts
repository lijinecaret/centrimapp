import { Component, OnInit, ViewChildren } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpConfigService } from '../services/http-config.service';
import { ToastController, Platform, IonSlides } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { GetObjectService } from '../services/get-object.service';

@Component({
  selector: 'app-callbooking',
  templateUrl: './callbooking.page.html',
  styleUrls: ['./callbooking.page.scss'],
})
export class CallbookingPage implements OnInit {
  @ViewChildren('slideWithNav') slideWithNav: IonSlides;
  date:Date;
  today:any;
  current:Date;
  duration:any;
  slot:any=[];
  start:any;
  end:any;
  hours;
  minutes;
  consumer_id:any;
  date_1:any;
  not_available:any=[];
  name:any;
  pic:any;
  subscription:Subscription;
  family:any=[];
  re:boolean;
  ar_avl: Array<string>;
  fff:string;
  sliderOne: any;
slidenum:any;
slideOptsOne = {
  initialSlide: 0,
  slidesPerView: this.checkScreen(),
  
};
day:string;
year:any;
month:any;
week=[];
currentDate:any=0;
sel_slot:any;
sel_branch:any;
  constructor(private storage:Storage,private http:HttpClient,private route:ActivatedRoute,private config:HttpConfigService,
    private toastCntlr:ToastController,private router:Router,private platform:Platform,private objService:GetObjectService) { }
    checkScreen(){
      if(window.innerWidth>=700){
          return 6;
      }else{
          return 3;
      }
  }
  ngOnInit() {
    this.duration="30";
    this.current=new Date();
    this.date=this.current;
    this.today=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
    this.date_1=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
    var mon = new Array();
    mon[0] = "January";
    mon[1] = "February";
    mon[2] = "March";
    mon[3] = "April";
    mon[4] = "May";
    mon[5] = "June";
    mon[6] = "July";
    mon[7] = "August";
    mon[8] = "September";
    mon[9] = "October";
    mon[10] = "November";
    mon[11] = "December";
    this.year=this.date.getFullYear();
    this.month=mon[this.date.getMonth()];

    
    

    let m=this.current.getMonth()+1
  // get current week
    for (let i = 0; i <= 6; i++) {

      // let first = this.current .getTime() - (7 * 24 * 60 * 60 * 1000)

      let mon=m;
      let first = this.current.getDate()+ i;
      if(m==1||m==3||m==5||m==7||m==8||m==10||m==12){
        if (first>31){
          first=first-31;
          mon=m+1
          
        }
      }else if(m==2){
        if(first>28){
          first=first-28;
          mon=m+1
        }
      }else{
        if(first>30){
          first=first-30;
          mon=m+1
        }
      }
      let x= this.current.getDay()+i;
     if(x>6){
       x=x-7;
     }
        if(x==0){
        this.day="Sun";
      }
      else if(x==1){
        this.day='Mon';
      }
      else if(x==2){
        this.day='Tue';
      }
      else if(x==3){
        this.day='Wed';
      }
      else if(x==4){
        this.day='Thu';
      }
      else if(x==5){
        this.day='Fri';
      }
      else if(x==6){
        this.day='Sat';
      }
    

      
      var con={'day':this.day,'date':first,'mon':mon};
      
      this.week.push(con);
      this.sliderOne =
      {
        isBeginningSlide: true,
        isEndSlide: false,
        isActive:false,
        week:[]=this.week
      };
    }

  }
  


slideNext(object, slideView) {
  slideView.slideNext(500).then(() => {
    this.checkIfNavDisabled(object, slideView);
  });
}

//Move to previous slide
slidePrev(object, slideView) {
  slideView.slidePrev(500).then(() => {
    this.checkIfNavDisabled(object, slideView);
  });;
}
  SlideDidChange(object, slideView) {
    this.checkIfNavDisabled(object, slideView);
    object.isActive= true;
  }
  checkIfNavDisabled(object, slideView) {
    this.checkisBeginning(object, slideView);
    this.checkisEnd(object, slideView);
  }
 
  checkisBeginning(object, slideView) {
    slideView.isBeginning().then((istrue) => {
      object.isBeginningSlide = istrue;
    });
  }
  checkisEnd(object, slideView) {
    slideView.isEnd().then((istrue) => {
      object.isEndSlide = istrue;
    });
  }

  setDate(item,index){
    //  let x=item.date+' '+this.current.toUTCString().slice(0,3)+this.current.toUTCString().slice(7);
    //  let y=item.date.toString();
    //  this.date=x;
  
    var setMonth = new Array();
    setMonth[1] = "January";
    setMonth[2] = "February";
    setMonth[3] = "March";
    setMonth[4] = "April";
    setMonth[5] = "May";
    setMonth[6] = "June";
    setMonth[7] = "July";
    setMonth[8] = "August";
    setMonth[9] = "September";
    setMonth[10] = "October";
    setMonth[11] = "November";
    setMonth[12] = "December";
  
  this.date=new Date(this.current.getFullYear()+'-' + this.fixDigit(item.mon)+'-'+this.fixDigit(item.date));
     this.date_1=this.current.getFullYear()+'-' + this.fixDigit(item.mon)+'-'+this.fixDigit(item.date);
     this.month=setMonth[item.mon];
      this.currentDate=index;
      console.log("index:",this.currentDate);
     
      this.getAvailableTimeSlots();
   }

  async ionViewWillEnter(){
    this.current=new Date();
    this.currentDate=0;
    // this.date=this.route.snapshot.paramMap.get('date');
    this.name=this.route.snapshot.paramMap.get('name');
    this.pic=this.route.snapshot.paramMap.get('pic');
    
    this.family=this.objService.getExtras();
    let date=new Date(this.current);
    console.log("fam:",date);
    this.date_1=date.getFullYear()+'-' + this.fixDigit(date.getMonth() + 1)+'-'+this.fixDigit(date.getDate());
    this.consumer_id=this.route.snapshot.paramMap.get('cid');
    this.sel_branch=this.route.snapshot.paramMap.get('branch');
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
        const token=await this.storage.get('TOKEN');

        let headers=new HttpHeaders({'branch_id':this.sel_branch.toString(),Authorization:'Bearer '+token});
        // let headers= await this.config.getHeader();
        // let url= this.config.domain_url+'company_details/'+data;
        let url= this.config.domain_url+'show_branch_call_time/'+this.sel_branch;
        let body={company_id:data}
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log("res:",res);
          this.start=res.branch.call_start_time;
          this.end=res.branch.call_end_time;
          this.getAvailableTimeSlots();
          
          // this.slot=this.timeArray(this.start,this.end,30);
        },error=>{
          console.log(error);
          
        })
    //   })
    //   })
    // })
    
    
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/choose-consumer']) ;
    }); 
  
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
  // onChange(dat) {
    
  //   this.date=new Date(dat);

  //   console.log(dat," ",this.date);
    
  //   console.log("date_1:",this.date_1);
  // }

  selectDuration(){
    console.log("dur:",this.duration);
    // this.slot=this.timeArray(this.start,this.end,30);
    this.getAvailableTimeSlots();
    // this.slot=this.timeArray(this.start,this.end,30);

    console.log("slots:",this.slot);
    
  }



  timeArray(start, end,duration){
    var start = start.split(":");
    var end = end.split(":");
duration= parseInt(duration);
    start = parseInt(start[0]) * 60 + parseInt(start[1]);
    end = parseInt(end[0]) * 60 + parseInt(end[1]);
console.log(start,end);

    var result = [];

    for (let time = start; time <= end; time+=duration){
        var slot=this.timeString(time);
        let disable;
       
            // alligator.includes("thick scales");
        // console.log("arraaaa:",this.ar_avl,"sl:",slot)
        // console.log("incl:",this.ar_avl.includes(slot));
        if(this.ar_avl.includes(slot)){
          disable=true;
        }else{
          disable=false;
        }

      // this.not_available.forEach(el => {
        
      
      //   console.log("loop executed");
        
      //   if(el.slots===slot){
      //     disable== true;
      //   }
      //   else{
      //     disable== false;
      //   }
      //   console.log("dis:",disable);
        
      // })
      result.push( {'slot':slot,'disable':disable});
      
    }
      console.log("result:",result);
      
    return result;
}
timeString(time){
  this.hours = Math.floor(time / 60);
  let h=Math.floor(time / 60);
   this.minutes = time % 60;
   if(this.hours>12){
    this.hours=this.hours-12 ;
  }
  if (this.hours < 10) {
     this.hours = "0" + this.hours; //optional
  }
  
  if (this.minutes < 10){
      this.minutes = "0" + this.minutes;
  }

if(h>=12){
  return this.hours + ":" + this.minutes +' PM';
}else{
  return this.hours + ":" + this.minutes +' AM';
}
}

checkSlot(s){
  // let re;
  return this.not_available.forEach(el=>{
    if(el===s){
      return true;
    }
    else{
      return false;
    }
    // console.log("re:",this.re)
    // return this.re;
  })
  
  // return this.not_available.filter(el=>{
  //   if(el===s){
  //     return true;
  //   }else{
  //     return false;
  //   }
  // })
}

async getAvailableTimeSlots(){
  this.not_available=[];
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('COMPANY_ID');
      const bid=await this.storage.get('BRANCH');
      const token=await this.storage.get('TOKEN');
      let headers=new HttpHeaders({'branch_id':this.sel_branch.toString(),Authorization:'Bearer '+token});
      // let headers= await this.config.getHeader();
      let url= this.config.domain_url+'timeslot_notavailable';
      let d;
      if(this.duration=='30'){
        d=1
      }else{
        d=2
      }
      let body={
        company_id:data,
        date:this.date_1,
        duration:d,
        user_id:this.consumer_id
      }
      console.log("body:",body);
      
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log("res:",res)
        res.data.forEach(element => {
          console.log(element);
          let t={slots:element}
          this.not_available.push(t);
          
          
        });
        this.ar_avl= res.json_data
        // this.not_available.push(res.data));
        console.log("fff:",this.ar_avl)
        this.slot=this.timeArray(this.start,this.end,30);
      },error=>{
        console.log(error);
      })
  //     })
  //   })
  // })
console.log("not:",this.not_available);
// this.ar_avl= this.not_available.map(p => p.slots)



}

//  method to fix month and date in two digits
fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
}

bookcall(item){
let currenttime=this.current.getTime();
let a=item.slot.split(' ');
let t=a[0].split(':');
let time;
if(a[1]=='AM'){
time=a[0]+':00'
}else{
   time=(parseInt(a[0])+12)+':'+t[1]+':00'
}
let x=this.today+'T'+time
let sel_slot=new Date(x);

console.log("x:",x,"slot:",sel_slot.getTime(),"curr:",currenttime);

  if(item.disable){
    this.presentAlert('This time slot is not available.Please select another.');
  }else if(this.date_1==this.today && sel_slot.getTime()<currenttime){
    console.log("current date");
    this.presentAlert('Please choose a valid time slot.');
  }else{
    this.sel_slot=item.slot;
    
    
  }
}
  async presentAlert(mes) {
    const alert = await this.toastCntlr.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'       
    });
    alert.present(); //update
  }

  back(){
    // this.router.navigate(['/choosedate',{consumer:this.consumer_id,name:this.name,pic:this.pic}]) ;
    this.router.navigate(['/choose-consumer']);
  }


  continue(){
    if(this.sel_slot==undefined||this.sel_slot==''||this.sel_slot==null){
      this.presentAlert('Please choose a time slot.');
    }else{
  
      this.objService.setExtras(this.family);
    
      console.log("dura:",this.duration);
      
      this.router.navigate(['/confirm-callbooking',{consumer:this.consumer_id,date:this.date_1,slot:this.sel_slot,duration:this.duration,name:this.name,pic:this.pic,branch:this.sel_branch}])
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { GetObjectService } from '../services/get-object.service';

@Component({
  selector: 'app-choosedate',
  templateUrl: './choosedate.page.html',
  styleUrls: ['./choosedate.page.scss'],
})
export class ChoosedatePage implements OnInit {
  date:Date;
  subscription:Subscription;
  consumer:any;
  family:any=[]
  name:any;
  pic:any;
  constructor(private platform:Platform,private router:Router,private route:ActivatedRoute,private objService:GetObjectService) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.date=new Date();
    this.family=this.objService.getExtras();
    this.consumer=this.route.snapshot.paramMap.get('cid');
    this.name=this.route.snapshot.paramMap.get('name');
    this.pic=this.route.snapshot.paramMap.get('pic');
    console.log("fam:",this.family,"cid:",this.consumer);
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/choose-consumer']) ;
    }); 
  
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
  onChange(dat) {
    let d=dat.slice(3,5)+'/' +dat.slice(0,2)+'/'+ dat.slice(6)
    this.date=new Date(d);

    console.log(dat," ",this.date);
    // this.date_1=this.date.getFullYear()+'-' + this.fixDigit(this.date.getMonth() + 1)+'-'+this.fixDigit(this.date.getDate());
    // console.log("date_1:",this.date_1);
  }

  callbooking(){
    this.objService.setExtras(this.family);
    this.router.navigate(['/callbooking',{date:this.date,consumer:this.consumer,name:this.name,pic:this.pic}])
  }

}

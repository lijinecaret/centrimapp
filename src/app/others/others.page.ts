import { Component, OnInit, SecurityContext } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ModalController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import {  StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Storage } from '@ionic/storage-angular';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';
@Component({
  selector: 'app-others',
  templateUrl: './others.page.html',
  styleUrls: ['./others.page.scss'],
})
export class OthersPage implements OnInit {
  subscription:Subscription;
 
  trustedVideoUrl: SafeResourceUrl;
  vid=[];
  folder:any=[];
  files:any=[];
  v_url:any;
  img:any;
  id:any;
  constructor(private domSanitizer: DomSanitizer,private config:HttpConfigService,private http:HttpClient,private platform:Platform,private router:Router,
    private streamingMedia:StreamingMedia,private iab: InAppBrowser,private storage:Storage,
    private route:ActivatedRoute,private modalCntrl:ModalController) { }

  ngOnInit() {
    
  }
  async ionViewWillEnter() {
    this.id=this.route.snapshot.paramMap.get('id');
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('TOKEN');
        let token=data;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    let headers= await this.config.getHeader()
    this.vid=[];

    let url=this.config.domain_url+'media/'+this.id;
    // let headers= await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log(res);
      
      this.folder=res.media_fldr;
      this.files=res.media_files;
      for(let i of this.files){
        let url;
        let link;
        let i_url;
        let type;
        if(i.url!='null'){
          if((i.url).includes('www.youtube.com')){
          console.log("you",i.url);
          let yurl=(i.url).replace('watch?v=','embed/')+'?enablejsapi=1';
           i_url=yurl;
          this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(yurl);
          type=0;
        // this.v_url=yurl+'?enablejsapi=1'
        //   let img=(i.url).substr(i.url.lastIndexOf('=') + 1);
        //   this.img='http://img.youtube.com/vi/'+img+'/default.jpg'
        // }else{
        //   this.v_url=i.url
        }else{
        
        i_url=i.url;
        this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(i.url);
        type=1;
        }
        // this.trustedVideoUrl = this.domSanitizer.sanitize(SecurityContext.URL,i.url)
        url=this.trustedVideoUrl;
        link='null';

        }else{
          url='null';
          if(i.link!=null){
            type=1;
          link=i.link
          }else{
            link=i.image[0].media_image;
            type=1;
          }
        }
        let video={url:url,link:link,title:i.title,id:i.id,play:i_url,type:type};
        this.vid.push(video);
      }
      
    })
//   })
// })
//     })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/entertainment']) ;
  });
  }

  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    // let listaFrames = document.getElementsByTagName("iframe");
    // for (var index = 0; index < listaFrames.length; index++) {
    //   let iframe = listaFrames[index].contentWindow;
    //   iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    // }
  }
  openFolder(item){
  
    this.router.navigate(['open-media',{flag:8,id:item.id,title:item.title,mid:this.id}])
  }

  read(item){
    console.log("url:",item.url);
  //   if(item.url!='null'){
  //   let options:InAppBrowserOptions ={
  //     location:'no',
  //       hideurlbar:'yes',
  //       zoom:'no'
  //   }
  //   const browser = this.iab.create(item.url,'_blank',options);
  // }
  //  if(item.link!=null){
    let options:InAppBrowserOptions ={
      location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'yes'
    }
    if(item.link.includes('.pdf')){
      this.showpdf(item.link)
  }else{
    const browser = this.iab.create('https://docs.google.com/viewer?url='+item.link+'&embedded=true','_blank',options);
  }
  // }else if(item.url=="null" && item.link==null){
  //   let options:InAppBrowserOptions ={
  //     location:'no',
  //       hideurlbar:'yes',
  //       zoom:'no'
  //   }
  //   const browser = this.iab.create(item.image[0].media_image,'_system',options);
  // }
}
play(item){
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'no'
  }
  screen.orientation.lock('landscape');
  const browser = this.iab.create(item.play,'_blank',options);
  browser.on('exit').subscribe(()=>{
    screen.orientation.unlock();
    screen.orientation.lock('portrait');
  })
}
async showpdf(file){
  const modal = await this.modalCntrl.create({
    component: ViewpdfComponent,
    cssClass:'fullWidthModal',
    componentProps: {
      
      data:file,
      
       
    },
    
    
  });
  return await modal.present();
}
}

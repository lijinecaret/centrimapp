import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { GetObjectService } from '../services/get-object.service';


@Component({
  selector: 'app-choose-consumer',
  templateUrl: './choose-consumer.page.html',
  styleUrls: ['./choose-consumer.page.scss'],
})
export class ChooseConsumerPage implements OnInit {
subscription:Subscription;
contacts:any=[];
family:any=[];
user_id:any;
next:boolean=false;
cid:any;
c_name:any;
c_pic:any;
fam:any=[];
fam_mem:any=[];
sel_con:any;
sel_fam:boolean=false;
sel_branch:any;
  constructor(private storage:Storage,private config:HttpConfigService,private http:HttpClient,private router:Router,
    private platform:Platform,private objService:GetObjectService,private toastCntlr:ToastController) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.sel_con=(-1);
    this.fam=[];
  this.fam_mem=[];
  this.family=[];
  this.contacts=[];
    // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_TYPE');
      const id=await this.storage.get('USER_ID');
        this.user_id=id
      // })
     
      if(data==6){
        this.getFamily();
        
      }else if(data==5){
        this.getContacts();
        
      }
  //   })
  // })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/bookedcall-list']) ;
    }); 
  
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
  async getFamily(){
    // this.storage.ready().then(()=>{
      const uid=await this.storage.get('USER_ID');
        const cid=await this.storage.get('COMPANY_ID');
          const bid=await this.storage.get('BRANCH');
          let url=this.config.domain_url+'showfamily/'+uid;
          let headers=await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log("parent:",res);
          
          res.data.forEach(ele=>{
            ele.family.forEach(element => {
              
              this.contacts.push({parent:element.parent_details[0],share:element.share_details})
            });
          })
         
        })

    //   })
    // })
    //   })
    // })
    console.log("arr:",this.contacts);
    
  }

// get contacts for resident login
async getContacts(){
  this.contacts=[];
  // this.storage.ready().then(()=>{
    const uid=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'showcontacts/'+uid;
      let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log("con:",res);
        res.data.forEach(element => {
          this.contacts.push({'primary':element.is_primary,'rel':element.relation,'user':element.user});
          
        });
        console.log("arr:",this.contacts);
      })

      

  //   })
  // })
  
}
async choose(item,idx){
  console.log("choose:",item)
  if(item.share==0){
    this.presentAlert("You don't have access to this profile right now.")
  }else if(item.parent.status==0){
    this.presentAlert("This resident is inactive.")
  }else{
  this.sel_con=idx;
  this.fam=[];
  this.fam_mem=[];
  this.family=[];
  this.next=true;
  this.cid=item.parent.user.user_id;
  this.c_name=item.parent.user.name;
  this.c_pic=item.parent.user.profile_pic;
  this.sel_branch=item.parent.wing.branch_id;
  let f_url=this.config.domain_url+'bookingConsumer/'+item.parent.user.user_id;
  console.log("url:",f_url);
  
  let headers= await this.config.getHeader();
  this.http.get(f_url,{headers}).subscribe((data:any)=>{
    console.log("fam:",data);
    this.family=[];
    let fam=data.data.contacts;
    fam.forEach(element => {
      if(element.user.user_id!=this.user_id){
        let ele={fam:element,selected:false}
        this.family.push(ele)
      }
      
    });
    
  })
}

}
chooseFam(item){
  if(item.fam.user.status==0){
    this.presentAlert('Inactive family member.')
  }else{
  item.selected=!item.selected;
  this.fam.push({cid:item.fam.user.user_id,name:item.fam.user.name,pic:item.fam.user.profile_pic});
  this.fam_mem=this.fam.reduce((arr,x)=>{
    let exists = !!arr.find(y => y.cid === x.cid);
    if(!exists){
        arr.push(x);
    }else{
      var index = arr.indexOf(x);
      arr.splice(index, 1);
    }
    
    return arr;
}, []);
}
  
  console.log(this.fam_mem);
  // if(this.fam_mem.includes())


}

async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}

  async callbooking(){
    // this.storage.ready().then(()=>{
    
        const bid=await this.storage.get('BRANCH');
    if(this.c_name==undefined || this.c_name==' ' || this.c_name==null){
      this.presentAlert('Please choose a participant.')
    }else{
    this.objService.setExtras(this.fam_mem);
    if(!this.sel_branch){
      this.sel_branch=bid
    }
    this.router.navigate(['/callbooking',{cid:this.cid,name:this.c_name,pic:this.c_pic,branch:this.sel_branch}]);
    }
//   })
// })
  }
  showlist(){
    this.router.navigate(['/bookedcall-list']);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseConsumerPageRoutingModule } from './choose-consumer-routing.module';

import { ChooseConsumerPage } from './choose-consumer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseConsumerPageRoutingModule
  ],
  declarations: [ChooseConsumerPage]
})
export class ChooseConsumerPageModule {}

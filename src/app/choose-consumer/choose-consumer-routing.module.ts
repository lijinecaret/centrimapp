import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseConsumerPage } from './choose-consumer.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseConsumerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseConsumerPageRoutingModule {}

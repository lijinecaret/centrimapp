import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChooseConsumerPage } from './choose-consumer.page';

describe('ChooseConsumerPage', () => {
  let component: ChooseConsumerPage;
  let fixture: ComponentFixture<ChooseConsumerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseConsumerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChooseConsumerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckoutDeclarationPageRoutingModule } from './checkout-declaration-routing.module';

import { CheckoutDeclarationPage } from './checkout-declaration.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckoutDeclarationPageRoutingModule
  ],
  declarations: [CheckoutDeclarationPage]
})
export class CheckoutDeclarationPageModule {}

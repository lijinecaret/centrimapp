import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import momenttz from 'moment-timezone';

@Component({
  selector: 'app-checkout-declaration',
  templateUrl: './checkout-declaration.page.html',
  styleUrls: ['./checkout-declaration.page.scss'],
})
export class CheckoutDeclarationPage implements OnInit {
  custom_fields:any[]=[];
  i_donot_have:any;
  notHaveArray:any[]=[];
  doNotHaveStatus:any;
  decl:any[]=[];
  
  temp:any;
  scale:any='1';
  res:any;
  companian:any;
  test:any;
testStatus:any=0;
tempStatus:any;
minDate=new Date().toISOString();
testDate:any;
testResult:any;
public CollectedData: {QuestionId : number , OptionId : number,not_prefered:number}[];
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,
    private router:Router,private route:ActivatedRoute,private toast:ToastController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    // let d=new Date();
    // this.display_date=moment(d).format('D MMM YYYY | hh:mm A');
    this.i_donot_have=[];
    this.custom_fields=[];
    this.decl=[];
    // this.Data=[];
    this.CollectedData=[];
   
    this.companian=this.route.snapshot.paramMap.get('name');
    // this.storage.ready().then(()=>{
      const test=await this.storage.get('RES-OUT-RECENT-TEST-DETAILS');
        this.test=test;
      // })

      // this.storage.get('DECLARATION_LINK').then(link=>{
      //   this.link=link;
      // })
      
        
        const temp=await this.storage.get('RES-OUT-TEMPARATURE');
          this.tempStatus=temp;
        // });
      const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
          let url=this.config.domain_url+'resident_visitor_settings';
          let headers=await this.config.getHeader();
          this.http.get(url,{headers}).subscribe((res:any)=>{
            console.log(res);
            
            this.doNotHaveStatus=res.checkout_settings.i_donot_have;
            if(this.doNotHaveStatus==1){
              this.i_donot_have= res.checkout_settings.i_donot_have_items.split(/\, +/);
            }
              this.custom_fields=res.checkout_settings.custom_field;
          })
    //     })
    //   })
    // })


    // this.audio.preloadSimple('alert','assets/audio/bell.mp3');
  }


  notHave(ev,item){
    if(ev.currentTarget.checked==true){
        this.notHaveArray.push(item);
    }else{
      if(this.notHaveArray.includes(item)){
        this.notHaveArray.splice(this.notHaveArray.indexOf(item));
      }
    }
  }
  
  
    async checkout(){
      let nothave;
      if(this.notHaveArray.length>0){
        nothave=this.notHaveArray[0];
        this.notHaveArray.forEach(ele=>{
          if(nothave==ele){
  
          }else{
            nothave=nothave+', '+ele
          }
        })
      }
      if(this.CollectedData.length<this.custom_fields.length){
        this.presentToast('Please answer all questions.')
      }else if(this.notHaveArray.length!=this.i_donot_have.length){
        this.presentToast('Please check all the boxes to ensure you do not have any symptoms.')
      }else if(this.CollectedData.some(x=>x.not_prefered==1)){
        // this.warning();
       
        // this.router.navigate(['/health-warning',{name:this.name}]);
      }else if(this.testStatus==1&&(this.testDate==undefined||this.testResult==undefined)){
        this.presentToast('Please provide your COVID-19 test details.');
      }else{
        // this.storage.ready().then(()=>{
          const temp=await this.storage.get('RES-OUT-TEMPARATURE');
            this.tempStatus=temp;
            console.log("temp:",this.tempStatus);
            
         
        if(this.tempStatus==1){
          if(this.temp==undefined || this.temp==''){
              this.presentToast('Please enter your body temperature.')
          }else{
            if(parseInt(this.temp)>38){
              // this.warning();
              
              // this.router.navigate(['/health-warning',{name:this.name}])
            }else{
              if(this.testStatus==1&&this.testResult==1){
                // this.warning();
              }else{
              this.gotonext();
              }
            }
          }
        }else{
          if(this.testStatus==1&&this.testResult==1){
            // this.warning();
          }else{
          this.gotonext();
          }
            //  this.router.navigate(['/res-checkout-confirmation',{res:this.res,temp:this.temp,scale:this.scale,notHave:nothave,qna:JSON.stringify(this.CollectedData),name:this.name,testDate:this.testDate,recent_test:this.testStatus,result:this.testResult}])
      }
  //   })
  // })
      }
    }
  
  
    mark(event,id,prefered){
      let not_prefered;
    if((prefered==0&&event.detail.value==0)||(prefered==1&&event.detail.value==1)||prefered==2){
      not_prefered=0
    }else{
      not_prefered=1
    }
      // console.log("ev:",event);
      if(this.CollectedData.length==0){
      this.CollectedData.push({QuestionId:id,OptionId:event.detail.value,not_prefered:not_prefered});
    }else{
      const targetIdx = this.CollectedData.map(item => item.QuestionId).indexOf(id);
      
      
      
      if(targetIdx>=0)
        {
          this.CollectedData[targetIdx].OptionId =event.detail.value;
          this.CollectedData[targetIdx].not_prefered=not_prefered;
        }
      else{
        this.CollectedData.push({QuestionId:id,OptionId:event.detail.value,not_prefered:not_prefered});
      }
      console.log("marked:",this.CollectedData,targetIdx);
    }
    }
  
    async presentToast(mes){
      const alert = await this.toast.create({
        message: mes,
        duration: 3000,
        cssClass:'toast-mess',
        position:'top'       
      });
      alert.present();
    }
    covidTest(ev){
      if(ev.detail.value=='1'){
        this.testStatus=1
      }else{
        this.testStatus=0;
        this.testResult=0;
      }
  }
  
  covidTestResult(ev){
    if(ev.detail.value=='1'){
      this.testResult=1
    }else{
      this.testResult=0;
    }
  }

  async next(){
    let nothave;
    if(this.notHaveArray.length>0){
      nothave=this.notHaveArray[0];
      this.notHaveArray.forEach(ele=>{
        if(nothave==ele){

        }else{
          nothave=nothave+', '+ele
        }
      })
    }
    if(this.companian=='undefined'){
      this.companian=null;
    }
    // this.storage.ready().then(()=>{
      const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');
          const tz=await this.storage.get('TIMEZONE');
          const img=await this.storage.get('PRO_IMG');
          let qna=[];
            if(this.CollectedData.length>0){
              this.CollectedData.forEach(element => {
                qna.push(element.QuestionId.toString(),element.OptionId.toString());
              });
            }
  
            let test_date;
            if(this.testDate==undefined){
             test_date=null
            }else{
              test_date=momenttz(this.testDate).format('yyyy-MM-DD');
            }
          let testResult;
          if(this.testResult==undefined){
            testResult=null;
          }
          let url=this.config.domain_url+'create_visitor_list';
          let headers=await this.config.getHeader();
            let body={
              visitor_type:'1',
              checkin_checkout:'1',
              name:null,
              phone:null,
              address:null,
              additional_visitor_count:null,
              additional_visitor:null,
              checkin_time:null,
              checkout_time:momenttz().tz(tz).format('yyyy-MM-DD HH:mm:ss'),
              i_donot_have:nothave,
              whom_to_visit:0,
              resident_user_id:this.res,
              companian:this.companian,
              purpose:null,
              body_temparature:this.temp,
              temp_measure:this.scale,
              photo:img,
              signature:null,
              qna:qna,
              recent_test:this.testStatus,
              test_date:test_date,
              test_result:testResult,
              vaccinated:null,
            covid_vaccinated:null,
            via_app:1,
            device_id:3,
            body_temparature_array:null,
            vaccine_status:null,
            vaccine_date:null,
            vaccine :null,
            expiry_date:null,
            proof:null,
            uploaded_by:null,
            uploaded_date:null,
            vaccine_array:null,
            vaccine_date_array:null,
            proof_array:null,
            vaccine_phone_array:null,
            firstdose_date_array:null,
            seconddose_date_array:null,
            vaccine_already_have:null,
            firstdose_date:null,
            seconddose_date:null,
            inf_vaccine_status:null,
            inf_vaccine_date:null,
            inf_vaccine_proof:null,
            inf_vaccine_status_array:null,
            inf_vaccine_date_array:null,
            inf_vaccine_proof_array:null
              
            }
            console.log("body:",body);
            
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log(res);
              this.router.navigate(['/confirm-checkout']);
            })
    //       })
    //     })
    //   })
    // })
  }

  async gotonext(){
    // this.storage.ready().then(()=>{
      const terms=await this.storage.get('RES-OUT-TERMS');
  
        if(terms==0){
          this.next();
        }else{
          let nothave;
     
      if(this.notHaveArray.length>0){
        nothave=this.notHaveArray[0];
        this.notHaveArray.forEach(ele=>{
          if(nothave==ele){
  
          }else{
            nothave=nothave+', '+ele
          }
        })
      }   
      this.router.navigate(['/checkout-vis-guidelines',{name:this.companian}])
        }
        
    //   })
    // })
  }
}

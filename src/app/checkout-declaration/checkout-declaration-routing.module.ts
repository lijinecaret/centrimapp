import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckoutDeclarationPage } from './checkout-declaration.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutDeclarationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckoutDeclarationPageRoutingModule {}

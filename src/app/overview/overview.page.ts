import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PopoverController, Platform, ModalController, IonSlides } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import { OverviewActivityComponent } from '../components/overview-activity/overview-activity.component';
import { MoreCallbookComponent } from '../components/more-callbook/more-callbook.component';
import { ShowVisitorsComponent } from '../components/show-visitors/show-visitors.component';
import { OverviewDiningComponent } from '../components/overview-dining/overview-dining.component';
import { CommentComponent } from '../components/comment/comment.component';
@Component({
  selector: 'app-overview',
  templateUrl: './overview.page.html',
  styleUrls: ['./overview.page.scss'],
})
export class OverviewPage implements OnInit {
  @ViewChild('slideWithNav', {static: false}) slideWithNav: IonSlides;
  currentDate:any=0;
  sliderOne: any;
slidenum:any;
slideOptsOne = {
  initialSlide: 0,
  slidesPerView: this.checkScreen(),
  
};
contentStyle ={top:'70px'};
current:Date=new Date();
week=[];
plat:boolean;
day:string;

  activity:any=[];
  dining:any=[];
  visitors:any=[];
  booking:any=[];
  cid:any;
  subscription:Subscription;
  flag:any;
  res_branch:any;
  offset = 0;
  date:any;
  calendarView:boolean=false;
  options:any;
  expanded1:boolean=true;
  expanded2:boolean=true;
  expanded3:boolean=true;
  expanded4:boolean=true;


  constructor(private router:Router,private route:ActivatedRoute,public popoverController:PopoverController,private http:HttpClient,
    private config:HttpConfigService,private platform:Platform,private storage:Storage,private modalCntlr:ModalController) { }

  ngOnInit() {
    if(this.platform.is('android')){
      this.contentStyle.top='75px';
      this.plat=true;
      // this.actCal.margintop='350px';
    }else if(this.platform.is('ios')){
      this.contentStyle.top='85px';
      this.plat=false;
      // this.actCal.margintop='370px';
    }

    // let m=this.current.getMonth()+1
    // // get current week
    //   for (let i = 0; i <= 6; i++) {
  
    //     // let first = this.current .getTime() - (7 * 24 * 60 * 60 * 1000)
  
    //     let mon=m;
    //     let first = this.current.getDate()+ i;
    //     if(m==1||m==3||m==5||m==7||m==8||m==10||m==12){
    //       if (first>31){
    //         first=first-31;
    //         mon=m+1
            
    //       }
    //     }else if(m==2){
    //       if(first>28){
    //         first=first-28;
    //         mon=m+1
    //       }
    //     }else{
    //       if(first>30){
    //         first=first-30;
    //         mon=m+1
    //       }
    //     }
    //     let x= this.current.getDay()+i;
    //    if(x>6){
    //      x=x-7;
    //    }
    //       if(x==0){
    //       this.day="Sun";
    //     }
    //     else if(x==1){
    //       this.day='Mon';
    //     }
    //     else if(x==2){
    //       this.day='Tue';
    //     }
    //     else if(x==3){
    //       this.day='Wed';
    //     }
    //     else if(x==4){
    //       this.day='Thu';
    //     }
    //     else if(x==5){
    //       this.day='Fri';
    //     }
    //     else if(x==6){
    //       this.day='Sat';
    //     }
      
  
        
    //     var con={'day':this.day,'date':first,'mon':mon};
        
    //     this.week.push(con);


    
      // }
  }
  ionViewWillEnter(){

    this.week=[];
   
    let current=moment().format('DD MMM YYYY');
    for(let i=1;i<=3;i++){
      let date=moment(current).add(-i,'days').format('DD MMM YYYY')
    this.week.push(date)
    }
    this.week.reverse();
    this.week.push(current);
    for(let i=1;i<=3;i++){
      let date=moment(current).add(i,'days').format('DD MMM YYYY')
    this.week.push(date)
    }

    this.currentDate=this.week.indexOf(current)
    this.date=moment().format('DD MMM YYYY');
    console.log('week:',this.week)

        this.sliderOne =
        {
          isBeginningSlide: true,
          isEndSlide: false,
          isActive:false,
          week:[]=this.week
        };
        // this.slideWithNav.slideTo(this.currentDate)
    this.options = {
      canBackwardsSelected: true
    }
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.res_branch=this.route.snapshot.paramMap.get('branch');
    // this.attended=[];
    
    // this.getActivities()
    // this.getAttendedActivities();
    this.getOverview();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/resident-overview',{flag:this.flag}],{replaceUrl:true}) ;
    }); 

  }
  slidesLoaded($event) {
    //move to slide number 2
    $event.target.slideTo(1);
    }

  ionViewWillLeave() { 
    this.subscription.unsubscribe();
   
 }
 



async getOverview(){
 
  const cid=await this.storage.get('COMPANY_ID');
  let url=this.config.domain_url+'load_resident_overview';
  console.log(url);
  const user_type=await this.storage.get('USER_TYPE');
  const uid=await this.storage.get('USER_ID');
  //console.log(uid);
  if(user_type==6)
  {
    var user=this.cid;
  }
  if(user_type==5)
  {
    var user=uid;
  }
  let body={
    date:moment(this.date).format('YYYY-MM-DD'),
    user_id:parseInt(user),
    action_status:0
  }
  
  let headers= await this.config.getHeader();
  console.log('body:',body,this.res_branch)
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
    this.activity=data.data.assigned_activities;
    if(data.data.dining){
      this.dining=data.data.dining.dining
    }
    this.booking=data.data.visit_booking;
    this.visitors=data.data.visitors;
    
    
    console.log("view:",data);
  },error=>{
    console.log(error);
  });
}
previous(){
  this.date=moment(this.date).add(-1,'days').format('DD MMM YYYY');
  this.getOverview();
}
next(){
  this.date=moment(this.date).add(1,'days').format('DD MMM YYYY');
  this.getOverview();
}
showDiningItems(item){
  let it;
  if(item.order_items&&item.order_items.length){
    // item.order_items.forEach(element => {
    //   it=it+element.menu_meta.itemdetails.item_name+','
    // });
    it=item.order_items[0].menu_meta.itemdetails.item_name
  }
  else{
    if(item.additional_order_items&&item.additional_order_items.length){
      it=item.additional_order_items[0].additional_details.item.item_name;
    }
  }
  // if(item.additional_order_items&&item.additional_order_items.length){
  //   item.additional_order_items.forEach(element => {
  //     it=it+element.additional_details.item.item_name+','
  //   });
  // }
  // it=it.substring(0,it.length-1)
  return it;
}
getItemCount(item){
  let it=[];
  if(item.order_items&&item.order_items.length){
    item.order_items.forEach(element => {
      it.push(element.menu_meta.itemdetails.item_name)
    });
    
  }
  
   
  if(item.additional_order_items&&item.additional_order_items.length){
    item.additional_order_items.forEach(element => {
      it.push(element.additional_details.item.item_name)
    });
  }
  if(it.length>1)
  // it=it.substring(0,it.length-1)
  return it.length-1;
}
served(item){
  let s=true
  if(item.order_items&&item.order_items.length){
    item.order_items.map(x => {
      if(x.serving_status==1){
        s=true;
      }
      else if(x.serving_status==0){
        s=false;
      }
      else{
        s=false;
      }
    });
  }
  else
  {
    s=false;
  }
  return s;
}

async showDetails(item){
  const modal = await this.modalCntlr.create({
    component: OverviewActivityComponent,
    cssClass:'activity-details-fw-modal',
    componentProps: {
      
      data:item,
      cid:this.cid
       
    },
    
    
  });
  return await modal.present();
}
async showDiningDetails(item){
  const modal = await this.modalCntlr.create({
    component: OverviewDiningComponent,
    componentProps: {
      
      data:item,
      cid:this.cid
       
    },
    
    
  });
  return await modal.present();
}
// selecting date from calendar
onChange(dat) {
    
   console.log('changed:',this.date)
  // this.date_1=dat.slice(6)+'-' + dat.slice(3,5)+'-'+dat.slice(0,2);
  // let d=dat.slice(3,5)+'/' +dat.slice(0,2)+'/'+ dat.slice(6)
  // this.date=new Date(d);
  // this.date=moment(this.date).format('DD MMM YYYY');
  // this.calendarView=false;
  this.getOverview();
}
openCalendar(){
  
  this.calendarView=!this.calendarView;
  // if(!this.calendarView){
    this.date=moment().format('DD MMM YYYY');
    this.getOverview();
    this.currentDate=0
  // }
}

async showOtherVisitors(item,i){
  let other=item
  if(i==2){
    other.splice(0,1);
  }
  const popover = await this.modalCntlr.create({
    component: MoreCallbookComponent,
    backdropDismiss:true,
    componentProps:{
      data:[],
      other:other,
      call:0
    },
    cssClass:'morepop'
    
    
  });
  return await popover.present();

}
async showVisitors(item){
  const popover = await this.popoverController.create({
    component: ShowVisitorsComponent,
    backdropDismiss:true,
    componentProps:{
      data:item,
      
     
    },
    cssClass:'morepop'
    
    
  });
  return await popover.present();

}

checkScreen(){
  if(window.innerWidth>=700){
      return 6;
  }else{
      return 3;
  }
}
reachedEnd($event) {//add date by slide 1 by one
  $event.target.slideTo(1);
  console.log("event:",$event);
  let i=this.week.length-1;
  let date=moment(this.week[i]).add(1,'days').format('DD MMM YYYY');
  this.currentDate=this.week.indexOf(this.date);
this.week.push(date);
}
reachedStart($event) {//add date by slide 1 by one
  $event.target.slideTo(1);
  let date=moment(this.week[0]).add(-1,'days').format('DD MMM YYYY');
  this.week.unshift(date);
  this.currentDate=this.week.indexOf(this.date);
}
//Move to Next slide
slideNext(object, slideView) {
  // let current=moment().format('DD MMM YYYY');
  let i=this.week.length-1
let date=moment(this.week[i]).add(1,'days').format('DD MMM YYYY');
// if(!this.week.includes(date)){
  if(object.isEndSlide){
    this.currentDate=this.week.indexOf(this.date)
this.week.push(date)
  }
// }
slideView.slideNext(500).then(() => {
  this.checkIfNavDisabled(object, slideView);
});

}

//Move to previous slide
slidePrev(object, slideView) {
  
  let date=moment(this.week[0]).add(-1,'days').format('DD MMM YYYY');
// if(!this.week.includes(date)){
//   console.log('not in array');
  if(object.isBeginningSlide){
    
this.week.unshift(date)
this.currentDate=this.week.indexOf(this.date)
  }
// }
console.log('weekpr:',object,slideView)
slideView.slidePrev(500).then(() => {
  this.checkIfNavDisabled(object, slideView);
});;


}
SlideDidChange(object, slideView) {
  this.checkIfNavDisabled(object, slideView);
  object.isActive= true;
  //this.slidePrev(object,slideView);

}
checkIfNavDisabled(object, slideView) {
  this.checkisBeginning(object, slideView);
  this.checkisEnd(object, slideView);
}

checkisBeginning(object, slideView) {
  slideView.isBeginning().then((istrue) => {
    object.isBeginningSlide = istrue;
  });
}
checkisEnd(object, slideView) {
  slideView.isEnd().then((istrue) => {
    object.isEndSlide = istrue;
  });
}
setDate(item,index){
  // this.date=this.current.getFullYear()+'-' + this.fixDigit(item.mon)+'-'+this.fixDigit(item.date);
  this.date=item
  console.log('changed:',this.date);
  this.currentDate=index;
  this.getOverview();
}
//  method to fix month and date in two digits
fixDigit(val){
  return val.toString().length === 1 ? "0" + val : val;
}
// calendarView(){
//   this.view=!this.view;
 
// }

openList(i){
if(i==1){
  this.expanded1=!this.expanded1;
  this.expanded2=false;
  this.expanded3=false;
  this.expanded4=false;
}else if(i==2){
  this.expanded1=false;
  this.expanded2=!this.expanded2;
  this.expanded3=false;
  this.expanded4=false;
}else if(i==3){
  this.expanded1=false;
  this.expanded2=false;
  this.expanded3=!this.expanded3;
  this.expanded4=false;
}else if(i==4){
  this.expanded1=false;
  this.expanded2=false;
  this.expanded3=false;
  this.expanded4=!this.expanded4;
}
}

async showComment(item){
  const popover = await this.popoverController.create({
    component: CommentComponent,
    backdropDismiss:true,
    componentProps:{
      data:item.comment
    },
    cssClass:'comments-popover'
    
    
  });
  return await popover.present();
}
}

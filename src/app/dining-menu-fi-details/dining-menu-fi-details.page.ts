import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dining-menu-fi-details',
  templateUrl: './dining-menu-fi-details.page.html',
  styleUrls: ['./dining-menu-fi-details.page.scss'],
})
export class DiningMenuFiDetailsPage implements OnInit {

  subscription:Subscription;
  menu:any;
  date:any;
  item:any;
  ser_time:any;
  flag:any;
  constructor(private route:ActivatedRoute,private router:Router,private platform:Platform) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    let menu=this.route.snapshot.paramMap.get('menu')
      this.menu=JSON.parse(menu);
      this.date=this.route.snapshot.paramMap.get('date');
      this.flag=this.route.snapshot.paramMap.get('flag');
      this.ser_time=this.route.snapshot.paramMap.get('ser_time');
    let item=this.route.snapshot.paramMap.get('item');
    this.item=JSON.parse(item)
    console.log('item:',this.item)
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      this.back();
      }); 
      
      }
      ionViewWillLeave() { 
      this.subscription.unsubscribe();
      }
      
        back(){
          this.router.navigate(['/dining-view-menu',{menu:JSON.stringify(this.menu),date:this.date}])
        }
}

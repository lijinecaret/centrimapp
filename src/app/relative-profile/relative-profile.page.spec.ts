import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RelativeProfilePage } from './relative-profile.page';

describe('RelativeProfilePage', () => {
  let component: RelativeProfilePage;
  let fixture: ComponentFixture<RelativeProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelativeProfilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RelativeProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

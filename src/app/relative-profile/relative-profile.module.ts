import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RelativeProfilePageRoutingModule } from './relative-profile-routing.module';

import { RelativeProfilePage } from './relative-profile.page';
import { NotificationcardComponent } from '../components/notificationcard/notificationcard.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RelativeProfilePageRoutingModule
  ],
  declarations: [RelativeProfilePage, NotificationcardComponent]
})
export class RelativeProfilePageModule {}

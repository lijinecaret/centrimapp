import { Component, HostListener, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import { AlertController, ModalController, Platform, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Subscription } from 'rxjs';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { NotificationComponent } from '../components/notification/notification.component';
import { PushNotifications } from '@capacitor/push-notifications';

@Component({
  selector: 'app-relative-profile',
  templateUrl: './relative-profile.page.html',
  styleUrls: ['./relative-profile.page.scss'],
})
export class RelativeProfilePage implements OnInit {
  name:string;
  p_name:string;
  img:any;
  email:any;
  details:any={};
  parent:any=[];
  notifyAll:boolean;
  notify:any=[];
  myStory:any;
  // allStory:any;
  activity:any;
  subStory:any;
  message:any;
  call:any;
  user_type:any;
  subscription:Subscription;
  expanded:boolean;
  status:any;
  my_comment:any;
  visit:any;
  maintenance_comment:any;
  isLoading:boolean=true;
  constructor(private auth: AuthService,private router:Router,public storage:Storage,private platform:Platform,
    public alertController: AlertController,private http:HttpClient,private config:HttpConfigService,private iab:InAppBrowser,
    private modalCntlr:ModalController,private toastCntl:ToastController) { }

  ngOnInit() {
    
    
  }
async ionViewWillEnter(){
  this.isLoading=true;
  console.log('pageload1:',this.isLoading)
  // this.storage.ready().then(()=>{
    const name=await this.storage.get('NAME');
      this.name=name;
      this.p_name=this.name.substr(0,1);
    // });
    const img=await this.storage.get('PRO_IMG');
      this.img=img;
    // });
    const mail=await this.storage.get('EMAIL');
      this.email=mail;
    // });
    // this.storage.get("NOTIFY").then(data=>{
    //   this.notifyAll=data;
    //   if(this.notifyAll==true){
    //       this.expanded=true
    //   }else{
    //     this.expanded=false
    //   }
    //   console.log("notify:",this.notifyAll,"expand:",this.expanded);
    // })
    const data=await this.storage.get('USER_TYPE');
      this.user_type=data;
    // })
  // });
  
  this.getContent();
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/menu']) ;
});  

}

ionViewWillLeave() { 
this.subscription.unsubscribe();
}
async select(ev,notifyAll){
  if(this.isLoading==false){
  if(ev.currentTarget.checked==true){
    // this.expanded=true;
    // this.myStory=1;
    // this.activity=1;
    // this.my_comment=1;
    // this.message=1;
    // this.call=1;
    // this.status=1;
    // // this.storage.set("NOTIFY",true);
    
    this.shownotificationsettings();

  }else{
    this.myStory=0;
    this.expanded=false;
    this.activity=0;
    this.my_comment=0;
    this.message=0;
    this.call=0;
    this.status=0;
    this.visit=0;
    this.maintenance_comment=0
    // this.storage.set("NOTIFY",false);
    this.updateNotificationStatus();
  }
  console.log("notify",ev);
  
  
  }
}
@HostListener('touchstart')
onTouchStart() {
  console.log('pageload:',this.isLoading)
  this.isLoading=false;
}
async logOut(){
  const uid=await this.storage.get('USER_ID');
  const bid=await this.storage.get('BRANCH');
  let headers= await this.config.getHeader();
  // this.auth.logout();
          let update_url=this.config.domain_url+'update_login_info';
        let update_body={
          user_id:uid,
          branch_id:bid,
          logout :1
        }
        this.http.post(update_url,update_body,{headers}).subscribe((res:any)=>{
          console.log('info:',res);
          
        })
        const ch=await this.storage.get('CHECKIN');
        await this.storage.clear();
         this.storage.set("loggedIn",'0');
         this.storage.set("CHECKIN",ch);
        this.storage.set("launched",true);
        console.log('setLoggedinValue:',await this.storage.get("loggedIn"));
        
        // this.deleteuuid();
        this.router.navigateByUrl('/login');
          await PushNotifications.unregister();
}
async presentAlertConfirm() {
  
  const alert = await this.alertController.create({
    header: 'Log out',
    mode:'ios',
    message: 'You will be returned to the login screen.',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Logout',
        handler: async () => {
          const ch=await this.storage.get('CHECKIN')
          if(ch==1){
            this.alert('You are now checked in to the premises.Please checkout before logging out')
          }else{
            this.logOut();
          }
          
        }
      }
    ]
  });

  await alert.present();
}

async alert(mes) {

  const alert = await this.alertController.create({

    message: mes,
    backdropDismiss: true
  });

  await alert.present();
}
 async getContent(){
  this.parent=[];
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'profile/'+data;
      let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log("get:",res,url);
          this.details=res.data;
          this.myStory=this.details.user_detail_notification.my_story;
              this.activity=this.details.user_detail_notification.consumer_activity;
              this.my_comment=this.details.user_detail_notification.my_story_comment;
              // this.subStory=this.details.user_detail_notification.post_subscribed;
              this.message=this.details.user_detail_notification.message;
              this.call=this.details.user_detail_notification.video_call;
              this.visit=this.details.user_detail_notification.visit_booking;
              this.maintenance_comment=this.details.user_detail_notification.maintenance_comment;
              this.status=this.details.user_detail_notification.status;
              console.log(this.myStory,this.activity,this.my_comment,this.message,this.call)
              if(this.details.user_detail_notification.status==1){
                this.notifyAll=true;
                this.expanded=true;
                
              }else{
                this.notifyAll=false;
                this.expanded=false;
              }
          this.details.family.forEach(element=>{
            
                // p_array.push(element.parent_details);
                if(element.parent_details[0].status==1){
                this.parent.push({parent:element.parent_details[0],share:element.share_details});
                }
              })
              // console.log("p:",p_array);
              
              // p_array.forEach(ele=>{
              //   this.parent.push(ele)
              // })
              console.log("parent",this.parent);
              
              // this.notify=[
              //   {'title':'New comment for my story','status':this.details.user_detail_notification.my_story_comment},
              //   {'title':'New comment for all stories','status':this.details.user_detail_notification.all_story_comment},
              //   {'title':'New comment for a story that I subscribed','status':this.details.user_detail_notification.post_subscribed},
              //   {'title':'New message','status':this.details.user_detail_notification.message},
              //   {'title':'Missed call','status':this.details.user_detail_notification.missed_call},
              // ]
              
              
        },error=>{
          console.log(error);
          
        });
    //   });
    // });
}



async updateNotificationStatus(){
  // this.storage.ready().then(()=>{
    
      
    const data=await this.storage.get('USER_ID');
    const role=await this.storage.get('USER_TYPE');
      let url=this.config.domain_url+'profile/'+data;
      let body;
      body={
        my_story:this.myStory,
        my_story_comment:this.my_comment,
       
        message:this.message,
        status:this.status,
        video_call:this.call,
        visit_booking:this.visit,
        maintenance_comment:this.maintenance_comment


      }
      if(role==6){
        body.consumer_activity=this.activity;
      }
      let headers= await this.config.getHeader();
      console.log("body:",body);
        this.http.put(url,body,{headers}).subscribe((res:any)=>{
          console.log("post:",res);
          this.presentAlert('Notification settings updated successfully.')
        },error=>{
          console.log(error);
          
        });
//       });
    
// })
   
}
// reload(){
//   this.ionViewWillEnter();
//   console.log("reload");
  
// }


reset(){
  this.router.navigate(['/reset-password',{flag:1}]);
}



// expandItem(): void {
  
//     this.expanded = !this.expanded;
  
// }

openTerms(){
  
  let options:InAppBrowserOptions ={
    location:'yes',
  hideurlbar:'yes',
  zoom:'no',
  hidenavigationbuttons:'yes'
  }
  // const browser = this.iab.create('https://app.centrim.life/terms-and-conditions','_blank',options);
  const browser=this.iab.create('https://app.centrim.life/legal','_blank',options)

}
openPolicy(){

let options:InAppBrowserOptions ={
  location:'yes',
hideurlbar:'yes',
zoom:'no',
hidenavigationbuttons:'yes'
}
// const browser = this.iab.create('https://www.centrimlife.com.au/privacy-policy.html','_blank',options);
const browser=this.iab.create('https://app.centrim.life/legal','_blank',options)

}

async shownotificationsettings(){
  const not={story:this.myStory,
    act:this.activity,
    com:this.my_comment,
    mes:this.message,
    call:this.call,
    visit:this.visit,
    maintenance_comment:this.maintenance_comment,
    status:this.status}
  const modal = await this.modalCntlr.create({
    component: NotificationComponent,
    cssClass:'notify-modal',
    componentProps: {
      data:not
      
      
       
    },
    
    
    
  });
  modal.onDidDismiss().then((dataReturned)=>{
    console.log(dataReturned)
    if(dataReturned.data){
      this.myStory=dataReturned.data.story;
    this.activity=dataReturned.data.act;
    this.my_comment=dataReturned.data.com;
    this.message=dataReturned.data.mes;
    this.call=dataReturned.data.call;
    this.visit=dataReturned.data.visit;
    this.maintenance_comment=dataReturned.data.maintenance_comment;
    this.status=1;
    this.updateNotificationStatus();
    }
    })
  return await modal.present();
}

async deleteuuid(){
  const uid=await this.storage.get('USER_ID');
  const uuid = await this.storage.get('uuid');
  let url=this.config.domain_url+'delete_devicetoken';
  let headers= await this.config.getHeader();
  let body={
    user_id:uid,
    device_token:uuid
  }
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log('deletetok:',res);
      
    })
}

async presentAlert(mes){
  const alert=await this.toastCntl.create({
    message:mes,
    duration:3000,
    cssClass:'toastStyle',
    position:'top'
  });
  alert.present();
  
}

}

import { Component, ViewChildren, QueryList } from '@angular/core';

import { Platform, IonRouterOutlet, ModalController, AlertController, NavController } from '@ionic/angular';
import { SplashScreen } from '@capacitor/splash-screen';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
// import { FCM } from '@ionic-native/fcm/ngx';
// import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Network } from '@ionic-native//network/ngx';

// import { ReceivecallPage } from './receivecall/receivecall.page';
import { NonetworkComponent } from './components/nonetwork/nonetwork.component';
import { ReceivecallComponent } from './components/receivecall/receivecall.component';
import { HttpConfigService } from './services/http-config.service';
import { HttpClient } from '@angular/common/http';
import { OncallComponent } from './components/oncall/oncall.component';
import { Market } from '@ionic-native/market/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';

import { Location } from '@angular/common';
import { filter, map, mergeMap } from 'rxjs/operators';
// import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { PushNotifications } from '@capacitor/push-notifications';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
 
notification:any={};
title:any;
calltoken:any;
pic:any;
toId:any;
msgcount:any;
receive:any;
call:any;
user_type:any;
currentPage:any;
  constructor(
    private platform: Platform,
   
    private statusBar: StatusBar,
    private storage:Storage,
    private router:Router,
    // private firebase:FirebaseX,
    private network:Network,
    private modalCntlr:ModalController,
    private config:HttpConfigService,
    private http:HttpClient,
    private alertController:AlertController,
    private nav:NavController,
    private market:Market,
    private version:AppVersion,
    private route:ActivatedRoute,
    private loc:Location,
    // private deeplinks:Deeplinks
  ) {

   
    
    // this.storage.ready().then(()=>{
      
    //   });
    // })

    this.initializeApp();
    



   



    
  }

  async ngOnInit(){
    const data=await this.storage.get('USER_ID');
    let headers= await this.config.getHeader();
    this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                map(() => this.route),
                map((route) => {
                  console.log('route:',route.snapshot.url)
                    while (route.firstChild) {
                        route = route.firstChild;
                        
                        
                    }
                    // console.log("1:",route.routeConfig.component.name);
                    // this.currentPage=route.routeConfig.component.name;
                    this.currentPage=this.loc.path();
                    console.log("1:",this.currentPage);
                    if(this.currentPage=='/message' ){
                      // this.storage.ready().then(()=>{
                        
                      let m_url=this.config.domain_url+'msgcount';
                     
                      let body={
                        user_one:data
                      }
                      this.http.post(m_url,body,{headers}).subscribe((mes:any)=>{
                        
                        this.msgcount=mes.data.msgcount;
                
                        console.log("mes:",mes );
                        
                
                      })
                  //   })
                  // })
                    }
                    return route;
                }),
                mergeMap((route) => route.data)
            )
            .subscribe(
                (routeData) => {
                    console.log(routeData);
                }
            );
  }

  async initializeApp() {
   
      this.statusBar.styleDefault();
      // this.statusBar.overlaysWebView(true);
      // this.statusBar.styleBlackTranslucent();
      this.statusBar.backgroundColorByHexString("#0077b5");
      //this.statusBar.overlaysWebView(false);
      await SplashScreen.show({
        autoHide: false,
      });
      SplashScreen.hide();
      // this.statusBar.hide();
      // this.initDeeplinks();
      
  
      await this.storage.create();


      const res=await this.storage.get("loggedIn");
      console.log('loggedInValue:',res);
      
      this.storage.set('USERLOG',1);
      if(res==0||res==null){
        this.router.navigateByUrl('/login');
      }
      else if(res==1){
      const id=await this.storage.get('USER_TYPE');
        this.router.navigateByUrl('/menu');
    
      //     this.platform.ready().then(()=>{
      //       if(this.platform.is('ios')||this.platform.is('ipad')||this.platform.is('iphone')){
      //         this.firebase.hasPermission().then((data)=>{
      //           if(!data)
      //           this.firebase.grantPermission().then(()=>{
                 
      //           })
      //         });
              
                
      //         }
      //     this.firebase.onMessageReceived().subscribe(data=>{
      //     console.log("notifi",data);
      //     this.notification=data;
      
      //   if(data){
      //     if(data.tap=='background'){
      //       console.log('tapped:',data)
      //       // if(data.type=='2'){
      //       //   this.title=data.name;
      //       //   this.calltoken=data.calltoken;
      //       //   this.pic=data.profile_pic;
      //       //   this.toId=data.fromuser;
      //       //   let callStatus=data.callstatus;
      //       //   // this.storage.ready().then(()=>{
      //       //   //   const data=await this.storage.get('USER_ID');
      //       //       if(callStatus=='1'){
      //       //     this.receivecall();
      //       //       }
      //       //       else if(callStatus='2'){
      //       //         this.onCall();
      //       //       }
      //       //       else if(callStatus='3'){
      //       //         this.call.dismiss();
      //       //         this.modalCntlr.dismiss();
      //       //       }
              
      //       //   //   })
      //       //   // });
      //       // } else {
      //         if(data.not_type=='1' ||data.not_type=='2' ||data.not_type=='3' || data.not_type=='5'|| data.not_type=='14' ){
      //           // this.router.navigate(['/story-details',{post_id:data.link_id}])
      //           this.nav.navigateRoot(['/story-details',{post_id:data.link_id}]);
      //         }
      //         else if(data.not_type=='4' ||data.not_type=='6'){
      //           // this.router.navigate(['/activity-details',{act_id:data.link_id}]);
      //           this.nav.navigateRoot(['/activity-details',{act_id:data.link_id}]);
      //         }
      //         else if(data.not_type=='7'){
      //           // this.router.navigate(['/activity']);
      //           this.nav.navigateRoot(['/activity-details',{act_id:data.link_id}]);
      //         }
      //         else if(data.not_type=='8'){
      //           // this.router.navigate(['/message']);
      //           this.nav.navigateRoot(['/message']);
      //         }
      //         else if(data.not_type=='9'){
      //           // this.router.navigate(['/message']);
      //           this.nav.navigateRoot(['/bookedcall-list']);
      //         }
      //         else if(data.not_type=='11'){
      //           this.nav.navigateRoot(['/newsletter'])
      //         }
      //         else if(data.not_type=='12'||data.not_type=='13'){
                
      //           if(id==4){
      //             this.nav.navigateRoot(['/maintenance-details',{filter:0,req_id:data.link_id}]);
      //           }else{
      //             this.nav.navigateRoot(['/consumer-maintenance-details',{req_id:data.link_id}])
      //           }
                
      //         }
      //         else if(data.not_type=='20'){
      //           this.nav.navigateRoot(['/poll'])
      //         }
      //       }
      //     }
    
      //     // }else{
      //     //   if(data.type=='2'){
      //     //     this.title=data.name;
      //     //     this.calltoken=data.calltoken;
      //     //     this.pic=data.profile_pic;
      //     //     this.toId=data.fromuser;
      //     //     let callStatus=data.callstatus;
              
      //     //         if(callStatus=='1'){
      //     //       this.receivecall();
      //     //         }
      //     //         else if(callStatus='2'){
      //     //           this.onCall();
      //     //         }
      //     //         else if(callStatus='3'){
      //     //           this.call.dismiss();
      //     //           this.modalCntlr.dismiss();
      //     //         }
              
              
      //     //   } 
      //     // }
          
      //     console.log("notification:",this.notification);
      //   });
      // })
      // const registerNotifications = async () => {
        let permStatus = await PushNotifications.checkPermissions();
      
        if (permStatus.receive === 'prompt') {
          permStatus = await PushNotifications.requestPermissions();
        }
      
        if (permStatus.receive !== 'granted') {
          throw new Error('User denied permissions!');
        }
      
        await PushNotifications.register();
      // }

      // const addListeners = async () => {
        await PushNotifications.addListener('registration', token => {
          console.log('Registration token: ', token);
          this.storage.set("uuid",token.value);
        });

        await PushNotifications.addListener('pushNotificationActionPerformed', notification => {
          console.log('Push notification action performed', notification.actionId, notification.inputValue);
          const data=notification.notification.data;
          if(data.not_type==1 ||data.not_type==2 ||data.not_type==3 || data.not_type==5|| data.not_type==14 ){
                      // this.router.navigate(['/story-details',{post_id:data.link_id}])
                      this.nav.navigateRoot(['/story-details',{post_id:data.link_id}]);
                    }
                    else if(data.not_type==4 ||data.not_type==6){
                      // this.router.navigate(['/activity-details',{act_id:data.link_id}]);
                      this.nav.navigateRoot(['/activity-details',{act_id:data.link_id}]);
                    }
                    else if(data.not_type==7){
                      // this.router.navigate(['/activity']);
                      this.nav.navigateRoot(['/activity-details',{act_id:data.link_id}]);
                    }
                    else if(data.not_type==8){
                      // this.router.navigate(['/message']);
                      this.nav.navigateRoot(['/message']);
                    }
                    else if(data.not_type==9){
                      // this.router.navigate(['/message']);
                      this.nav.navigateRoot(['/bookedcall-list']);
                    }else if(data.not_type==21){
                      // this.router.navigate(['/message']);
                      this.nav.navigateRoot(['/visit-booking']);
                    }
                    else if(data.not_type==11){
                      this.nav.navigateRoot(['/newsletter'])
                    }
                    else if(data.not_type==12||data.type==13){
                      
                      if(id==4){
                        this.nav.navigateRoot(['/maintenance-details',{filter:0,req_id:data.link_id}]);
                      }else{
                        this.nav.navigateRoot(['/consumer-maintenance-details',{req_id:data.link_id}])
                      }
                      
                    }
                    else if(data.not_type==20){
                      this.nav.navigateRoot(['/poll'])
                    }
                    else if(data.not_type==17){
                      this.nav.navigateRoot(['/dining-menu'])
                    }
        });

      // }












      const uid=await this.storage.get('USER_ID');
      const bid=await this.storage.get('BRANCH');
      let headers=await this.config.getHeader();
      let update_url=this.config.domain_url+'update_login_info';
      let update_body={
        user_id:uid,
        branch_id:bid,
        
      }
      this.http.post(update_url,update_body,{headers}).subscribe((res:any)=>{
        console.log('info:',res);
        
      })
      // })
      }
    
     
    const type=await this.storage.get('USER_TYPE');
      this.user_type=type;

      this.forceUpdate();


      // this.storage.ready().then(()=>{
        const data=await this.storage.get('USER_ID');
      let m_url=this.config.domain_url+'msgcount';
      let headers= await this.config.getHeader();
      let body={
        user_one:data
      }
      this.http.post(m_url,body,{headers}).subscribe((mes:any)=>{
        
        this.msgcount=mes.data.msgcount;

        console.log("mes:",mes );
        mes.user.forEach(element => {
          this.storage.set("PRO_IMG",element.profile_pic)
          console.log("mes:",element.profile_pic );
        });

      })
  //   })
  // })


     
      
      
  
    // })
    
    
    

    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log("dissconnect")
      this.openModal();
    });

    
  }


  // backButtonEvent() {
  //   document.addEventListener("backbutton", () => {
  //   this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
  //   if (outlet && outlet.canGoBack()) {
  //   outlet.pop();
  //   }else if(this.router.url === "stories"){
  //   navigator['app'].exitApp(); // work for ionic 4
  //   } else if (this.router.url === "/") {
  //   navigator['app'].exitApp(); // work for ionic 4
  //   }
  //   });
  //   });
  //   }
  

  async openModal() {
    const modal = await this.modalCntlr.create({
      component: NonetworkComponent,
      componentProps: {
        
        
      }
    });
    return await modal.present();
  }
async receivecall(){
   this.receive = await this.modalCntlr.create({
    component: ReceivecallComponent,
    componentProps: {
      title:this.title,
        token:this.calltoken,
        pic:this.pic,
        toId:this.toId
    }
  });
  return await this.receive.present();
}

async onCall(){
  this.call = await this.modalCntlr.create({
    component: OncallComponent,
    componentProps: {
      flag:2,
      title:this.title,
        token:this.calltoken,
       
    }
  });
  return await this.call.present();
}

// forceUpdate(){
//   this.storage.ready().then(()=>{
//     this.storage.get("COMPANY_ID").then(res=>{
            
//   let url=this.config.domain_url+'force_update';
//   let headers= await this.config.getHeader();
//   let body={company_id:res}
  
//   this.http.post(url,body,{headers}).subscribe((res:any)=>{
//     console.log("force:",res.data)
//     this.storage.set('version',res.data[0].version_code_current);
    
//       this.version.getVersionNumber().then(value=>{


//         if(this.platform.is('android')){
//           console.log("android:",value);
          
//         if(res.data[0].version_code_current!=value){
//           if(res.data[0].force_update=2){
//           this.showForceUpdateAlert();
//           }else if(res.data[0].force_update=1){
//             this.showUpdateAlert();
//           }
//         }
//         }else{
//           if(res.data[0].version_code_current_ios!=value){
//             if(res.data[0].force_update=2){
//             this.showForceUpdateAlert();
//             }else if(res.data[0].force_update=1){
//               this.showUpdateAlert();
//             }
//           }
//         }
//       //   if(res.data[0].version_code_current!=value){
//       //     if(res.data[0].force_update=2){
//       //     this.showForceUpdateAlert();
//       //     }else if(res.data[0].force_update=1){
//       //       this.showUpdateAlert();
//       //     }
//       //     console.log("version:",value);
         
       
//       // }
//       })
        
    
//   })
// })
//   })

// }



async forceUpdate(){
  // this.storage.ready().then(()=>{
    const cid=await this.storage.get('COMPANY_ID');
    let headers= await this.config.getHeader();
    this.platform.ready().then(()=>{
    
      this.version.getVersionNumber().then(value=>{
        let url=this.config.domain_url+'all_force_update';
        
        let body;
        let id;
        let b;
        if(this.platform.is('android')){
          id="com.ecaret.centrimlife";
          b=1;
          body={
            company_id:cid,
            consumer_app:value,
            android:1
          }
        }else{
          id='1522607204';
          b=2;
          body={
            company_id:cid,
            consumer_app:value,
            ios:1
          }
        }
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log("force:",res,body,value);
              if(res.data.consumer_app_fu==0){
                this.showForceUpdateAlert(id,b);
              }

        })
      })
  //   })
  })
}

async showForceUpdateAlert(id,b){
  let buttonText;
    if(b==1){
      buttonText='GO TO PLAYSTORE'
    }else{
      buttonText='GO TO APPSTORE'
    }
    const alert = await this.alertController.create({
      mode:'ios',
      header: 'Update Now!',
      cssClass: 'updateAlert',
      message: '<p>It seems you are using an old version of the application and there have been a few updates since.</p><p>It is recommended you update as soon as you can to benefit from all of the new features, improvements and bug fixes!</p>',
      buttons: [
        {
          text: buttonText,
        
        // cssClass: 'secondary',
        handler: () => {
          this.market.open(id);
          navigator['app'].exitApp();
        }
      }
    ],
    backdropDismiss:false
  });

  await alert.present();
}
async showUpdateAlert(){
  const alert = await this.alertController.create({
    header: 'New version available',
    message: 'There is a newer version available for download! Please update the app to continue. ',
    buttons: [
      {
        text: 'CANCEL',
        
        // cssClass: 'secondary',
        handler: () => {
          this.alertController.dismiss();
        }
      },
      {
        text: 'UPDATE',
        
        // cssClass: 'secondary',
        handler: () => {
          this.market.open('com.ecaret.centrimlife');
          navigator['app'].exitApp();
        }
      }
    ],
    backdropDismiss:true
  });

  await alert.present();
}
// initDeeplinks() {
 
//   this.storage.ready().then(()=>{
//       this.storage.get("loggedIn").then(res=>{
//   this.deeplinks.route({ '/maintenance-details/': 'maintenance',
// '/story/':'stories' }).subscribe(
//   match => {
//     console.log('deeplinks:',match);
//   const path = `/${match.$route}/${match.$args['slug']}`;
//   console.log('route:',match.$link.path);
//   if(res==0){
//     this.router.navigateByUrl('/');
//   }
//   else{
//   if(match.$link.path.includes('maintenance-details')){
//     this.nav.navigateRoot(['/maintenance']);
//   }
//   if(match.$link.path.includes('story')){
//     this.nav.navigateRoot(['/stories']);
//   }
//   // Run the navigation in the Angular zone
//   // this.zone.run(() => {
//   // this.router.navigateByUrl(path);
//   // });
// }
//   },
//   nomatch => {
//   console.log("Deeplink that didn't match", nomatch);
//   console.log('route:',nomatch.$link.path,res);
//   if(res==0){
//     this.router.navigateByUrl('/');
//   }
//   else{
//   if(nomatch.$link.path.includes('maintenance-details')){
//     this.nav.navigateRoot(['/maintenance']);
//   }
//   if(nomatch.$link.path.includes('story')){
//     this.nav.navigateRoot(['/stories']);
//   }

// }
//   });

// });
//   })
  
  // }
}

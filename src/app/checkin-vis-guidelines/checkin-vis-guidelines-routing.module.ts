import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckinVisGuidelinesPage } from './checkin-vis-guidelines.page';

const routes: Routes = [
  {
    path: '',
    component: CheckinVisGuidelinesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckinVisGuidelinesPageRoutingModule {}

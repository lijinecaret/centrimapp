import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import momenttz from 'moment-timezone';
import moment from 'moment';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-checkin-vis-guidelines',
  templateUrl: './checkin-vis-guidelines.page.html',
  styleUrls: ['./checkin-vis-guidelines.page.scss'],
})
export class CheckinVisGuidelinesPage implements OnInit {
  terms:any;
  date:any;
  more:any;
  res:any;
temp:any;
scale:any=1;
notHave:any;
qna:any;
purpose:any;
agreed_or_not:boolean=true;
count:any;
recent_test:any;
testResult:any;
test_date:any;
flag:any;
onPremises:any[]=[];
temp_array:any;
vacStatus:any;
vacDate:any;
expDate:any;
vaccine:any;

subscription:Subscription;
user:any;
from:any;
disable:boolean=false;
loaded:boolean=false;
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,
    private route:ActivatedRoute,private router:Router,private toast:ToastController,
    private platform:Platform) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.disable=false;
    this.more=JSON.parse(this.route.snapshot.paramMap.get('more'));
    this.res=this.route.snapshot.paramMap.get('res_id');
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.temp=this.route.snapshot.paramMap.get('temp');
    this.scale=this.route.snapshot.paramMap.get('scale');
    this.from=this.route.snapshot.paramMap.get('from');
    this.notHave=this.route.snapshot.paramMap.get('notHave');
    this.user=JSON.parse(this.route.snapshot.paramMap.get('user_info'));
    this.qna=JSON.parse(this.route.snapshot.paramMap.get('qna'));
    this.purpose=this.route.snapshot.paramMap.get('purpose');
    this.count=this.route.snapshot.paramMap.get('count');
    this.recent_test=this.route.snapshot.paramMap.get('recent_test');
    this.test_date=this.route.snapshot.paramMap.get('testDate');
    this.testResult=this.route.snapshot.paramMap.get('result');
    this.onPremises=JSON.parse(this.route.snapshot.paramMap.get('onPremises'));
    this.vacStatus=this.route.snapshot.paramMap.get('vacStatus');
    this.vacDate=this.route.snapshot.paramMap.get('vacDate');
    this.expDate=this.route.snapshot.paramMap.get('expDate');
    this.vaccine=this.route.snapshot.paramMap.get('vaccine');
    this.temp_array=JSON.parse(this.route.snapshot.paramMap.get('multi_temp'));
    // this.storage.ready().then(()=>{
    
     
    
    
        let url=this.config.domain_url+'visitor_settings';
        let headers=await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log("res:",res);
            this.terms=res.data.terms
        })
  //     })
  //   })
  // })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    // this.router.navigate(['/checkin-scan-qr']) ;
    this.back() ;
 
  }); 
}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}
@HostListener('touchstart')
onTouchStart() {
  
  this.loaded=true
}
agree(ev){
  if(this.loaded&&this.terms){
  if(ev.currentTarget.checked==true){
    this.agreed_or_not=true;
  }else{
    this.agreed_or_not=false;
  }
}
}
next(){
  if(this.agreed_or_not){
    this.disable=true;
    this.confirm();
    // this.router.navigate(['/visit-confirmation',{name:this.name,phone:this.phone,address:this.address,img:this.img,more:JSON.stringify(this.more),res:this.res,res_name:this.res_name,vpType:this.vpType,tovisit:this.tovisit,temp:this.temp,scale:this.scale,notHave:this.notHave,qna:JSON.stringify(this.qna),purpose:this.purpose,flag:this.flag,from:this.from,todec:this.todec,count:this.count,testDate:this.test_date,recent_test:this.recent_test,result:this.testResult}])
}else{
  this.presentToast('You must accept our terms & conditions.');
}




  
}

async presentToast(mes){
  const alert = await this.toast.create({
    message: mes,
    duration: 3000,
    cssClass:'toast-mess',
    position:'top'       
  });
  alert.present();
}


async confirm(){


  // this.storage.ready().then(()=>{
    const cid=await this.storage.get('COMPANY_ID');
      const bid=await this.storage.get('BRANCH');
        const tz=await this.storage.get('TIMEZONE');
          const name=await this.storage.get('NAME');
            const ph=await this.storage.get('PHONE');
            const img=await this.storage.get('PRO_IMG');
              let additional=[];
              let phone_array=[];
              let temp_array=[];
              // let count;
              if(this.user){
              if(!this.user.additional_info){
                additional=null;
                phone_array=null;
                temp_array=null;
              }else{
                temp_array=this.user.temp_array
                this.user.additional_info.forEach(element => {
                  additional.push(element.name);
                  let phone = element.phone.substring(1);
                
                  phone_array.push('+61'+phone)
                
                });
              }
            }
          // count=0;
        
        if(this.count=='null' ||this.count=='undefined'){
          this.count=null
        }
        let qna=[];
        console.log("qna:",this.qna);
        if(this.qna=='null'||this.qna==null){
          qna=null
        }else{
        if(this.qna.length>0){
          this.qna.forEach(element => {
            qna.push(element.QuestionId.toString(),element.OptionId.toString());
          });
        }
      }
        
        let test_date;

        if(this.test_date=='undefined'||this.test_date==undefined){
         test_date=null
        }else{
          test_date=moment(this.test_date).format('yyyy-MM-DD');
        }
        if(this.testResult=='undefined'||this.testResult=='null'){
          this.testResult=null;
        }
        if(this.notHave=='undefined'||this.testResult=='null'){
          this.notHave=null;
        }
        if(this.recent_test=='undefined'||this.recent_test=='null'){
          this.recent_test=null;
        }
        if(this.temp=='undefined'||this.temp=='null'){
          this.temp=null
        }
        if(this.scale=='undefined'||this.scale=='null'){
          this.scale=null
        }
        let phone=null;
        if(ph){
        let phn=ph.toString();
        if(phn.charAt( 0 )==='+'){
         phone = phn.substring(1);
        }else{
          phone=phn
        }
      }


        // let vacDate;
        // if(this.vacDate==undefined||this.vacDate=='undefined'){
        //   vacDate=null
        // }else{
        //   vacDate=moment(this.vacDate).format('yyyy-MM-DD');

          
        // }
        // let expDate;
        // if(this.expDate==undefined||this.expDate=='undefined'){
        //   expDate=null
          
        // }else{
        //   expDate=moment(this.expDate).format('yyyy-MM-DD')
        // }
        // if(this.vaccine==undefined||this.vaccine=='undefined'){
        //   this.vaccine=null
        // }
        // if(this.vacStatus==undefined||this.vacStatus=='undefined'||this.vacStatus=='null'||this.vacStatus==null){
        //   this.vacStatus=0
        // }
        // let temp_array={};
        // console.log("tempArray:",this.temp_array);
        // if(this.temp_array=='null'||this.temp_array=='undefined'||this.temp_array==null){
        //   temp_array=null
        // }else{
        //   this.temp=null;
        //   this.temp_array.forEach((element,index )=> {
        //     Object.entries(element).map(([key,value])=>{
        //       // temp_array[key]=value;
        //       // temp_array.push({[key]:value});
        //     temp_array[key]=value;
        //     })
        //   })
        // }
        // let vaccinated=0;
        // let covid_vaccinated=0;
        // if(this.vacStatus==0){
        //   expDate=null;
        //   vacDate=null;
        //   this.vaccine=null;
        //   vaccinated=null;
        //   covid_vaccinated=null

        // }
        // let res_name=(this.res_name.split('-'))[0];
        let vac_status,vac_date,vac,vac_proof,inf_vaccine_status,temp,
          inf_vaccine_date,inf_vaccine_proof,firstDose,secondDose,vaccinated,covidVaccinated,uploaded_date=null;
        let vac_status_array=[];
        let vac_date_array=[];
        let vac_array=[];
        let proof_array=[]
        let vac_dose_array=[];
        let first_array=[];
        let second_array=[];
        
        let already_have=[];
        
        let inf_vaccine_status_array=[];
        let inf_vaccine_date_array=[];
        let inf_vaccine_proof_array=[];
        
    
    
  if(this.user){
    temp=this.user.temp
        if(this.user.vaccine){
          vac_status=this.user.vaccine.vac_status;
          vac_date=this.user.vaccine.vac_date;
          vac_proof=this.user.vaccine.proof;
          vac=this.user.vaccine.id;
          firstDose=this.user.vaccine.firstDose;
          secondDose=this.user.vaccine.secondDose;
          inf_vaccine_status=this.user.vaccine.inf_vaccine_status
          inf_vaccine_date=this.user.vaccine.inf_vaccine_date;
          inf_vaccine_proof=this.user.vaccine.inf_vaccine_proof;
          vaccinated=this.user.vaccine.vaccinated;
          covidVaccinated=this.user.vaccine.covidVaccinated;
          uploaded_date=this.user.vaccine.uploaded_date;
          
        }
        if(this.user.additional&&this.user.additional.length){
          this.user.additional.forEach(element => {
            vac_array.push(element.id);
            // vac_date_array.push(element.vac_date);
            vac_dose_array.push(element.dose);
            proof_array.push(element.proof);
            vac_status_array.push(element.vac_status);
            
          });
        }else{
          vac_status_array=null;
          // vac_date_array=null;
          vac_array=null;
          proof_array=null
          vac_dose_array=null;
        }
        
        if(this.user.firstDoseArray){      
          if(this.user.firstDoseArray.length>0){
            first_array=this.user.firstDoseArray
          }else{
            first_array=null
          }
        }else{
          first_array=null
        }
          if(this.user.secondDoseArray){
          if(this.user.secondDoseArray.length>0){
            second_array=this.user.secondDoseArray
          }else{
            second_array=null
          }
        }else{
          second_array=null
        }
         
        if(this.user.vaccine_date_array){
          if(this.user.vaccine_date_array.length>0){
            vac_date_array=this.user.vaccine_date_array
          }else{
            vac_date_array=null
          }
        }else{
          vac_date_array=null
        }
        //   if(this.user.vaccine_phone){
        //   if(this.user.vaccine_phone.length>0){
        //     phone_array=this.user.vaccine_phone
        //   }else{
        //     phone_array=null
        //   }
        // }else{
        //   phone_array=null
        // }
        if(this.user.inf_vaccine_status_array){
          if(this.user.inf_vaccine_status_array.length>0){
            inf_vaccine_status_array=this.user.inf_vaccine_status_array
          }else{
            inf_vaccine_status_array=null
          }
        }else{
          inf_vaccine_status_array=null
        }
        if(this.user.inf_vaccine_date_array){
          if(this.user.inf_vaccine_date_array.length>0){
            inf_vaccine_date_array=this.user.inf_vaccine_date_array
          }else{
            inf_vaccine_date_array=null
          }
        }else{
          inf_vaccine_date_array=null
        }
        
        if(this.user.inf_vaccine_proof_array){
          if(this.user.inf_vaccine_proof_array.length>0){
            inf_vaccine_proof_array=this.user.inf_vaccine_proof_array
          }else{
            inf_vaccine_proof_array=null
          }
        }else{
          inf_vaccine_proof_array=null
        }
        
        if(this.user.already_have){
          if(this.user.already_have.length>0){
            already_have=this.user.already_have
          }else{
            already_have=null
          }
        }else{
          already_have=null
        }
      }else{
        vac_status_array=null
        vac_date_array=null
         vac_array=null
         proof_array=null
         vac_dose_array=null
         first_array=null
         second_array=null
        
         already_have=null
        
         inf_vaccine_status_array=null
         inf_vaccine_date_array=null
         inf_vaccine_proof_array=null
      }
        let url=this.config.domain_url+'create_visitor_list';
        let headers=await this.config.getHeader();
        let body={
          visitor_type:'0',
          checkin_checkout:'0',
          name:name,
          phone:'+'+phone,
          address:null,
          additional_visitor_count:this.count,
          additional_visitor:additional,
          checkin_time:momenttz().tz(tz).format('yyyy-MM-DD HH:mm:ss'),
          checkout_time:null,
          i_donot_have:this.notHave,
          whom_to_visit:'0',
          resident_user_id:this.res,
          companian:null,
          purpose:null,
          body_temparature:temp,
          
          temp_measure:this.scale,
          photo:img,
          signature:null,
          qna:qna,
          recent_test:this.recent_test,
          test_date:test_date,
          test_result:this.testResult,
          via_app:1,
            visitor_id:null,
            vaccinated:vaccinated,
            covid_vaccinated:covidVaccinated,
            device_id:3,
            body_temparature_array:temp_array,
            
            vaccine_status:vac_status,
            vaccine_date:vac_date,
            vaccine :vac,
            expiry_date:null,
            proof:vac_proof,
            uploaded_by:null,
            uploaded_date:uploaded_date,
            // vaccine_status_array:vac_status_array,
            vaccine_array:vac_array,
            vaccine_date_array:vac_date_array,
            // vaccine_dose_array:vac_dose_array,
            proof_array:proof_array,
            vaccine_phone_array:phone_array,
            firstdose_date_array:first_array,
            seconddose_date_array:second_array,
            vaccine_already_have:already_have,
            firstdose_date:firstDose,
            seconddose_date:secondDose,
            inf_vaccine_status:inf_vaccine_status,
            inf_vaccine_date:inf_vaccine_date,
            inf_vaccine_proof:inf_vaccine_proof,
            inf_vaccine_status_array:inf_vaccine_status_array,
            inf_vaccine_date_array:inf_vaccine_date_array,
            inf_vaccine_proof_array:inf_vaccine_proof_array

          
        }
        console.log("body:",body);
        
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
           console.log(res);
           this.disable=false;
           this.router.navigate(['/confirm-checkin',{ref_id:res.data.reference_id,additional:this.count}])
        },error=>{
          this.disable=false;
        })
  //       })
  //     })
  //   })
  // })


  //   })
  // })
  
  // this.router.navigate(['/digital-badge',{name:this.name,ref_id:this.ref_id,phone:this.phone,img:this.img,res:this.res,checkin:this.checkin,vpType:this.vpType}])
}

back(){
if(this.flag==1){
  this.router.navigate(['/checkin-add-additional-visitors',{res_id:this.res,purpose:this.purpose,onPremises:JSON.stringify(this.onPremises),more:JSON.stringify(this.more),from:this.from,user_info: JSON.stringify(this.user)}]);
}else if(this.flag==8){
  this.router.navigate(['/checkin-additional-visitors',{res_id:this.res,purpose:this.purpose,onPremises:JSON.stringify(this.onPremises),from:this.from,user_info: JSON.stringify(this.user)}])
}else{
  
  this.router.navigate(['/checkin-health-declaration',{more:JSON.stringify(this.more),res_id:this.res,purpose:this.purpose,count:this.count,temp:this.temp,scale:this.scale,notHave:this.notHave,qna:JSON.stringify(this.qna),testDate:this.test_date,recent_test:this.recent_test,result:this.testResult,back:1,onPremises:JSON.stringify(this.onPremises),multi_temp:JSON.stringify(this.temp_array),vacStatus:this.vacStatus,vacDate:this.vacDate,expDate:this.expDate,vaccine:this.vaccine,from:this.from,user_info: JSON.stringify(this.user)}])
}
}
}

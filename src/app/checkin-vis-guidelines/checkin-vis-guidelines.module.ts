import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckinVisGuidelinesPageRoutingModule } from './checkin-vis-guidelines-routing.module';

import { CheckinVisGuidelinesPage } from './checkin-vis-guidelines.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckinVisGuidelinesPageRoutingModule
  ],
  declarations: [CheckinVisGuidelinesPage]
})
export class CheckinVisGuidelinesPageModule {}

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { PopoverController, Platform, ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { OncallComponent } from '../components/oncall/oncall.component';
import { OpenimageComponent } from '../components/openimage/openimage.component';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-consumer-gallery',
  templateUrl: './consumer-gallery.page.html',
  styleUrls: ['./consumer-gallery.page.scss'],
})
export class ConsumerGalleryPage implements OnInit {
cid:any;
media:any=[];
begin:any=0;
finish:any=20;
subscription:Subscription;
flag:any;
  constructor(private router:Router,private route:ActivatedRoute,public popoverController:PopoverController,private http:HttpClient,
    private config:HttpConfigService,private iab:InAppBrowser,private orient:ScreenOrientation,private platform:Platform,
    private smedia:StreamingMedia,private modalCntrl:ModalController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.media=[];
    this.cid=this.route.snapshot.paramMap.get('cid');
    this.flag=this.route.snapshot.paramMap.get('flag');
    this.getMedia();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/resident-overview',{flag:this.flag}],{replaceUrl:true}) ;
    }); 

  }


  ionViewWillLeave() { 
    this.subscription.unsubscribe();
   
 }

  async getMedia(){
    this.media=[];
    let headers=await this.config.getHeader();
    let url=this.config.domain_url+'user_profile_gallery/'+this.cid;
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log("getMedia:",res);
      this.media=[];
  res.data.reverse().forEach(ele => {
    if(ele.videos.length!=0){
      ele.videos.forEach(element => {
        let url;
        if(element.file==null || element.file==''){
          
      if(element.url.includes('www.youtube.com')){
        url=(element.url).replace('watch?v=','embed/');
       }else if(element.url.includes('youtu.be')){
        let id=(element.url).substr(element.url.lastIndexOf('/') + 1);
        url='https://www.youtube.com/embed/'+id
       }else 
          url=element.url;
        }else{
          url=element.file;
        }
        let item={img:element.thumbnail,url:url,vid:1};
        this.media.push(item);
      });
    }
    if(ele.images.length!=0){
      ele.images.forEach(element => {
        let item={img:element.post_attachment,url:null,vid:0};
        this.media.push(item);
      });
  
    }
  });
      
      console.log("media:",this.media);
      
  
    })
  }
  open(item){
    if(item.vid==1){
      if(item.url.includes('vimeo.com')){
        let options:InAppBrowserOptions ={
          location:'no',
          hideurlbar:'yes',
          zoom:'no'
        }
        screen.orientation.lock('landscape');
          
        const browser = this.iab.create(item.url,'_blank',options);
        browser.on('exit').subscribe(()=>{
          screen.orientation.unlock();
          screen.orientation.lock('portrait');
        })
      }else if((item.url.includes('www.youtube.com'))){
        this.playUtb(item.url)
      }else{
        let options: StreamingVideoOptions = {
          successCallback: () => { console.log('Video played')
  
          
         },
          errorCallback: (e) => { console.log('Error streaming')
          
         },
          
          orientation: 'landscape',
          shouldAutoClose: true,
          controls: true
        };
      
        this.smedia.playVideo(item.url, options);
        }
      
    }else{
      this.openImg(item.img)
    }
  }
  async playUtb(link){
    const modal = await this.modalCntrl.create({
      component: OncallComponent,
      cssClass:"vdoModal",
      componentProps: {
       data:link,
        
          
      }
    });
    modal.onDidDismiss().then(()=>{
      this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
    })
    return await modal.present();
  }
  
  
  async openImg(item) {
    console.log("openDoc",item);
    
      const popover = await this.popoverController.create({
        component: OpenimageComponent,
        backdropDismiss:true,
        componentProps:{
          data:item
        },
        cssClass:'msg_attach'
      });
      return await popover.present();
    }
  viewMore(){
    this.finish +=20;
  }
}

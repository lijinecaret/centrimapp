import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckoutThanksPageRoutingModule } from './checkout-thanks-routing.module';

import { CheckoutThanksPage } from './checkout-thanks.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckoutThanksPageRoutingModule
  ],
  declarations: [CheckoutThanksPage]
})
export class CheckoutThanksPageModule {}

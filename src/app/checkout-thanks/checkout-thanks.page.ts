import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import  momenttz from 'moment-timezone';

@Component({
  selector: 'app-checkout-thanks',
  templateUrl: './checkout-thanks.page.html',
  styleUrls: ['./checkout-thanks.page.scss'],
})
export class CheckoutThanksPage implements OnInit {
name:any;
date:any;
  constructor(private storage:Storage,private router:Router) { }

  ngOnInit() {
  }


  async ionViewWillEnter(){
    // this.storage.ready().then(()=>{
      const name=await this.storage.get('NAME');
        this.name=name;
      // })
      const tz=await this.storage.get('TIMEZONE');
        this.date=momenttz().tz(tz).format('hh:mm A');
      // })
      this.storage.set('CHECKIN',0);
    // })
    setTimeout(() => { 
   
      this.router.navigate(['/menu'])
    
    }, 5000)
  }
}

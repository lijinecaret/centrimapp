import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckoutThanksPage } from './checkout-thanks.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutThanksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckoutThanksPageRoutingModule {}

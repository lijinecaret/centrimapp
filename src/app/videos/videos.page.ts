import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Storage } from '@ionic/storage-angular';



@Component({
  selector: 'app-videos',
  templateUrl: './videos.page.html',
  styleUrls: ['./videos.page.scss'],
})
export class VideosPage implements OnInit {
  subscription:Subscription;
  trustedVideoUrl: SafeResourceUrl;
  v_url:any;
  vid=[];
  folder:any=[];
  files:any=[];
  img:any;
  id:any;
  // array_of_objects = [{vid_link:"https://www.youtube.com/embed/v64KOxKVLVg",title:"360° Underwater National Park | National Geographic"},
  // {vid_link:"https://www.youtube.com/embed/rG4jSz_2HDY",title:"360° Great Hammerhead Shark Encounter | National Geographic"}]
  constructor(private router:Router,private platform:Platform,private http:HttpClient,
    private config:HttpConfigService,private iab: InAppBrowser,private storage:Storage,
    private route:ActivatedRoute) { }

  ngOnInit() {
    
  }
  async ionViewWillEnter() {
    this.id=this.route.snapshot.paramMap.get('id');
    // window.screen.orientation.unlock();
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('TOKEN');
        let token=data;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    let headers= await this.config.getHeader()
    this.vid=[];
    let url=this.config.domain_url+'media/'+this.id;
    // let headers= await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log('res:',res);
      this.folder=res.media_fldr;
      this.files=res.media_files;
   

    for(let i of this.files){
      if((i.url).includes('www.youtube.com')){
        console.log("you",i.url);
        let yurl=(i.url).replace('watch?v=','embed/');
        if(yurl.includes('&')){
          yurl=yurl.substr(0,yurl.indexOf('&'));
         }
        this.v_url=yurl+'?enablejsapi=1';
        let img=(this.v_url).substring((this.v_url).lastIndexOf("/") + 1, (this.v_url).lastIndexOf("?"));
        this.img='http://img.youtube.com/vi/'+img+'/default.jpg'
      }else if((i.url).includes('youtu.be')){
        this.v_url=i.url
        let img=(i.url).substr(i.url.lastIndexOf('/') + 1);
        this.img='http://img.youtube.com/vi/'+img+'/default.jpg'
      } 
      // var myregexp = (i.url).match(/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/gi);
      // var videoID = myregexp[1];
      // console.log("vid_id:",videoID);
      
      // if((i.url).match(myregexp)){
      //   // let yurl=(i.url).replace('watch?v=','embed/');
      //     this.v_url=i.url;
      //     // let img=(i.url).substr(i.url.lastIndexOf('=') + 1);
      //     this.img='http://img.youtube.com/vi/'+videoID+'/default.jpg'
      // }
      
      else{    
        this.v_url=i.url;
        this.img="/assets/entertainment/vimeo-video-thumbnail.svg"; 
        // this.v_url=i.url;
        // let id=(i.url).substr(i.url.lastIndexOf('/') + 1);
        // console.log(this.v_url);
        // let headers=new HttpHeaders({
        //   'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        // })
        // this.http.post('http://vimeo.com/api/v2/video/'+id+'.json',{headers}).subscribe((res:any)=>{
        //   console.log(res);
        //   this.img=res[0].thumbnail_medium
        // })
      }
      
      // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.v_url);
      let video={url:this.v_url,title:i.title,id:i.id,img:this.img};
      this.vid.push(video);
    }
  })
// })
//       })
//     })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/entertainment']) ;
  });  
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    // let listaFrames = document.getElementsByTagName("iframe");
    // for (var index = 0; index < listaFrames.length; index++) {
    //   let iframe = listaFrames[index].contentWindow;
    //   iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    // }
      
      //  let iframe = listaFrames.contentWindow;
      //  iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    }
 
    openFolder(item){
  
      this.router.navigate(['open-media',{flag:1,id:item.id,title:item.title,mid:this.id}])
    }
    play(url){
      // window.open(encodeURI(url),"_system","location=yes");


      // let options: StreamingVideoOptions = {
      //   successCallback: () => { console.log('Video played') },
      //   errorCallback: (e) => { console.log('Error streaming',e) },
      //   orientation: 'landscape',
      //   shouldAutoClose: true,
      //   controls: true
      // };
      
      // this.streamingMedia.playVideo(url, options);
      // this.router.navigate(['/open-media',{url:url,flag:10}]);

      let options:InAppBrowserOptions ={
        location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'no'
      }
      screen.orientation.lock('landscape');
        
      const browser = this.iab.create(url,'_blank',options);
      browser.on('exit').subscribe(()=>{
        screen.orientation.unlock();
        screen.orientation.lock('portrait');
      })
      
     
      
    }




    
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinContinentalFiDetailsPage } from './din-continental-fi-details.page';

describe('DinContinentalFiDetailsPage', () => {
  let component: DinContinentalFiDetailsPage;
  let fixture: ComponentFixture<DinContinentalFiDetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinContinentalFiDetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinContinentalFiDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-din-continental-fi-details',
  templateUrl: './din-continental-fi-details.page.html',
  styleUrls: ['./din-continental-fi-details.page.scss'],
})
export class DinContinentalFiDetailsPage implements OnInit {

  subscription:Subscription;
  menu:any;
  date:any;
  item:any;
  details:any=[];
  ser_time:any;
  ser_time_id:any;
  sub:any;
  constructor(private route:ActivatedRoute,private router:Router,private platform:Platform) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.ser_time=this.route.snapshot.paramMap.get('ser_time')
      this.ser_time_id=this.route.snapshot.paramMap.get('ser_time_id')
    let menu=this.route.snapshot.paramMap.get('menu')
      this.menu=JSON.parse(menu);
      this.date=this.route.snapshot.paramMap.get('date');
      
    let item=this.route.snapshot.paramMap.get('item');
    this.item=JSON.parse(item);
    let sub=this.route.snapshot.paramMap.get('sub');
    this.sub=JSON.parse(sub);
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
      this.back();
      }); 
      
      }
      ionViewWillLeave() { 
      this.subscription.unsubscribe();
      }
      
        back(){
          this.router.navigate(['/din-continental-breakfast',{menu:JSON.stringify(this.menu),date:this.date,item:JSON.stringify(this.item),ser_time:this.ser_time,ser_time_id:this.ser_time_id}])
        }

}

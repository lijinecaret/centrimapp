import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { ModalController, Platform } from '@ionic/angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Subscription } from 'rxjs';
import { Storage } from '@ionic/storage-angular';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';

@Component({
  selector: 'app-subfolderfiles',
  templateUrl: './subfolderfiles.page.html',
  styleUrls: ['./subfolderfiles.page.scss'],
})
export class SubfolderfilesPage implements OnInit {
  res_id:any;
  fl_id:any;
  resource:any=[];
  title:any;
  res_title:any;
  hide:boolean=false;
  terms:any;
  folder:any=[];
  subscription:Subscription;
  constructor(private route:ActivatedRoute,private http:HttpClient,private iab: InAppBrowser,
    private config:HttpConfigService,private router:Router,private platform:Platform,private storage:Storage,
    private dialog:SpinnerDialog,private modalCntlr:ModalController) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.res_id=this.route.snapshot.paramMap.get('res_id');
    this.fl_id=this.route.snapshot.paramMap.get('fl_id');
    this.title=this.route.snapshot.paramMap.get('title');
    this.res_title=this.route.snapshot.paramMap.get('res_title');
    this.getContent();
    this.terms='';
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/resources-details',{res_id:this.res_id,res_title:this.res_title}])
    });  
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    }
  async getContent(){
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('TOKEN');
        let token=data;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    let headers= await this.config.getHeader();
    let url=this.config.domain_url+'resource/'+this.fl_id;
    // let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.resource=res.resource_files;
        this.folder=res.resource_fldr;
       
        
      },error=>{
        console.log(error);
        
      })
//     })
//   })
// })
  }


  openDoc(item){
    if(item.url=="null"){
      // window.open(encodeURI(item.file),"_system","location=yes");
      let options:InAppBrowserOptions ={
        location:'yes',
        hidenavigationbuttons:'yes',
      hideurlbar:'yes',
      zoom:'yes',
      beforeload:'yes',
      clearcache:'yes'
      }
      this.platform.ready().then(() => {
        if(item.file.includes('.pdf')){
            this.showpdf(item.file)
        }else{
          let browser;
          if(this.platform.is('ios')){
            browser = this.iab.create(item.file,'_blank',options);
          }else{
           browser = this.iab.create(encodeURI('https://docs.google.com/gview?embedded=true&url='+item.file),'_blank',options);
          }
    this.dialog.show();

      browser.on('loadstart').subscribe(() => {
    
        this.dialog.hide();   
       
      }, err => {
        this.dialog.hide();
      })
    
      browser.on('loadstop').subscribe(()=>{
        this.dialog.hide();;
      }, err =>{
        this.dialog.hide();
      })
    
      browser.on('loaderror').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
      
      browser.on('exit').subscribe(()=>{
        this.dialog.hide();
      }, err =>{
        this.dialog.hide();
      })
    }
    })
      }else{
        let options:InAppBrowserOptions ={
          location:'yes',
          hidenavigationbuttons:'yes',
    
        hideurlbar:'yes',
        zoom:'no'
        }
        const browser = this.iab.create(item.url,'_blank',options);
      
      }


  }
  back(){
    this.router.navigate(['/resources-details',{res_id:this.res_id,res_title:this.res_title}])
  }
  cancel(){
    this.hide=false;
    this.terms='';
  }
  search(){
      this.hide=true;
      
    }

    async showpdf(file){
      const modal = await this.modalCntlr.create({
        component: ViewpdfComponent,
        cssClass:'fullWidthModal',
        componentProps: {
          
          data:file,
          
           
        },
        
        
      });
      return await modal.present();
    }
}

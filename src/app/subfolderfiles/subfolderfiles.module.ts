import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubfolderfilesPageRoutingModule } from './subfolderfiles-routing.module';

import { SubfolderfilesPage } from './subfolderfiles.page';
import { ApplicationPipesModule } from '../application-pipes.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApplicationPipesModule,
    SubfolderfilesPageRoutingModule,
    
  ],
  declarations: [SubfolderfilesPage]
})
export class SubfolderfilesPageModule {}

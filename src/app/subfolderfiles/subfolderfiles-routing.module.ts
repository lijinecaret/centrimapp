import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubfolderfilesPage } from './subfolderfiles.page';

const routes: Routes = [
  {
    path: '',
    component: SubfolderfilesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubfolderfilesPageRoutingModule {}

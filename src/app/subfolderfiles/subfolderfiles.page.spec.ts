import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SubfolderfilesPage } from './subfolderfiles.page';

describe('SubfolderfilesPage', () => {
  let component: SubfolderfilesPage;
  let fixture: ComponentFixture<SubfolderfilesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubfolderfilesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SubfolderfilesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

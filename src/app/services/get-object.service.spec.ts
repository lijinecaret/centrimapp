import { TestBed } from '@angular/core/testing';

import { GetObjectService } from './get-object.service';

describe('GetObjectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetObjectService = TestBed.get(GetObjectService);
    expect(service).toBeTruthy();
  });
});

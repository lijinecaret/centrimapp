import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Platform, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject, from, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage-angular';
// import { JwtHelperService } from '@auth0/angular-jwt';


// const helper = new JwtHelperService();
const TOKEN_KEY = 'jwt-token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user: Observable<any>;
  private userData = new BehaviorSubject(null);
 
  constructor(private storage: Storage, private http: HttpClient, private plt: Platform, private router: Router,
    private loadingCtrl:LoadingController) { 
    // this.loadStoredToken();  
  }
 
  // loadStoredToken() {
  //   let platformObs = from(this.plt.ready());
 
  //   this.user = platformObs.pipe(
  //     switchMap(() => {
  //       return from(this.storage.get(TOKEN_KEY));
  //     }),
  //     map(token => {
  //       if (token) {
  //         let decoded = helper.decodeToken(token); 
  //         this.userData.next(decoded);
  //         return true;
  //       } else {
  //         return null;
  //       }
  //     })
  //   );
  // }
 
  // login(email: string, pw: string ): Observable<AuthResponse> {
  //   // Normally make a POST request to your APi with your login credentials
  //   // if (credentials.email != 'saimon@devdactic.com' || credentials.pw != '123') {
  //   //   return of(null);
  //   // }
  //   let body={
  //     email:email,
  //     password:pw
  //   }
  //   console.log("body:",body);
  //   return this.http.post('https://cors-anywhere.herokuapp.com/http://13.54.73.136/lifestyle/public/api/login',body).pipe(
  //     take(1),
  //     map((res:AuthResponse) => {
  //       // Extract the JWT, here we just fake it
  //       console.log('token:',res.user1.token);
  //       return res.user1.token;
        
        
  //     }),
  //     switchMap(token => {
  //       let decoded = helper.decodeToken(token);
  //       this.userData.next(decoded);
 
  //       let storageObs = from(this.storage.set(TOKEN_KEY, token));
  //       return storageObs;
  //     })
  //   );
   

    
  // }



  login(email: string, pw: string ): Observable<AuthResponse> {
    // let header={
    //   origin:'origin,x-requested-with'
    // }
    // this.showLoading();
    let body={
          email:email,
          password:pw,
          nw_id:0
        }
        
    return this.http.post('http://13.54.73.136/centrim_api/public/api/auth/login',body).pipe(
      tap(async (res: AuthResponse) => {
        console.log("ogin:",res)
        
        if (res) {
          // this.http.get('http://13.54.73.136/centrim_api/api/branch_details/'+res.data.user_id).subscribe((data:any)=>{
          //   console.log("bran:",data)
          //   this.storage.set("BRANCH",data.data.id);
          // })
          await this.storage.set("TOKEN", res.token);
          await this.storage.set("EXPIRES_IN", res.expires);
          await this.storage.set("SURVEY_CID", res.company_id);
          await this.storage.set("USER_ID", res.data.user_id);
          await this.storage.set("USER_TYPE",res.data.user_type_id);
          await this.storage.set("NAME", res.data.user_name);
          await this.storage.set("EMAIL", res.data.email);
          await this.storage.set("PRO_IMG",res.data.profile_pic);
          await this.storage.set("PHONE",res.data.phone);
          await this.storage.set("COMPANY_ID",res.data.nw_id);
          await this.storage.set("BRANCH_NAME", res.data.branchname);
          await this.storage.set("RESIDENT_ID", res.data.resident_id);
          await this.storage.set("BRANCH",res.data.user_details.branch_id);
          // await this.storage.set("loggedIn",1);
          this.userData.next(true);
          // this.dismissLoader();
        }
      })
    );
    
    
    
  }

 
  // getUser() {
  //   return this.userData.getValue();
  // }
 
  async logout() {
    // this.storage.ready().then(()=>{
      const ch=await this.storage.get('CHECKIN')
         this.storage.clear();
         this.storage.set("loggedIn",0);
         this.storage.set("CHECKIN",ch);
        this.storage.set("launched",true);
        this.userData.next(false);
      // })
    // })
    // await this.storage.remove("TOKEN");
    // await this.storage.remove("EXPIRES_IN");
    
  }

  isLoggedIn() {
    return this.userData.asObservable();
  }



  async showLoading() {
    const loading = await this.loadingCtrl.create({
     
      message: 'Please wait...',
      spinner: 'circles',
      duration:3000
    });
    return await loading.present();
}

  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }


  normalLogin(email: string, pw: string){
    let body={
      email:email,
      password:pw,
      nw_id:0
    }
    this.http.post('http://13.54.73.136/centrim_api/public/api/auth/login',body).subscribe((res:any)=>{
      console.log("ogin:",res);
      this.storage.set("TOKEN", res.token);
           this.storage.set("EXPIRES_IN", res.expires);
           this.storage.set("SURVEY_CID", res.company_id);
           this.storage.set("USER_ID", res.data.user_id);
           this.storage.set("USER_TYPE",res.data.user_type_id);
           this.storage.set("NAME", res.data.user_name);
           this.storage.set("EMAIL", res.data.email);
           this.storage.set("PRO_IMG",res.data.profile_pic);
           this.storage.set("PHONE",res.data.phone);
           this.storage.set("COMPANY_ID",res.data.nw_id);
           this.storage.set("BRANCH_NAME", res.data.branchname);
           this.storage.set("RESIDENT_ID", res.data.resident_id);
           this.storage.set("BRANCH",res.data.user_details.branch_id);
          //  this.storage.set("loggedIn",1);
    })
  }


}





export interface AuthResponse {
  
      data:{
        user_id: any,
        user_name: string,
        email: string,
        profile_pic:string,
        nw_id:any,
        phone:any,
        user_type_id:any,
        branchname: string,
        resident_id:any,
        user_details:{
          branch_id:any
        }
      },
      company_id:any,
      expires:string,
      token: string;
      
  
}
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class HttpConfigService {
  
  

  // base_path = 'https://api.centrim.life/centrim_api/api/story_app';
  // domain_url = 'https://api.centrim.life/centrim_api/api/';
  // login_path = 'https://api.centrim.life/centrim_api/public/api/auth/login';

  // staging links 

  domain_url='http://52.65.155.193/centrim_api/api/';
  base_path='http://52.65.155.193/centrim_api/api/story_app';
  login_path = 'http://52.65.155.193/centrim_api/api/auth/login';
  
  head:any;
  token:string;
  id:any;
  branch:string;
  // feedback_url = 'https://app.smiley.reviews/smileyreviewapp_user_api/index.php/Centrim_Cntrl/';
  feedback_url = 'https://app.personcentredfeedback.com.au/smileyreviewapp_user_api/index.php/Centrim_Cntrl/';

  constructor(private http: HttpClient,private storage:Storage) {
    
 
   console.log("config invoked");
   
    // this.storage.ready().then(()=>{
          // const dat=await this.storage.get('TOKEN');
          //   this.token=dat;
          
          // const da=await this.storage.get('USER_ID');
          //   this.id=da;
        
          //   const data=await this.storage.get('BRANCH');
          //     this.branch=data.toString();
          //     this.head=new HttpHeaders({
          //       'Authorization': 'Bearer'+this.token ,
          //       'Content-Type': 'application/json',
          //       'Accept': 'application/json',
          //       'branch_id':this.branch
          //     });
            // })
    //       })
    //   });
    // });
    
        
 }
 async getHeader(){
  
    const data=await this.storage.get('BRANCH');
    const cid=await this.storage.get('COMPANY_ID');
    const token=await this.storage.get('TOKEN');
              this.branch=data.toString();
              let headers=new HttpHeaders({
                Authorization: 'Bearer '+token ,
                'branch_id':data.toString(),
                'company_id':cid.toString()
              });
 
  return headers


 }
  async getListItems(params,uid,bid) {
    let headers= await this.getHeader();
    let body={
                user_id:uid,
                branch_id:bid
              }
    // let body=`user_id=${uid.toString()}&branch_id=${bid.toString()}`
        console.log(body);
        
     
    return this.http.post(this.base_path + params,body,{headers});
  }

  async getUserPermission(){
   
    const data= await this.storage.get("USER_ID");
     
     let headers=await this.getHeader();
    let url=this.domain_url+'user_module_permissions';
            let body={
              user_id:data,
             
            }
            
            
            console.log(url,body);
           
             return this.http.post(url,body,{headers});
              
         
          
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResidentDirectoryPageRoutingModule } from './resident-directory-routing.module';

import { ResidentDirectoryPage } from './resident-directory.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResidentDirectoryPageRoutingModule
  ],
  declarations: [ResidentDirectoryPage]
})
export class ResidentDirectoryPageModule {}

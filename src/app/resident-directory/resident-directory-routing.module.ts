import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResidentDirectoryPage } from './resident-directory.page';

const routes: Routes = [
  {
    path: '',
    component: ResidentDirectoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResidentDirectoryPageRoutingModule {}

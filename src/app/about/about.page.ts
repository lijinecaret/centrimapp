import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { GetObjectService } from '../services/get-object.service';
import { Subscription } from 'rxjs';
import { LoadingController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
 
  public items: any = [];
  company:any=[];
  name:any;
  subscription:Subscription
  constructor(private router:Router,private config:HttpConfigService,private http:HttpClient,public storage:Storage,
    private objService:GetObjectService,private platform:Platform,private loadingCtrl:LoadingController,
    private route:ActivatedRoute) { 

     

  }

  ngOnInit() {
  }
  ionViewWillEnter(){
   
    
    this.showLoading();
    this.getBranch();    
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/menu']) ;
  }); 

}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}

  gotoDetails(branch){
    // let br=JSON.parse(branch);
   
   
    this.objService.setExtras(branch)
    this.router.navigate(['/facility-details',{com_name:this.name}]);
  }

  expandItem(item): void {
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.items.map(listItem => {
        if (item == listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }
  }
  // fetch company details
  async getBranch(){
    this.items=[];
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('COMPANY_ID');
        let url=this.config.domain_url+'company/'+data;
        let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((data:any)=>{
          this.company=data.companies;
       
          
          
          
          this.company.forEach(ele=>{
            this.name=ele.c_name;
            ele.regions.forEach(element => {
            let exp;
              if(element==ele.regions[0]){
                exp=true
              }else{
                exp=false
              }
              var centre={ expanded: exp ,head:element }
              this.items.push(centre);
                // { expanded: false ,head:'Queensland' },
                
              this.dismissLoader();
            });
          })
          
        })
    //   })
    // })
  }



  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }
}

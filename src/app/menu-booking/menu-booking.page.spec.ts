import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuBookingPage } from './menu-booking.page';

describe('MenuBookingPage', () => {
  let component: MenuBookingPage;
  let fixture: ComponentFixture<MenuBookingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuBookingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuBookingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-menu-booking',
  templateUrl: './menu-booking.page.html',
  styleUrls: ['./menu-booking.page.scss'],
})
export class MenuBookingPage implements OnInit {
  modules:any[]=[];
  subscription:Subscription;
  constructor(private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private router:Router,private platform:Platform) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    // this.storage.ready().then(()=>{
    const cid=await this.storage.get('COMPANY_ID');
    let headers= await this.config.getHeader();
      let murl=this.config.domain_url+'get_modules/'+cid;
      this.http.get(murl,{headers}).subscribe((mod:any)=>{
        this.modules=mod
        console.log("modules:",this.modules);
        
      })
  //   })
  // })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{  
    this.back()
  }); 

}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}
  back(){
    this.router.navigate(['/menu'])
  }
}

import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.page.html',
  styleUrls: ['./movies.page.scss'],
})
export class MoviesPage implements OnInit {
  subscription:Subscription;
  trustedVideoUrl: SafeResourceUrl;
  folder:any=[];
  files:any=[];
  vid=[];
  v_url:any;
  img:any;
  id:any;
  // array_of_objects = [{vid_link:"https://www.youtube.com/embed/PC9RXlJGn60",title:"The Call Of The Wild"}]
  constructor(private platform:Platform,private router:Router,private http:HttpClient,
    private config:HttpConfigService,private streamingMedia:StreamingMedia,private iab: InAppBrowser,private storage:Storage,
    private route:ActivatedRoute) { }
  
  ngOnInit() {
      
  }
  async ionViewWillEnter() {
    this.id=this.route.snapshot.paramMap.get('id');
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('TOKEN');
        let token=data;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    let headers= await this.config.getHeader()
    this.vid=[];
    let url=this.config.domain_url+'media/'+this.id;
    // let headers= await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      this.folder=res.media_fldr;
      this.files=res.media_files;
   

    for(let i of this.files){
      if((i.url).includes('www.youtube.com')){
        console.log("you",i.url);
        let yurl=(i.url).replace('watch?v=','embed/');
        this.v_url=yurl+'?enablejsapi=1'
        let img=(i.url).substr(i.url.lastIndexOf('=') + 1);
        this.img='http://img.youtube.com/vi/'+img+'/1.jpg'
      }else{
        this.v_url=i.url;
        this.img="/assets/entertainment/vimeo-video-thumbnail.svg"
      }
      // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.v_url);
      let video={url:this.v_url,title:i.title,id:i.id,img:this.img};
      this.vid.push(video);


      
    }
  })
// })
//       })
//     })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/entertainment']) ;
});
   
  }

  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    // let listaFrames = document.getElementsByTagName("iframe");
    // for (var index = 0; index < listaFrames.length; index++) {
    //   let iframe = listaFrames[index].contentWindow;
    //   iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    // }
  }
  play(url){
    // window.open(encodeURI(url),"_system","location=yes");

    let options:InAppBrowserOptions ={
      location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'no'
    }
    screen.orientation.lock('landscape');
    const browser = this.iab.create(url,'_blank',options);
    browser.on('exit').subscribe(()=>{
      screen.orientation.unlock();
      screen.orientation.lock('portrait');
    })
    // let options: StreamingVideoOptions = {
    //   successCallback: () => { console.log('Video played') },
    //   errorCallback: (e) => { console.log('Error streaming') },
    //   orientation: 'landscape',
    //   shouldAutoClose: true,
    //   controls: true
    // };
    
    // this.streamingMedia.playVideo(url, options);
  }

openFolder(item){
  
  this.router.navigate(['open-media',{flag:2,id:item.id,title:item.title,mid:this.id}])
}

}

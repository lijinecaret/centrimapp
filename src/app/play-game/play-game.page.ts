import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-play-game',
  templateUrl: './play-game.page.html',
  styleUrls: ['./play-game.page.scss'],
})
export class PlayGamePage implements OnInit {
  subscription:Subscription;
  constructor(private platform:Platform,private router:Router) { 
    
  }
  loadScript () { 
      var script = document.createElement('script'); 
      script.type = 'text/javascript';
      script.src = 'assets/game/2048.js'; 
    document.body.appendChild(script); 
    
  };
  ngOnInit() {
  }
ionViewWillEnter(){
  this.loadScript ();
  
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/games']) ;
  });  
  
  }
  
  ionViewWillLeave() { 
  this.subscription.unsubscribe();
 
}
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckoutFfmemberNamePage } from './checkout-ffmember-name.page';

describe('CheckoutFfmemberNamePage', () => {
  let component: CheckoutFfmemberNamePage;
  let fixture: ComponentFixture<CheckoutFfmemberNamePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutFfmemberNamePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckoutFfmemberNamePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

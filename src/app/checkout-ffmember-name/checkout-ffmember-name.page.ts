import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import momenttz from 'moment-timezone';
// import moment from 'moment';
@Component({
  selector: 'app-checkout-ffmember-name',
  templateUrl: './checkout-ffmember-name.page.html',
  styleUrls: ['./checkout-ffmember-name.page.scss'],
})
export class CheckoutFfmemberNamePage implements OnInit {
name:any;
  constructor(private storage:Storage,private http:HttpClient,private config:HttpConfigService,
    private router:Router) { }

  ngOnInit() {
  }


  async ionViewWillEnter(){
    // this.storage.ready().then(()=>{
      
          let url=this.config.domain_url+'resident_visitor_settings';
          let headers=await this.config.getHeader();
          this.http.get(url,{headers}).subscribe((res:any)=>{
            console.log("res:",res);
            
            this.storage.set("RES-IN-ADDITIONAL-VISITOR-INFO",res.checkin_settings.additional_visitor_info);
            // this.storage.set("RES-IN-ADDRESS-REQUIRED",res.checkin_settings.address_required);
            this.storage.set("RES-IN-HEALTH-DECLARATION",res.checkin_settings.health_declaration);
            // this.storage.set("RES-IN-PHOTO-REQUIRED",res.checkin_settings.photo_required);
            this.storage.set("RES-IN-RECENT-TEST-DETAILS",res.checkin_settings.recent_test_details);
            // this.storage.set("RES-IN-SIGNATURE",res.checkin_settings.signature);
            this.storage.set("RES-IN-TEMPARATURE",res.checkin_settings.temparature);
            this.storage.set("RES-IN-TERMS",res.checkin_settings.terms_conditions);


            this.storage.set("RES-OUT-ADDITIONAL-VISITOR-INFO",res.checkout_settings.additional_visitor_info);
            // this.storage.set("RES-OUT-ADDRESS-REQUIRED",res.checkout_settings.address_required);
            this.storage.set("RES-OUT-HEALTH-DECLARATION",res.checkout_settings.health_declaration);
            // this.storage.set("RES-OUT-PHOTO-REQUIRED",res.checkout_settings.photo_required);
            this.storage.set("RES-OUT-RECENT-TEST-DETAILS",res.checkout_settings.recent_test_details);
            // this.storage.set("RES-OUT-SIGNATURE",res.checkout_settings.signature);
            this.storage.set("RES-OUT-TEMPARATURE",res.checkout_settings.temparature);
            this.storage.set("RES-OUT-TERMS",res.checkout_settings.terms_conditions);


          })
    //     })
    //   })
    // })
  }

  async next(){
    // this.storage.ready().then(()=>{
      const decl=await this.storage.get('RES-OUT-HEALTH-DECLARATION');
        const terms=await this.storage.get('RES-OUT-TERMS');
          const cid=await this.storage.get('COMPANY_ID');
            const bid=await this.storage.get('BRANCH');
              const tz=await this.storage.get('TIMEZONE');
              const img=await this.storage.get('PRO_IMG');
                const id=await this.storage.get('USER_ID');
          if(decl==1){
              this.router.navigate(['/checkout-declaration',{name:this.name}])
          }else if(terms==1){
            this.router.navigate(['/checkout-vis-guidelines',{name:this.name}])
          }else{
            let name;
            if(this.name==undefined||this.name==null){
              name=null
            }else{
              name=this.name
            }
            let url=this.config.base_path+'create_visitor_list';
            let headers=await this.config.getHeader();
        let body={
          visitor_type:'1',
          checkin_checkout:'1',
          name:null,
          phone:null,
          address:null,
          additional_visitor_count:null,
          additional_visitor:null,
          checkin_time:null,
          checkout_time:momenttz().tz(tz).format('yyyy-MM-DD HH:mm:ss'),
          i_donot_have:null,
          whom_to_visit:0,
          resident_user_id:id,
          companian:name,
          purpose:null,
          body_temparature:null,
          temp_measure:null,
          photo:img,
          signature:null,
          qna:null,
          recent_test:null,
          test_date:null,
          test_result:null,

          vaccinated:null,
            covid_vaccinated:null,
            via_app:1,
            device_id:3,
            body_temparature_array:null,
            vaccine_status:null,
            vaccine_date:null,
            vaccine :null,
            expiry_date:null,
            proof:null,
            uploaded_by:null,
            uploaded_date:null,
            vaccine_array:null,
            vaccine_date_array:null,
            proof_array:null,
            vaccine_phone_array:null,
            firstdose_date_array:null,
            seconddose_date_array:null,
            vaccine_already_have:null,
            firstdose_date:null,
            seconddose_date:null,
            inf_vaccine_status:null,
            inf_vaccine_date:null,
            inf_vaccine_proof:null,
            inf_vaccine_status_array:null,
            inf_vaccine_date_array:null,
            inf_vaccine_proof_array:null
          
        }
        console.log("body:",body);
        
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          this.router.navigate(['/confirm-checkout']);
        })
            
          }
        // })
  //       })
  //     })
  //     })
  //   })
  // })
  //   })
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckoutFfmemberNamePageRoutingModule } from './checkout-ffmember-name-routing.module';

import { CheckoutFfmemberNamePage } from './checkout-ffmember-name.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckoutFfmemberNamePageRoutingModule
  ],
  declarations: [CheckoutFfmemberNamePage]
})
export class CheckoutFfmemberNamePageModule {}

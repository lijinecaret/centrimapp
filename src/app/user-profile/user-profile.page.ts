import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PopoverController, Platform, ModalController } from '@ionic/angular';
import { InvitationComponent } from '../invitation/invitation.component'
import { Storage } from '@ionic/storage-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PettycashConsumersComponent } from '../components/pettycash-consumers/pettycash-consumers.component';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { OpenimageComponent } from '../components/openimage/openimage.component';
import { OncallComponent } from '../components/oncall/oncall.component';
import moment from 'moment';
import { Chart } from 'chart.js'; 
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {
  @ViewChild("doughnutCanvas",{static:false}) doughnutCanvas: ElementRef;
  private doughnutChart: Chart;
tabStatus:any=1;
id:any;
user:any={};
room:any;
contacts:any=[];
pending:any=[];
activity:any=[];
attended:any=[];
members:any=[];
media:any=[];
branch:any;
profile_view:boolean;
hideInvite:boolean=false;
subscription:Subscription;
res_id:any;
begin:any=0;
finish:any=20;
offset = 0;

cons_id:any;
current:Date=new Date();
labels:any[]=[];
data:any[]=[];
count:any;
count_30:any;
start:any;
end:any;
no_access:boolean=false;
res_branch:any;
  constructor(public popoverController:PopoverController,private storage:Storage,private http:HttpClient,
    private config:HttpConfigService,private router:Router,private platform:Platform,
    private modalCntrl:ModalController,private iab:InAppBrowser,private orient:ScreenOrientation,
    private smedia:StreamingMedia) { }

  ngOnInit() {
    
  }
  ionViewWillEnter(){
    this.members=[];
    this.media=[];
    this.offset=0;
    this.attended=[];
    this.begin=0;
    this.end=moment(this.current).format('yyyy-MM-DD');
    
    this.start=moment().subtract(1,'months').format('yyyy-MM-DD');
    this.getMembers();
    this.getUser();
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu'],{replaceUrl:true}) ;
    }); 

  }
  back(){
    this.router.navigate(['/menu'],{replaceUrl:true}) ;
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    this.offset=0
 }

  setContent(val){
    this.tabStatus=val
   
    console.log("show:",val);
  }
 // open invitation  
  async invite() {
    console.log('ev:',this.res_id);
    const popover = await this.popoverController.create({
      component: InvitationComponent,
     
      cssClass:'invite_pop',
      componentProps:{
        id:this.res_id
      }
    });
    popover.onDidDismiss().then(()=>{
      this.getUser();
    })
    return await popover.present();
  }

// get details of consumer
async getUser(){
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_TYPE');
     
      const token=await this.storage.get('TOKEN');
        const cid=await this.storage.get('COMPANY_ID');
        
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    
    // this.createChart();
      if(data==5){
        
        this.hideInvite=true;
        this.profile_view=false;
              const dat=await this.storage.get('RESIDENT_ID');
                const id=await this.storage.get('USER_ID');
                this.id=dat;
                this.cons_id=dat;
                this.res_id=id;
                this.media=[];
                this.getMedia(this.id);
                this.getActivities(this.id);
                this.getActivityReport(id);
                let url=this.config.domain_url+'consumer/'+this.id;
                console.log(url);

                let headers= await this.config.getHeader();
                this.http.get(url,{headers}).subscribe((data:any)=>{
                  this.user=data.data.user;
                  this.room=data.data.room;
                  console.log("data:",data);
                  console.log("room",this.room);
                  
                  
                  console.log(this.user);
                  this.contacts=data.data.contacts;
                  this.pending=data.data.pending_contacts;
                 
                  // this.user.forEach(el=>{
                    // this.user.user.forEach(ele=>{
                      // this.attended=this.user.attended_activity;
                      this.branch=data.data.user.company.c_name;
                      console.log("branch:",this.branch);
                    // })
                  // })
                  this.activity=data.user_activity;
                  console.log("act:",this.activity);
                },error=>{
                  console.log(error);
                });
    
              //  });
              // });
            
      }else if(data==6){
        this.members=[];
        // this.getMembers();

        this.profile_view=true;
        // this.storage.ready().then(()=>{
        const id=await this.storage.get('USER_ID');
        
           
          this.id=id;
        
        let url=this.config.domain_url+'myfamily/'+this.id;
        let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log("fam:",res)
          res.data[0].family.forEach(ele => {
            this.members.push({parent:ele.parent_details[0],share:ele.share_details})
          })
          
            let uid=res.data[0].family[0].parent_details[0].id;
            
            let id=res.data[0].family[0].parent_details[0].user_id;
            this.res_branch=res.data[0].family[0].parent_details[0].wing.branch_id;
        this.cons_id=uid;
        if(res.data[0].family[0].share_details==0){
            console.log("no-access");
          }else{
            console.log("access")
        this.getMedia(uid);
        this.getActivities(uid);
        this.getActivityReport(id);
          }
          
          // res.data.forEach(el => {
            


        console.log("firstmem:",this.members)
      
        this.res_id=res.data[0].family[0].parent_details[0].user_id;
        // this.media=[];
        let headers=new HttpHeaders({'branch_id':this.res_branch.toString(),'company_id':cid.toString(),'Authorization': 'Bearer '+token})
        let url=this.config.domain_url+'consumer/'+uid;
        console.log(url);
        // let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((data:any)=>{
          this.user=data.data.user;
          this.room=data.data.room;
            console.log("con:",data);
            
            // this.attended=data.attended_activity;
            this.branch=data.data.user.company.c_name;
            if(res.data[0].family[0].share_details==0){
              console.log("no-access");
            }else{
              console.log("access")
            this.contacts=data.data.contacts;
            this.activity=data.user_activity;
            this.pending=data.data.pending_contacts;
            console.log("att:",this.activity,"bran:",this.branch);
                  this.contacts.forEach(ele=>{
                    if(ele.is_primary==1){
                      let primaryId=ele.contact_user_id;
                      // this.storage.ready().then(()=>{
                      //   this.storage.get("USER_ID").then(usr_id=>{
                          if(this.id==primaryId){
                            this.hideInvite=true;
                          }else{
                            this.hideInvite=false;
                          }
                        }
                      })
                    }
                    })
                  
                  
         });
         
    
        // })
      // })
      // })
    }
//   })
// })
//       })
//     })
//   });
}

// get profiles of all the consumers related to the user
async getMembers(){
  // this.members=[];
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_TYPE');
      const tok=await this.storage.get('TOKEN');
        const cid=await this.storage.get('COMPANY_ID');
        let token=tok;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    // let headers=new HttpHeaders({
    //   'Authorization': 'Bearer'+token ,
    //   'Content-Type': 'application/json',
    //   'Accept': 'application/json',
    //   'branch_id':branch
    // });
      if(data==6){
        this.profile_view=true;
        const id=await this.storage.get('USER_ID');
          this.id=id;
         
        let url=this.config.domain_url+'myfamily/'+this.id;
        let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log("res:",res)
          // res.data.forEach(el => {
            res.data[0].family.forEach(ele => {
              if(ele.is_primary==1){
                let primaryId=ele.contact_user_id;
                // this.storage.ready().then(()=>{
                //   this.storage.get("USER_ID").then(usr_id=>{
                    if(this.id==primaryId){
                      this.hideInvite=true;
                    }else{
                      this.hideInvite=false;
                    }
                  }
              // this.members.push(ele.parent_details[0])
              
            // });
          });
          console.log("mem:",this.members);
          
          
          
        })
      // })
      }else if(data==5){
        this.profile_view=false;
      }
//     })
//   })
// })
//     })
//   });
}

// // get user activity
// getActivity(){
//   this.storage.ready().then(()=>{
//     const data=await this.storage.get('USER_ID');
//       this.id=data;
    
//   let url=this.config.domain_url+'consumer_activities';
//   // let header=this.config.head;
//   let body={
//     date:'daily',
//     date_1:'2020-05-24',
//     user_id:this.id
//   }
//   this.http.post(url,body).subscribe((data:any)=>{
//     this.activity=data.data;
//     console.log(data);
//   },error=>{
//     console.log(error);
    
//   });
// })
//   })
// }


// switch to view consumer profile if more than one is present
async switchProfile(consumerId,resId){
  this.user=[];
  this.contacts=[];
  this.activity=[];
  this.attended=[];
  this.pending=[];
  this.media=[];
  this.offset=0;
  this.res_id=resId;
  this.cons_id=consumerId;
  const token=await this.storage.get('TOKEN');
  // this.getActivities(consumerId);
  // this.storage.ready().then(()=>{
    // const bid=await this.storage.get('BRANCH');
      // let branch=bid.toString();
      let headers=new HttpHeaders({'branch_id':this.res_branch.toString(),'company_id':this.cons_id.toString(),'Authorization': 'Bearer '+token})
    
  let url=this.config.domain_url+'consumer/'+consumerId;
        // let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((data:any)=>{
            this.user=data.data.user;
            console.log("con:",data);
            this.contacts=data.data.contacts;
            this.pending=data.data.pending_contacts;
            // this.attended=data.data.user.attended_activity;
            this.branch=data.data.user.company.c_name;
            this.activity=data.user_activity;
            this.room=data.data.room;
            console.log("att:",this.activity,"bran:",this.branch);
            this.contacts.forEach(ele=>{
              if(ele.is_primary==1){
                let primaryId=ele.contact_user_id;
                // this.storage.ready().then(()=>{
                //   this.storage.get("USER_ID").then(usr_id=>{
                    if(this.id==primaryId){
                      this.hideInvite=true;
                    }else{
                      this.hideInvite=false;
                    }
                  }
                })
            },error=>{
                console.log(error);
         });
        // })
      // })
}
async showConsumer(){
  const modal = await this.modalCntrl.create({
    component: PettycashConsumersComponent,
   
    componentProps: {
      data:this.members
    }
    
  });
  modal.onDidDismiss().then((dataReturned) => {
    console.log("dataRet:",dataReturned);
    if(dataReturned.data!=undefined||dataReturned.data!=null){
    this.no_access=this.no_access;
    this.res_branch=dataReturned.data.parent.wing.branch_id;
      this.switchProfile(dataReturned.data.parent.id,dataReturned.data.parent.id);
      this.getMedia(dataReturned.data.parent.id);
      this.getActivities(dataReturned.data.parent.id);
      this.getActivityReport(dataReturned.data.parent.user_id);
      
      // this.cons_id=dataReturned.data.user_id
      this.finish=20;
      // this.membername=dataReturned.data.user.name;
      // this.memberpic=dataReturned.data.user.profile_pic
    }
  })
  return await modal.present();
}


async getMedia(cid){
  const token=await this.storage.get('TOKEN');
  const id=await this.storage.get('COMPANY_ID');
  let headers=new HttpHeaders({'branch_id':this.res_branch.toString(),'Authorization': 'Bearer '+token ,'company_id':id.toString()})
  this.media=[];
  let url=this.config.domain_url+'user_profile_gallery/'+cid;
  this.http.get(url,{headers}).subscribe((res:any)=>{
    console.log("getMedia:",res);
    this.media=[];
res.data.reverse().forEach(ele => {
  if(ele.videos.length!=0){
    ele.videos.forEach(element => {
      let url;
      if(element.file==null || element.file==''){
        
    if(element.url.includes('www.youtube.com')){
      url=(element.url).replace('watch?v=','embed/');
     }else if(element.url.includes('youtu.be')){
      let id=(element.url).substr(element.url.lastIndexOf('/') + 1);
      url='https://www.youtube.com/embed/'+id
     }else 
        url=element.url;
      }else{
        url=element.file;
      }
      let item={img:element.thumbnail,url:url,vid:1};
      this.media.push(item);
    });
  }
  if(ele.images.length!=0){
    ele.images.forEach(element => {
      let item={img:element.post_attachment,url:null,vid:0};
      this.media.push(item);
    });

  }
});
    
    console.log("media:",this.media);
    

  })
}
open(item){
  if(item.vid==1){
    if(item.url.includes('vimeo.com')){
      let options:InAppBrowserOptions ={
        location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'no'
      }
      screen.orientation.lock('landscape');
        
      const browser = this.iab.create(item.url,'_blank',options);
      browser.on('exit').subscribe(()=>{
        screen.orientation.unlock();
        screen.orientation.lock('portrait');
      })
    }else if((item.url.includes('www.youtube.com'))){
      this.playUtb(item.url)
    }else{
      let options: StreamingVideoOptions = {
        successCallback: () => { console.log('Video played')

        
       },
        errorCallback: (e) => { console.log('Error streaming')
        
       },
        
        orientation: 'landscape',
        shouldAutoClose: true,
        controls: true
      };
    
      this.smedia.playVideo(item.url, options);
      }
    
  }else{
    this.openImg(item.img)
  }
}
async playUtb(link){
  const modal = await this.modalCntrl.create({
    component: OncallComponent,
    cssClass:"vdoModal",
    componentProps: {
     data:link,
      
        
    }
  });
  modal.onDidDismiss().then(()=>{
    this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
  })
  return await modal.present();
}


async openImg(item) {
  console.log("openDoc",item);
  
    const popover = await this.popoverController.create({
      component: OpenimageComponent,
      backdropDismiss:true,
      componentProps:{
        data:item
      },
      cssClass:'msg_attach'
    });
    return await popover.present();
  }
viewMore(){
  this.finish +=20;
}
// createChart(){
//   this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
//     type: "doughnut",
//     data: {
//       labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
//       datasets: [
//         {
//           label: "# of Votes",
//           data: [12, 19, 3, 5, 2, 3],
//           backgroundColor: [
//             "rgba(255, 99, 132, 0.2)",
//             "rgba(54, 162, 235, 0.2)",
//             "rgba(255, 206, 86, 0.2)",
//             "rgba(75, 192, 192, 0.2)",
//             "rgba(153, 102, 255, 0.2)",
//             "rgba(255, 159, 64, 0.2)"
//           ],
//           hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56"]
//         }
//       ]
//     }
//   });
// }


async getActivities(id){
  console.log("offset:",this.offset);
  const token=await this.storage.get('TOKEN');
  const cid=await this.storage.get('COMPANY_ID');
  // this.storage.ready().then(()=>{
    const bid=await this.storage.get('BRANCH');
      let branch=bid.toString();
      let headers=new HttpHeaders({'branch_id':this.res_branch.toString(),'Authorization': 'Bearer '+token ,'company_id':cid.toString()})
    
  let url=this.config.domain_url+'attended_activities';
  let body={
    resident_id:id,
    limit:20,
    offset:this.offset
  }
  // let headers= await this.config.getHeader();
  console.log("actBody:",body);
  
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log("act:",res);
    this.count=res.total_count;
    // if(this.attended.length!=0){
    //   this.attended.forEach(ele=> {
    //     res.attended_activities.forEach(element => {
    //       if(ele.id==element.id){

    //       }else{
    //       this.attended.push(element);
    //       }
    //     });
    //   });
    // }else{
      for(let i in res.attended_activities){
      this.attended.push(res.attended_activities[i]);
      }
    // }
   
    
  })
// })
// })
}

loadMore(){
  this.offset=this.offset+20;
  this.getActivities(this.cons_id);
}

async getActivityReport(id){
  const token=await this.storage.get('TOKEN');
  const cid=await this.storage.get('COMPANY_ID');
  let headers=new HttpHeaders({'branch_id':this.res_branch.toString(),'Authorization': 'Bearer '+token ,'company_id':cid.toString()})
  let url=this.config.domain_url+'consumerAttendReport';
  let body={
    user_id:id,
    start:this.start,
    end:this.end
  }
  this.http.post(url,body,{headers}).subscribe((res:any)=>{
    console.log("actRep:",res,body);
    this.labels=res.categories;
    // this.attended=res.attended;
    // this.declined=res.declined;
    this.data=res.attended;
    this.count_30=this.data.reduce((a, b) => a + b, 0)
    // this.max=Math.max.apply(null,this.attended);
    // console.log("max:",this.max);
    
    this.createChart();
    
  })
  
}

createChart(){
  console.log("label:",this.labels,this.data)
  this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
    type: "doughnut",
    data: {
     
      labels: this.labels,
      datasets: [
        {
          // labels: this.labels,
          data: this.data,
          backgroundColor: [
            "#ed3f36",
            "#ec7b2d",
            "#f1a430",
            "#facf32",
            "#87c341",
            "#56b847",
            "#4cb3d4",
            "#377abf",
            "#834c9d",
            "#ea4a94"
          ],
          hoverBackgroundColor: ["#ed3f36",
          "#ec7b2d",
          "#f1a430",
          "#facf32",
          "#87c341",
          "#56b847",
          "#4cb3d4",
          "#377abf",
          "#834c9d",
          "#ea4a94"]
        }
      ],
      options:{
        animation:{
          animateScale:true,
         
        }
      }
    }
  });
}
}

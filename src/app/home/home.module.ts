import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { HomePage } from './home.page';


  

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage,
        children: [
          {
            path: 'stories',
            loadChildren: () => import('../stories/stories.module').then( m => m.StoriesPageModule)
          },
          {
            path: 'activities',
            loadChildren: () => import('../activities/activities.module').then( m => m.ActivitiesPageModule)
          },
          {
            path: 'menu',
            loadChildren: () => import('../menu/menu.module').then( m => m.MenuPageModule)
          },
          {
            path: 'message',
            loadChildren: () => import('../message/message.module').then( m => m.MessagePageModule)
          },
          {
            path: 'user-profile',
            loadChildren: () => import('../user-profile/user-profile.module').then( m => m.UserProfilePageModule)
          },
          {
            path: '',
            redirectTo: '/home/stories'
          }
        ]

      }
    ])
  ],
  declarations: [HomePage]
})
export class HomePageModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TechnicianNewRequestPageRoutingModule } from './technician-new-request-routing.module';

import { TechnicianNewRequestPage } from './technician-new-request.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TechnicianNewRequestPageRoutingModule
  ],
  declarations: [TechnicianNewRequestPage]
})
export class TechnicianNewRequestPageModule {}

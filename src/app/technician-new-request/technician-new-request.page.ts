import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Storage } from '@ionic/storage-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Platform, LoadingController, ToastController, ActionSheetController } from '@ionic/angular';
import { HttpConfigService } from '../services/http-config.service';
import { Router } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard/ngx';
// import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Camera,CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-technician-new-request',
  templateUrl: './technician-new-request.page.html',
  styleUrls: ['./technician-new-request.page.scss'],
})
export class TechnicianNewRequestPage implements OnInit {
  title:any;
  location:any;
  priority:any="1";
  type:any;
  description:any;
  currentDate:Date;
  due_date:Date;
  image:any=[];
  img:any=[];
  imageResponse:any=[];
  subscription:Subscription;
  other_type:any;
  risk:any="0";
  hazard:any="0";
  out_of_order:any="0";
  keyboardStyle = { width: '100%', height: '0px' };
  typeArray:any=[{id:1,type:'Electrical'},
                 {id:2,type:'Equipment'},
                 {id:3,type:'Garden'},
                 {id:4,type:'Mechanical'},
                 {id:5,type:'Plumbing'},
                 {id:0,type:'Other'}
                 ]
branch:any=[];
bid:any;
  constructor(private http:HttpClient,private storage:Storage,private config:HttpConfigService,private router:Router,
    private platform:Platform,private loadingCtrl:LoadingController,private toast:ToastController,private keyboard:Keyboard,
   private actionsheetCntlr:ActionSheetController,private camera:Camera) { }

  ngOnInit() {
    // this.imagePicker.requestReadPermission();
    this.keyboard.onKeyboardWillShow().subscribe( {
      next: x => {
        this.keyboardStyle.height = x.keyboardHeight + 'px';
      },
      error: e => {
        console.log(e);
      }
    });
    this.keyboard.onKeyboardWillHide().subscribe( {
      next: x => {
        this.keyboardStyle.height = '0px';
        // document.getElementById('desc').scrollIntoView(false);
      },
      error: e => {
        console.log(e);
      }
    });
   

  }

  pushDesc(){
    console.log("push desc");
    
    this.keyboard.onKeyboardDidShow().subscribe(()=>{
      document.getElementById('des').scrollIntoView(true)
    })
  }
  pushTitle(){
    console.log("push desc");
    
    this.keyboard.onKeyboardDidShow().subscribe(()=>{
      document.getElementById('req').scrollIntoView(true)
    })
  }
  pushLoc(){
    console.log("push desc");
    
    this.keyboard.onKeyboardDidShow().subscribe(()=>{
      document.getElementById('loc').scrollIntoView(true)
    })
  }


  ionViewWillEnter(){
    this.img=[];
    this.imageResponse=[];
    this.branch=[];
    this.getBranch();
  }
  async selectImage() {
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.addImage();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage();
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  
  
  pickImage(){
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      
      this.imageResponse.push(base64Image);
      this.img.push(imageData);
       
    }, (err) => {
      // Handle error
    });
  }

  addImage(){
  
    let options;
    options={
      maximumImagesCount: 5,
      outputType: 1
    }
    if(this.platform.is('ios')){
      options.disable_popover=true
    }
      // this.imagePicker.getPictures(options).then((results) => {
      //   for (var i = 0; i < results.length; i++) {
      //       console.log('Image URI: ' + results[i]);
            
      //       if((results[i]==='O')||results[i]==='K'){
      //         console.log("no img");
              
      //         }else{
      //           this.img.push(results[i]);
      //         this.imageResponse.push('data:image/jpeg;base64,' + results[i])
      //         }
      //   }
      // }, (err) => { });
    
    console.log("imagg:",this.img);
    
    }
    removeImg(i){
      this.imageResponse.splice(i,1);
      this.img.splice(i,1);
    }
  async submit(){
    this.showLoading();
  
    // this.storage.ready().then(()=>{
      const uid=await this.storage.get('USER_ID');
        // const bid=await this.storage.get('BRANCH');
          this.currentDate=new Date();
          if(this.priority==2){
            this.due_date=this.currentDate
          }else if(this.priority==1){
            this.due_date=new Date(Date.now()+2*24*60*60*1000);
          }else if(this.priority==0){
            this.due_date=new Date(Date.now()+3*24*60*60*1000);
          }
          if(this.description==undefined){
            this.description=null
          }
          // let files = this.fileField.getFiles();
          // console.log(files);
          let current=this.currentDate.getFullYear()+'-'+this.fixDigit((this.currentDate.getMonth()+1))+'-'+this.fixDigit(this.currentDate.getDate());
          let due=this.due_date.getFullYear()+'-'+this.fixDigit((this.due_date.getMonth()+1))+'-'+this.fixDigit(this.due_date.getDate());
          
          // let formData = new FormData();
          // formData.append('request_title', this.title); // Add any other data you want to send
          // formData.append('location', this.location);
          // formData.append('risk_rating', '1');
          // formData.append('hazard', '1'); // Add any other data you want to send
          // formData.append('priority', this.priority);
          // formData.append('out_of_order', '1');
          // formData.append('status', '0'); // Add any other data you want to send
          // formData.append('due_date', due);
          // formData.append('request_date', current);
          // formData.append('type', this.type); // Add any other data you want to send
          // formData.append('priority', this.priority);
          // formData.append('other_type', null);
          // formData.append('description', this.description);
          // formData.append('created_by', uid); // Add any other data you want to send
          // formData.append('user_id', uid);
          // formData.append('branch_id', bid);
          // files.forEach((file) => {
          //   formData.append('images[]', file.rawFile);
          // });
          
          if(this.type!=0){
            this.other_type=null
          }
          if(/^\d+$/.test(this.title)){
            this.presentAlert('Please enter a proper request title.');
          }else if(/^\d+$/.test(this.location)){
            this.presentAlert('Please enter proper location.');
          }else{
            this.showLoading();

          let body={
            request_title:this.title,
            location:this.location,
            risk_rating:this.risk,
            hazard:this.hazard,
            priority:this.priority,
            out_of_order:this.out_of_order,
            status:0,
            due_date:due,
            request_date:current,
            type:this.type,
            other_type:this.other_type,
            description:this.description,
            created_by:uid,
            user_id:uid,
            branch_id:this.bid,
            images:this.img
          }
          console.log("body:",body)
          let url=this.config.domain_url+'store_maintenance_request';
          this.http.post(url,body).subscribe((res:any)=>{
            console.log(res);
            this.presentAlert('Your maintenance request has been placed.');
            this.dismissLoader();
            this.router.navigate(['/maintenance-count'])
          },error=>{
            this.dismissLoader();
            this.presentAlert('Something went wrong.Please try again.');
            console.log(error);
            
          })
        }
        // })
    //   })
    // })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/maintenance-count']) ;
    }); 
  }
  
  fixDigit(val){
    return val.toString().length === 1 ? "0" + val : val;
  }
  async presentAlert(message) {
    const alert = await this.toast.create({
      message: message,
      cssClass:'toastStyle',
      duration: 3000      
    });
    alert.present(); //update
  }
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      // duration: 3000
    });
    return await loading.present();
  }
  async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }


  
async getBranch(){
  // this.branch.push({id:0,status:'All'});
  // this.storage.ready().then(()=>{
    // const data=await this.storage.get('COMPANY_ID');
      const data=await this.storage.get('USER_ID');
      // let url=this.config.domain_url+'branch_viatype';
      let url=this.config.domain_url+'technician_branches'
      let body={user_id:data}
      // let body={company_id:data,
      //           role_id:4}
      let headers=new HttpHeaders({company_id:data.toString()});
      this.http.post(url,body).subscribe((res:any)=>{
        console.log("res:",res);
        res.forEach(element=>{
          let branch={id:element.branch_id,status:element.branches.name}
          this.branch.push(branch);
        })
        
      })
  //   })
  // })
}
}

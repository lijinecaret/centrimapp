import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TechnicianNewRequestPage } from './technician-new-request.page';

const routes: Routes = [
  {
    path: '',
    component: TechnicianNewRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TechnicianNewRequestPageRoutingModule {}

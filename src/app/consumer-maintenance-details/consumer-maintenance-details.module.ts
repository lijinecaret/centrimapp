import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsumerMaintenanceDetailsPageRoutingModule } from './consumer-maintenance-details-routing.module';

import { ConsumerMaintenanceDetailsPage } from './consumer-maintenance-details.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsumerMaintenanceDetailsPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [ConsumerMaintenanceDetailsPage]
})
export class ConsumerMaintenanceDetailsPageModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConsumerMaintenanceDetailsPage } from './consumer-maintenance-details.page';

describe('ConsumerMaintenanceDetailsPage', () => {
  let component: ConsumerMaintenanceDetailsPage;
  let fixture: ComponentFixture<ConsumerMaintenanceDetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsumerMaintenanceDetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConsumerMaintenanceDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

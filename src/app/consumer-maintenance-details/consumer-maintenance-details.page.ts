import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Subscription } from 'rxjs';
import { ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { MultiFileUploadComponent } from '../components/multi-file-upload/multi-file-upload.component';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { OpenimageComponent } from '../components/openimage/openimage.component';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';
import { ReportAbuseComponent } from '../components/report-abuse/report-abuse.component';
import { ReportReasonComponent } from '../components/report-reason/report-reason.component';

@Component({
  selector: 'app-consumer-maintenance-details',
  templateUrl: './consumer-maintenance-details.page.html',
  styleUrls: ['./consumer-maintenance-details.page.scss'],
})
export class ConsumerMaintenanceDetailsPage implements OnInit {
  details:any=[];
  comment:any=[];
  images:any=[];
  technician:any;
  priority:any;
  
  request_id:any;
  subscription:Subscription;
  start:any=0;
end:any=5;
flag:any;

segment='0';


// entry_permission:boolean=false;

  constructor(private http:HttpClient,private storage:Storage,private route:ActivatedRoute,private toastCntlr:ToastController,
    private config:HttpConfigService,private router:Router,private platform:Platform,private popoverController:PopoverController,
    private iab:InAppBrowser,private dialog:SpinnerDialog,private modalCntrl:ModalController) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.details=[];
    this.comment=[];
    this.images=[];
    // this.technician=[];
    // this.type=[];
    const data=await this.storage.get('USER_TYPE');
    let headers=await this.config.getHeader();
    this.request_id=this.route.snapshot.paramMap.get('req_id');
    this.flag=this.route.snapshot.paramMap.get('flag');
    let url=this.config.domain_url+'get_request_details/'+this.request_id+'/'+data;
    
    
    this.http.get(url,{headers}).subscribe((res:any)=>{
      console.log(res);
      this.details=res.data;
      this.images=res.images;
      this.comment=res.comment;
      
      this.details.forEach(element => {
        this.priority=element.priority.toString();
        // this.technician=element.technician;
        
      })
      if(res.assignee!=null&&res.assignee.length>0){
        this.technician=res.assignee[0].technician.name
        
        res.assignee.forEach(ele => {
         
          if(this.technician==ele.technician.name){

          }else{
          this.technician=this.technician+','+ele.technician.name
          }
        });
      }
    },error=>{
      console.log(error);
      
    })

    if(this.flag==1){
      this.segment='1';
      document.getElementById('commentSection').scrollIntoView();
     // console.log("offset:",y);
     
     // this.content.scrollToPoint(0, y);
     }
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/maintenance']) ;
    }); 
  }
  async changePriority(priority){
  console.log("priority:",priority);
  let headers=await this.config.getHeader();
  let url=this.config.domain_url+'update_request_priority';
      let body={
          id:this.request_id,
          priority:priority
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        
      },error=>{
        console.log(error);
        
      })
  
}
addComment(){
  this.router.navigate(['/maintenance-comment',{flag:2,req_id:this.request_id}],{replaceUrl:true})
}

async showImages(ev: any) {

  const popover = await this.popoverController.create({
    component: MultiFileUploadComponent,
    event: ev,
    componentProps:{
      data:this.images
    },
    cssClass:'image_pop'
  });
  return await popover.present();
}

async viewImage(ev,img){
  const popover = await this.popoverController.create({
    component: OpenimageComponent,
    // event: ev,
    componentProps:{
      data:img
    },
    cssClass:'image_pop'
  });
  return await popover.present();
}
viewMore(){
  this.end +=5;
}


openDoc(item){


  // window.open(encodeURI(item.post_attachment),"_system","location=yes");

// if(item.attachment.length>0){
  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
  hideurlbar:'yes',
  zoom:'yes',
  beforeload:'yes',
      clearcache:'yes'
  }
  this.platform.ready().then(() => {
    if(item.includes('.pdf')){
      this.showpdf(item)
  }else{
  const browser = this.iab.create('https://docs.google.com/viewer?url='+item+'&embedded=true','_blank',options);
  this.dialog.show();
  browser.on('loadstart').subscribe(() => {
    console.log('start');
    this.dialog.hide();   
   
  }, err => {
    console.log(err);
    
    this.dialog.hide();
  })

  browser.on('loadstop').subscribe(()=>{
    console.log('stop');
    
    this.dialog.hide();;
  }, err =>{
    this.dialog.hide();
  })

  browser.on('loaderror').subscribe(()=>{
    this.dialog.hide();
  }, err =>{
    this.dialog.hide();
  })
  
  browser.on('exit').subscribe(()=>{
    this.dialog.hide();
  }, err =>{
    this.dialog.hide();
  })
}
})
}
async showpdf(file){
  const modal = await this.modalCntrl.create({
    component: ViewpdfComponent,
    cssClass:'fullWidthModal',
    componentProps: {
      
      data:file,
      
       
    },
    
    
  });
  return await modal.present();
}
async report(item,ev){
  const popover = await this.popoverController.create({
    component: ReportAbuseComponent,
    event:ev,
    componentProps:{
      type:2
    },
    backdropDismiss:true,
    cssClass:'message-options'
    
    
  });
  popover.onDidDismiss().then((data)=>{
   if(data.data){
    this.reason(item,data.data);
   }
  })
  return await popover.present();
}


async reason(item,type){
  const modal = await this.modalCntrl.create({
    component: ReportReasonComponent,
    cssClass:'report-reason-modal',
    backdropDismiss:true,
    
    
  });
  modal.onDidDismiss().then((data)=>{
    let reason;
      if(data.role=='1'){
        if(data.data){
       reason=data.data
      }else{
        reason='';
      }
      this.reportPost(item,reason,type);
    }
      
  })
  
  return await modal.present();
}
async reportPost(item,reason,type){

const uid=await this.storage.get('USER_ID');
const bid=await this.storage.get('BRANCH');
 const cid=await this.storage.get('COMPANY_ID');
      let url=this.config.domain_url+'report-issue';
      let headers=await this.config.getHeader();
      let body;
      body={
        user_id:uid,
        reason:reason
      }
      if(type==2){
        body.content_type='MCOMMENT';
        body.request_id=item.id
      }else{
        body.content_type='USER';
        if(item.user){
        body.request_id=item.user.user_id
        }
      }
      console.log('report:',body)
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.ionViewWillEnter();
        if(type==2){
          
        this.presentAlert('This comment has been reported. We shall verify and take appropriate action');
        }else{
          this.presentAlert('Request to block user has been submitted. We shall verify and take appropriate action');
        }
      },error=>{
        this.presentAlert('Something went wrong.Please try again later');
      })
}

async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}
}

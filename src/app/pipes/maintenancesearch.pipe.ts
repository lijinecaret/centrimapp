import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'maintenancesearch'
})
export class MaintenancesearchPipe implements PipeTransform {

  transform(items: any[], terms: string): any[] {
    if(!items) return [];
    if(!terms) return items;
    terms = terms.toLowerCase();
    return items.filter( it => {
      let filteredItems;
      if(it.request_title.toLowerCase().includes(terms)){
        filteredItems=it.request_title.toLowerCase().includes(terms)
      }else if(it.reference_id.toLowerCase().includes(terms)){
        filteredItems=it.reference_id.toLowerCase().includes(terms)
      }else if(it.location.toLowerCase().includes(terms)){
        filteredItems=it.location.toLowerCase().includes(terms)
      }
      console.log("fitered:",terms,filteredItems);
      
      return filteredItems; // only filter  name
    });
  }

}

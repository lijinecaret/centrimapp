import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-entertainment',
  templateUrl: './entertainment.page.html',
  styleUrls: ['./entertainment.page.scss'],
})
export class EntertainmentPage implements OnInit {
subscription:Subscription;
media:any=[];
  constructor(private router:Router,private platform:Platform,private http:HttpClient,private config:HttpConfigService,
    private toastCntrl:ToastController,private storage:Storage) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('TOKEN');
        let token=data;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    let headers= await this.config.getHeader()
    console.log("entertainment");
    let url=this.config.domain_url+'media';
    // let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        this.media=res.data;
        console.log(res);
      })
//     })
//   })
// })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu']) ;
      
        

     
    }); 

  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
 }

 getMedia(item){
  if(item.category=='Videos'){
    this.router.navigate(['/videos',{id:item.id}]);
   }else if(item.category=='Movies'){
    this.router.navigate(['/movies',{id:item.id}]);
   }else if(item.category=='360° Videos'){
    this.router.navigate(['/threesixty-videos',{id:item.id}]);
   }else if(item.category=='Games'){
    // this.presentAlert();
    this.router.navigate(['/games',{id:item.id}]);
   }else if(item.category=='Music'){
    //  this.presentAlert();
    this.router.navigate(['/music',{id:item.id}]);
   }else if(item.category=='Audio Books'){
    this.router.navigate(['/ebooks',{flag:1,id:item.id}]);
    // this.presentAlert()
   }else if(item.category=='Books'){
    this.router.navigate(['/ebooks',{flag:2,id:item.id}]);
   }else if(item.category=='Others'){
    this.router.navigate(['/others',{id:item.id}]);
    // this.presentAlert();
   }
   
 }
 async presentAlert() {
  const alert = await this.toastCntrl.create({
    message: 'No access right now.',
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}
}

import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Storage } from '@ionic/storage-angular';
@Component({
  selector: 'app-threesixty-videos',
  templateUrl: './threesixty-videos.page.html',
  styleUrls: ['./threesixty-videos.page.scss'],
})
export class ThreesixtyVideosPage implements OnInit {
  subscription:Subscription;
  trustedVideoUrl: SafeResourceUrl;
  vid=[];
  folder:any=[];
  files:any=[];
  v_url:any;
  img:any;
  id:any;
  // array_of_objects = [{vid_link:"https://www.youtube.com/embed/v64KOxKVLVg",title:"360° Underwater National Park | National Geographic"},
  // {vid_link:"https://www.youtube.com/embed/rG4jSz_2HDY",title:"360° Great Hammerhead Shark Encounter | National Geographic"}]
  constructor(private router:Router,private platform:Platform,private http:HttpClient,
    private config:HttpConfigService,private iab: InAppBrowser,private storage:Storage,
    private route:ActivatedRoute) { }
  ngOnInit() {
    
  }
  async ionViewWillEnter() {
    this.id=this.route.snapshot.paramMap.get('id');
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('TOKEN');
        let token=data;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    let headers= await this.config.getHeader()
    this.vid=[];
    let url=this.config.domain_url+'media/'+this.id;
    // let headers= await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      this.folder=res.media_fldr;
      this.files=res.media_files;
    

    for(let i of this.files){
      if((i.url).includes('www.youtube.com')){
        console.log("you",i.url);
        let yurl=(i.url).replace('watch?v=','embed/');
        if(yurl.includes('&')){
          yurl=yurl.substr(0,yurl.indexOf('&'));
         }
        this.v_url=yurl+'?enablejsapi=1'
        let img=(this.v_url).substring((this.v_url).lastIndexOf("/") + 1, (this.v_url).lastIndexOf("?"));
        this.img='http://img.youtube.com/vi/'+img+'/default.jpg'
      }else{
        this.v_url=i.url;
        this.img="/assets/entertainment/vimeo-video-thumbnail.svg"
      }
      // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.v_url);
      let video={url:this.v_url,title:i.title,id:i.id,img:this.img};
      this.vid.push(video);
    }
  })
// })
//       })
//     })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/entertainment']) ;
  }); 
     
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    // let listaFrames = document.getElementsByTagName("iframe");
    // for (var index = 0; index < listaFrames.length; index++) {
    //   let iframe = listaFrames[index].contentWindow;
    //   iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    // }
    }

    openFolder(item){
  
      this.router.navigate(['open-media',{flag:3,id:item.id,title:item.title,mid:this.id}])
    }


    play(url){
      // window.open(encodeURI(url),"_system","location=yes");
      let options:InAppBrowserOptions ={
        location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'no'
      }
      screen.orientation.lock('landscape');
      const browser = this.iab.create(url,'_blank',options);
      browser.on('exit').subscribe(()=>{
        screen.orientation.unlock();
        screen.orientation.lock('portrait');
      })
    //   let options: StreamingVideoOptions = {
    //     successCallback: () => { console.log('Video played') },
    //     errorCallback: (e) => { console.log('Error streaming') },
    //     orientation: 'landscape',
    //     shouldAutoClose: true,
    //     controls: true
    //   };
      
    //   this.streamingMedia.playVideo(url, options);
    }
}

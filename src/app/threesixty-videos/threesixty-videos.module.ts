import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ThreesixtyVideosPageRoutingModule } from './threesixty-videos-routing.module';

import { ThreesixtyVideosPage } from './threesixty-videos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ThreesixtyVideosPageRoutingModule
  ],
  declarations: [ThreesixtyVideosPage]
})
export class ThreesixtyVideosPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ThreesixtyVideosPage } from './threesixty-videos.page';

const routes: Routes = [
  {
    path: '',
    component: ThreesixtyVideosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ThreesixtyVideosPageRoutingModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ThreesixtyVideosPage } from './threesixty-videos.page';

describe('ThreesixtyVideosPage', () => {
  let component: ThreesixtyVideosPage;
  let fixture: ComponentFixture<ThreesixtyVideosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreesixtyVideosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ThreesixtyVideosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

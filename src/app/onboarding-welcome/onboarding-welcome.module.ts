import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnboardingWelcomePageRoutingModule } from './onboarding-welcome-routing.module';

import { OnboardingWelcomePage } from './onboarding-welcome.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OnboardingWelcomePageRoutingModule
  ],
  declarations: [OnboardingWelcomePage]
})
export class OnboardingWelcomePageModule {}

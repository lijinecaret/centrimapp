import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnboardingWelcomePage } from './onboarding-welcome.page';

const routes: Routes = [
  {
    path: '',
    component: OnboardingWelcomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OnboardingWelcomePageRoutingModule {}

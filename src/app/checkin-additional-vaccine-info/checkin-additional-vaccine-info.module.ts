import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckinAdditionalVaccineInfoPageRoutingModule } from './checkin-additional-vaccine-info-routing.module';

import { CheckinAdditionalVaccineInfoPage } from './checkin-additional-vaccine-info.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckinAdditionalVaccineInfoPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [CheckinAdditionalVaccineInfoPage]
})
export class CheckinAdditionalVaccineInfoPageModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckinAdditionalVaccineInfoPage } from './checkin-additional-vaccine-info.page';

describe('CheckinAdditionalVaccineInfoPage', () => {
  let component: CheckinAdditionalVaccineInfoPage;
  let fixture: ComponentFixture<CheckinAdditionalVaccineInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinAdditionalVaccineInfoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckinAdditionalVaccineInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ActionSheetController, AlertController, LoadingController, ModalController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import moment from 'moment';
import momenttz from 'moment-timezone';
import { TemperatureWarningComponent } from '../components/temperature-warning/temperature-warning.component';
import { OpenimageComponent } from '../components/openimage/openimage.component';
import { Chooser } from '@ionic-native/chooser/ngx'
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';
import { Camera,CameraResultType, CameraSource } from '@capacitor/camera';
import { Filesystem } from '@capacitor/filesystem';
@Component({
  selector: 'app-checkin-additional-vaccine-info',
  templateUrl: './checkin-additional-vaccine-info.page.html',
  styleUrls: ['./checkin-additional-vaccine-info.page.scss'],
})
export class CheckinAdditionalVaccineInfoPage implements OnInit {
  res_id: any;
  purpose: any;
  onPremises: any[] = [];
  count: any;
  subscription: Subscription;
  users: any = {};
  vaccine: any = {};
  temp: any;
  testDate: any;
  firstDose: any;
  secondDose: any;
  viewEntered: boolean = false;
  tempStatus: any;
  vac_proof: any;
  inf_proof: any;
  dose: any;
  user: any = {};
  vacArray: any[] = [];
  vaccine_id: any;
  vaccination: any;
  vacOrNot: any;
  inf_status: any;
  // multiple_user:boolean;
  user_vaccine: any = [];
  vac_status: any = 0;
  inf_vac_status: any = 0;
  show_covid_vac: boolean = true;
  show_inf_vac: boolean = true;
  expiryDate: any;
  loaded: boolean = false;
  repeat_covid: boolean = false;
  name: any;
  phone: any;
  from: any;
  minDate: any = new Date().toISOString();
  disable:boolean=false;
  constructor(private router: Router, private storage: Storage, private route: ActivatedRoute, private platform: Platform,
    private http: HttpClient, private config: HttpConfigService, private dialog: SpinnerDialog,
    private iab: InAppBrowser, private popCntl: PopoverController, private modalCntl: ModalController,
    private toast: ToastController, private loadingCtrl: LoadingController, private alertCntrl: AlertController,
    private actionsheetCntlr: ActionSheetController, private chooser: Chooser) { }

  ngOnInit() {
  }
  async ionViewWillEnter() {
    this.disable=false;
    this.res_id = this.route.snapshot.paramMap.get('res_id');
    this.purpose = this.route.snapshot.paramMap.get('purpose');
    this.onPremises = JSON.parse(this.route.snapshot.paramMap.get('onPremises'));
    this.user = JSON.parse(this.route.snapshot.paramMap.get('user_info'));
    this.count = this.route.snapshot.paramMap.get('count');
    this.from = this.route.snapshot.paramMap.get('from');
    this.name = this.user.additional_info[this.user.additional_info.length - 1].name;
    this.phone = this.user.additional_info[this.user.additional_info.length - 1].phone;
    if (!this.user.additional) {
      this.user.additional = [];
      this.user.temp_array = [];
      this.user.firstDoseArray = [];
      this.user.secondDoseArray = [];
      this.user.vaccine_array = [];
      this.user.vaccine_date_array = [];
      this.user.vaccine_phone = [];
      this.user.already_have = [];
      this.user.inf_vaccine_status_array = [];
      this.user.inf_vaccine_date_array = [];
      this.user.inf_vaccine_proof_array = [];

    }
    this.user_vaccine = [];

    this.getDetails();
    // this.storage.ready().then(() => {
      const temp=await this.storage.get('VIS-TEMPARATURE');

        // const temp = await this.storage.get('VIS-TEMPARATURE');
        // const temp = await this.storage.get('VIS-TEMPARATURE');
        this.tempStatus = temp;
      // })
      // })
      const vac=await this.storage.get('VIS-VACCINATION');
        // const vac = await this.storage.get('VIS-VACCINATION');
        this.vaccination = vac;
        if (this.vaccination == 1) {
          const vacArray=await this.storage.get('VACCINES');

            let vac = JSON.parse(vacArray);
            vac.forEach(element => {
              this.vacArray.push(element.vaccine_id)
            });
          // })
        }
    //   })
    // })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {

      // this.router.navigate(['/consumer-profile',{id:this.id}])
      this.back();

    });
  }
  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  back() {
    // if(this.flag==1){
    // this.router.navigate(['/capture-photo',{name:this.name,phone:this.phone,address:this.address,recurring:this.recurring,rep_res:JSON.stringify(this.rep_res)}]);
    // }else{
    this.user.additional_info = [];
    this.router.navigate(['/checkin-add-additional-visitors', { res_id: this.res_id, purpose: this.purpose, onPremises: JSON.stringify(this.onPremises), user_info: JSON.stringify(this.user), from: this.from }]);
    // }
  }
  async getDetails() {
    // this.users=[];
    let phone = this.phone.substring(1);
    // this.storage.ready().then(() => {

      const cid=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');

          let url = this.config.domain_url + 'search_uservaccination_app';
          let body = {
            mobile: '+61' + phone,
            name: this.name,
            visitor_type: 0,
            branch_id: bid,
            company_id: cid
          }
          let headers = await this.config.getHeader();
          this.http.post(url, body, { headers }).subscribe((res: any) => {
            console.log('vac-info:', res, url, body);
            this.viewEntered = true;
            this.users = res.data
            this.vaccine = res.vaccines;

            if (this.vaccine && this.vaccine.length) {
              this.vaccine.forEach(element => {
                this.user_vaccine.push(element.vaccine_id)
                if (element.vaccine && element.vaccine.id == 1) {

                  this.inf_vac_status = 1;
                  this.inf_status = 1;

                  // if(element.proof!=null){
                  //   this.inf_proof= element.proof;

                  //   }

                  this.testDate = moment(element.vaccine_date).format();
                  this.expiryDate = new Date(element.expiry_date).getTime();
                  //   let ex=new Date(element.expiry_date).getTime();
                  // let days=ex-new Date().getTime();;
                  // let day=Math.floor(days/(60*60*24*1000));
                  // if(day>=-1){
                  //   this.show_inf_vac=false;
                  // }
                } else if (element.vaccine && element.vaccine.id == 2) {
                  this.vac_status = 1;
                  this.vaccine_id = element.vaccine.id;
                  this.vacOrNot = 1;
                  // if(element.proof!=null){
                  //   this.vac_proof= element.proof;

                  //   }
                  if (element.firstdose_date != null) {
                    this.firstDose = moment(element.firstdose_date).format();
                  }
                  if (element.seconddose_date != null) {
                    this.secondDose = moment(element.seconddose_date).format();
                  }
                  // if(element.firstdose_date != null&&element.seconddose_date != null){
                  //   this.show_covid_vac=false;
                  // }
                }
              });
              if (this.user_vaccine.includes(2)) {
                if (this.secondDose != undefined) {
                  this.show_covid_vac = false;

                }
                if (this.firstDose != undefined && this.secondDose == undefined) {
                  this.repeat_covid = true;
                }
              }
              if (this.user_vaccine.includes(1)) {
                let days = this.expiryDate - new Date().getTime();;
                let day = Math.floor(days / (60 * 60 * 24 * 1000));
                if (day >= -1) {
                  this.show_inf_vac = false;
                } else {
                  // this.inf_vac_status=0
                  // this.vaccinated=0
                }
              }
              // this.vaccine_id = this.vaccine.vaccine.id.toString();

              // this.getVaccineList();
              console.log('covac:', this.vac_proof, this.secondDose, this.firstDose, this.vacOrNot);
              console.log('ifvac:', this.inf_status, this.inf_proof, this.testDate)
            }
          })
      //   })
      // })
    // })
  }



  async gotoNext() {
    this.disable=true;
    // this.storage.ready().then(() => {
      const temp=await this.storage.get('VIS-TEMPARATURE');
        this.tempStatus = temp;
        const vac=await this.storage.get('VIS-VACCINATION');
          if (this.tempStatus == 1 && (this.temp == undefined || this.temp == '')) {
            this.disable=false;
            this.presentToast('Please enter your body temperature.');

          } else if (this.tempStatus == 1 && (parseFloat(this.temp) > 37.4)||parseFloat(this.temp)<30) {

            // this.audio.play('alert').then((res) => {
            //   console.log("audio:", res);

            // });
            this.warning();
            // } else if(vac==1&&this.vacOrNot==1&&this.vac_proof==undefined){
            //     this.presentToast('Please upload your covid-19 vaccination evidence')
            //   }else if(vac==1&&this.inf_status==1&&this.inf_proof==undefined){
            //     this.presentToast('Please upload your influenza vaccination evidence')
            // } else if(vac==1&&this.vacOrNot==1&& this.firstDose==undefined&&this.secondDose==undefined){
            //   this.presentToast('Please choose covid-19 vaccination date')
            // } else if(vac==1&&this.inf_status==1&&this.testDate==undefined){
            //   this.presentToast('Please choose influenza vaccination date')
            // }else if(vac==1&&this.vacOrNot!==1&& this.inf_status!==1){
            //   this.presentToast('Please provide your vaccination details')
          } else {
            // if(vac == 1 && this.vacOrNot !== 1 && this.inf_status !== 1){
            //   this.audio.play('vac_alert').then((res) => {
            //     console.log("audio:", res);

            //   });
            // }
            
            if (!this.vac_proof) {
              this.vac_proof = null
            }
            if (!this.inf_proof) {
              this.inf_proof = null
            }
            this.user.additional.push({
              id: this.vaccine_id,
              vac_date: this.testDate,
              dose: this.dose,
              proof: this.vac_proof,
              vac_status: this.vac_status
            })
            this.user.inf_vaccine_proof_array.push(this.inf_proof);
            this.user.inf_vaccine_status_array.push(this.inf_vac_status);
            console.log('inf:', this.inf_vac_status, this.user.inf_vaccine_status_array);

            this.user.temp_array.push(this.temp);
            if (!this.firstDose) {
              this.user.firstDoseArray.push(null)
            } else {
              if (this.repeat_covid && this.secondDose) {
                this.user.firstDoseArray.push(null)
              } else {
                this.user.firstDoseArray.push(moment(this.firstDose).format('yyyy-MM-DD HH:mm:ss'));
              }
            }
            if (!this.secondDose) {
              this.user.secondDoseArray.push(null)
            } else {
              this.user.secondDoseArray.push(moment(this.secondDose).format('yyyy-MM-DD HH:mm:ss'));
            }
            this.user.vaccine_array.push(this.vaccine_id);
            if (!this.testDate) {
              this.user.vaccine_date_array.push(null)
              this.user.inf_vaccine_date_array.push(null)
            } else {
              this.user.vaccine_date_array.push(moment(this.testDate).format('yyyy-MM-DD'));
              this.user.inf_vaccine_date_array.push(moment(this.testDate).format('yyyy-MM-DD'));
            }

            let phone = this.phone.substring(1);
            this.user.vaccine_phone.push('+61' + phone);
            // let already;
            // if(this.vaccine){
            //   already=1
            // }else{
            //   already=0
            // }
            this.user.already_have.push(this.vac_status);

            console.log('count:', this.count, this.user.additional_info.length, this.user.additional_info)
            if (this.user.additional_info.length !== parseInt(this.count)) {
              this.disable=false;
              this.router.navigate(['/checkin-additional-info', { res_id: this.res_id, purpose: this.purpose, count: this.count, onPremises: JSON.stringify(this.onPremises), user_info: JSON.stringify(this.user), from: this.from }], { replaceUrl: true })
            } else {
              // this.storage.ready().then(() => {
                const decl=await this.storage.get('VIS-HEALTH-DECLARATION');
                  const donot=await this.storage.get('VIS-DONOTHAVE');
                    console.log("decl:", decl);

                    const terms=await this.storage.get('VIS-TERMS');
                      if (decl == 1 || donot == 1) {
                        this.disable=false;
                        this.router.navigate(['/checkin-health-declaration', { res_id: this.res_id, purpose: this.purpose, count: this.count, back: 0, onPremises: JSON.stringify(this.onPremises), user_info: JSON.stringify(this.user), from: this.from }]);
                      } else if (terms == 1) {
                        this.disable=false;
                        this.router.navigate(['/checkin-vis-guidelines', { res_id: this.res_id, purpose: this.purpose, count: this.count, flag: 1, onPremises: JSON.stringify(this.onPremises), user_info: JSON.stringify(this.user), from: this.from }])

                      } else {
                        // this.storage.ready().then(() => {
                          const cid=await this.storage.get('COMPANY_ID');
                            const bid=await this.storage.get('BRANCH');
                              const tz=await this.storage.get('TIMEZONE');
                                const name=await this.storage.get('NAME');
                                  const ph=await this.storage.get('PHONE');
                                  const img=await this.storage.get('PRO_IMG');
                                    let additional = [];
                                    let phone_array = [];
                                    let temp_array = [];
                                    // let count;
                                    if (this.user) {
                                      if (!this.user.additional_info) {
                                        additional = null;
                                        phone_array = null;
                                        temp_array = null
                                      } else {
                                        temp_array = this.user.temp_array;
                                        this.user.additional_info.forEach(element => {
                                          additional.push(element.name);
                                          let phone = element.phone.substring(1);

                                          phone_array.push('+61' + phone)

                                        });
                                      }
                                    }
                                    // count=0;

                                    if (this.count == 'null' || this.count == 'undefined') {
                                      this.count = null
                                    }

                                    // if(this.img=='undefined'){
                                    //   this.img=null
                                    // }
                                    // if(this.res=='null'){
                                    //   this.res=null
                                    // }if(this.address=='null'){
                                    //   this.address=null;
                                    // }
                                    // if(this.purpose=='null'){
                                    //   this.purpose=null;
                                    // }

                                    // let expDate;
                                    // if(this.expDate==undefined){
                                    //   expDate=moment(this.expDate).format('yyyy-MM-DD')
                                    // }else{
                                    //   expDate=null
                                    // }
                                    // if(this.vaccine==undefined){
                                    //   this.vaccine=null
                                    // }
                                    // let vaccinated=0;
                                    // let covid_vaccinated=0;
                                    // if(this.vacStatus==0){
                                    //   // expDate=null;
                                    //   vacDate=null;
                                    //   this.vaccine=null;
                                    //   vaccinated=null;
                                    //   covid_vaccinated=null
                                    // }
                                    let phone=null;
                                    if(ph){
                                    let phn = ph.toString();
                                    if (phn.charAt(0) === '+') {
                                      phone = phn.substring(1);
                                    } else {
                                      phone = phn
                                    }
                                  }



                                    // let temp;
                                    // let tempArray={};
                                    // if(parseInt(this.count)>0){
                                    //   temp=null;
                                    //   tempArray=this.temp_copy_array;
                                    // }else{
                                    //   temp=this.temp;
                                    //   tempArray=null;
                                    // }

                                    let vac_status, vac_date, vac, vac_proof, inf_vaccine_status, temp,
                                      inf_vaccine_date, inf_vaccine_proof, firstDose, secondDose, vaccinated, covidVaccinated, uploaded_date = null;
                                    let vac_status_array = [];
                                    let vac_date_array = [];
                                    let vac_array = [];
                                    let proof_array = []
                                    let vac_dose_array = [];
                                    let first_array = [];
                                    let second_array = [];

                                    let already_have = [];

                                    let inf_vaccine_status_array = [];
                                    let inf_vaccine_date_array = [];
                                    let inf_vaccine_proof_array = [];



                                    if (this.user) {
                                      temp = this.user.temp

                                      if (this.user.vaccine) {
                                        vac_status = this.user.vaccine.vac_status;
                                        vac_date = this.user.vaccine.vac_date;
                                        vac_proof = this.user.vaccine.proof;
                                        vac = this.user.vaccine.id;
                                        firstDose = this.user.vaccine.firstDose;
                                        secondDose = this.user.vaccine.secondDose;
                                        inf_vaccine_status = this.user.vaccine.inf_vaccine_status
                                        inf_vaccine_date = this.user.vaccine.inf_vaccine_date;
                                        inf_vaccine_proof = this.user.vaccine.inf_vaccine_proof;
                                        vaccinated = this.user.vaccine.vaccinated;
                                        covidVaccinated = this.user.vaccine.covidVaccinated;
                                        uploaded_date = this.user.vaccine.uploaded_date;
                                      }
                                      if (this.user.additional && this.user.additional.length) {
                                        this.user.additional.forEach(element => {
                                          vac_array.push(element.id);
                                          // vac_date_array.push(element.vac_date);
                                          vac_dose_array.push(element.dose);
                                          proof_array.push(element.proof);
                                          vac_status_array.push(element.vac_status)
                                        });
                                      } else {
                                        vac_status_array = null;
                                        // vac_date_array=null;
                                        vac_array = null;
                                        proof_array = null
                                        vac_dose_array = null;
                                      }

                                      if (this.user.firstDoseArray) {
                                        if (this.user.firstDoseArray.length > 0) {
                                          first_array = this.user.firstDoseArray
                                        } else {
                                          first_array = null
                                        }
                                      } else {
                                        first_array = null
                                      }
                                      if (this.user.secondDoseArray) {
                                        if (this.user.secondDoseArray.length > 0) {
                                          second_array = this.user.secondDoseArray
                                        } else {
                                          second_array = null
                                        }
                                      } else {
                                        second_array = null
                                      }

                                      if (this.user.vaccine_date_array) {
                                        if (this.user.vaccine_date_array.length > 0) {
                                          vac_date_array = this.user.vaccine_date_array
                                        } else {
                                          vac_date_array = null
                                        }
                                      } else {
                                        vac_date_array = null
                                      }
                                      //   if(this.user.vaccine_phone){
                                      //   if(this.user.vaccine_phone.length>0){
                                      //     phone_array=this.user.vaccine_phone
                                      //   }else{
                                      //     phone_array=null
                                      //   }
                                      // }else{
                                      //   phone_array=null
                                      // }
                                      if (this.user.inf_vaccine_status_array) {
                                        if (this.user.inf_vaccine_status_array.length > 0) {
                                          inf_vaccine_status_array = this.user.inf_vaccine_status_array
                                        } else {
                                          inf_vaccine_status_array = null
                                        }
                                      } else {
                                        inf_vaccine_status_array = null
                                      }
                                      if (this.user.inf_vaccine_date_array) {
                                        if (this.user.inf_vaccine_date_array.length > 0) {
                                          inf_vaccine_date_array = this.user.inf_vaccine_date_array
                                        } else {
                                          inf_vaccine_date_array = null
                                        }
                                      } else {
                                        inf_vaccine_date_array = null
                                      }

                                      if (this.user.inf_vaccine_proof_array) {
                                        if (this.user.inf_vaccine_proof_array.length > 0) {
                                          inf_vaccine_proof_array = this.user.inf_vaccine_proof_array
                                        } else {
                                          inf_vaccine_proof_array = null
                                        }
                                      } else {
                                        inf_vaccine_proof_array = null
                                      }

                                      if (this.user.already_have) {
                                        if (this.user.already_have.length > 0) {
                                          already_have = this.user.already_have
                                        } else {
                                          already_have = null
                                        }
                                      } else {
                                        already_have = null
                                      }
                                    } else {
                                      vac_status_array = null
                                      vac_date_array = null
                                      vac_array = null
                                      proof_array = null
                                      vac_dose_array = null
                                      first_array = null
                                      second_array = null

                                      already_have = null

                                      inf_vaccine_status_array = null
                                      inf_vaccine_date_array = null
                                      inf_vaccine_proof_array = null
                                    }
                                    let url = this.config.domain_url + 'create_visitor_list';
                                    let headers = await this.config.getHeader();
                                    let body = {
                                      visitor_type: '0',
                                      checkin_checkout: '0',
                                      name: name,
                                      phone: '+' + phone,
                                      address: null,
                                      additional_visitor_count: this.count,
                                      additional_visitor: additional,
                                      checkin_time: momenttz().tz(tz).format('yyyy-MM-DD HH:mm:ss'),
                                      checkout_time: null,
                                      i_donot_have: null,
                                      whom_to_visit: '0',
                                      resident_user_id: this.res_id,
                                      companian: null,
                                      purpose: null,
                                      body_temparature: temp,

                                      temp_measure: 0,
                                      photo: img,
                                      signature: null,
                                      qna: null,
                                      recent_test: null,
                                      test_date: null,
                                      test_result: null,
                                      via_app: 1,
                                      visitor_id: null,
                                      vaccinated: vaccinated,
                                      covid_vaccinated: covidVaccinated,
                                      device_id: 3,
                                      body_temparature_array: temp_array,

                                      vaccine_status: vac_status,
                                      vaccine_date: vac_date,
                                      vaccine: vac,
                                      expiry_date: null,
                                      proof: vac_proof,
                                      uploaded_by: null,
                                      uploaded_date: uploaded_date,
                                      // vaccine_status_array:vac_status_array,
                                      vaccine_array: vac_array,
                                      vaccine_date_array: vac_date_array,
                                      // vaccine_dose_array:vac_dose_array,
                                      proof_array: proof_array,
                                      vaccine_phone_array: phone_array,
                                      firstdose_date_array: first_array,
                                      seconddose_date_array: second_array,
                                      vaccine_already_have: already_have,
                                      firstdose_date: firstDose,
                                      seconddose_date: secondDose,
                                      inf_vaccine_status: inf_vaccine_status,
                                      inf_vaccine_date: inf_vaccine_date,
                                      inf_vaccine_proof: inf_vaccine_proof,
                                      inf_vaccine_status_array: inf_vaccine_status_array,
                                      inf_vaccine_date_array: inf_vaccine_date_array,
                                      inf_vaccine_proof_array: inf_vaccine_proof_array


                                    }
                                    console.log("body:", body);

                                    this.http.post(url, body, { headers }).subscribe((res: any) => {
                                      console.log(res);
                                      this.disable=false;
                                      this.router.navigate(['/confirm-checkin', { ref_id: res.data.reference_id,additional:this.count }])
                                    },error=>{
                                      this.disable=false;
                                    })
                              //     })
                              //   })
                              // })
                            // })
                          // })
                        // })
                      }
                    // })
                  // })
              //   })
              // })
            }
          }

      //   })
      // })
    // })
  }


  async warning() {
    const modal = await this.modalCntl.create({
      component: TemperatureWarningComponent,
      cssClass: 'warningModal',
      componentProps: {
        // name: this.user.name,


      },
      id: 'warning'
    });
    this.disable=false;
    return await modal.present();
  }

  async presentToast(mes) {
    const alert = await this.toast.create({
      message: mes,
      duration: 3000,
      cssClass: 'toast-mess',
      position: 'top'
    });
    alert.present();
  }
  async getVaccineList() {
    let headers=await this.config.getHeader();
    this.vacArray = [];
    let url = this.config.domain_url + 'get_vaccine';
    this.http.get(url,{headers}).subscribe((res: any) => {

      this.vacArray = res.data;

      console.log('vac:', res, this.vaccine_id);


    })
  }

  async capturePhoto(i) {

    // if(this.platform.is('android')){
    //   this.presentAlert('Please rotate to front camera.',i)
    // }else{
    // this.takePhoto(i)
    // }
    const actionSheet = await this.actionsheetCntlr.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(i);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.takePhoto(i);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();


  }
  async uploadImage(img, i) {
    // if(this.isLoading==false){
    this.showLoading();
    // }
    let headers=await this.config.getHeader();
    let url = this.config.domain_url + 'upload_file';
    let body = { file: img }
    console.log("body:", img);

    this.http.post(url, body,{headers}).subscribe((res: any) => {
      console.log("uploaded:", res);
      if (i == 2) {
        this.vac_proof = res.data;
        // this.vacOrNot=1
        this.vaccine_id = 2
      } else {
        this.inf_proof = res.data;
        // this.inf_status=1
      }
      this.dismissLoader();
    }, error => {
      // this.imageResponse.slice(0,1);
      console.log(error);
      this.dismissLoader();
      this.presentToast('Upload failed.Please try again.')

    })
  }

  async showImg(img) {
    const popover = await this.popCntl.create({
      component: OpenimageComponent,
      cssClass: 'msg_attach',
      id: 'viewImg',
      // event: ev,
      componentProps: {
        data: img
      },
      // cssClass:'image_pop'
    });
    return await popover.present();
  }
  async showLoading() {

    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      message: 'Please wait...',
      spinner: null,
      id: 'loader'
      // duration: 3000
    });
    return await loading.present();
  }

  async dismissLoader() {

    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }

  clearDate(i) {
    if (i == 1) {
      this.firstDose = undefined
      if (this.secondDose == undefined) {
        this.vacOrNot = undefined;
        this.vac_status = 0;
        this.vaccine_id = null;
      }
    } else if (i == 2) {
      this.secondDose = undefined
      if (this.firstDose == undefined) {
        this.vacOrNot = undefined;
        this.vac_status = 0;
        this.vaccine_id = null;
      }
    } else if (i == 3) {
      this.testDate = undefined
      this.inf_status = undefined;
      this.inf_vac_status = 0;
    }
  }
  removeImg(i) {
    if (i == 1) {
      this.vac_proof = undefined
    } else {
      this.inf_proof = undefined
    }
  }

  changeStatus(i) {

    if (this.loaded) {

      if (i == 1 || i == 2) {
        this.vacOrNot = 1;
        this.vac_status = 0;
        this.vaccine_id = 2;
      } else if (i == 3) {
        this.inf_status = 1;
        this.inf_vac_status = 0;
      }
    }
    console.log('changed:', this.inf_vac_status)

  }
  //  async presentAlert(mes,i){
  //   const alert = await this.alertCntl.create({
  //     // header: 'Log out',
  //     backdropDismiss: true,
  //     message: mes,
  //     buttons: [
  //       {
  //         text: 'Ok',
  //         role: 'cancel',
  //         // cssClass: 'secondary',
  //         handler: (blah) => {
  //           this.takePhoto(i)
  //         }
  //       }
  //     ]
  //   });

  //   await alert.present();
  // }
  async pickImage(i) {
    const image = await Camera.pickImages({
      quality: 90,
      correctOrientation:true,
      limit:1
      
    });
  
  
    for (var j = 0; j < image.photos.length; j++) {
         
            
            
            const contents = await Filesystem.readFile({
              path: image.photos[j].path
            });
            
            
              this.uploadImage('data:image/jpeg;base64,' + contents.data,i)
            
              
         
             
        }
       
  }
  async takePhoto(i) {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      correctOrientation:true,
      source:CameraSource.Camera
    });
  
    var imageUrl = image.base64String;
   
    let base64Image = 'data:image/jpeg;base64,' + imageUrl;
    console.log('image:',imageUrl);
    
    this.uploadImage(base64Image,i);
  
  }
  @HostListener('touchstart')
  onTouchStart() {

    this.loaded = true;
  }
  openDoc(item) {


    // window.open(encodeURI(item.post_attachment),"_system","location=yes");

    // if(item.attachment.length>0){
    let options: InAppBrowserOptions = {
      location:'yes',
    hidenavigationbuttons:'yes',
    hideurlbar:'yes',
    zoom:'yes',
    
    }
    if(item.includes('.pdf')){
      this.showpdf(item)
  }else{
    const browser = this.iab.create('https://docs.google.com/viewer?url=' + item + '&embedded=true', '_blank', options);
    browser.on('loadstart').subscribe(() => {
      console.log('start');
      this.dialog.show();

    }, err => {
      console.log(err);

      this.dialog.hide();
    })

    browser.on('loadstop').subscribe(() => {
      console.log('stop');

      this.dialog.hide();;
    }, err => {
      this.dialog.hide();
    })

    browser.on('loaderror').subscribe(() => {
      this.dialog.hide();
    }, err => {
      this.dialog.hide();
    })

    browser.on('exit').subscribe(() => {
      this.dialog.hide();
    }, err => {
      this.dialog.hide();
    })
  }
  }

  async attachfile(i) {
    const actionSheet = await this.actionsheetCntlr.create({
      header: 'Select',
      mode:'ios',
      buttons: [{
        text: 'Image',
        // role: 'destructive',
        icon: 'image-outline',
        handler: () => {
          this.capturePhoto(i);
          console.log('Delete clicked');
        }
      }, {
        text: 'Document',
        icon: 'document-outline',
        handler: () => {
          this.selectDoc(i);
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancel',
        // icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  async selectDoc(i) {
    this.chooser.getFile()
      .then(file => {
        console.log(file ? file : 'canceled');
        if (file) {
          if (file.mediaType == "application/pdf" || file.mediaType == "application/msword") {
            let data;
            if (file.mediaType == "application/msword") {
              let type = /msword/
              data = file.dataURI.replace(type, 'doc')
            } else {
              data = file.dataURI
            }
            this.uploadImage(data, i);
          } else {
            this.alert('File format not supported');
          }
        }
      })
      .catch((error: any) => console.error(error));
  }

  async alert(mes) {

    const alert = await this.alertCntrl.create({

      message: mes,
      backdropDismiss: true
    });

    await alert.present();
  }

  async showpdf(file){
    const modal = await this.modalCntl.create({
      component: ViewpdfComponent,
      cssClass:'fullWidthModal',
      componentProps: {
        
        data:file,
        
         
      },
      
      
    });
    return await modal.present();
  }
}

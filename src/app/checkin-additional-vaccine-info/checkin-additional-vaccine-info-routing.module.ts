import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckinAdditionalVaccineInfoPage } from './checkin-additional-vaccine-info.page';

const routes: Routes = [
  {
    path: '',
    component: CheckinAdditionalVaccineInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckinAdditionalVaccineInfoPageRoutingModule {}

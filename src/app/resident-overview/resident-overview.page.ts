import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, Platform } from '@ionic/angular';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import { PettycashConsumersComponent } from '../components/pettycash-consumers/pettycash-consumers.component';
@Component({
  selector: 'app-resident-overview',
  templateUrl: './resident-overview.page.html',
  styleUrls: ['./resident-overview.page.scss'],
})
export class ResidentOverviewPage implements OnInit {
  id:any;
  user:any={};
  room:any;
  rv:any;
  property_name:any;
  // contacts:any=[];
  // pending:any=[];
  // activity:any=[];
  // attended:any=[];
  members:any=[];
  // media:any=[];
  branch:any;
  profile_view:boolean;
  // hideInvite:boolean=false;
  subscription:Subscription;
  res_id:any;
  // begin:any=0;
  // finish:any=20;
  // offset = 0;
  
  cons_id:any;
  current:Date=new Date();
  // labels:any[]=[];
  // data:any[]=[];
  count:any;
  // count_30:any;
  // start:any;
  // end:any;
  // no_access:boolean=false;
  res_branch:any;
  wing:any;
  flag:any;
  hours:any;
  meals:any;
  visitors:any;
  modules:any=[];
  constructor(private storage:Storage,private http:HttpClient,private modalCntrl:ModalController,
    private config:HttpConfigService,private router:Router,private platform:Platform,private route:ActivatedRoute) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.members=[];
    this.flag=this.route.snapshot.paramMap.get('flag');
    // this.media=[];
    // this.offset=0;
    // this.attended=[];
    // this.begin=0;
    // this.end=moment(this.current).format('yyyy-MM-DD');
    
    // this.start=moment().subtract(1,'months').format('yyyy-MM-DD');
    this.getModules();
    this.getMembers();
    this.getUser();
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/menu'],{replaceUrl:true}) ;
    }); 

  }
  back(){
    this.router.navigate(['/menu'],{replaceUrl:true}) ;
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    // this.offset=0
 }

 // get details of consumer
async getUser(){
  this.rv=await this.storage.get('RVSETTINGS');
  //console.log("rv",rv);
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_TYPE');
      const tok=await this.storage.get('TOKEN');
        const cid=await this.storage.get('COMPANY_ID');
        let token=tok;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    
    // this.createChart();
      if(data==5){
        let headers= await this.config.getHeader();
        // this.hideInvite=true;
        this.profile_view=false;
              const dat=await this.storage.get('RESIDENT_ID');
                const id=await this.storage.get('USER_ID');
                this.id=dat;
                this.cons_id=dat;
                this.res_id=id;
                this.res_branch=bid
                // this.media=[];
                // this.getMedia(this.id);
                // this.getActivities(this.id);
                // this.getActivityReport(id);
                this.getActivityHours();
                this.getMealsRefused();
                this.getVisitors();
                let url=this.config.domain_url+'consumer/'+this.id;
                console.log(url);

                // let headers= await this.config.getHeader();
                this.http.get(url,{headers}).subscribe((data:any)=>{
                  this.user=data.data.user;
                  this.room=data.data.room;
                  if(data.data.property_details&&data.data.property_detailscontract_details&&data.property_details.contract_details.property.location){
                  this.property_name=data.data.property_details.contract_details.property.location.name;
                  }
                  console.log("name:",this.property_name);
                  if(data.data.wing){
                  this.wing=data.data.wing.name;
                  }
                  console.log("data:",data);
                  console.log("room",this.room);
                  
                  
                  console.log(this.user);
                  // this.contacts=data.data.contacts;
                  // this.pending=data.data.pending_contacts;
                 
                  // this.user.forEach(el=>{
                    // this.user.user.forEach(ele=>{
                      // this.attended=this.user.attended_activity;
                      this.branch=data.data.user.company.c_name;
                      
                      console.log("branch:",this.branch);
                    // })
                  // })
                  // this.activity=data.user_activity;
                  // console.log("act:",this.activity);
                },error=>{
                  console.log(error);
                });
    
              //  });
              // });
            
      }else if(data==6){
        this.members=[];
        // this.getMembers();

        this.profile_view=true;
        // this.storage.ready().then(()=>{
        const id=await this.storage.get('USER_ID');
        const uid=await this.storage.get('RESIDENT_USER_ID');
        this.res_branch=await this.storage.get('BRANCH');
        let headers= await this.config.getHeader()
          this.id=id;
        
        let url=this.config.domain_url+'myfamily/'+this.id;
        // let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe(async (res:any)=>{
          console.log("fam:",res)
          res.data[0].family.forEach(ele => {
            this.members.push({parent:ele.parent_details[0],share:ele.share_details})
          })
          
            // let uid=res.data[0].family[0].parent_details[0].id;

           
            
            let id=res.data[0].family[0].parent_details[0].user_id;
            // this.res_branch=res.data[0].family[0].parent_details[0].wing.branch_id;
        this.cons_id=uid;
        if(res.data[0].family[0].share_details==0){
            console.log("no-access");
          }else{
            console.log("access")
        // this.getMedia(uid);
        // this.getActivities(uid);
        // this.getActivityReport(id);
          }
          
          // res.data.forEach(el => {
            


        console.log("firstmem:",this.members)
      
        this.res_id=res.data[0].family[0].parent_details[0].user_id;
        this.getActivityHours();
                this.getMealsRefused();
                this.getVisitors();
        // this.media=[];
        let headers= await this.config.getHeader()
        let url=this.config.domain_url+'consumer/'+uid;
        console.log(url);
        // let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((data:any)=>{
          this.user=data.data.user;
          this.room=data.data.room;
          if(data.data.wing){
          this.wing=data.data.wing.name
          }
            console.log("con:",data);
            
            // this.attended=data.attended_activity;
            this.branch=data.data.user.company.c_name;
            if(res.data[0].family[0].share_details==0){
              console.log("no-access");
            }else{
              console.log("access")
            // this.contacts=data.data.contacts;
            // this.activity=data.user_activity;
            // this.pending=data.data.pending_contacts;
            // console.log("att:",this.activity,"bran:",this.branch);
            //       this.contacts.forEach(ele=>{
            //         if(ele.is_primary==1){
            //           let primaryId=ele.contact_user_id;
            //           // this.storage.ready().then(()=>{
            //           //   this.storage.get("USER_ID").then(usr_id=>{
            //               if(this.id==primaryId){
            //                 this.hideInvite=true;
            //               }else{
            //                 this.hideInvite=false;
            //               }
            //             }
            //           })
                    }
                    })
                  
                  
         });
         
    
        // })
      // })
      // })
    }
//   })
// })
//       })
//     })
//   });
}

// get profiles of all the consumers related to the user
async getMembers(){
  // this.members=[];
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_TYPE');
      const tok=await this.storage.get('TOKEN');
        const cid=await this.storage.get('COMPANY_ID');
        let token=tok;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    // let headers=new HttpHeaders({
    //   'Authorization': 'Bearer'+token ,
    //   'Content-Type': 'application/json',
    //   'Accept': 'application/json',
    //   'branch_id':branch
    // });
      if(data==6){
        this.profile_view=true;
        const id=await this.storage.get('USER_ID');
          this.id=id;
          let headers= await this.config.getHeader()
        let url=this.config.domain_url+'myfamily/'+this.id;
        // let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log("res:",res)
          // res.data.forEach(el => {
          //   res.data[0].family.forEach(ele => {
          //     if(ele.is_primary==1){
          //       let primaryId=ele.contact_user_id;
          //       // this.storage.ready().then(()=>{
          //       //   this.storage.get("USER_ID").then(usr_id=>{
          //           if(this.id==primaryId){
          //             this.hideInvite=true;
          //           }else{
          //             this.hideInvite=false;
          //           }
          //         }
          //     // this.members.push(ele.parent_details[0])
              
          //   // });
          // });
          console.log("mem:",this.members);
          
          
          
        })
      // })
      }else if(data==5){
        this.profile_view=false;
      }

}




// switch to view consumer profile if more than one is present
async switchProfile(consumerId,resId){
  const cid=await this.storage.get('COMPANY_ID');
  const token=await this.storage.get('TOKEN');
  this.user=[];
  // this.contacts=[];
  // this.activity=[];
  // this.attended=[];
  // this.pending=[];
  // this.media=[];
  // this.offset=0;
  this.res_id=resId;
  this.cons_id=consumerId;
  this.getActivityHours();
  this.getMealsRefused();
  this.getVisitors();
      let headers=new HttpHeaders({'branch_id':this.res_branch.toString(),'company_id':cid.toString(),'Authorization': 'Bearer '+token })
    
  let url=this.config.domain_url+'consumer/'+consumerId;
        // let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((data:any)=>{
            this.user=data.data.user;
            console.log("con:",data,this.res_id,this.cons_id);
            // this.contacts=data.data.contacts;
            // this.pending=data.data.pending_contacts;
            // // this.attended=data.data.user.attended_activity;
            // this.branch=data.data.user.company.c_name;
            // this.activity=data.user_activity;
            this.room=data.data.room;
            if(data.data.wing){
              this.wing=data.data.wing.name
            }
            // console.log("att:",this.activity,"bran:",this.branch);
            // this.contacts.forEach(ele=>{
            //   if(ele.is_primary==1){
            //     let primaryId=ele.contact_user_id;
               
            //         if(this.id==primaryId){
            //           this.hideInvite=true;
            //         }else{
            //           this.hideInvite=false;
            //         }
            //       }
            //     })
            },error=>{
                console.log(error);
         });
       
}
async showConsumer(){
  const modal = await this.modalCntrl.create({
    component: PettycashConsumersComponent,
   
    componentProps: {
      data:this.members
    }
    
  });
  modal.onDidDismiss().then((dataReturned) => {
    console.log("dataRet:",dataReturned);
    if(dataReturned.data!=undefined||dataReturned.data!=null){
    // this.no_access=this.no_access;
    this.res_branch=dataReturned.data.parent.wing.branch_id;
      this.switchProfile(dataReturned.data.parent.id,dataReturned.data.parent.user_id);
     
      
      // this.cons_id=dataReturned.data.user_id
      // this.finish=20;
      // this.membername=dataReturned.data.user.name;
      // this.memberpic=dataReturned.data.user.profile_pic
    }
  })
  return await modal.present();
}
async getActivityHours(){
  const cid=await this.storage.get('COMPANY_ID');
  const token=await this.storage.get('TOKEN');
  let end=moment().format('YYYY-MM-DD');
  let start=moment(end).add(-30,'days').format('YYYY-MM-DD')
  let url=this.config.domain_url+'consumerAttendHoursReport';
  
  let headers=new HttpHeaders({'branch_id':this.res_branch.toString(),'company_id':cid.toString(),'Authorization': 'Bearer '+token ,})
  let body={
    user_id:this.res_id,
    start:start,
    end:end,
    one_to_one:0
  }
  console.log('actbody:',body)
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
     console.log('hour:',data)
     if(data.total_hours=='0.00'){
      this.hours=0
     }else{
     this.hours=data.total_hours
     }
      },error=>{
          console.log(error);
   });
}
async getMealsRefused(){
  let end=moment().format('YYYY-MM-DD');
  let start=moment(end).add(-30,'days').format('YYYY-MM-DD')
  let url=this.config.domain_url+'dining_report_range';
  const token=await this.storage.get('TOKEN');
  const cid=await this.storage.get('COMPANY_ID');
  let headers=new HttpHeaders({'branch_id':this.res_branch.toString(),'company_id':cid.toString(),'Authorization': 'Bearer '+token ,})
  let body={
    user_id:this.res_id,
    start:start,
    end:end,
   
  }
  console.log('dinebody:',body)
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
     console.log('dine:',data)
     this.meals=data.refused
      },error=>{
          console.log(error);
   });
}
async getVisitors(){
  let end=moment().format('YYYY-MM-DD');
  let start=moment(end).add(-30,'days').format('YYYY-MM-DD')
  let url=this.config.domain_url+'visitor_report_range';
  const token=await this.storage.get('TOKEN');
  const cid=await this.storage.get('COMPANY_ID');
  let headers=new HttpHeaders({'branch_id':this.res_branch.toString(),'company_id':cid.toString(),'Authorization': 'Bearer '+token ,})
  let body={
    user_id:this.res_id,
    start:start,
    end:end,
  
  }
  console.log('visbody:',body)
  this.http.post(url,body,{headers}).subscribe((data:any)=>{
     console.log('vis:',data)
     this.visitors=data.visitor_count
      },error=>{
          console.log(error);
   });
}
async getModules(){
  let headers= await this.config.getHeader();
  const cid=await this.storage.get('COMPANY_ID');
  
        let murl = this.config.domain_url + 'get_modules/' + cid;
        this.http.get(murl,{headers}).subscribe((mod: any) => {
          // this.show = true;
          this.modules = mod
          console.log("modules:", this.modules);

        })
}
}

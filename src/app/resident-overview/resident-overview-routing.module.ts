import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResidentOverviewPage } from './resident-overview.page';

const routes: Routes = [
  {
    path: '',
    component: ResidentOverviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResidentOverviewPageRoutingModule {}

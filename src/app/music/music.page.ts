import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ModalController } from '@ionic/angular';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { ShowfileComponent } from '../components/showfile/showfile.component';
import { Storage } from '@ionic/storage-angular';
@Component({
  selector: 'app-music',
  templateUrl: './music.page.html',
  styleUrls: ['./music.page.scss'],
})
export class MusicPage implements OnInit {
  subscription:Subscription;
 
  trustedVideoUrl: SafeResourceUrl;
  folder:any=[];
  files:any=[];
  vid=[];
  v_url:any;
  img:any;
  link:any;
  id:any;
  constructor(private domSanitizer: DomSanitizer,private config:HttpConfigService,private http:HttpClient,private platform:Platform,private router:Router,
    private streamingMedia:StreamingMedia,private iab: InAppBrowser,private modalCntlr:ModalController,private storage:Storage,
    private route:ActivatedRoute) { }

  ngOnInit() {
    
  }
  async ionViewWillEnter() {
    this.id=this.route.snapshot.paramMap.get('id');
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('TOKEN');
        let token=data;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    let headers= await this.config.getHeader()
    this.vid=[];
    let url=this.config.domain_url+'media/'+this.id;
    // let headers= await this.config.getHeader();
    this.http.get(url,{headers}).subscribe((res:any)=>{
      this.folder=res.media_fldr;
      this.files=res.media_files;
      console.log(res);


      for(let i of this.files){
        if(i.url!="null"){
        if((i.url).includes('www.youtube.com')){
          console.log("you",i.url);
          let yurl=(i.url).replace('watch?v=','embed/');
          this.v_url=yurl+'?enablejsapi=1'
          let img=(i.url).substr(i.url.lastIndexOf('=') + 1);
          this.img='http://img.youtube.com/vi/'+img+'/default.jpg';
          this.link='null';
        }else{
          this.v_url=i.url;
          this.img="assets/entertainment/music/mp3.svg"
        }
        // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.v_url);
        
      }else if(i.link!=null){
        this.v_url='null';
        this.link=i.link;
        this.img="assets/entertainment/music/mp3.svg"
      }else if(i.url=='null' && i.link==null){
        console.log("im:",i.image[0]);
        
        this.v_url=i.image[0].media_image;
        this.link='null';
        this.img="assets/entertainment/music/mp3.svg"
      }
      let video={url:this.v_url,title:i.title,id:i.id,img:this.img,link:this.link};
        this.vid.push(video);
      }
      console.log("video:",this.vid);
      
    })
//   })
// })
//     })
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
      this.router.navigate(['/entertainment']) ;
  }); 
  }

  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    // let listaFrames = document.getElementsByTagName("iframe");
    // for (var index = 0; index < listaFrames.length; index++) {
    //   let iframe = listaFrames[index].contentWindow;
    //   iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
    // }
  }
  openFolder(item){
  
    this.router.navigate(['open-media',{flag:5,id:item.id,title:item.title,mid:this.id}])
  }

  play(item){
    // window.open(encodeURI(url),"_system","location=yes");
    if(item.url!='null'){
    let options:InAppBrowserOptions ={
      location:'yes',
    hidenavigationbuttons:'yes',
        hideurlbar:'yes',
        zoom:'no'
    }
    console.log(item.url);
    screen.orientation.lock('landscape');
    const browser = this.iab.create(item.url,'_blank',options);
    browser.on('exit').subscribe(()=>{
      screen.orientation.unlock();
      screen.orientation.lock('portrait');
    })
  }else {
    this.openModal(item.link);
  }
  }
  async openModal(link){
    const modal = await this.modalCntlr.create({
      component: ShowfileComponent,
      componentProps: {
        link:link,
        
         
      },
      showBackdrop:false,
      cssClass:'audioModal'
    });
    return await modal.present();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DinChooseSeasonDatePageRoutingModule } from './din-choose-season-date-routing.module';

import { DinChooseSeasonDatePage } from './din-choose-season-date.page';
import { ApplicationPipesModule } from '../application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DinChooseSeasonDatePageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [DinChooseSeasonDatePage]
})
export class DinChooseSeasonDatePageModule {}

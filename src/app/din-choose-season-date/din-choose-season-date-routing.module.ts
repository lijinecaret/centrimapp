import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DinChooseSeasonDatePage } from './din-choose-season-date.page';

const routes: Routes = [
  {
    path: '',
    component: DinChooseSeasonDatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DinChooseSeasonDatePageRoutingModule {}

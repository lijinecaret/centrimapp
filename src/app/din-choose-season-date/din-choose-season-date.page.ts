import { DOCUMENT } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IonContent, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { HttpConfigService } from '../services/http-config.service';

@Component({
  selector: 'app-din-choose-season-date',
  templateUrl: './din-choose-season-date.page.html',
  styleUrls: ['./din-choose-season-date.page.scss'],
})
export class DinChooseSeasonDatePage implements OnInit {
  subscription:Subscription;
  menu:any;
  currentDate:any;
  start:any;
  end:any;
  dates:any[]=[];
  hide:boolean=false;
  flag:any;
  progress:any=[];


  @ViewChild(IonContent, { static: false }) content: IonContent;
    constructor(private router:Router,private platform:Platform,private route:ActivatedRoute,
      @Inject(DOCUMENT) private document: Document,private http:HttpClient,
      private storage:Storage,private config:HttpConfigService) { }

  ngOnInit() {
  }
  async ionViewWillEnter(){
    this.dates=[];
    let menu=this.route.snapshot.paramMap.get('menu')
    this.menu=JSON.parse(menu);
    this.flag=this.route.snapshot.paramMap.get('flag');
    console.log('menu:',this.menu,menu);
    this.getDiningDetails();
    this.currentDate=moment().format('DD MMM YYYY');
    
    this.start=moment(this.menu.start_date);
    this.end=moment(this.menu.enddate);
    var start=this.start.clone();
   
    while(start.isSameOrBefore(this.end)){
      this.dates.push(start.format('DD MMM YYYY'));
      start.add(1,'days');
    }
    // setTimeout(()=>{
    //   var titleELe = this.document.getElementById(this.currentDate);
    //   console.log(titleELe,titleELe.offsetHeight,titleELe.offsetTop)
    //   this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    // },500)
    
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{ 
   this.back();
   }); 
  }

  ionViewWillLeave() { 
    this.subscription.unsubscribe();
    }
    back(){
     this.router.navigate(['/dining-menu']);
    }

    async getDiningDetails(){
   
      const cid=await this.storage.get('COMPANY_ID');
      const bid=await this.storage.get('BRANCH');
      const tz=await this.storage.get('TIMEZONE');
      let headers= await this.config.getHeader();
      let url=this.config.domain_url+'get_dining_ordering_details';
      const type=await this.storage.get('USER_TYPE');
      const uid=await this.storage.get('USER_ID');
      let consumer;
      if(type==5){
        consumer=uid
      }else{
        consumer= await this.storage.get('RESIDENT_ID');
      }
      let body={
        company_id:cid,
        dining_id:this.menu.id,
        period_status:true,
        consumer_user_id:consumer
      }
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log('dining:',res);
        this.progress=res.data.period_progress;
        setTimeout(()=>{
          var titleELe = this.document.getElementById(this.currentDate);
          console.log(titleELe,titleELe.offsetHeight,titleELe.offsetTop)
          this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
          
        },500)
      },error=>{
        
      });
    }
    setIndicatorLine(item){
      let date=moment(item).format('yyyy-MM-DD');
      
      const idx=this.progress.map(x=>x.date).indexOf(date);
      
      let i;
      if(this.progress[idx].color=='x-danger'){
        i=0
      }else if(this.progress[idx].color=='x-success'){
        i=1
      }else if(this.progress[idx].color=='x-warning'){
        i=2
      }else if(this.progress[idx].color=='x-info'){
        i=3
      }
      
       return i;
     
       
     
    }

    next(item){
      if(this.flag==1){
        this.router.navigate(['/dining-take-order',{menu:JSON.stringify(this.menu),date:item,flag:this.flag}],{replaceUrl:true})
      }else{
        this.router.navigate(['/dining-view-menu',{menu:JSON.stringify(this.menu),date:item,flag:this.flag}],{replaceUrl:true})
      }

    }
    hideLabel(){
      this.hide=!this.hide;
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinChooseSeasonDatePage } from './din-choose-season-date.page';

describe('DinChooseSeasonDatePage', () => {
  let component: DinChooseSeasonDatePage;
  let fixture: ComponentFixture<DinChooseSeasonDatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinChooseSeasonDatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinChooseSeasonDatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

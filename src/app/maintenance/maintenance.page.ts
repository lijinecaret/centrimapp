import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { Router } from '@angular/router';
import { LoadingController, Platform, PopoverController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { MaintenanceFilterComponent } from '../components/maintenance-filter/maintenance-filter.component';

@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.page.html',
  styleUrls: ['./maintenance.page.scss'],
})
export class MaintenancePage implements OnInit {
  request: any = [];
  subscription: Subscription;
  sel_filter: any;
  sel_id: any;
  type: any[] = [];
  limit = 21;
  index: any = 0;
  terms:any;
  hideLoader:boolean=false;
  constructor(private http: HttpClient, private storage: Storage, private config: HttpConfigService, private router: Router,
    private platform: Platform, private popoverCntlr: PopoverController,private loadingCtrl:LoadingController) { }

  ngOnInit() {
  }


  ionViewWillEnter() {
    this.showLoading();
    this.request = [];
    this.limit = 21;
    this.index = 0;
    this.hideLoader=false;
    this.terms=undefined;
    
    this.getRequests(false, "");
    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.router.navigate(['/menu']);
    });
  }

  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      duration: 3000
    });
    return await loading.present();
}

  async dismissLoader() {
      return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  }
  async getRequests(isFirstLoad, event) {
    // this.storage.ready().then(() => {
     const rv=await this.storage.get('RES-RV-SETTING');
      const data=await this.storage.get('BRANCH');
        const uid=await this.storage.get('USER_ID');
          const type=await this.storage.get('USER_TYPE');
            // let url=this.config.domain_url+'get_maintenance_requests/'+uid;
            let url = this.config.domain_url + 'maintenance_filter';
            let id = data.toString();
            let headers= await this.config.getHeader()
           
            let body;
            body = {
              status: 'unresolved',
              offset: 0,   // always 0
              limit: this.limit,
              role_id: type,
              

            }
            if(type==5&&rv==1){
              body.created_for=uid
            }else{
              body.created_by=uid
            }
            console.log("head:", url,body);
            this.http.post(url,body, {headers}).subscribe((res: any) => {
              console.log(res);
              // this.request=res.data;
              for (let i = this.index; i < res.data.length; i++) {
                this.request.push(res.data[i]);
              }
              this.type = res.type;
              if (isFirstLoad)
                event.target.complete();

              this.limit = this.limit + 21;
              this.index = this.index + 21;
              this.dismissLoader();
            }, error => {
              console.log(error);
                this.dismissLoader();
            })
    //       })
    //     })
    //   })
    // })
  }


  doInfinite(event) {
    console.log('loading more open jobs')
    this.getRequests(true, event)
  }

  gotoDetails(id) {
    this.router.navigate(['/consumer-maintenance-details', { req_id: id }],{replaceUrl:true})
  }


  async search(){
    // this.filter=true;
    console.log("terms:",this.terms);
    
    if(this.terms==''||this.terms==null){
      // this.ionViewWillEnter();
      this.request = [];
    this.limit = 21;
    this.index = 0;
    this.hideLoader=false;
    this.terms=undefined;
    
    this.getRequests(false, "");
    }else{
      this.hideLoader=true;
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('USER_ID');
        const bid=await this.storage.get('BRANCH');
          const type=await this.storage.get('USER_TYPE');
           
            // let url;
            let url=this.config.domain_url+'maintenance_filter';
          
            let headers= await this.config.getHeader()
              // console.log("nead:",headers);
              let body={
              
                //  in case of technician login
                offset : 0,   // always 0
               search:this.terms,
               created_by:data
              }
              
            this.http.post(url,body,{headers}).subscribe((res:any)=>{
              console.log("requests:",res.data);
              this.type=res.type;
              // if(this.filter==3){
                this.request=res.data;
               
              //   // this.loader.dismissLoader();
              // }else{
              // for (let i = 0; i < res.data.length; i++) {
              //   this.request.push(res.data[i]);
              // }
              
              // this.request=res.data.data;
              
                // this.tec=res.technician;
                console.log(res);
                // this.loader.dismissLoader();
               
    
               
              // }
            },error=>{
              // this.loader.dismissLoader();
              console.log(error);
            })
          
          




    //       })
    //     })
    //   })
    // })
  }
  }
  cancel(){
    // this.hide=false;
   
    // this.ionViewWillEnter();
    this.request = [];
    this.limit = 21;
    this.index = 0;
    this.hideLoader=false;
    this.terms=undefined;
    
    this.getRequests(false, "");
  }
  async filterRequest(ev) {
    // open filter

    const popover = await this.popoverCntlr.create({
      component: MaintenanceFilterComponent,
      event: ev,
      backdropDismiss: true,
      cssClass: 'maintenance_filter',
      componentProps: {

        flag: 2
      },

      // translucent: true
    });
    popover.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.sel_filter = dataReturned.data.filter;
        this.sel_id = dataReturned.data.id;
        console.log("fil_data:", dataReturned);
        //alert('Modal Sent Data :'+ dataReturned);
        this.filterContent();
      }
    });
    return await popover.present();
  }
  async filterContent() {

    // this.storage.ready().then(() => {
      const data=await this.storage.get('BRANCH');
        const uid=await this.storage.get('USER_ID');


          let url;
          console.log("filtering");
          if (this.sel_filter == "All") {
            // this.ionViewWillEnter();
            this.request = [];
    this.limit = 21;
    this.index = 0;
    this.hideLoader=false;
    this.terms=undefined;
    
    this.getRequests(false, "");
          } else if (this.sel_filter == "Status") {
            url = this.config.domain_url + 'get_maintenance_requests?status=' + this.sel_id;
          } else if (this.sel_filter == "Date") {
            let current = new Date();
            let later;
            if (this.sel_id == 0) {
              later = current
            } else if (this.sel_id == 1) {
              later = new Date(Date.now() - 1 * 24 * 60 * 60 * 1000);
            } else if (this.sel_id == 2) {
              later = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);
            } else if (this.sel_id == 3) {
              later = new Date(Date.now() - 30 * 24 * 60 * 60 * 1000);
            }
            let to_date = current.getFullYear() + '-' + this.fixDigit((current.getMonth() + 1)) + '-' + this.fixDigit(current.getDate());
            let from_date = later.getFullYear() + '-' + this.fixDigit((later.getMonth() + 1)) + '-' + this.fixDigit(later.getDate());

            url = this.config.domain_url + 'get_maintenance_requests?sdate=' + from_date + '&edate=' + to_date;

          } else if (this.sel_filter == "Priority") {
            url = this.config.domain_url + 'filter_by_priority?type=' + this.sel_id;
          }

          let id = data.toString();
          let headers= await this.config.getHeader()
          console.log("head:", url);

          this.http.get(url, { headers }).subscribe((res: any) => {
            console.log(res);
            this.request = res.data;
          }, error => {
            console.log(error);

          })

    //     })
    //   })
    // })

  }


  fixDigit(val) {
    return val.toString().length === 1 ? "0" + val : val;
  }
  createReq() {
    this.router.navigate(['/create-maintenance-request', { type: JSON.stringify(this.type) }],{replaceUrl:true})
  }
}

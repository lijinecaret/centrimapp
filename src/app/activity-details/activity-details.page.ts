import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { LoadingController, ModalController, Platform, PopoverController } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { HttpConfigService } from '../services/http-config.service';
import { Subscription } from 'rxjs';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ActivityImageComponent } from '../components/activity-image/activity-image.component';
import { ViewpdfComponent } from '../components/viewpdf/viewpdf.component';
// import { Jitsi } from 'capacitor-jitsi-meet';
// declare var jitsiplugin;
@Component({
  selector: 'app-activity-details',
  templateUrl: './activity-details.page.html',
  styleUrls: ['./activity-details.page.scss'],
})
export class ActivityDetailsPage implements OnInit {
act_id:any;
token:any;
activity:any=[];
doc:any=[];
type:any={};
date:string;
day:any;
month:any;
year:any;
subscription:Subscription;
contentStyle={top:'75px'};
image:any=[];
photos:any=[];
cid:any[]=[];
assigned:boolean=false;
signupcount:any;
waitinglist:boolean=false;
rvsettings:any;
  constructor(private route:ActivatedRoute,private http:HttpClient,private storage:Storage,private platform:Platform,
   private file:File,private config:HttpConfigService,private modalCntlr:ModalController,private loadingCtrl:LoadingController,
    private router:Router,private iab:InAppBrowser,private dialog:SpinnerDialog,private popCntlr:PopoverController) { }

  ngOnInit() {


    if(this.platform.is('android')){
      this.contentStyle.top='75px'
    }else if(this.platform.is('ios')){
      this.contentStyle.top='95px'
    }



    
  }
  
  async getContent(){
    // this.storage.ready().then(()=>{
      const uid=await this.storage.get('USER_ID');
        const utype=await this.storage.get('USER_TYPE');
        this.doc=[];
       
      const data=await this.storage.get('TOKEN');
        let token=data;
        const bid=await this.storage.get('BRANCH');
          let branch=bid.toString();
    
    let url=this.config.domain_url+'activity/'+this.act_id;
    let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.activity=res.activity;
        this.image=this.activity.images;
        this.date=this.activity.activity_start_date;
        this.photos=this.activity.activity_photos;
        this.year=this.date.slice(0,4);
        let x= this.date.slice(5,7);
        if(x=='01'){
          this.month="Jan";
        }
        
        else if(x=='02'){
          this.month="Feb";
        }
        else if(x=='03'){
          this.month="Mar";
        }
        else if(x=='04'){
          this.month="Apr";
        }
        else if(x=='05'){
          this.month="May";
        }
        else if(x=='06'){
          this.month="Jun";
        }
        else if(x=='07'){
          this.month="Jul";
        }
        else if(x=='08'){
          this.month="Aug";
        }
        else if(x=='09'){
          this.month="Sep";
        }
        else if(x=='10'){
          this.month="October";
        }
        else if(x=='11'){
          this.month="Nov";
        }
        else if(x=='12'){
          this.month="Dec";
        }
        this.day=this.date.slice(8,10);
        this.activity.attachments.forEach(el=>{
          let attch_name=el.activity_attachment.substr(el.activity_attachment.lastIndexOf('_') + 1);
          let ext=el.activity_attachment.substr(el.activity_attachment.lastIndexOf('.') + 1);
          this.doc.push({'title':attch_name,'post_attachment':el.activity_attachment,'ext':ext});
        })
        
        console.log("doc:",this.doc);
        this.type=this.activity.type;
        this.dismissLoader();
      },error=>{
        console.log(error);
        this.dismissLoader();
      })
      // let cid;
      if(utype==5){
        this.cid=null;
        this.assigned_or_not();
        this.signupList();
      }else{
        let url=this.config.domain_url+'profile/'+uid;
        let headers= await this.config.getHeader();
          this.http.get(url,{headers}).subscribe((res:any)=>{
            console.log('prores:',res);
            res.data.family.forEach(element => {
              if(element.parent_details[0].status==1){
              this.cid.push(element.resident_user_id);
              }
            });
            // this.cid=res.data.family[0].parent_details[0].user_id;
              this.assigned_or_not();

              console.log('profilefam:',this.cid);
              this.signupList();
          })
      }
      
//     })
//   })
// })
// })
// })
  }
async assigned_or_not(){
  // this.storage.ready().then(()=>{
    const uid=await this.storage.get('USER_ID');
      const utype=await this.storage.get('USER_TYPE');
  let headers=await this.config.getHeader();
  let url1=this.config.domain_url+'activity_attended_or_not';
      let body1={
        activity_id:this.act_id,
        user_id:uid,
        consumer_id:this.cid
      }
      console.log('body1:',body1)
      this.http.post(url1,body1,{headers}).subscribe((res:any)=>{
        console.log("ass:",res);
        if(utype==5){
          if(res.data.assigned_consumer==true){
            this.assigned=true
          }
        }else{
          if(res.data.assigned_consumer==true||res.data.assigned_family==true){
            this.assigned=true;
          }
        }
        
      })
//     })
//   })
// })
}
// open document
openDoc(item){
  // window.open(encodeURI(item.activity_attachment),"_system","location=yes");


  let options:InAppBrowserOptions ={
    location:'yes',
    hidenavigationbuttons:'yes',
    hideurlbar:'yes',
    zoom:'yes',
  beforeload:'yes',
      clearcache:'yes'
  }
  this.platform.ready().then(() => {
    if(item.post_attachment.includes('.pdf')){
      this.showpdf(item.post_attachment)
  }else{
  const browser = this.iab.create('https://docs.google.com/viewer?url='+item.post_attachment+'&embedded=true','_blank',options);
  this.dialog.show();
  browser.on('loadstart').subscribe(() => {
    console.log('start');
    this.dialog.hide();   
   
  }, err => {
    console.log(err);
    
    this.dialog.hide();
  })

  browser.on('loadstop').subscribe(()=>{
    console.log('stop');
    
    this.dialog.hide();;
  }, err =>{
    this.dialog.hide();
  })

  browser.on('loaderror').subscribe(()=>{
    this.dialog.hide();
  }, err =>{
    this.dialog.hide();
  })
  
  browser.on('exit').subscribe(()=>{
    this.dialog.hide();
  }, err =>{
    this.dialog.hide();
  })
  }
  })
  // let path = null;
  //     if(this.platform.is('ios')){
  //       path=this.file.documentsDirectory;
  //     }
  //     else if(this.platform.is('android')){
  //       path=this.file.externalDataDirectory;
  //     }
  //       const fileTransfer: FileTransferObject=this.transfer.create();
  //       fileTransfer.download(item.activity_attachment,path+'Activity report').then(entry=>{
  //         console.log("download");
          
  //         let url=entry.toURL();
  //         console.log("url",url);
          
  //         var fileExtension = item.activity_attachment.substr(item.activity_attachment.lastIndexOf('.') + 1);
  //         let fileMIMEType=this.getMIMEtype(fileExtension);
  //       // if(fileExtension=='pdf'){
  //       this.fileOpener.open(url,fileMIMEType);
  //       // }
  //       // else if(fileExtension=='doc'){
  //       //   this.fileOpener.open(url,'application/msword');
  //       // }
  //       });
}



getMIMEtype(extn){
  let ext=extn.toLowerCase();
  let MIMETypes={
    'txt' :'text/plain',
    'docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'doc' : 'application/msword',
    'pdf' : 'application/pdf',
    'jpg' : 'image/jpeg',
    'bmp' : 'image/bmp',
    'png' : 'image/png',
    'xls' : 'application/vnd.ms-excel',
    'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'rtf' : 'application/rtf',
    'ppt' : 'application/vnd.ms-powerpoint',
    'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
  }
  return MIMETypes[ext];
}
async ionViewWillEnter(){
  this.showLoading();
  this.cid=[];
   //get activity id through navigation 
   this.act_id=this.route.snapshot.paramMap.get('act_id');
   this.getContent();
   
  
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/activities']) ;
  }); 
  const rv=await this.storage.get('RVSETTINGS');
    this.rvsettings=rv
  // this.cid=[];

}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}


async start(item){
  console.log("url:",item);


  // const roomId =item.substr(item.lastIndexOf('/') + 1);

  // const result = await Jitsi.joinConference({
  //   // required parameters
    
  //   roomName: roomId, // room identifier for the conference
  //   url: 'https://meet.jit.si', // endpoint of the Jitsi Meet video bridge

  //   // recommended settings for production build. see full list of featureFlags in the official Jitsi Meet SDK documentation
  //   featureFlags: {
  //       'prejoinpage.enabled': false, // go straight to the meeting and do not show the pre-join page
  //       'recording.enabled': false, // disable as it requires Dropbox integration
  //       'live-streaming.enabled': false, // 'sign in on Google' button not yet functional
  //       'android.screensharing.enabled': false, // experimental feature, not fully production ready
  //   },
  // })
 
//  if(this.platform.is('android')){
//   const roomId =item.substr(item.lastIndexOf('/') + 1);

//   // const roomId = 'VqyaB3flP';
//   jitsiplugin.loadURL('https://meet.jit.si/' + roomId, roomId, false, function (data) {
//     if (data === "CONFERENCE_WILL_LEAVE") {
//         jitsiplugin.destroy(function (data) {
//             // call finished
//         }, function (err) {
//             console.log(err);
//         });
//     }
// }, function (err) {
//     console.log(err);
// });
//  }else {
  let options:InAppBrowserOptions ={
    location:'no',
    hideurlbar:'yes',
    zoom:'no'
  }
  
  const browser = this.iab.create(item,'_system',options);
  
//  }

}
videocall(html,ev){
  if(this.platform.is('android')){
    ev.preventDefault();
  if(this.type.id==9){
    var regex = /href\s*=\s*(['"])(https?:\/\/.+?)\1/ig;
    var link;
  
    while((link = regex.exec(html)) !== null) {
console.log('link:',link[2]);
if(link[2].includes('calllink')){
  ev.preventDefault();
this.start(link[2]);
      // html = html.replace(link[2], "https://ctrlq.org?redirect_to" + encodeURIComponent(link[2]));
    }
    }
    // return html;
  }
}
}

async showImages(ev: any,i) {
  let img;
  let flag;
if(i==1){
img=this.image;
flag=2
}else{
  img=this.photos;
  flag=3
}
  const popover = await this.popCntlr.create({
    component: ActivityImageComponent,
    event:ev,
    cssClass:'image_pop',
    componentProps:{
      data:img,
      flag:flag
    },
    
  });
  return await popover.present();
}

async signupList(){
  const uid=await this.storage.get('USER_ID');
  const res_id=await this.storage.get('RESIDENT_ID');
  const utype=await this.storage.get('USER_TYPE');
  let user;
  if(utype==5){
    user=uid
  }else{
    user=res_id
  }
  let url=this.config.domain_url+'activity_signup_list/'+this.act_id;
    let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log('signup:',res,this.cid);
        if(res.data.signup&&res.data.signup.length){
          this.signupcount=res.data.signup.length
        }else{
          this.signupcount=0
        }
        if(res.data.waiting_list&&res.data.waiting_list.length){
          res.data.waiting_list.map(x=>{
            // if(this.cid&&this.cid.includes(x.user_id)){
            //   this.waitinglist=true
            // }
            if(user==x.user_id){
              this.waitinglist=true
            }
          })
          console.log('res_id:',res_id,this.waitinglist);
          
        }
      })
}

async showpdf(file){
  const modal = await this.modalCntlr.create({
    component: ViewpdfComponent,
    cssClass:'fullWidthModal',
    componentProps: {
      
      data:file,
      
       
    },
    
    
  });
  return await modal.present();
}
async showLoading() {
  const loading = await this.loadingCtrl.create({
    cssClass: 'custom-loading',
    // message: '<ion-img src="assets/loader.gif"></ion-img>',
    spinner: null,
    // duration: 3000
  });
  return await loading.present();
}

async dismissLoader() {
    return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}
}

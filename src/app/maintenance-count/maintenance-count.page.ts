import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from '../services/http-config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-maintenance-count',
  templateUrl: './maintenance-count.page.html',
  styleUrls: ['./maintenance-count.page.scss'],
})
export class MaintenanceCountPage implements OnInit {
request:any={};
subscription:Subscription;
  constructor(private http:HttpClient,private storage:Storage,private config:HttpConfigService,private router:Router,
    private platform:Platform) { }

  ngOnInit() {
  }
async ionViewWillEnter(){
  this.request=[];
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');
    let headers= await this.config.getHeader();
      let url=this.config.domain_url+'get_maintenance_request_technician?user_id='+data;
      // let id=data.toString();
      // let headers=new HttpHeaders({'branch_id':id});
      // console.log("head:",headers);
      // let body={user_id:}
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log(res);
        this.request=res;
      },error=>{
        console.log(error);
        
      })
  //   })
  // })
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    this.router.navigate(['/menu']) ;
  }); 
}
gotoDetails(){
  this.router.navigate(['/maintenance-list',{filter:1}])
}
gotoDetailsByStatus(id){
  this.router.navigate(['/maintenance-list',{filter:2,data:id}])
}
gotoDetailsByPriority(id){
  this.router.navigate(['/maintenance-list',{filter:3,data:id}])
}
}

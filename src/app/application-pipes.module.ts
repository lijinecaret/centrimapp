import { NgModule } from '@angular/core';
import { DateformatPipe } from './pipes/dateformat.pipe';
import { ResourcesearchPipe } from './pipes/resourcesearch.pipe';
import { MaintenancesearchPipe } from './pipes/maintenancesearch.pipe';
import { LocationsearchPipe } from './pipes/locationsearch.pipe';
import { ExpandableComponent } from './components/expandable/expandable.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { DebounceDirective } from './directives/debounce.directive';
import { MessagesearchPipe } from './pipes/messagesearch.pipe';

@NgModule({
    imports: [
      // dep modules
      // FileUploadModule
     
      // PdfViewerModule
      
    ],
    declarations: [ 
      DateformatPipe,
      ResourcesearchPipe,
      MaintenancesearchPipe,
      LocationsearchPipe,
      MessagesearchPipe,
      ExpandableComponent,
      DebounceDirective,
      // PdfViewerModule
      // MultiFileUploadComponent
    
    ],
    exports: [
      DateformatPipe,
      ResourcesearchPipe,
      MaintenancesearchPipe,
      LocationsearchPipe,
      MessagesearchPipe,
      ExpandableComponent,
      PdfViewerModule,
      DebounceDirective,
      // MultiFileUploadComponent
      
    ]
  })
  export class ApplicationPipesModule {}
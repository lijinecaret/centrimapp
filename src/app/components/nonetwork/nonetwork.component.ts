import { Component, OnInit } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-nonetwork',
  templateUrl: './nonetwork.component.html',
  styleUrls: ['./nonetwork.component.scss'],
})
export class NonetworkComponent implements OnInit {

  constructor(private network:Network,private modalCntrl:ModalController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      // We just got a connection but we need to wait briefly
       // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        // if (this.network.type === 'wifi') {
        //   console.log('we got a wifi connection, woohoo!');
        // }
        this.modalCntrl.dismiss();
      }, 3000);
    });
  }

}

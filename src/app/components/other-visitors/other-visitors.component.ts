import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-other-visitors',
  templateUrl: './other-visitors.component.html',
  styleUrls: ['./other-visitors.component.scss'],
})
export class OtherVisitorsComponent implements OnInit {
  anArray:any=[];
  @Input() more;
  constructor(private toastCntlr:ToastController,private modalCntl:ModalController) { }

  ngOnInit() {}


  ionViewWillEnter(){
    this.anArray=[];
    console.log('more:',this.more);
    if(this.more==undefined||this.more=='undefined'||this.more==null||!this.more.length){
      this.anArray.push({'value':''});
    }else{
      this.more.forEach(element => {
        this.anArray.push({'value':element})
      });
  }
}
Add(){
  
      this.anArray.push({'value':''});
   
  
  }

  next(){
    this.more=[];
   console.log("person:",this.anArray);
 
     if(this.anArray[0].value==''){
       this.presentAlert('Please fill the field.');
     }else{
     this.anArray.forEach(element => {
       if(element.value!=''){
           this.more.push(element.value);
       }
     });
     const onClosedData=this.more;
   this.modalCntl.dismiss(onClosedData);
    //  this.confirm();
   }
     
  
   
   
 }

 async presentAlert(mes){
  const alert = await this.toastCntlr.create({
    message: mes,
    cssClass:'toastStyle',
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}
cancel(){
  this.modalCntl.dismiss();
}
}

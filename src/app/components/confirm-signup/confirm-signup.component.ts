import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';


@Component({
  selector: 'app-confirm-signup',
  templateUrl: './confirm-signup.component.html',
  styleUrls: ['./confirm-signup.component.scss'],
})
export class ConfirmSignupComponent implements OnInit {
@Input() item:any;
contacts:any[]=[];
selected:any[]=[];
utype:any;
companian:any[]=[];
  constructor(private modalCntl: ModalController,private storage:Storage,private http:HttpClient,private config:HttpConfigService,
    private toastCntl:ToastController) { }

  ngOnInit() {}

  async ionViewWillEnter(){
    console.log('item:',this.item);
    this.utype=await this.storage.get('USER_TYPE');
    this.contacts=[];
    this.selected=[];
    if(this.utype==6){
    this.getFamily();
    }else{
      this.getCompanian();
    }
  }

  dismiss(){
    this.modalCntl.dismiss();
  }
  async getFamily(){
    // this.storage.ready().then(()=>{
      const uid=await this.storage.get('USER_ID');
        const cid=await this.storage.get('COMPANY_ID');
          const bid=await this.storage.get('BRANCH');
          let url=this.config.domain_url+'showfamily/'+uid;
          let headers=new HttpHeaders({'branch':bid,'company_id':cid.toString()});
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log("parent:",res);
          
          res.data.forEach(ele=>{
            ele.family.forEach(element => {
              if(element.parent_details[0].branches.user_branch.retirement_living==1){
              this.contacts.push({parent:element.parent_details[0],share:element.share_details})
              }
            });
          })
         
        })

    console.log("arr:",this.contacts);
    
  }


  choose(item,i){
    console.log("choose:",item)
    if(i==1){
    if(item.share==0){
      this.presentAlert("You don't have access to this profile right now.")
    }else if(item.parent.status==0){
      this.presentAlert("This resident is inactive.")
    }else{
    
      console.log('sele:',this.selected,item.parent.user.user_id);
      
      if(this.selected.includes(item.parent.user.user_id)){
        const idx=this.selected.indexOf(item.parent.user.user_id);
        this.selected.splice(idx,1)
      }else{
        this.selected.push(item.parent.user.user_id)
      }
    }
  }else{
    if(item.user_details.status==0){
      this.presentAlert("This resident is inactive.")
    }else{
    
     
      
      if(this.selected.includes(item.user_details.user_id)){
        const idx=this.selected.indexOf(item.user_details.user_id);
        this.selected.splice(idx,1)
      }else{
        this.selected.push(item.user_details.user_id)
      }
    }
  }
  
  }

  async presentAlert(mes) {
    const alert = await this.toastCntl.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 3000,
      position:'top'       
    });
    alert.present(); //update
  }

  async confirm(){
    if(this.utype==6&&this.selected.length<1||this.utype==5&&this.companian.length&&this.selected.length<1){
      this.presentAlert('Please select any participant');
    }else if(this.utype==5&&!this.companian.length){
      const uid=await this.storage.get('USER_ID');
      const data:any=[uid];
      this.modalCntl.dismiss(data)
    }else{
      const data:any=this.selected;
      this.modalCntl.dismiss(data)
    }
  }

 async getCompanian(){
    const data=await this.storage.get('USER_ID');
    let url=this.config.domain_url+'profile/'+data;
    let headers= await this.config.getHeader();
      this.http.get(url,{headers}).subscribe((res:any)=>{
        console.log("get:",res,url);
        if(res.data.user_details.resident_details.property_details&&
          res.data.user_details.resident_details.property_details.contract_details.all_residents&&
          res.data.user_details.resident_details.property_details.contract_details.all_residents.length>1){
            this.companian=res.data.user_details.resident_details.property_details.contract_details.all_residents
          }
      })
  }
}

import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, PopoverController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { VisitBookingRescheduleComponent } from '../visit-booking-reschedule/visit-booking-reschedule.component';

@Component({
  selector: 'app-visit-booking-options',
  templateUrl: './visit-booking-options.component.html',
  styleUrls: ['./visit-booking-options.component.scss'],
})
export class VisitBookingOptionsComponent implements OnInit {
@Input() data;
  constructor(private http:HttpClient,private alertCnlr:AlertController,private modalCntl:ModalController,
    private config:HttpConfigService,private popCntl:PopoverController) { }

  ngOnInit() {}


  cancel(){
    this.presentAlertConfirm();
    }
    
    
    async presentAlertConfirm() {
      const alert = await this.alertCnlr.create({
        header: 'Cancel booking',
        message: 'Are you sure you want to cancel this booking ?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            // cssClass: 'secondary',
            handler: (blah) => {
              this.popCntl.dismiss()
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Yes',
            handler: () => {
             
              this.cancelBooking();
            }
          }
        ]
      });
    
      await alert.present();
    }
    
      cancelBooking(){
    
        let url=this.config.domain_url+'delete_visit_booking/'+this.data.id;
        // let headers= await this.config.getHeader();
    
        this.http.delete(url).subscribe((res:any)=>{
          console.log(res);
    
          this.popCntl.dismiss(1);
        })
    
      }

      async reschedule(){
        const modal = await this.modalCntl.create({
          component: VisitBookingRescheduleComponent,
         
          componentProps: {
            
            data:this.data
      
          },
          
        });
        modal.onDidDismiss().then((dataReturned)=>{
          if(dataReturned.data){
          this.popCntl.dismiss(1);
          }else{
            this.popCntl.dismiss();
          }
        })
        return await modal.present();
      }
}

import { Component, Input, NgModule, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { ModalController, NavParams } from '@ionic/angular';
import { PdfViewerModule } from 'ng2-pdf-viewer';
// @NgModule({
//   imports: [
//     PdfViewerModule
//   ],
// })
@Component({
  selector: 'app-viewpdf',
  templateUrl: './viewpdf.component.html',
  styleUrls: ['./viewpdf.component.scss'],
})
export class ViewpdfComponent implements OnInit {
  // data:SafeResourceUrl;
  @Input() data:any;
  pdfSource:any;
  zoom_to:any=1;
  constructor(private domSanitizer: DomSanitizer,private modalCntl:ModalController,private navParams: NavParams) { }

  ngOnInit() {
    // this.data='https://docs.google.com/gview?embedded=true&url='+this.data
  }
  dismissModal(){
    this.modalCntl.dismiss();
  }
  ionViewWillEnter(){
    // this.data=this.data+'?origin='+ window.location.host
    // this.pdfSource='https://app.centrimlife.uk/public/media/resource/1594786364_faith-in-time-%20of-pandemic_v2.pdf'
    this.pdfSource=this.data
    // this.data=this.domSanitizer.bypassSecurityTrustResourceUrl(this.navParams.data.data);
    
    console.log('data:',this.data)
  }
  zoomIn() {
    this.zoom_to = this.zoom_to + 0.25;
  }

  zoomOut() {
    if (this.zoom_to > 1) {
       this.zoom_to = this.zoom_to - 0.25;
    }
  }
}

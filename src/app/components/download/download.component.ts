import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss'],
})
export class DownloadComponent implements OnInit {

  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}

  download(){
    this.popCntl.dismiss(1);
  }

}

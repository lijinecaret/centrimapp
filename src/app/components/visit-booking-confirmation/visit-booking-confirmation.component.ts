import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-visit-booking-confirmation',
  templateUrl: './visit-booking-confirmation.component.html',
  styleUrls: ['./visit-booking-confirmation.component.scss'],
})
export class VisitBookingConfirmationComponent implements OnInit {
@Input() fam;
@Input() name;
@Input() date;
@Input() slot;
@Input() duration;
@Input() pic;
@Input() other;
img:any;
user_name:any;
comment:any;
  constructor(private modalCntl:ModalController,private storage:Storage) { }

  ngOnInit() {}

  async ionViewWillEnter(){
    console.log('fam:',this.fam)
    // this.storage.ready().then(()=>{
      const img=await this.storage.get('PRO_IMG');
        this.img=img
      // });
      const name=await this.storage.get('NAME');
        this.user_name=name
      // })
    // })
  }

  async cancel(){
    await this.modalCntl.dismiss();
  }

  async confirm(){
    let com=null
    if(this.comment==undefined||this.comment==''){
      com=null
    }else{
      com=this.comment
    }
    await this.modalCntl.dismiss({comment:com});
  }
}

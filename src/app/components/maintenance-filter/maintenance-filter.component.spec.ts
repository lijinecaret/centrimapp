import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MaintenanceFilterComponent } from './maintenance-filter.component';
import { ApplicationPipesModule } from 'src/app/application-pipes.module';
// import { ExpandableComponent } from '../expandable/expandable.component';

describe('MaintenanceFilterComponent', () => {
  let component: MaintenanceFilterComponent;
  let fixture: ComponentFixture<MaintenanceFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceFilterComponent],
      imports: [IonicModule.forRoot(),ApplicationPipesModule]
    }).compileComponents();

    fixture = TestBed.createComponent(MaintenanceFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from 'src/app/services/http-config.service';
// import { ExpandableComponent } from '../components/expandable/expandable.component';
@Component({
  selector: 'app-maintenance-filter',
  templateUrl: './maintenance-filter.component.html',
  styleUrls: ['./maintenance-filter.component.scss'],
})
export class MaintenanceFilterComponent implements OnInit {
  @Input() data;
 @Input() flag;
  sel_index:any;
  sub_id:any;
  type:any;
  sub:any=[];
filters:any;
branch:any=[];
  constructor(private popCntl:PopoverController,private http:HttpClient,private storage:Storage,private config:HttpConfigService) { }

  ngOnInit() {}
  async ionViewWillEnter(){
    this.sub=[];
    this.branch=[];
    
    if(this.flag==1){

      // this.storage.ready().then(()=>{
        const data=await this.storage.get('COMPANY_ID');
          let url=this.config.domain_url+'branch_viatype';
          let body={company_id:data,
                    role_id:4}
          let headers=new HttpHeaders({company_id:data.toString()});
          this.http.post(url,body).subscribe((res:any)=>{
            console.log("res:",res);
            res.data.forEach(element=>{
              let branch={id:element.id,status:element.name}
              this.branch.push(branch);
            })
            
          })
      //   })
      // })
      
      this.filters=[
        // {
        //   filter:"All",
        //   sub:null
        // },
        // {
        //   filter:"Branch",
        //   sub:this.branch
        // },
        {
          filter:"Status",
          sub:[{id:0,status:"Pending"},
               {id:1,status:"Inprogress"},
               {id:2,status:"With third party"},
               {id:3,status:"Closed"},
               {id:4,status:"Reopened"},
               {id:5,status:"Waiting for feedback"}
              ]
        },
        
        {
          filter:"Date",
          sub:[{id:0,status:"Today"},
               {id:1,status:"Yesterday"},
               {id:2,status:"Last 7 days"},
               {id:3,status:"Last 30 days"}
              ]
        },
        {
          filter:"Risk rating",
          sub:[{id:0,status:"Low"},
               {id:1,status:"Medium"},
               {id:2,status:"High"}
              ]
        }
      ]
      
    this.data.forEach(element => {
      let tec={id:element.user_id,status:element.name}
       this.sub.push(tec);
      
    });
    this.filters.push({filter:"Technician",sub:this.sub})
  }else{
    this.filters=[
      {
        filter:"All",
        sub:null
      },
      {
        filter:"Status",
        sub:[{id:0,status:"Pending"},
             {id:1,status:"Inprogress"},
             {id:2,status:"With third party"},
             {id:3,status:"Closed"},
             {id:4,status:"Reopened"},
             {id:5,status:"Waiting for feedback"}
            ]
      },
      
      {
        filter:"Date",
        sub:[{id:0,status:"Today"},
             {id:1,status:"Yesterday"},
             {id:2,status:"Last 7 days"},
             {id:3,status:"Last 30 days"}
            ]
      },
      {
        filter:"Priority",
        sub:[{id:0,status:"Low"},
             {id:1,status:"Medium"},
             {id:2,status:"Urgent"}
            ]
      }
    ]
  }
  }
  expand(item,i){

      this.sel_index=i;
    
  }
  selected(sub,id){
    this.type=sub.filter;
    this.sub_id=id;
    this.closeModal();
  }

  async closeModal() {
    const onClosedData: any = {filter:this.type,id:this.sub_id};
    await this.popCntl.dismiss(onClosedData);
  }
}

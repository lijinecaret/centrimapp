import { Component, OnInit, Renderer2, AfterViewInit, ElementRef, ViewChild, Input } from '@angular/core';

@Component({
  selector: 'app-notificationcard',
  templateUrl: './notificationcard.component.html',
  styleUrls: ['./notificationcard.component.scss'],
})
export class NotificationcardComponent implements AfterViewInit {
  @ViewChild("expandWrapper", { read: ElementRef,static:false }) expandWrapper: ElementRef;
  @Input("expanded") expanded: boolean = false;
  @Input("expandHeight") expandHeight: string = "500px";
  constructor(public renderer: Renderer2) { }

  
  ngAfterViewInit() {
    this.renderer.setStyle(this.expandWrapper.nativeElement, "max-height", this.expandHeight);
  }
}

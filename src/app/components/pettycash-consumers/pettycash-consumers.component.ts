import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-pettycash-consumers',
  templateUrl: './pettycash-consumers.component.html',
  styleUrls: ['./pettycash-consumers.component.scss'],
})
export class PettycashConsumersComponent implements OnInit {
  @Input() data:any;
  constructor(private modalCntrl:ModalController,private toastCntl:ToastController) { }

  ngOnInit() {
    console.log("data:",this.data)
  }
  dismiss(){
    this.modalCntrl.dismiss();
  }
  switchProfile(item){
    if(item.share==0){
      this.presentAlert("You don't have access to this profile right now." )
    }else{
    const onClosedData:any=item;
    this.modalCntrl.dismiss(onClosedData);
    }
  }

  async presentAlert(mes) {
    const alert = await this.toastCntl.create({
      message: mes,
      cssClass:'toastStyle',
      duration: 4000,
      position:'top'       
    });
    alert.present(); //update
  }
}

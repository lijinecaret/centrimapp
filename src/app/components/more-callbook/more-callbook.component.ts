import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-more-callbook',
  templateUrl: './more-callbook.component.html',
  styleUrls: ['./more-callbook.component.scss'],
})
export class MoreCallbookComponent implements OnInit {
  @Input() data;
  @Input() other;
  @Input() call;
  user_id:any;
   mem:any=[];
   other_mem:any=[];
  constructor(private storage:Storage,private modalCntl:ModalController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    console.log("fam:",this.data);
    
    this.mem=[];
    if(this.data.length){
    this.mem=this.data.slice(1);
    
    if(this.data.length==0&&this.other){
      this.other_mem=this.other.slice(1);
    }else{
      this.other_mem=this.other
    }
  }else{
    this.other_mem=this.other
  }
    console.log(this.data);
  }

  dismiss(){
    this.modalCntl.dismiss();
  }

}

import { Component, OnInit, ElementRef } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { WebrtcService } from 'src/app/services/webrtc.service';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';
import { OncallComponent } from '../oncall/oncall.component';

@Component({
  selector: 'app-receivecall',
  templateUrl: './receivecall.component.html',
  styleUrls: ['./receivecall.component.scss'],
})
export class ReceivecallComponent implements OnInit {
  // topVideoFrame = 'partner-video';
  title:string;
  token:string;
  userId: string;
  partnerId: string;
  // myEl: HTMLMediaElement;
  // partnerEl: HTMLMediaElement;
  peerId:string;
  pic:any;
    constructor(private navParams: NavParams,public webRTC: WebrtcService,private config:HttpConfigService,
      public modalCntl:ModalController,private http:HttpClient,public elRef: ElementRef,private storage:Storage) { }
  
    ngOnInit() {
    }
    ionViewWillEnter(){
      this.title=this.navParams.data.title;
      this.token=this.navParams.data.token;
      this.partnerId=this.navParams.data.toId;
      this.pic=this.navParams.data.pic;
      console.log("params:",this.title,this.token,this.partnerId);
      // this.init();
      
    }



    // init() {
    //   this.storage.ready().then(()=>{
    //     const id=await this.storage.get('USER_ID');
    //       this.userId=id;
    //   this.myEl = this.elRef.nativeElement.querySelector('#my-video');
    //   this.partnerEl = this.elRef.nativeElement.querySelector('#partner-video');
    //   this.webRTC.init(this.token, this.myEl, this.partnerEl);
    //     })
    //   })
    // }
  
    async call(){
      this.peerId=this.generatePeerId();
      // this.storage.ready().then(()=>{
        const id=await this.storage.get('USER_ID');
          this.userId=id;
      let url=this.config.domain_url+'sendPushVideocall';
      let headers= await this.config.getHeader();
      let body={
        to_user_id:this.partnerId,
        from_user_id:this.userId,
        callstatus:2,
        calltoken:this.peerId
      }
      console.log("callbody:",body);
    
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        
      })
      // this.webRTC.call(this.partnerId);
      // this.swapVideo('my-video');
      this.onCall();
      this.modalCntl.dismiss();
  //   })
  // })
  }
  
    async endCall(){
      // this.storage.ready().then(()=>{
        const id=await this.storage.get('USER_ID');
          this.userId=id;
      
      let url=this.config.domain_url+'sendPushVideocall';
      let headers= await this.config.getHeader();
      let body={
        to_user_id:this.partnerId,
        from_user_id:this.userId,
        callstatus:3,
        calltoken:this.token
      }
      console.log("hangbody:",body);
      
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log(res);
        
      })
      this.modalCntl.dismiss();
      
  //   })
  // })
    }



    // swapVideo(topVideo: string) {
    //   this.topVideoFrame = topVideo;
    // }
    async onCall(){
      const modal = await this.modalCntl.create({
        component: OncallComponent,
        componentProps: {
          flag:1,
          title:this.title,
            token:this.token,
            toId:this.peerId
            
        }
      });
      return await modal.present();
    }



    generatePeerId(){
      let outString: string = '';
      let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';
    
      for (let i = 0; i < 16; i++) {
    
        outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
    
        
      }
      return outString;
    }


  }
  

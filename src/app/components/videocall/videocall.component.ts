import { Component, OnInit, ElementRef } from '@angular/core';
import { WebrtcService } from 'src/app/services/webrtc.service';
import { HttpClient } from '@angular/common/http';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { NavParams, ModalController } from '@ionic/angular';


declare var JitsiMeetExternalAPI;
@Component({
  selector: 'app-videocall',
  templateUrl: './videocall.component.html',
  styleUrls: ['./videocall.component.scss'],
})
export class VideocallComponent implements OnInit {
  topVideoFrame = 'partner-video';
  userId: string;
  partnerId: string;
  // myEl: HTMLMediaElement;
  // partnerEl: HTMLMediaElement;
  pic:any;
  name:any;
  peerId:string;
  room:any;
  api:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private navParams: NavParams,
    public modalCntl:ModalController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    this.room=this.navParams.data.room;
    // this.init();
  this.jitsi();
    // setTimeout(()=>{
    //   this.endCall();
    // },20000);
    
  }
  async init() {
    this.peerId=this.generatePeerId();
    console.log("random:",this.peerId);
    let url=this.config.domain_url+'sendPushVideocall';
    let headers= await this.config.getHeader();
    let body={
      to_user_id:this.partnerId,
      from_user_id:this.userId,
      callstatus:1,
      calltoken:this.peerId
    }
    console.log("body:",body);
    
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      
    })
    
  }
  
  
  generatePeerId(){
    let outString: string = '';
    let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';
  
    for (let i = 0; i < 16; i++) {
  
      outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));
  
      
    }
    return outString;
  }
  
  
  async endCall(){
    
    let url=this.config.domain_url+'sendPushVideocall';
    let headers= await this.config.getHeader();
    let body={
      to_user_id:this.partnerId,
      from_user_id:this.userId,
      callstatus:3,
      calltoken:this.peerId
    }
    console.log("endbody:",body);
    
    this.http.post(url,body,{headers}).subscribe((res:any)=>{
      console.log(res);
      
    })
    
    this.modalCntl.dismiss();
  }
  
  
  jitsi(){
    let domain='meet.jit.si';
  let options={
    roomName:this.room,
    parentNode: document.querySelector('#meeting'),
    configOverwrite: {
      disableDeepLinking: true,
            enableWelcomePage:false,                                
            defaultLanguage: 'en',
            disableThirdPartyRequests: true,    
            prejoinPageEnabled: false,
            enableClosePage:false,
      },
      interfaceConfigOverwrite: {
        DEFAULT_BACKGROUND: '#612d620',
        TOOLBAR_ALWAYS_VISIBLE: true,
        SHOW_PROMOTIONAL_CLOSE_PAGE:false,
        SHOW_BRAND_WATERMARK: false,
        MOBILE_APP_PROMO: false,
        HIDE_INVITE_MORE_HEADER: true,
        SHOW_JITSI_WATERMARK:false,
        SHOW_WATERMARK_FOR_GUESTS: false,
        DISPLAY_WELCOME_PAGE_CONTENT:false,
        SHOW_POWERED_BY: false,
        SHOW_CHROME_EXTENSION_BANNER: false,
        AUTHENTICATION_ENABLE: false,
        TOOLBAR_BUTTONS: [
  'microphone', 'camera', 'closedcaptions', 'fullscreen', 'fodeviceselection', 'hangup', 'profile','etherpad', 'settings',
  'videoquality', 'filmstrip',  'feedback', 'shortcuts',
  'tileview', 'download','mute-everyone', 'security' ],                     
  },
  }
  this.api = new JitsiMeetExternalAPI(domain, options);
  this.api.on('readyToClose', function(){    
                              
                            
  
    this.modalCntl.dismiss(); 
   
  });
  }
  closeCall(){
    this.api.executeCommand('hangup');
    this.modalCntl.dismiss();
  }
  audiocontrol() {
      
    var element = document.getElementById("audioicon");
        if ( element.classList.contains('mic') ) { // Check for class
            element.classList.add("mute");
          element.classList.remove("mic");
        }else{
            element.classList.remove("mute");
                 element.classList.add("mic");
        } 
    
    this.api.executeCommand('toggleAudio');
  
      }
      videocontrol() {    
        var element = document.getElementById("vdoicon");
        if ( element.classList.contains('vdoon') ) { // Check for class
            element.classList.remove("vdoon");
            element.classList.add("vdoOff");
        }else{
            element.classList.add("vdoon");
            element.classList.remove("VdoOff");
        }                   
    this.api.executeCommand('toggleVideo');
  }
  
  screenshare() {
  
  
    this.api.executeCommand('toggleShareScreen');
   
  }

}

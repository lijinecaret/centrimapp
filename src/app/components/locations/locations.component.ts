import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent implements OnInit {
  public items: any = [];
  @Input() data;
 
  terms:any;
  constructor(private http:HttpClient,private config:HttpConfigService,private storage:Storage,
    private modalCntrl:ModalController) { }

  ngOnInit() {}
  async ionViewWillEnter(){
    // this.idArray=[];
    this.terms='';
    // this.storage.ready().then(()=>{
      const bid=await this.storage.get('BRANCH');
        // let headers=new HttpHeaders({'company_id':cid.toString()});
        let url=this.config.domain_url+'get_location_list/'+bid;
        this.http.get(url).subscribe((res:any)=>{
          console.log("res:",res,url);
          this.items=[];
          res.locations.forEach(element => {
            
         
            this.items.push({expanded:false,data:element});
          })
          // this.items=res.data
          
        })
    //   })
    // })
    // var data=[{loc:'Wing A',sub:['Room 1','Room 2']},{loc:'Wing B',sub:['Room 1','Room 2']}]
    // var centre={ expanded: false,data: {loc:'Wing A',sub:['Room 1','Room 2']}}
    //           this.items.push({ expanded: false,data: {loc:'Wing A',sub:['Room 1','Room 2']}});
    //           this.items.push({ expanded: false,data: {loc:'Wing B',sub:['Room 1','Room 2']}});

      console.log("items:",this.items);
  }
  expandItem(item): void {
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.items.map(listItem => {
        if (item == listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }
  }

  async dismiss(){
    this.modalCntrl.dismiss();
  }

  select(id,name,len,p,loc){
    // if(len==0){
    // if(this.filter!==0){
    
    const onClosedData:any={id:id,loc_name:name,parent:p};
    // const loc:any=;
    // const parent:any=p
    this.modalCntrl.dismiss(onClosedData);
    // }else{
    if(loc==1){
      const onClosedData:any={id:id,loc_name:name,flag:1};
    // const loc:any=;
    // const parent:any=p
    this.modalCntrl.dismiss(onClosedData);
    }else{
      const onClosedData:any={id:id,loc_name:name,flag:2};
    // const loc:any=;
    // const parent:any=p
    this.modalCntrl.dismiss(onClosedData);
    // }
    }
  // }
  }
  // select(id,name,len,p){
  //   if(len==0){
  //   const onClosedData:any={id:id,loc_name:name,parent:p};
  //   // const loc:any=;
  //   // const parent:any=p
  //   this.modalCntrl.dismiss(onClosedData);
  //   }
  // }
  cancel(){
    // this.hide=false;
    this.terms='';
    
  }

  selectedLoc(loc){
    const found = loc.some(el => el.id === this.data);
    console.log("found:",found);
    return found;
    
  }
}

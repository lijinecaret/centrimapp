import { Component, Input, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss'],
})
export class ThankyouComponent implements OnInit {
flag:any;
@Input() data:any;
  constructor(private modalCntlr:ModalController,private router:Router,private navParams: NavParams) { }

  ngOnInit() {}
  ionViewWillEnter(){
    this.flag=this.navParams.data.flag;
   setTimeout(() => { 
    this.router.navigate(['/menu'],{replaceUrl:true})
     this.closeModal();
    //  if(this.flag==2){
     
    //  }else{
    //    this.router.navigate(['/survey'])
    //  }
   }, 3000)
  }
 
 
  closeModal(){
    this.modalCntlr.dismiss();
  }
}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-visitors',
  templateUrl: './show-visitors.component.html',
  styleUrls: ['./show-visitors.component.scss'],
})
export class ShowVisitorsComponent implements OnInit {
@Input() data:any;
  constructor() { }

  ngOnInit() {}

}

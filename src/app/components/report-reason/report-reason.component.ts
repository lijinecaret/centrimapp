import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-report-reason',
  templateUrl: './report-reason.component.html',
  styleUrls: ['./report-reason.component.scss'],
})
export class ReportReasonComponent implements OnInit {
reason:any;
  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}

  submit(){
    let onClosedData=this.reason;
    this.modalCntl.dismiss(onClosedData,'1');
  }

  dismiss(){
    this.modalCntl.dismiss();
  }
}

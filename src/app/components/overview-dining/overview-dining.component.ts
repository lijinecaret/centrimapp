import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { OverviewFoodoptionsComponent } from '../overview-foodoptions/overview-foodoptions.component';

@Component({
  selector: 'app-overview-dining',
  templateUrl: './overview-dining.component.html',
  styleUrls: ['./overview-dining.component.scss'],
})
export class OverviewDiningComponent implements OnInit {
@Input() data:any;
  constructor(private modalCntl:ModalController,private popCntlr:PopoverController) { }

  ngOnInit() {}

  ionViewWillEnter(){
console.log('item:',this.data)
  }
  dismiss(){
    this.modalCntl.dismiss();
  }

  async showOptions(options,ev){
    
    const popover = await this.popCntlr.create({
      component: OverviewFoodoptionsComponent,
      event:ev,
      backdropDismiss:true,
      componentProps:{
        data:options,
       
      },
     
      
      
    });
    return await popover.present();
  }
}

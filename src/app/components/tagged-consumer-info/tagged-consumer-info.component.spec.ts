import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TaggedConsumerInfoComponent } from './tagged-consumer-info.component';

describe('TaggedConsumerInfoComponent', () => {
  let component: TaggedConsumerInfoComponent;
  let fixture: ComponentFixture<TaggedConsumerInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaggedConsumerInfoComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TaggedConsumerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

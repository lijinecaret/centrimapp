import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-tagged-consumer-info',
  templateUrl: './tagged-consumer-info.component.html',
  styleUrls: ['./tagged-consumer-info.component.scss'],
})
export class TaggedConsumerInfoComponent implements OnInit {
  @Input() data:any;
  constructor(private modalCntrl:ModalController) { }

  ngOnInit() {}
  dismiss(){
    this.modalCntrl.dismiss();
  }
}

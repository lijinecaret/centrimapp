import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { ModalController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { ActivityImageComponent } from '../activity-image/activity-image.component';
import moment from 'moment';
import { ViewpdfComponent } from '../viewpdf/viewpdf.component';

@Component({
  selector: 'app-overview-activity',
  templateUrl: './overview-activity.component.html',
  styleUrls: ['./overview-activity.component.scss'],
})
export class OverviewActivityComponent implements OnInit {
@Input() data:any;
@Input() cid:any;
day:any;
month:any;
year:any;
activity:any;
image:any=[];
photos:any=[];
doc:any=[];

signupcount:any;
waitinglist:boolean=false;
rvsettings:any;
pastactivity:boolean=true;
  constructor(private modalCntrl:ModalController,private storage:Storage,private http:HttpClient,
    private config:HttpConfigService,private popCntlr:PopoverController,private iab:InAppBrowser,
    private dialog:SpinnerDialog,private platform:Platform) { }

  ngOnInit() {}

  async ionViewWillEnter(){
    console.log('item:',this.data)
    const rv=await this.storage.get('RVSETTINGS');
    this.rvsettings=rv
    this.getContent();
    this.signupList();
  }
  dismiss(){
    this.modalCntrl.dismiss();
  }
  async getContent(){
   
        const uid=await this.storage.get('USER_ID');
          const utype=await this.storage.get('USER_TYPE');
  
         
        const data=await this.storage.get('TOKEN');
          let token=data;
          const bid=await this.storage.get('BRANCH');
            let branch=bid.toString();
      let headers=new HttpHeaders({
        'Authorization': 'Bearer'+token ,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'branch_id':branch
      });
      let url=this.config.domain_url+'activity/'+this.data.id;
      // let headers= await this.config.getHeader();
      let current=moment().format('DD MMMM YYYY');
    
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log(res);
          this.activity=res.activity;
          this.image=this.activity.images;
          
          this.photos=this.activity.activity_photos;
         
          this.activity.attachments.forEach(el=>{
            let attch_name=el.activity_attachment.substr(el.activity_attachment.lastIndexOf('_') + 1);
            let ext=el.activity_attachment.substr(el.activity_attachment.lastIndexOf('.') + 1);
            this.doc.push({'title':attch_name,'post_attachment':el.activity_attachment,'ext':ext});
          })
          let d=moment(this.activity.activity_start_date).format('DD MMMM YYYY')
          if (moment(d).isSameOrAfter(moment(current))) {
            this.pastactivity = false
          } else {
            this.pastactivity=true
          }
          
          console.log("doc:",this.doc,moment(this.activity.activity_start_date),this.pastactivity);
         
        },error=>{
          console.log(error);
          
        })
     
        
 
    
  }

  async showImages(ev: any,i) {
    let img;
    let flag;
  if(i==1){
  img=this.image;
  flag=2
  }else{
    img=this.photos;
    flag=3
  }
    const popover = await this.popCntlr.create({
      component: ActivityImageComponent,
      event:ev,
      cssClass:'image_pop',
      componentProps:{
        data:img,
        flag:flag
      },
      
    });
    return await popover.present();
  }
  
  async signupList(){
    const uid=await this.storage.get('USER_ID');
    let url=this.config.domain_url+'activity_signup_list/'+this.data.id;
      // let headers= await this.config.getHeader();
        this.http.get(url).subscribe((res:any)=>{
          console.log('signup:',res,uid);
          if(res.data.signup&&res.data.signup.length){
            this.signupcount=res.data.signup.length
          }else{
            this.signupcount=0
          }
          if(res.data.waiting_list&&res.data.waiting_list.length){
            res.data.waiting_list.map(x=>{
              if(this.cid.includes(x.user_id)){
                this.waitinglist=true
              }
            })
          }
        })
  }

  openDoc(item){
    // window.open(encodeURI(item.activity_attachment),"_system","location=yes");
  
  
    let options:InAppBrowserOptions ={
      location:'yes',
    hidenavigationbuttons:'yes',
    hideurlbar:'yes',
    zoom:'yes',
    beforeload:'yes',
        clearcache:'yes'
    }
    this.platform.ready().then(() => {
      if(item.post_attachment.includes('.pdf')){
        this.showpdf(item.post_attachment)
    }else{
    const browser = this.iab.create('https://docs.google.com/viewer?url='+item.post_attachment+'&embedded=true','_blank',options);
    this.dialog.show();
    browser.on('loadstart').subscribe(() => {
      console.log('start');
      this.dialog.hide();   
     
    }, err => {
      console.log(err);
      
      this.dialog.hide();
    })
  
    browser.on('loadstop').subscribe(()=>{
      console.log('stop');
      
      this.dialog.hide();;
    }, err =>{
      this.dialog.hide();
    })
  
    browser.on('loaderror').subscribe(()=>{
      this.dialog.hide();
    }, err =>{
      this.dialog.hide();
    })
    
    browser.on('exit').subscribe(()=>{
      this.dialog.hide();
    }, err =>{
      this.dialog.hide();
    })
    }
    })
  
  }
  
  
  
  getMIMEtype(extn){
    let ext=extn.toLowerCase();
    let MIMETypes={
      'txt' :'text/plain',
      'docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'doc' : 'application/msword',
      'pdf' : 'application/pdf',
      'jpg' : 'image/jpeg',
      'bmp' : 'image/bmp',
      'png' : 'image/png',
      'xls' : 'application/vnd.ms-excel',
      'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'rtf' : 'application/rtf',
      'ppt' : 'application/vnd.ms-powerpoint',
      'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    }
    return MIMETypes[ext];
  }

  async showpdf(file){
    const modal = await this.modalCntrl.create({
      component: ViewpdfComponent,
      cssClass:'fullWidthModal',
      componentProps: {
        
        data:file,
        
         
      },
      
      
    });
    return await modal.present();
  }
}

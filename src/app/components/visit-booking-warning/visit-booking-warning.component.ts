import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-visit-booking-warning',
  templateUrl: './visit-booking-warning.component.html',
  styleUrls: ['./visit-booking-warning.component.scss'],
})
export class VisitBookingWarningComponent implements OnInit {
@Input() data;
  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}

  continue(){
    this.modalCntl.dismiss();
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit {
@Input() data:any;
myStory:any;
  
  activity:any;
  status:any;
  my_comment:any;
  message:any;
  call:any;
  visit:any;
  maintenance_comment:any;
  role:any;
  constructor(private modalCntl:ModalController,private storage:Storage) { }

  ngOnInit() {}

  async ionViewWillEnter(){
    this.role=await this.storage.get('USER_TYPE');
    this.myStory=this.data.story;
    this.activity=this.data.act;
    this.my_comment=this.data.com;
    this.message=this.data.mes;
    this.call=this.data.call;
    this.status=this.data.status;
    this.visit=this.data.visit;
    this.maintenance_comment=this.data.maintenance_comment;
  }

  dismiss(){
    this.modalCntl.dismiss();
  }
  continue(){
    const not={
      story:this.myStory,
      act:this.activity,
      com:this.my_comment,
      mes:this.message,
      call:this.call,
      visit:this.visit,
      maintenance_comment:this.maintenance_comment,
      status:this.status}
      this.modalCntl.dismiss(not)
  }
  myStoryStatus(i){
    // if(status.currentTarget.checked==true)
    // if(this.isLoading==false){
    if(i.currentTarget.checked==true){
      this.myStory=1;
    }else{
      this.myStory=0;
    }
  
   
    // this.updateNotificationStatus();
    }
  
  
  activityStatus(i){
    // if(this.isLoading==false){
    if(i.currentTarget.checked==true){
      this.activity=1;
    }else{
      this.activity=0;
    }
    
      // this.updateNotificationStatus();
      // }
  }
  commentStatus(i){
    // if(this.isLoading==false){
    if(i.currentTarget.checked==true){
      this.my_comment=1;
    }else{
      this.my_comment=0;
    }
    
      // this.updateNotificationStatus();
      // }
  }
  messageStatus(i){
    // if(this.isLoading==false){
    if(i.currentTarget.checked==true){
      this.message=1;
    }else{
      this.message=0;
    }
  
      // this.updateNotificationStatus();
      // }
  }
  callStatus(i){
    // if(this.isLoading==false){
    if(i.currentTarget.checked==true){
      this.call=1;
    }else{
      this.call=0;
    }
    
      // this.updateNotificationStatus();
      // }
  }
  visitStatus(i){
    if(i.currentTarget.checked==true){
      this.visit=1;
    }else{
      this.visit=0;
    }
  }
  maintenanceCommentStatus(i){
    if(i.currentTarget.checked==true){
      this.maintenance_comment=1;
    }else{
      this.maintenance_comment=0;
    }
  }
}

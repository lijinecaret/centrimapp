import { DOCUMENT } from '@angular/common';
import { Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { IonContent, ModalController } from '@ionic/angular';
import moment from 'moment';

@Component({
  selector: 'app-choose-date',
  templateUrl: './choose-date.component.html',
  styleUrls: ['./choose-date.component.scss'],
})
export class ChooseDateComponent implements OnInit {

  @Input() menu:any;
  currentDate:any;
  start:any;
  end:any;
  dates:any[]=[];
  @ViewChild(IonContent, { static: false }) content: IonContent;
    constructor(private modalCntrl:ModalController,@Inject(DOCUMENT) private document: Document) { }
  
    ngOnInit() {}
    async dismiss(){
      this.modalCntrl.dismiss()
    }
    ionViewWillEnter(){
      this.dates=[];
      
      console.log('menu:',this.menu);
      this.currentDate=moment().format('DD MMM YYYY');
      this.start=moment(this.menu.start_date);
      this.end=moment(this.menu.enddate);
      var start=this.start.clone();
      while(start.isSameOrBefore(this.end)){
        this.dates.push(start.format('DD MMM YYYY'));
        start.add(1,'days');
      }
  
      setTimeout(()=>{
        var titleELe = this.document.getElementById(this.currentDate+'1');
        console.log(titleELe)
        this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
      },500)
    }
    async choose(item){
      let onClosedData=item;
      this.modalCntrl.dismiss(item)
    }
  }

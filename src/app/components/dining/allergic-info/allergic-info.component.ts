import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-allergic-info',
  templateUrl: './allergic-info.component.html',
  styleUrls: ['./allergic-info.component.scss'],
})
export class AllergicInfoComponent implements OnInit {

  @Input() consumer:any;
@Input() allergies:any;
allergic:any;
  constructor(private modalCntrl:ModalController) { }

  ngOnInit() {}
  ionViewWillEnter(){
    this.allergic=this.allergies[0].alergy.alergy;
    this.allergies.forEach(element => {
      if(this.allergic==element.alergy.alergy){
        
      }else{
        this.allergic=this.allergic+','+element.alergy.alergy
      }
    });
  }
  async dismiss(i){
  
    this.modalCntrl.dismiss(i)
  }
}

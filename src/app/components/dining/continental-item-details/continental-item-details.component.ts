import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-continental-item-details',
  templateUrl: './continental-item-details.component.html',
  styleUrls: ['./continental-item-details.component.scss'],
})
export class ContinentalItemDetailsComponent implements OnInit {
  @Input() item:any;
  @Input() date:any;
  @Input() servingtime:any;
  quantity:any=1;
  size:any;
  comments:any;
  optionData: { OptionId: number, Value: number }[];
  selectedVal:any[]=[];
  ml_sz_id:any;
    constructor(private modalCntl:ModalController) { }
  
    ngOnInit() {}
    ionViewWillEnter(){
      console.log('item:',this.item)
      this.optionData = [{ OptionId: 0, Value: 0 }];
      if(this.item.selected_data){
        this.quantity=this.item.selected_data.quantity
          this.comments=this.item.selected_data.comment;
      if(this.item.selected_data.size){
        this.ml_sz_id=this.item.selected_data.size.id.toString()
      }else{
        if(this.item.itemdetails.servingqunatity&&this.item.itemdetails.servingqunatity.length){
          this.ml_sz_id=this.item.itemdetails.servingqunatity[0].size_id.toString()
        }
      }
      if(this.item.selected_data.items_selected_options&&this.item.selected_data.items_selected_options.length){
        this.item.selected_data.items_selected_options.map(x=>{
          // x.valuess.map(y=>{
           
            this.selectedVal.push({option:x.option_id,val:x.option_value_id})
            this.optionData.push({ OptionId: x.option_id, Value: x.option_value_id});
            
          // })
          
        })
      }
    }else{
      
      if(this.item.itemdetails.options&&this.item.itemdetails.options){
        this.item.itemdetails.options.map(x=>{
          x.valuess.map(y=>{
            if(y.default_value==1){
            this.selectedVal.push({option:x.id,val:y.id})
            this.optionData.push({ OptionId: x.id, Value: y.id });
            }
          })
          
        })
      }
      if(this.item.itemdetails.servingquantity&&this.item.itemdetails.servingquantity.length){
        this.ml_sz_id=this.item.itemdetails.servingquantity[0].size_id.toString()
      }
    }
    }
    async dismiss(){
      this.modalCntl.dismiss();
    }
  
    async save(){
     let options,values;
      this.optionData.splice(0, 1);
        if(this.optionData&&this.optionData.length){
          for (let data of this.optionData) {
            //  
            if (options == '' || options == undefined){
              options = data.OptionId;
            }
            if (options == data.OptionId) {
              console.log("firstOpt");
    
            } else {
              
              options += "*" + data.OptionId;
            }
            if (values == '' || values == undefined){
              values = data.Value;
            }
            if (values == data.Value) {
              console.log("firstOpt");
    
            } else {
              
              values += "*" + data.Value;
            }
          }
          
        }else{
          options=null;
          values=null
        }
        if(this.comments==undefined||this.comments==''){
          this.comments=null
        }
      const data={menu_item_id:this.item.id,quantity:this.quantity,size:this.ml_sz_id,comment:this.comments,options:options,option_values:values}
      console.log('closed:',data)
      this.modalCntl.dismiss(data)
    }
  
    increment(){
      if(this.quantity<5){
        this.quantity++;
        }
    }
    decrement(){
      if(this.quantity>1){
      this.quantity--;
      }
    }
    selectOptions(opt,val){
      if(this.optionData&&this.optionData.length){
      const targetIdx = this.optionData.map(item => item.OptionId).indexOf(opt);
      if (targetIdx >= 0) {
  
        this.optionData[targetIdx].Value = val;
        
      }else{
        this.optionData.push({ OptionId: opt, Value: val });
      }
    }else{
      this.optionData.push({ OptionId: opt, Value: val });
    }
    const targetIdx = this.selectedVal.map(item => item.option).indexOf(opt);
      
      this.selectedVal[targetIdx].val = val;
    }
    setMealSize(id){
      this.ml_sz_id=id
    }
}

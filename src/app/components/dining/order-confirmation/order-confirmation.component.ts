import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.component.html',
  styleUrls: ['./order-confirmation.component.scss'],
})
export class OrderConfirmationComponent implements OnInit {

  @Input() status;
  constructor(private modalCntrl:ModalController) { }

  ngOnInit() {}
  async dismiss(){
    this.modalCntrl.dismiss()
  }
  confirm(){
    this.modalCntrl.dismiss(1)
  }
}

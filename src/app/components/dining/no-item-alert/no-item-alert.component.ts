import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-no-item-alert',
  templateUrl: './no-item-alert.component.html',
  styleUrls: ['./no-item-alert.component.scss'],
})
export class NoItemAlertComponent implements OnInit {
  @Input() date:any;

  constructor(private modalCntrl:ModalController) { }

  ngOnInit() {}
  async dismiss(){
    this.modalCntrl.dismiss()
  }
  confirm(){
    this.modalCntrl.dismiss(1)
  }

}

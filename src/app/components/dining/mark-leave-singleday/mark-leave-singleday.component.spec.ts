import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MarkLeaveSingledayComponent } from './mark-leave-singleday.component';

describe('MarkLeaveSingledayComponent', () => {
  let component: MarkLeaveSingledayComponent;
  let fixture: ComponentFixture<MarkLeaveSingledayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkLeaveSingledayComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MarkLeaveSingledayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

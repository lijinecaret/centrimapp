import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-order-success-message',
  templateUrl: './order-success-message.component.html',
  styleUrls: ['./order-success-message.component.scss'],
})
export class OrderSuccessMessageComponent implements OnInit {

  constructor(private modalCntl:ModalController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    setTimeout(()=>{
        this.modalCntl.dismiss();
    },3000)
  }

}

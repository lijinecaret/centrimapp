import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-view-menu-additional',
  templateUrl: './view-menu-additional.component.html',
  styleUrls: ['./view-menu-additional.component.scss'],
})
export class ViewMenuAdditionalComponent implements OnInit {

  @Input()menu;
  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}

  showItem(i){
    if(i==1){
      this.menu.show=true;
    }else{
      this.menu.show=false;
    }
    
    this.popCntl.dismiss(2,this.menu)
  }

}

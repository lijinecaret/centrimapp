import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';

@Component({
  selector: 'app-dietary-preferences',
  templateUrl: './dietary-preferences.component.html',
  styleUrls: ['./dietary-preferences.component.scss'],
})
export class DietaryPreferencesComponent implements OnInit {
  @Input()details:any;
  last_updated_at:any;
    constructor(private modalCntrl:ModalController,private storage:Storage) { }
  
    ngOnInit() {}
    async ionViewWillEnter(){
      const tz=await this.storage.get('TIMEZONE');
      this.last_updated_at=moment.utc(this.details.last_updated_at).tz(tz).format('DD/MM/YYYY, hh:mm A');
    }
    async dismiss(){
      this.modalCntrl.dismiss()
    }
  
}

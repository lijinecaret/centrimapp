import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-scanerror',
  templateUrl: './scanerror.component.html',
  styleUrls: ['./scanerror.component.scss'],
})
export class ScanerrorComponent implements OnInit {

  constructor(private modalCntl:ModalController,private router:Router) { }

  ngOnInit() {}
  close(){
    this.modalCntl.dismiss();
    this.router.navigate(['/checkin-scan-qr']);
  }
}

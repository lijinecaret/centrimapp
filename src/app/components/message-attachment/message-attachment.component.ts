import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, Input, OnInit, ViewChildren } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Base64 } from '@ionic-native/base64/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
// import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { ModalController, Platform } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';

@Component({
  selector: 'app-message-attachment',
  templateUrl: './message-attachment.component.html',
  styleUrls: ['./message-attachment.component.scss'],
})
export class MessageAttachmentComponent implements OnInit {
  @Input()data:any;
  @Input()ext:any;
  @Input() roomid:any;
  @Input()user:any;
  type:any;
  base64File:any;
  img:any;
  vid:any;
  
  constructor(private modalCntl:ModalController,private config:HttpConfigService,
    private http:HttpClient,private platform:Platform,private base64:Base64,private file:File,
    private webView:WebView,private dialog:SpinnerDialog,private streamingMedia:StreamingMedia,
    private sanitizer:DomSanitizer,private filePath:FilePath) { }

  ngOnInit() {
    
  }
  ionViewWillEnter(){
    console.log('data:',this.data)
    if(this.ext=='jpg'){
      this.type=0;
      let img=this.webView.convertFileSrc(this.data);
      if(this.platform.is('ios')){
        this.img=this.sanitizer.bypassSecurityTrustUrl(img)
      }else{
        this.img=img
      }
    }else{
      this.type=1;
      if(this.platform.is('android')){
        this.filePath.resolveNativePath(this.data)
        .then(filePath => {console.log('filePath:',filePath);
        
        this.vid=this.webView.convertFileSrc(filePath)+'#t=0.05'
      })
        .catch(err => console.log('err:',err));
      }else{
        
        this.vid=this.webView.convertFileSrc(this.data)+'#t=0.05'
      }
      
      // this.vid.load();
      // let vid=this.webView.convertFileSrc(this.data)
      // this.vid=this.sanitizer.bypassSecurityTrustUrl(vid)+'#t=0.05';
      console.log(this.vid);
      
    }
  }

  send(){
    const onClosedData:any=this.data;
    const data:any=this.ext;
    this.modalCntl.dismiss(onClosedData,data);
    // if(this.ext=='jpg'){
    //   this.sendImg();
    // }else{
    //   this.sendVdo();
    // }
  }



  // sendImg(){
  //   this.dialog.show();   
  //   // console.log("attachsend:",this.attach);
  //   const fileTransfer: FileTransferObject=this.transfer.create();
  //         let options: FileUploadOptions = {
  //           fileKey: 'attachment',
  //           // fileName: this.id+'.jpg',
  //           chunkedMode:false,
  //           httpMethod:'post',
  //           mimeType:'image/jpg',
  //           headers:{},
  //           params:{
  //             roomid:this.roomid,
  //             message:'',
  //             user_id:this.user}
  //         }
  //         let  url = this.config.domain_url+'messaging';
  //         fileTransfer.upload(this.data,url,options).then((data)=>{
  //           console.log("success");
  //           console.log(data);
  //           this.dialog.hide();
  //           this.back();
            
  //         },error=>{
  //           console.log(error);
  //           this.dialog.hide();
            
  //         })
  
  
          
  
  // }
  sendVdo(){
   this.dialog.show();
  if(this.platform.is('android')){
  
    this.base64.encodeFile(this.data).then((base64: string) => {
      
      
       this.base64File=base64.replace('data:image/*;charset=utf-8;base64,','data:video/'+this.ext+';base64,')
       console.log("vdo:",this.base64File);
       let  url = this.config.domain_url+'messaging';
       let body ={
         roomid:this.roomid,
         message:'',
         user_id:this.user,
         video_bs:this.base64File
     }
       // let headers=new HttpHeaders({'enctype': 'multipart/form-data'})
       this.http.post(url,body).subscribe((res:any)=>{
         console.log(res);
         this.dialog.hide();
         this.back();
       },error=>{
         console.log(error);
         this.dialog.hide();
       })
      }, (err) => {
        console.log(err);
        this.dialog.hide();
      });
    }else{
      let fileName =  this.data.substring(this.data.lastIndexOf('/')+1);
     let path =  this.data.substring(0, this.data.lastIndexOf("/") + 1);
     //path=path.replace('Users/','');
     console.log("path:",path);
      console.log('name:',fileName);
      let that=this;
      this.file.resolveLocalFilesystemUrl(this.data).then((entry: any)=>{
        entry.file(function (file) {
          var reader = new FileReader();
    
          reader.onloadend = function (encodedFile: any) {
            var src = encodedFile.target.result;
            src = src.split("base64,");
            var contentAsBase64EncodedString = src[1];
            that.base64File='data:video/'+that.ext+';base64,'+contentAsBase64EncodedString;
            console.log("base:",contentAsBase64EncodedString);
            console.log("vdo:",that.base64File);
            let  url = that.config.domain_url+'messaging';
            let body ={
              roomid:that.roomid,
              message:'',
              user_id:that.user,
              video_bs:that.base64File
          }
            // let headers=new HttpHeaders({'enctype': 'multipart/form-data'})
            that.http.post(url,body).subscribe((res:any)=>{
              console.log(res);
              that.dialog.hide();
              that.back();
            },error=>{
              console.log(error);
              that.dialog.hide();
            })
            
          };
          reader.readAsDataURL(file);
        })
      }).catch((error)=>{
        console.log(error);
      })
    }
    
  
    
  
  
  }


  back(){
    this.modalCntl.dismiss();
  }





  playVdo(){
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => { console.log('Error streaming') },
      orientation: 'landscape',
      shouldAutoClose: true,
      controls: true
    };
    
    this.streamingMedia.playVideo(this.data, options);
  }
}

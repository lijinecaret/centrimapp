import { Component, Input, OnInit, ViewChildren } from '@angular/core';
import { IonSlides, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-multi-file-upload',
  templateUrl: './multi-file-upload.component.html',
  styleUrls: ['./multi-file-upload.component.scss'],
})
export class MultiFileUploadComponent implements OnInit {
  @Input() data;
  @ViewChildren('slideWithNav') slideWithNav: IonSlides;
 
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    
  };
  sliderOne: any;
  viewEntered = false;
  constructor(private popCntl:PopoverController) { }

  ngOnInit() {
    console.log(this.data);
    
  }
  ionViewDidEnter() {
    this.viewEntered = true;
  }
ionViewWillEnter(){
  this.sliderOne =
  {
    isBeginningSlide: true,
    isEndSlide: false,
    isActive:false,
    data:[]=this.data
  };
}
  slideNext(object,slideView) {
    slideView.slideNext(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });
  }

  //Move to previous slide
  slidePrev(object,slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }
    SlideDidChange(object,slideView) {
      this.checkIfNavDisabled(object, slideView);
      object.isActive= true;
    }
    checkIfNavDisabled(object, slideView) {
      this.checkisBeginning(object, slideView);
      this.checkisEnd(object, slideView);
    }
   
    checkisBeginning(object, slideView) {
      slideView.isBeginning().then((istrue) => {
        object.isBeginningSlide = istrue;
      });
    }
    checkisEnd(object, slideView) {
      slideView.isEnd().then((istrue) => {
        object.isEndSlide = istrue;
      });
    }

    dismiss(){
      this.popCntl.dismiss();
    }
}

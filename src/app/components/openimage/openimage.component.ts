import { Component, OnInit, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-openimage',
  templateUrl: './openimage.component.html',
  styleUrls: ['./openimage.component.scss'],
})
export class OpenimageComponent implements OnInit {
@Input() data;
  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}
  back(){
    this.popCntl.dismiss();
  }
}

import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit,  } from '@angular/core';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { Storage } from '@ionic/storage-angular';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-set-resident-profile',
  templateUrl: './set-resident-profile.component.html',
  styleUrls: ['./set-resident-profile.component.scss'],
})
export class SetResidentProfileComponent implements OnInit {
parent:any[]=[];
@Input() res:any;
  constructor(private storage:Storage,private config:HttpConfigService,private http:HttpClient,
    private popCntl:PopoverController) { }

  ngOnInit() {}


  async ionViewWillEnter(){
    this.parent=[];
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');
      let url=this.config.domain_url+'profile/'+data;
      let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log("get:",res,url);
          
              res.data.family.forEach(element=>{
                if(element.parent_details[0].status==1){
               
                this.parent.push({parent:element.parent_details[0],share:element.share_details});
                }
              })
             
              console.log("parent",this.parent);
              
            
              
        },error=>{
          console.log(error);
          
        });
    //   });
    // });
  }

  select(item){
    if(item.share==0){

    }else{
      this.res=item.parent.user.user_id
      this.storage.set('BRANCH',item.parent.wing.branch_id);
      this.storage.set('PCFBRANCH',item.parent.branches.user_branch.pcs_branch_id);
      this.storage.set('RESIDENT_ID',item.parent.user.user_id);
      this.storage.set('RESIDENT_USER_ID',item.parent.id);
      this.storage.set("RESIDENT_NAME",item.parent.user.name);
      this.storage.set('COMAPANY_NAME',item.parent.user.company.c_name);
      this.storage.set('RESIDENT_IMG',item.parent.user.profile_pic);
      this.storage.set('RES-RV-SETTING',item.parent.branches.user_branch.retirement_living);
      const res:any={resident:item.parent.user.user_id,name:item.parent.user.name,branch:item.parent.wing.branch_id,img:item.parent.user.profile_pic,c_name:item.parent.user.company.c_name}
      this.popCntl.dismiss(res);
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController, AlertController } from '@ionic/angular';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-cancel',
  templateUrl: './cancel.component.html',
  styleUrls: ['./cancel.component.scss'],
})
export class CancelComponent implements OnInit {
call_id:any;
  constructor(private nav:NavParams,private config:HttpConfigService,private http:HttpClient,
    private pop:PopoverController,private alertCnlr:AlertController) { }

  ngOnInit() {}


ionViewWillEnter(){
  this.call_id=this.nav.data.call_id;
  console.log("id:",this.call_id);
  
}
cancel(){
this.presentAlertConfirm();
}


async presentAlertConfirm() {
  const alert = await this.alertCnlr.create({
    header: 'Cancel booking',
    message: 'Are you sure you want to cancel this booking ?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          this.pop.dismiss()
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Yes',
        handler: () => {
         
          this.cancelBooking();
        }
      }
    ]
  });

  await alert.present();
}

  async cancelBooking(){

    let url=this.config.domain_url+'booking/'+this.call_id;
    let headers= await this.config.getHeader();

    this.http.delete(url,{headers}).subscribe((res:any)=>{
      console.log(res);

      this.pop.dismiss();
    })

  }



}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisitBookingRescheduleComponent } from './visit-booking-reschedule.component';

describe('VisitBookingRescheduleComponent', () => {
  let component: VisitBookingRescheduleComponent;
  let fixture: ComponentFixture<VisitBookingRescheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitBookingRescheduleComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisitBookingRescheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import moment from 'moment';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { VisitBookingWarningComponent } from '../visit-booking-warning/visit-booking-warning.component';
@Component({
  selector: 'app-visit-booking-reschedule',
  templateUrl: './visit-booking-reschedule.component.html',
  styleUrls: ['./visit-booking-reschedule.component.scss'],
})
export class VisitBookingRescheduleComponent implements OnInit {
@Input() data:any;
max:any;
duration: any = '1';
slot: any = [];
  start: any;
  end: any;
  hours;
  minutes;
  not_available: any = [];
  ar_avl: Array<string>;
  sel_slot: any;
  date:any;
  
  constructor(private modalCntlr:ModalController,private storage:Storage,private http:HttpClient,
    private config:HttpConfigService,private toast:ToastController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    console.log("data:",this.data);
    this.date=this.data.date
    this.duration=this.data.duration.toString();
    this.getAvailableTimeSlots();
    this.start = '05:00:00';
    this.end = '23:00:00';
  }
  onChange(ev) {

    let current = moment();
    let date = moment(this.date);
    console.log('date:', current, date, date.diff(current, 'days'))
     if ((date.diff(current, 'days') > 14)) {
      this.openWarning(2);
      this.date=this.data.date;
    } else {

      this.getAvailableTimeSlots();
    }

  }

  selectDuration() {
    console.log("dur:", this.duration);
    // this.slot=this.timeArray(this.start,this.end,30);
    this.getAvailableTimeSlots();
    // this.slot=this.timeArray(this.start,this.end,30);

    console.log("slots:", this.slot);

  }

  async getAvailableTimeSlots() {
    this.not_available = [];
    // this.storage.ready().then(() => {
      const data=await this.storage.get('COMPANY_ID');
        const bid=await this.storage.get('BRANCH');

          let headers = new HttpHeaders({ 'branch_id': bid.toString() });
          // let headers= await this.config.getHeader();
          let url = this.config.domain_url + 'visiting_timeslots';
          // let d;
          // if(this.duration=='30'){
          //   d=1
          // }else{
          //   d=2
          // }
          let body = {
            company_id: data,
            date: moment(this.date).format('yyyy-MM-DD'),
            duration: this.duration,
            wing_id: this.data.wing.id,
            type: this.data.type
          }
          console.log("body:", body);

          this.http.post(url, body, { headers }).subscribe((res: any) => {
            console.log("res:", res)
            res.data.forEach(element => {
              console.log(element);
              let t = { slots: element }
              this.not_available.push(t);


            });
            this.ar_avl = res.data
            // this.not_available.push(res.data));
            console.log("fff:", this.ar_avl)
            this.slot = this.timeArray(this.start, this.end, 30);
          }, error => {
            console.log(error);
          })
    //     })
    //   })
    // })
    console.log("not:", this.not_available);
    // this.ar_avl= this.not_available.map(p => p.slots)



  }


  timeArray(start, end, duration) {
    var start = start.split(":");
    var end = end.split(":");
    duration = parseInt(duration);
    start = parseInt(start[0]) * 60 + parseInt(start[1]);
    end = parseInt(end[0]) * 60 + parseInt(end[1]);
    console.log(start, end);

    var result = [];

    for (let time = start; time <= end; time += duration) {
      var slot = this.timeString(time);
      let disable;

      // alligator.includes("thick scales");
      // console.log("arraaaa:",this.ar_avl,"sl:",slot)
      // console.log("incl:",this.ar_avl.includes(slot));
      if (this.ar_avl.length > 0) {
        if (this.ar_avl.includes(slot)) {
          disable = false;
        } else {
          disable = true;
        }
      } else {
        disable = true
      }
      // this.not_available.forEach(el => {


      //   console.log("loop executed");

      //   if(el.slots===slot){
      //     disable== true;
      //   }
      //   else{
      //     disable== false;
      //   }
      //   console.log("dis:",disable);

      // })
      result.push({ 'slot': slot, 'disable': disable });

    }
    console.log("result:", result);

    return result;
  }

  timeString(time) {
    this.hours = Math.floor(time / 60);
    let h = Math.floor(time / 60);
    this.minutes = time % 60;
    if (this.hours > 12) {
      this.hours = this.hours - 12;
    }
    if (this.hours < 10) {
      this.hours = "0" + this.hours; //optional
    }

    if (this.minutes < 10) {
      this.minutes = "0" + this.minutes;
    }

    if (h >= 12) {
      return this.hours + ":" + this.minutes + ' PM';
    } else {
      return this.hours + ":" + this.minutes + ' AM';
    }
  }
 


  async openWarning(i) {
    const popover = await this.modalCntlr.create({
      component: VisitBookingWarningComponent,
      backdropDismiss: true,
      componentProps: {
        data: i
      },
      cssClass: 'morepop'


    });
    return await popover.present();
  }
  async dismiss(){
    this.modalCntlr.dismiss();
  }
  async continue(){
    // this.storage.ready().then(() => {
     
        const bid=await this.storage.get('BRANCH');
    let url=this.config.domain_url+'reschedule_visit';
    
        let headers=new HttpHeaders({'branch_id':bid.toString()});
    // let time=this.sel_slot.split(" ")[0];
    let time=moment(this.date).format('yyyy-MM-DD')+' '+this.sel_slot;
    console.log(time);
    let body={
        visit_id:this.data.id,
        duration:this.duration,
        start_time:moment(time,'yyyy-MM-DD hh:mm A').format('HH:mm:ss'),
        date:moment(this.date).format('yyyy-MM-DD')
    }
    console.log("body:",body,time);
    if(this.sel_slot!=undefined)
    {
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
          console.log(res);
          this.presentAlert('Rescheduled successfully.')
          this.modalCntlr.dismiss(1);
        },error=>{
          console.log(error);
          this.presentAlert('Something went wrong. Please try again later.')
        })
    //   })
    // })
  }
  else{
    this.presentAlert('Please select a Time Slot.');
  }
  }
  bookvisit(item) {
   
    if (item.disable) {
      this.presentAlert('This time slot is not available.Please select another.');

    } else {
      this.sel_slot = item.slot;
     
    }
 
  }
  async presentAlert(mes) {
    const alert = await this.toast.create({
      message: mes,
      cssClass: 'toastStyle',
      duration: 3000,
      position:'top' 
    });
    alert.present(); //update
  }
}

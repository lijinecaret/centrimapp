import { Component, OnInit, Input, ViewChildren, ElementRef, ViewChild } from '@angular/core';
import { NavParams, ModalController, Platform, IonRange, LoadingController } from '@ionic/angular';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { Media, MediaObject } from '@ionic-native/media/ngx';
@Component({
  selector: 'app-showfile',
  templateUrl: './showfile.component.html',
  styleUrls: ['./showfile.component.scss'],
})
export class ShowfileComponent implements OnInit {
  @ViewChild('range',{static : false}) range: IonRange;
  myEl: HTMLMediaElement;
  url:any;
  trustedVideoUrl:SafeResourceUrl;
  subscription:Subscription
  play:boolean=true;
  audio:MediaObject;
  scrubber:HTMLInputElement;
  progress=0;
  duration:any;
  dur_play:any;
  constructor(private nav:NavParams,private domSanitizer:DomSanitizer,private modalCntl:ModalController,private platform:Platform,
    private elref:ElementRef,private media:Media,private loadingCtrl:LoadingController) { }

  ngOnInit() {}

  ionViewWillEnter(){
    this.url=this.nav.data.link
    this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.url);
    console.log(this.trustedVideoUrl);
    
    // this.myEl=this.elref.nativeElement.querySelector('#audioPlayer');
    // this.myEl.src=this.url;
    // this.myEl.crossOrigin='anonymous';
    // this.myEl.play();
    // this.scrubber=this.elref.nativeElement.querySelector('#seek')
     this.audio=this.media.create(this.url);
    //  this.duration=this.audio.getDuration();
    //  console.log("dur1:",this.duration);
     
    //  this.audio.srcObject=this.url;
    //  this.audio.src=this.url;
    //  this.audio.crossOrigin="anonymous";
    //  this.audio.load();
    //  this.audio.play();
    this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{  
      this.audio.stop();
      this.audio.release();
  
      this.modalCntl.dismiss();
      });
  }
  ionViewWillLeave() { 
    this.subscription.unsubscribe();
  }
  playAudio(){
    this.play=false;
    this.audio.play();
    // this.audio.seekTo(100);
    // this.duration=this.audio.getDuration();
    console.log("dur2:",this.duration);
    
      this.initDuration()
    
    
    
  }
  pauseAudio(){
    this.play=true;
    this.audio.pause();
  }
  stopAudio(){
    this.play=true;
    this.audio.stop();
    // this.audio.currentTime=0;
  }
  initDuration()
  {
  
    
    setTimeout(() => {
      this.duration = Math.round(this.audio.getDuration()).toString();
      
      this.dur_play=this.duration;
        var hr = Math.floor(this.duration/ 3600)
        var minutes = Math.floor(this.duration %3600 / 60);
        var seconds = this.duration %3600 % 60;
        // this.dur_play=seconds+60;
        var hour =hr<10 ? '0'+hr:hr;
        var minute =minutes < 10 ? '0'+minutes : minutes;
        var sec = seconds < 10 ? '0'+seconds :seconds;

        // var hDisplay = hr > 0 ? hr + (hr == 1 ? " hour, " : " hours, ") : "";
        // var mDisplay = min > 0 ? min + (min == 1 ? " minute, " : " minutes, ") : "";
        // var sDisplay = sec > 0 ? sec + (sec == 1 ? " second" : " seconds") : "";

        this.duration=hour+':'+minute+':'+sec
      
        // this.dur_play=this.duration;
        // var secn = this.duration < 10 ? '0'+this.duration :this.duration;
        // this.duration='00:'+secn
      
      console.log("dur:",this.dur_play);
      
      console.log("initdur:",this.duration);
      
    }, 100);
  
    setInterval(async () => {
      // this.progress = this.progress+1
    }, 1000)
    // this.Msg = "Media Playing"
  
    // this.isplaying = true;
  
    
  console.log("endofinit:",this.progress);
  
  }

  async showLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'custom-loading',
      // message: '<ion-img src="assets/loader.gif"></ion-img>',
      spinner: null,
      duration: 3000
    });
    return await loading.present();
  }
  // async dismissLoader() {
  //   return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
  // }
}

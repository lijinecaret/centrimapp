import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-temperature-warning',
  templateUrl: './temperature-warning.component.html',
  styleUrls: ['./temperature-warning.component.scss'],
})
export class TemperatureWarningComponent implements OnInit {
name:any;
  constructor(private router:Router,private modalCntl:ModalController,private storage:Storage) { }

  ngOnInit() {}
  async ionViewWillEnter(){
  //  this.storage.ready().then(()=>{
     const name=await this.storage.get('NAME');
      this.name=name;
  //    })
  //  })
  }
  close(){
    this.router.navigate(['/menu']);
    this.modalCntl.dismiss();
  }
  dismiss(){
    this.modalCntl.dismiss();
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-report-abuse',
  templateUrl: './report-abuse.component.html',
  styleUrls: ['./report-abuse.component.scss'],
})
export class ReportAbuseComponent implements OnInit {
@Input() type;
  constructor(private popCntl:PopoverController) { }

  ngOnInit() {}

  report(i){
    const onClosedData:any=i;
    this.popCntl.dismiss(onClosedData);
  }

}

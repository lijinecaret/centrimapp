import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OncallComponent } from './oncall.component';

describe('OncallComponent', () => {
  let component: OncallComponent;
  let fixture: ComponentFixture<OncallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OncallComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OncallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

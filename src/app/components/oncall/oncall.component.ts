import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { WebrtcService } from 'src/app/services/webrtc.service';
import { HttpConfigService } from 'src/app/services/http-config.service';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-oncall',
  templateUrl: './oncall.component.html',
  styleUrls: ['./oncall.component.scss'],
})
export class OncallComponent implements OnInit {
// token:any;
// flag:any;
// topVideoFrame = 'partner-video';
//   userId: string;
//   partnerId: string;
//   myEl: HTMLMediaElement;
//   partnerEl: HTMLMediaElement;
data:SafeResourceUrl;
  constructor(private domSanitizer: DomSanitizer,private navParams: NavParams,private orientation:ScreenOrientation,
    private modalCntl:ModalController) { }

  ngOnInit() {

  }

  dismiss(){
    this.modalCntl.dismiss();
  }
ionViewWillEnter(){
//   // this.title=this.navParams.data.title;
//   this.flag=this.navParams.data.flag;
//   console.log("flag:",this.flag);
  
//       this.token=this.navParams.data.token;
      
//       this.init();
this.orientation.lock(this.orientation.ORIENTATIONS.LANDSCAPE);
this.data=this.domSanitizer.bypassSecurityTrustResourceUrl(this.navParams.data.data);
console.log(this.data);

      
}



// init(){
//   this.myEl = this.elRef.nativeElement.querySelector('#my-video');
//       this.partnerEl = this.elRef.nativeElement.querySelector('#partner-video');
//       this.webRTC.init(this.token, this.myEl, this.partnerEl);
//       if(this.flag==1){
//         this.partnerId=this.navParams.data.toId;
        
//         console.log("flag:",this.flag);
//         console.log("call ",this.partnerId);
//         this.call();
        
//       }
// }

// call(){
//   this.webRTC.call(this.partnerId);
// console.log("call attended");

//   this.swapVideo('my-video');

// }
// swapVideo(topVideo: string) {
//       this.topVideoFrame = topVideo;
// }


// endCall(){
//   this.webRTC.hangup();
//   let url=this.config.domain_url+'sendPushVideocall';
//   let headers= await this.config.getHeader();
//   let body={
//     to_user_id:this.partnerId,
//     from_user_id:this.userId,
//     callstatus:3,
//     calltoken:this.token
//   }
//   this.http.post(url,body,{headers}).subscribe((res:any)=>{
//     console.log(res);
    
//   })
  
//   this.modalCntl.dismiss();
  
// }


}

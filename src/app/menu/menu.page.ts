import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { Platform, ToastController, AlertController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';

import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { PushNotifications } from '@capacitor/push-notifications';
import { AuthService } from '../services/auth.service';
import { SetResidentProfileComponent } from '../components/set-resident-profile/set-resident-profile.component';
// import { FirebaseX } from '@ionic-native/firebase-x/ngx';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  name:string;
  img:any;
  p_name:string;
  subscription:Subscription;
  user_type:any;
  msgcount:any=0;
  counter=0;
  applaunched:boolean;
  notify:any;
  welcome:string;
  poll:any[]=[];
  checkin:any;
  modules: any[] = [];

  show: boolean = false;
  additional:boolean=false;
  an_count:any;
  parent:any[]=[];
  resident:any;
  r_name:any;
  res_branch:any;
  res_img:any;
  c_name:any;

 
  constructor(public storage:Storage,private platform:Platform,private router:Router,private toastCntlr:ToastController,
    private http:HttpClient,private config:HttpConfigService,private alertCntlr:AlertController,
    private orient:ScreenOrientation,private auth:AuthService,
    private popCntl:PopoverController,
    // private firebase:FirebaseX
    ) { }

  ngOnInit() {
   
  }
async ionViewWillEnter(){
 
 
  this.orient.lock(this.orient.ORIENTATIONS.PORTRAIT);
  let date=new Date();
  let hr=date.getHours();
  console.log("hr:",hr)
  if(hr<=6){
    this.welcome='Welcome'
  }
  else if(hr<12){
    this.welcome='Good morning'
  }
  else if(hr<17&&hr>=12){
    this.welcome='Good afternoon'
  }
  else if(hr>=17){
    this.welcome='Good evening'
  }
  
  this.applaunched=true;
  
     this.storage.set("launched",this.applaunched);
  // this.storage.ready().then(()=>{
    const name=await this.storage.get('NAME');
      this.name=name;
      this.p_name=this.name.substr(0,1);
    // });
    this.ValidateCheckin();
    this.getUserPermission();
    const da=await this.storage.get('CHECKIN');
      this.checkin=da;
      console.log('checkin:',this.checkin)
    // });
    const img=await this.storage.get('PRO_IMG');
      this.img=img;
    // });
    
    const data=await this.storage.get('USER_TYPE');
      this.user_type=data;
      
      if(this.user_type==6){
        this.getResidents();
      }else{
        const data=await this.storage.get('USER_ID');
    const r=await this.storage.get('RESIDENT_ID');
    const bid=await this.storage.get('BRANCH');
    const n=await this.storage.get('RESIDENT_NAME');
    this.c_name=await this.storage.get('COMPANY_NAME');
    const img=await this.storage.get('RESIDENT_IMG');
    this.resident=r;
                    this.r_name=n;
                    this.res_branch=bid;
                    this.res_img=img;
                  
                    
      }
    // });
    let headers= await this.config.getHeader()
      const cid=await this.storage.get('COMPANY_ID');
        let murl = this.config.domain_url + 'get_modules_family/' + cid;
        this.http.get(murl,{headers}).subscribe((mod: any) => {
          
          // this.show = true;
          this.modules = mod
          

        })
      // })
    const uid=await this.storage.get('USER_ID');
      
        let url1=this.config.domain_url+'notification/'+uid;
        let body={
          id:uid,
          offset:0,
          limit:20
        }
        this.http.get(url1,{headers}).subscribe((res:any)=>{
          
          this.notify=res.data.length;
          this.an_count=res.announcement_count;
          
        
        })

        // let an_url=this.config.domain_url+'announcement_list/'+data;
        
        // this.http.get(an_url).subscribe((res:any)=>{
        //   console.log(res);
          
        // })
      let m_url=this.config.domain_url+'msgcount';
      
      let body1={
        user_one:uid
      }
      this.http.post(m_url,body1,{headers}).subscribe((mes:any)=>{
        
        this.msgcount=mes.data.msgcount;
       

      })
   

      this.pushNotificationSettings();

    // const uuid=await this.storage.get('uuid');
    //   const token=await this.storage.get('USERTOKEN');
    //   const bid=await this.storage.get('BRANCH');
    //   const user_log=await this.storage.get('USERLOG');
    //   console.log("uuid",uuid);
    //   let url=this.config.domain_url+'devicetoken';
    //   let device;
    //   if(this.platform.is('ios')){
    //     device=2
    //   }
    //   else if(this.platform.is('android')){
    //     device=1
    //   }
      
    //   let body;
    //   body={
    //     user_id:uid,
    //     device_type:device,
    //     device_token:uuid,
    //     app_version:1,
    //     user_token:token
    //   }
    //   if(user_log==1){
    //     body.userlog=1
    //   }
    //   // let headers= await this.config.getHeader();
    //   console.log("body:",body);
    //   this.http.post(url,body,{headers}).subscribe((res:any)=>{
    //     console.log("device:",res);
    //     this.storage.set('USERLOG',0);
    //     if(res.message=='Token Expired Please login again'){
    //     this.auth.logout();
    //     this.router.navigateByUrl('/');
    //     let update_url=this.config.domain_url+'update_login_info';
    //     let update_body={
    //       user_id:uid,
    //       branch_id:bid,
    //       logout :1
    //     }
    //     this.http.post(update_url,update_body).subscribe((res:any)=>{
    //       console.log('login_info:',res);
          
    //     })
    //     }
    //   },error=>{
    //     console.log(error);
        
    //   })
   


    const log=await this.storage.get('firstLogin');
      if(log==0){
        let url=this.config.domain_url+'update_first_login';
        let body={id:uid}
        this.http.post(url,body,{headers}).subscribe((res:any)=>{
         
          
        })
      }
      
      
    
     let purl=this.config.domain_url+'publicPolls';
    
    
      let pbody={user_id:uid}
      this.http.post(purl,pbody,{headers}).subscribe((res:any)=>{
        
        this.poll=res.data;
      })
    


  
  const br=await this.storage.get('BRANCH_COPY');
  this.subscription = this.platform.backButton.subscribeWithPriority(1, ()=>{    
    if(this.counter==0){
      this.counter++;
      this.exitAlert();
      console.log("counter");
      
      setTimeout(() => { this.counter = 0 }, 3000)
    }  
    else {
      this.applaunched=false;
      
      this.storage.set("launched",this.applaunched);
      this.storage.remove('RESIDENT_ID');
      let bid=this.storage.get('BRANCH');
        this.storage.set('BRANCH',br);
        
      let update_url=this.config.domain_url+'update_login_info';
        let update_body={
          user_id:uid,
          branch_id:bid,
          logout :1
        }
        this.http.post(update_url,update_body,{headers}).subscribe((res:any)=>{
         
          
        })
      // })
      this.storage.set('USERLOG',0);
    navigator['app'].exitApp(); 
    }
    
  }); 

}
ionViewWillLeave() { 
  this.subscription.unsubscribe();
}

notification(){
  this.router.navigate(['/notifications',{flag:2}]);
}

survey(){
  
        this.presentAlert('You dont have access to this feature right now!');
     
}
async exitAlert() {
  const alert = await this.toastCntlr.create({
    message: 'Press again to exit.',
    cssClass:'toastStyle',
    duration: 3000      
  });
  alert.present(); //update
}

async presentAlert(mes) {
  const alert = await this.toastCntlr.create({
    message: mes,
    duration: 3000,
    position:'top'       
  });
  alert.present(); //update
}


async feedback(){
  
            // this.router.navigate(['/feedback'])
            // this.storage.ready().then(()=>{
              const dat=await this.storage.get('PCFBRANCH');
                let id=dat.toString();
                // this.id=id.toString()
                const x=await this.storage.get('SURVEY_CID');
                  let cid=x.toString();
                  // this.cid=cid.toString()
                  // let url=this.config.feedback_url+'get_all_reviews';
                  let url=this.config.feedback_url+'get_defaulted_review';
                  let body = `company_id=${cid}&branch_id=${id}`;
                  //  body=({
                  //   company_id:"14",
                  //   user_id:"3"
                  // })
                  // body.set('company_id','14');
                  // body.set('user_id','3');
                
                  
                  let headers=new HttpHeaders({
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                  })
                  this.http.post(url,body,{headers}).subscribe((data:any)=>{
                    
                    // if(data.data.length==1){
                      this.router.navigate(['/feedback-type',{sid:data.data.review_branch_id}],{replaceUrl:true})
                    // }else{
                    //   this.router.navigate(['/feedback'])
                    // }
                    
                    
                  })
          
            //     })
            //   })
            // })
}



async checkoutAlert(){
  const alert = await this.alertCntlr.create({
    header: 'Check out',
    mode:'ios',
    backdropDismiss:true,
    message: 'Do you want to check out?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        // cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'check out',
        handler: () => {
         
          this.checkAdditionalVisitors();
        //  this.checkout();

        }
      }
    ]
  });
  
  await alert.present();
}

async checkAdditionalVisitors(){
  // this.storage.ready().then(()=>{
    const bid=await this.storage.get('BRANCH');
      const data=await this.storage.get('PHONE');
      let url=this.config.domain_url+'check_additional_visitor';
      let phone=null;
      if(data){
      let ph=data.toString();
      if(ph.charAt( 0 )==='+'){
        phone = ph.substring(1);
       }else{
         phone=ph
       }
      }
      let body={
        branch_id:bid,
        phone:'+'+phone
      }
      let headers= await this.config.getHeader()
      
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log("add:",res);

        if(res.additional_visitor_count==0){
          this.checkout();
        }else{
          this.additionalAlert();
        }
        
      },error=>{
        console.log("adderror:",error);
        
      })
//     })
//   })
// })
}

async additionalAlert(){
  const alert = await this.alertCntlr.create({
    header: 'Checkout',
    id:'alert',
    mode:'ios',
    message: 'Do you want to checkout the additional visitors too?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
          this.additional=false;
          this.checkout();
        }
      }, {
        text: 'Yes',
        handler: () => {
          this.additional=true;
          this.checkout();
        }
      }
    ]
  });

  await alert.present();
}


async checkout(){
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('PHONE');
      const bid=await this.storage.get('BRANCH');
      let headers= await this.config.getHeader()
      let url=this.config.domain_url+'checkout_visitor_by_phone';
      let phone=null;
      if(data){
      let ph=data.toString();
      if(ph.charAt( 0 )==='+'){
        phone = ph.substring(1);
       }else{
         phone=ph
       }
      }
       let additional;
              if(this.additional){
                additional=1
              }else{
                additional=0
              }
      let body={
        branch_id:bid,
        visitor_type:'0',
        phone:'+'+phone,
        checkout_additional:additional
      }
      console.log('body:',body);
      const da=await this.storage.get('checkinPhone');
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log("byphone:",res);

        
          if(da==1){
            this.storage.set('PHONE',null);
            this.storage.remove('checkinPhone');
          }
        // })
        // if(res.message=='Data not found'){
        //   this.presentAlert('Please enter the phone number used at checked in')
        // }else{
          this.router.navigate(['/checkout-thanks']);
        // }
        
      })
  //   })
  //   })
  // })
}

async ValidateCheckin(){
  let url=this.config.domain_url+'validate_checkin_details';
  let headers= await this.config.getHeader()
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('PHONE');
      const bid=await this.storage.get('BRANCH');
        let phone=null;
        if(data){
        let ph=data.toString();
        if(ph.charAt( 0 )==='+'){
          phone = ph.substring(1);
         }else{
           phone=ph
         }
        }
          let body={
            mobile:'+'+phone,
            branch_id:bid,
            name:this.name,
            visitor_type:0
          }
          const dat=await this.storage.get('checkinPhone');
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log("repvis:",res);
            if(res.status=='success'){
              this.storage.set('CHECKIN',0);
              this.checkin=0;
              
                if(dat==1){
                  this.storage.set('PHONE',null);
                  this.storage.remove('checkinPhone');
                }
              // })
            }else{
              console.log("checkedin:",this.checkin)
              // this.presentToast('Someone already checked in using this phone number.');
            }
          },error=>{
            // const data=await this.storage.get('checkinPhone');
              if(dat==1){
                this.storage.set('PHONE',null);
                this.storage.remove('checkinPhone');
              }
            })
    //       })
    //     })
    //   })
    // })
}

checkIn(){
  this.router.navigate(['/checkin-scan-qr'],{replaceUrl:true});
}
async setResident(ev){
  if(this.user_type==6&&this.parent.length>1){
  const popover = await this.popCntl.create({
    component: SetResidentProfileComponent,
    event:ev,
    backdropDismiss:true,
    componentProps:{
      res:this.resident
    }
  });
  popover.onDidDismiss().then((dataReturned)=>{
    console.log(dataReturned)
    if(dataReturned.data){
      this.resident=dataReturned.data.resident;
      this.r_name=dataReturned.data.name
      this.res_branch=dataReturned.data.branch
      this.res_img=dataReturned.data.img;
      this.c_name=dataReturned.data.c_name;
    }
  })
  return await popover.present();
}
}
async getResidents(){
  this.parent=[];
  // this.storage.ready().then(()=>{
    const data=await this.storage.get('USER_ID');
    const r=await this.storage.get('RESIDENT_ID');
    const bid=await this.storage.get('BRANCH');
    const n=await this.storage.get('RESIDENT_NAME');
    const img=await this.storage.get('RESIDENT_IMG');
    const c_name=await this.storage.get('COMAPANY_NAME');
      let url=this.config.domain_url+'profile/'+data;
      let headers= await this.config.getHeader();
        this.http.get(url,{headers}).subscribe((res:any)=>{
          console.log("get:",res,url);
          
              res.data.family.forEach(element=>{
            
               if(element.parent_details[0].status==1){
                this.parent.push({parent:element.parent_details[0],share:element.share_details});
               }
              })
             
              console.log("parent",this.parent,r,n,img);
              // this.storage.ready().then(()=>{
                
                  if(!n){
                    this.storage.set('BRANCH',this.parent[0].parent.wing.branch_id);
                    this.storage.set('RESIDENT_ID',this.parent[0].parent.user.user_id);
                    this.storage.set('RESIDENT_USER_ID',this.parent[0].parent.id);
                    this.storage.set('RESIDENT_NAME',this.parent[0].parent.user.name)
                    this.storage.set('RESIDENT_IMG',this.parent[0].parent.user.profile_pic);
                    this.storage.set('RES-RV-SETTING',this.parent[0].parent.branches.user_branch.retirement_living);
                    this.storage.set('COMAPANY_NAME',this.parent[0].parent.user.company.c_name);
                    this.resident=this.parent[0].parent.user.user_id;
                    this.r_name=this.parent[0].parent.user.name;
                    this.c_name=this.parent[0].parent.user.company.c_name;
                    this.res_branch=this.parent[0].parent.branches.branch_id;
                    this.res_img=this.parent[0].parent.user.profile_pic;
                  }else{
                    this.resident=r;
                    this.r_name=n;
                    this.res_branch=bid;
                    this.res_img=img;
                    this.c_name=c_name;
                  }
                // })
              // })
            
            
              
        },error=>{
          console.log(error);
          
        });
    //   });
    // });

}

residentProfile(){
  let flag;
  if(this.modules.includes('Petty cash')){
    flag=1
  }else{
    flag=0
  }
  this.router.navigate(['overview',{cid:this.resident,flag:flag,branch:this.res_branch}])
}

gotoFeeds(){
  if(this.modules.includes('Story')){
    this.router.navigate(['/stories'])
  }else{
    this.presentAlert('Access denied');
  }
}
gotoActivity(){
  if(this.modules.includes('Activity')){
    this.router.navigate(['/activities'])
  }else{
    this.presentAlert('Access denied');
  }
}
gotoMessage(){
  if(this.modules.includes('Messages')){
    this.router.navigate(['/message'])
  }else{
    this.presentAlert('Access denied');
  }
}

async pushNotificationSettings(){
  const usertoken=await this.storage.get('USERTOKEN');
      const bid=await this.storage.get('BRANCH');
      const uid=await this.storage.get('USER_ID');
      const user_log=await this.storage.get('USERLOG');
      let headers= await this.config.getHeader();
  let permStatus = await PushNotifications.checkPermissions();
      
        if (permStatus.receive === 'prompt') {
          permStatus = await PushNotifications.requestPermissions();
        }
      
        if (permStatus.receive !== 'granted') {
          throw new Error('User denied permissions!');
        }
      
        await PushNotifications.register();
      // }
      let counter=0;
      // const addListeners = async () => {
        await PushNotifications.addListener('registration', token => {
          console.log('Registration token: ', token);
          counter++;
            if(counter==0){
          this.storage.set("uuid",token.value);

          let url=this.config.domain_url+'devicetoken';
      let device;
      if(this.platform.is('ios')){
        device=2
      }
      else if(this.platform.is('android')){
        device=1
      }
      
      let body;
      body={
        user_id:uid,
        device_type:device,
        device_token:token.value,
        app_version:1,
        user_token:usertoken
      }
      if(user_log==1){
        body.userlog=1
      }
      
      console.log("body:",body);
      this.http.post(url,body,{headers}).subscribe((res:any)=>{
        console.log("device:",res);
        this.storage.set('USERLOG',0);
        if(res.message=='Token Expired Please login again'){
        this.auth.logout();
        this.router.navigateByUrl('/login');
        let update_url=this.config.domain_url+'update_login_info';
        let update_body={
          user_id:uid,
          branch_id:bid,
          logout :1
        }
        this.http.post(update_url,update_body,{headers}).subscribe((res:any)=>{
          console.log('login_info:',res);
          
        })
        }
      },error=>{
        console.log(error);
        
      })
    }
        });
        // const uuid=await this.storage.get('uuid');
      
      
      
}

async getUserPermission(){
  (await this.config.getUserPermission()).subscribe((res: any) => {
    
  })
}

}

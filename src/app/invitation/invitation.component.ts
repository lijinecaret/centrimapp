import { Component, Input, OnInit } from '@angular/core';
import { PopoverController, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { HttpConfigService } from '../services/http-config.service';
import { Storage } from '@ionic/storage-angular';
// import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.scss'],
})
export class InvitationComponent implements OnInit {
name:string;
relation:string;
email:string;
phone:any;
mobile:any;
@Input() id:any;
disable:boolean=false;
// invitationform:FormGroup;
  constructor(public popoverController:PopoverController,private http:HttpClient,private config:HttpConfigService,
    private storage:Storage,private toastCntlr:ToastController) { }

  ngOnInit() {

    // let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    // this.invitationform = new FormGroup({
    //   name: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(15)]),
    //   phone: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
      
    //   email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
    //   relation :new FormControl('', [Validators.required])
    // });

  }

  async send(){
let phone;
    // var pattern = new RegExp('(https?:\\/\\/)?'+ // protocol
    //             '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    //             '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    //             '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    //             '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    //             '(\\#[-a-z\\d_]*)?$','i');
    var regex = new RegExp("(\\d)\\1{5}")
    var pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    console.log(this.name,this.email,this.relation,this.phone)
    if(this.phone==undefined || this.phone==''){
    this.phone=null;
    
    }
    if(this.name==undefined||this.email==undefined||this.relation==undefined){
      this.presentAlert('Please fill all required fields.');
      
    }else if(this.name==''||this.email==''||this.relation==''){
      this.presentAlert('Please fill all required fields.');
      
    }else if(this.phone!=null &&(this.phone.toString().length<10|| this.phone.toString().length>10)){
      this.presentAlert('Please enter a 10 digit phone number.');
      
    }else if(this.phone&&this.phone.match(regex)){
      this.presentAlert('please enter a valid phone number.')
    }else if(/^\d+$/.test(this.name)){
      this.presentAlert('Name field contains only digits.');
    }else if(/^\d+$/.test(this.relation)){
      this.presentAlert('Please enter a proper relation.');
    }else{
      if(this.phone==null){
        this.mobile=null
      }else{
        this.mobile=phone
      }
      if(pattern.test(this.email)){
        this.disable=true;
        // this.storage.ready().then(()=>{
          const data=await this.storage.get('USER_ID');
            console.log("id:",this.id);
            let headers= await this.config.getHeader();
            let url=this.config.domain_url+'validate_invite_contact';
           
            let body={
              resident_user_id:this.id.toString(),
              
              email:this.email,
              
            }
            console.log("body:",body)
              this.http.post(url,body,{headers}).subscribe((res:any)=>{
                console.log(res);
               this.invite();
            },error=>{
              this.presentAlert('Already registered email Id.')
              this.disable=false
              console.log(error);
            })
            
            
          // })
            
          
      // })

      
    }else{
      this.presentAlert('Please enter a valid email Id.')
      this.disable=false
    }
      }
    }
    async presentAlert(mes) {
      const alert = await this.toastCntlr.create({
        message: mes,
        duration: 3000,
        position:'top'       
      });
      alert.present(); //update
    }

  async inviteAlert() {
    const alert = await this.toastCntlr.create({
      message: 'Invitation send successfully',
      duration: 3000,
      position:'top'      
    });
    alert.present(); //update
  }


  async invite(){
    // this.storage.ready().then(()=>{
      const data=await this.storage.get('USER_ID');
        console.log("id:",this.id);
        
        let url=this.config.domain_url+'invite_contact';
        let headers= await this.config.getHeader();
        let body={
          resident_user_id:this.id.toString(),
          name:this.name,
          relation:this.relation,
          email:this.email,
          phone:this.mobile,
          is_primary:0,
          dining_access:0
        }
        console.log("body:",body)
          this.http.post(url,body,{headers}).subscribe((res:any)=>{
            console.log(res);
            this.inviteAlert();
          this.popoverController.dismiss(1);
        }),error=>{
          this.disable=false;
          console.log(error);
        }
        
        
  //     })
        
      
  // })
  }
  dismiss(){
    // this.disable=true;
    console.log('dismiising pop');
    
    this.popoverController.dismiss(0);
  }
}

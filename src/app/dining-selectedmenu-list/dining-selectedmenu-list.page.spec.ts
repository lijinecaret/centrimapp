import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiningSelectedmenuListPage } from './dining-selectedmenu-list.page';

describe('DiningSelectedmenuListPage', () => {
  let component: DiningSelectedmenuListPage;
  let fixture: ComponentFixture<DiningSelectedmenuListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiningSelectedmenuListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiningSelectedmenuListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiningSelectedmenuListPage } from './dining-selectedmenu-list.page';

const routes: Routes = [
  {
    path: '',
    component: DiningSelectedmenuListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiningSelectedmenuListPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiningSelectedmenuListPageRoutingModule } from './dining-selectedmenu-list-routing.module';

import { DiningSelectedmenuListPage } from './dining-selectedmenu-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiningSelectedmenuListPageRoutingModule
  ],
  declarations: [DiningSelectedmenuListPage]
})
export class DiningSelectedmenuListPageModule {}

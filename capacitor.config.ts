import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.ecaret.centrimlife',
  appName: 'Centrimlife',
  webDir: 'www',
  bundledWebRuntime: false,
  cordova: {
    preferences: {
      ScrollEnabled: 'false',
      'android-minSdkVersion': '30',
      'android-targetSdkVersion': '33',
      BackupWebStorage: 'none',
      SplashMaintainAspectRatio: 'true',
      ShowSplashScreenSpinner: 'false',
      WKWebViewOnly: 'true',
      'deployment-target': '9.0',
      
    }
  },
  plugins:{
    SplashScreen:{
      launchShowDuration:6000,
      backgroundColor:"#2d9efe",
      launchAutoHide:true,
      androidScaleType: "CENTER_CROP",
      splashFullScreen:false,
      splashImmersive:true,
      useDialog:false
    },
    PushNotifications:{
      presentationOptions: ["badge", "sound", "alert"]
    }
  }
};

export default config;
